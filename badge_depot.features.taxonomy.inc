<?php
/**
 * @file
 * badge_depot.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function badge_depot_taxonomy_default_vocabularies() {
  return array(
    'badge_alignment_targets' => array(
      'name' => 'Badge Alignment Targets',
      'machine_name' => 'badge_alignment_targets',
      'description' => 'Names of framework nodes used as alignments for badges (use dot-notation).',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'badge_lrmi_keywords' => array(
      'name' => 'Badge Keywords',
      'machine_name' => 'badge_lrmi_keywords',
      'description' => 'Tags used to describe the badge content',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
