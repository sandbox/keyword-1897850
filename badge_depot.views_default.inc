<?php
/**
 * @file
 * badge_depot.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function badge_depot_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'badge_depot_activities';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'badge_memo';
  $view->human_name = 'Activities';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Badge Activities';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['label'] = 'Badge Database';
  $handler->display->display_options['header']['area_text_custom']['content'] = 'Search the (mostly) STEM badges that have been submitted to this site.';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No published badges with activities were found.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Badge Memo Entity: Image URL */
  $handler->display->display_options['fields']['image_url']['id'] = 'image_url';
  $handler->display->display_options['fields']['image_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['image_url']['field'] = 'image_url';
  $handler->display->display_options['fields']['image_url']['label'] = '';
  $handler->display->display_options['fields']['image_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['image_url']['alter']['text'] = '<img src="[image_url]" width="90" height="90" />';
  $handler->display->display_options['fields']['image_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['image_url']['display_as_link'] = FALSE;
  /* Field: Badge Memo Entity: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<b>[title]</b>';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Badge Memo Entity: HTML Field Handler */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Badge Memo Entity: Issuer Organization */
  $handler->display->display_options['fields']['issuer_organization']['id'] = 'issuer_organization';
  $handler->display->display_options['fields']['issuer_organization']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['issuer_organization']['field'] = 'issuer_organization';
  $handler->display->display_options['fields']['issuer_organization']['label'] = 'Organization';
  /* Field: Badge Memo Entity: Issuer URL */
  $handler->display->display_options['fields']['issuer_url']['id'] = 'issuer_url';
  $handler->display->display_options['fields']['issuer_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['issuer_url']['field'] = 'issuer_url';
  $handler->display->display_options['fields']['issuer_url']['label'] = '';
  $handler->display->display_options['fields']['issuer_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['issuer_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['issuer_url']['display_as_link'] = FALSE;
  /* Field: Badge Memo Entity: Issuer Name */
  $handler->display->display_options['fields']['issuer_name']['id'] = 'issuer_name';
  $handler->display->display_options['fields']['issuer_name']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['issuer_name']['field'] = 'issuer_name';
  $handler->display->display_options['fields']['issuer_name']['label'] = 'Issuer';
  $handler->display->display_options['fields']['issuer_name']['alter']['text'] = '<a href="[issuer_url]" target="_blank">[issuer_name]</a>';
  $handler->display->display_options['fields']['issuer_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['issuer_name']['alter']['path'] = '[issuer_url]';
  $handler->display->display_options['fields']['issuer_name']['alter']['target'] = '_blank';
  /* Field: Badge Memo Entity: Activity URL */
  $handler->display->display_options['fields']['activity_url']['id'] = 'activity_url';
  $handler->display->display_options['fields']['activity_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['activity_url']['field'] = 'activity_url';
  $handler->display->display_options['fields']['activity_url']['label'] = '';
  $handler->display->display_options['fields']['activity_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['activity_url']['alter']['text'] = '<a href="[activity_url]" target="_blank">Activity Page</a>
<hr />
';
  $handler->display->display_options['fields']['activity_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['activity_url']['display_as_link'] = FALSE;
  /* Filter criterion: Badge Memo Entity: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'badge_memo';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'word';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Search By Title Containing the Word:';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => 0,
    1 => '1',
    3 => 0,
    4 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'badge-depot/badges/activities';
  $export['badge_depot_activities'] = $view;

  $view = new view();
  $view->name = 'badge_depot_database';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'badge_memo';
  $view->human_name = 'Database';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Badge Database';
  $handler->display->display_options['use_more'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'Show More';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['type'] = 'ol';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['label'] = 'Badge Database';
  $handler->display->display_options['header']['area_text_custom']['content'] = 'Lists the (mostly) STEM badges that have been submitted to this site.<br />There are many badges! We link to activities and criteria pages when found.';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No published badges were found. Use the badge inspector tool to add.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Badge Memo Entity: Image URL */
  $handler->display->display_options['fields']['image_url']['id'] = 'image_url';
  $handler->display->display_options['fields']['image_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['image_url']['field'] = 'image_url';
  $handler->display->display_options['fields']['image_url']['label'] = '';
  $handler->display->display_options['fields']['image_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['image_url']['alter']['text'] = '<img src="[image_url]" width="90" height="90" />';
  $handler->display->display_options['fields']['image_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['image_url']['display_as_link'] = FALSE;
  /* Field: Badge Memo Entity: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<b>[title]</b>';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Badge Memo Entity: HTML Field Handler */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Badge Memo Entity: Issuer Organization */
  $handler->display->display_options['fields']['issuer_organization']['id'] = 'issuer_organization';
  $handler->display->display_options['fields']['issuer_organization']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['issuer_organization']['field'] = 'issuer_organization';
  $handler->display->display_options['fields']['issuer_organization']['label'] = 'Organization';
  /* Field: Badge Memo Entity: Issuer URL */
  $handler->display->display_options['fields']['issuer_url']['id'] = 'issuer_url';
  $handler->display->display_options['fields']['issuer_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['issuer_url']['field'] = 'issuer_url';
  $handler->display->display_options['fields']['issuer_url']['label'] = '';
  $handler->display->display_options['fields']['issuer_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['issuer_url']['alter']['text'] = '<a href="[issuer_url]" target="_blank">[issuer_name]</a>';
  $handler->display->display_options['fields']['issuer_url']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['issuer_url']['alter']['path'] = '[issuer_url]';
  $handler->display->display_options['fields']['issuer_url']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['issuer_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['issuer_url']['display_as_link'] = FALSE;
  /* Field: Badge Memo Entity: Issuer Name */
  $handler->display->display_options['fields']['issuer_name']['id'] = 'issuer_name';
  $handler->display->display_options['fields']['issuer_name']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['issuer_name']['field'] = 'issuer_name';
  $handler->display->display_options['fields']['issuer_name']['label'] = 'Issuer';
  $handler->display->display_options['fields']['issuer_name']['alter']['text'] = '<a href="[issuer_url]" target="_blank">[issuer_name]</a>';
  $handler->display->display_options['fields']['issuer_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['issuer_name']['alter']['path'] = '[issuer_url]';
  $handler->display->display_options['fields']['issuer_name']['alter']['target'] = '_blank';
  /* Field: Badge Memo Entity: Activity URL */
  $handler->display->display_options['fields']['activity_url']['id'] = 'activity_url';
  $handler->display->display_options['fields']['activity_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['activity_url']['field'] = 'activity_url';
  $handler->display->display_options['fields']['activity_url']['label'] = '';
  $handler->display->display_options['fields']['activity_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['activity_url']['alter']['text'] = '<a href="[activity_url]" target="_blank">Activity Page</a>';
  $handler->display->display_options['fields']['activity_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['activity_url']['display_as_link'] = FALSE;
  /* Field: Badge Memo Entity: Criteria URL */
  $handler->display->display_options['fields']['criteria_url']['id'] = 'criteria_url';
  $handler->display->display_options['fields']['criteria_url']['table'] = 'badge_memo';
  $handler->display->display_options['fields']['criteria_url']['field'] = 'criteria_url';
  $handler->display->display_options['fields']['criteria_url']['label'] = '';
  $handler->display->display_options['fields']['criteria_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['criteria_url']['alter']['text'] = '<a href="[criteria_url]" target="_blank">Criteria Page</a>
<hr />';
  $handler->display->display_options['fields']['criteria_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['criteria_url']['empty'] = '<hr />
';
  $handler->display->display_options['fields']['criteria_url']['display_as_link'] = FALSE;
  /* Sort criterion: Badge Memo Entity: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'badge_memo';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Created';
  $handler->display->display_options['sorts']['created']['granularity'] = 'day';
  /* Sort criterion: Badge Memo Entity: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'badge_memo';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  $handler->display->display_options['sorts']['title']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['title']['expose']['label'] = 'Title';
  /* Sort criterion: Badge Memo Entity: Issuer Organization */
  $handler->display->display_options['sorts']['issuer_organization']['id'] = 'issuer_organization';
  $handler->display->display_options['sorts']['issuer_organization']['table'] = 'badge_memo';
  $handler->display->display_options['sorts']['issuer_organization']['field'] = 'issuer_organization';
  $handler->display->display_options['sorts']['issuer_organization']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['issuer_organization']['expose']['label'] = 'Issuer Organization';
  /* Filter criterion: Badge Memo Entity: Is Published? */
  $handler->display->display_options['filters']['is_published']['id'] = 'is_published';
  $handler->display->display_options['filters']['is_published']['table'] = 'badge_memo';
  $handler->display->display_options['filters']['is_published']['field'] = 'is_published';
  $handler->display->display_options['filters']['is_published']['value'] = array(
    'value' => '1',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'badge-depot/badges/database';
  $export['badge_depot_database'] = $view;

  return $export;
}
