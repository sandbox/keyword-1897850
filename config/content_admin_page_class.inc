<?php
/**
 * @file
 * Contains BadgeDepotContentAdministrationPage.
 */

class BadgeDepotContentAdministrationPage extends BadgeDepotMenuLinksPage {

  public function __construct() {
    parent::__construct();
  }

  public function buildDefault($form, &$form_state) {
    $links_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Content Administration'),
      '#description' => t('The following quick links can be used to manage content related to badges.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['badge_links_fieldset'] = &$links_fieldset;
    $links_fieldset['badge_links'] = $this->renderMenuLinks(
      array(
        $this->getMenuLink('Badge Assertion entity data', BADGE_DEPOT_ADMIN_ASSERTION_PATH, badge_depot_help(BADGE_DEPOT_ADMIN_ASSERTION_PATH, explode('/', BADGE_DEPOT_ADMIN_ASSERTION_PATH))),
        $this->getMenuLink('Badge Issuer entity data', BADGE_DEPOT_ADMIN_ISSUER_PATH, badge_depot_help(BADGE_DEPOT_ADMIN_ISSUER_PATH, explode('/', BADGE_DEPOT_ADMIN_ISSUER_PATH))),
        $this->getMenuLink('Badge Manifest entity data', BADGE_DEPOT_ADMIN_MANIFEST_PATH, badge_depot_help(BADGE_DEPOT_ADMIN_MANIFEST_PATH, explode('/', BADGE_DEPOT_ADMIN_MANIFEST_PATH))),
        $this->getMenuLink('Badge Memo entity data', BADGE_DEPOT_ADMIN_MEMO_PATH, badge_depot_help(BADGE_DEPOT_ADMIN_MEMO_PATH, explode('/', BADGE_DEPOT_ADMIN_MEMO_PATH))),
        $this->getMenuLink('Advanced pages for Badge Criteria', 'node/add/badge-criteria-page'),
      )
    );

    return parent::buildDefault($form, $form_state);
  }
}
