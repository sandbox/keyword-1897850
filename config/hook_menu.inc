<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

/**
 * Adds the hook_menu info for this modules main configuration page.
 */
function _badge_depot_append_config_menu(&$items) {
  $items[BADGE_DEPOT_CONFIG_MAIN_PATH] = array(
    'title' => 'Badges',
    'description' => 'Administer the Badge Depot module.',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'weight' => -8,
    'position' => 'right',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
  );

  $items[BADGE_DEPOT_CONFIG_PATH_PREFIX . 'badge-depot'] = array(
    'title' => 'Badge Depot settings',
    'description' => 'General configuration setting of the Badge Depot module.',
    'type' => MENU_NORMAL_ITEM,
    // This include file handles OOP Forms API callbacks.
    'file' => 'core/forms/page_callback.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('BadgeDepotMainConfigurationPage'),
    'access arguments' => array('access administration pages'),
  );

  $items[BADGE_DEPOT_CONFIG_PATH_PREFIX . 'tools-links'] = array(
    'title' => 'Badge tools',
    'description' => 'Index of general tools provided by the Badge Depot module.',
    'type' => MENU_NORMAL_ITEM,
    // This include file handles OOP Forms API callbacks.
    'file' => 'core/forms/page_callback.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('BadgeDepotToolsPage'),
    'access arguments' => array('access administration pages'),
  );

  $items[BADGE_DEPOT_CONFIG_PATH_PREFIX . 'content-links'] = array(
    'title' => 'Content administration',
    'description' => 'Index of administration pages for managing content of the Badge Depot module.',
    'type' => MENU_NORMAL_ITEM,
    // This include file handles OOP Forms API callbacks.
    'file' => 'core/forms/page_callback.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('BadgeDepotContentAdministrationPage'),
    'access arguments' => array('access administration pages'),
  );
}
