<?php
/**
 * @file
 * Contains BadgeDepotMenuLinksPage.
 */

class BadgeDepotMenuLinksPage extends BadgeDepotFormPage {

  public function __construct() {
    parent::__construct();
  }

  protected function renderMenuLinks(array $items) {
    return array(
      '#type' => 'ul',
      '#theme' => 'item_list',
      '#attributes' => array('class' => 'admin-list'),
      '#items' => $items,
    );
  }

  protected function getMenuLink($title, $path, $description = NULL) {
    if (empty($title) || (!isset($description))) {
      $menu_item = menu_get_item($path);
      if (empty($title)) {
        $title = (isset($menu_item['title']) ? $menu_item['title'] : NULL);
      }

      if (!isset($description)) {
        $description = (isset($menu_item['description']) ? $menu_item['description'] : NULL);
      }
    }

    if (empty($title)) {
      $title = $path;
    }
    else {
      $title = t($title);
    }

    if (empty($description)) {
      $description = '&nbsp;';
    }

    return (l($title, $path) . '<div class="description">' . $description . '</div>');
  }
}
