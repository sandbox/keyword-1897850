<?php
/**
 * @file
 * Contains BadgeDepotToolsPage.
 */

class BadgeDepotToolsPage extends BadgeDepotMenuLinksPage {

  public function __construct() {
    parent::__construct();
  }

  public function buildDefault($form, &$form_state) {
    $links_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Tools'),
      '#description' => t('The Badge Depot module provides several useful tools, such as the pages listed below.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['badge_links_fieldset'] = &$links_fieldset;
    $links_fieldset['badge_links'] = $this->renderMenuLinks(
      array(
        $this->getMenuLink(NULL, BADGE_DEPOT_INSPECTOR_DEFAULT_PATH, t('View the details of a badge by uploading the badge\'s PNG image.')),
        $this->getMenuLink('Inspect a Backpack', BADGE_DEPOT_INSPECTOR_BACKPACK_PATH, t('View the details of badges in an Mozilla backpack.')),
        $this->getMenuLink('Inspect an Assertion', BADGE_DEPOT_INSPECTOR_DIRECT_PATH, t('View the details of a hosted badge-assertion by entering the assertion\'s URL directly. Due to security concerns, this page is not publicly available.')),
      )
    );

    return parent::buildDefault($form, $form_state);
  }
}
