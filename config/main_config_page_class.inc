<?php
/**
 * @file
 * Contains BadgeDepotMainConfigurationPage.
 */

class BadgeDepotMainConfigurationPage extends BadgeDepotFormPage {

  public function __construct() {
    parent::__construct();
  }

  // Define the identifiers of the form fields that contain submission data.
  const FORM_CONTENT_ASSERTION_OPTIONS = 'assertion_options_fieldset';
  const FORM_CONTENT_BADGE_BAKER = 'badge_baker_fieldset';
  const FORM_CONTENT_WIKI_INFO = 'wiki_info_fieldset';
  const FORM_CONTENT_DISPLAYER_API = 'badge_displayer_fieldset';
  const FORM_FIELD_ASSERTIONS_INCLUDE_DYNAMIC_IMAGE = 'assertions_include_dynamic_image';
  const FORM_FIELD_ASSERTIONS_AUTO_SAVE_EMAIL = 'assertions_auto_save_email';
  const FORM_FIELD_ASSERTIONS_INFO_URL = 'obi_assertions_info_url';
  const FORM_FIELD_DISPLAYER_API_INFO_URL = 'obi_displayer_api_info_url';
  const FORM_FIELD_DISPLAYER_API_BASE_URL = 'obi_displayer_api_base_url';
  const FORM_FIELD_DISPLAYER_API_USER_ID_URL = 'obi_displayer_api_user_id_url';
  const FORM_FIELD_BADGE_BAKER_INFO_URL = 'obi_badge_baker_info_url';
  const FORM_FIELD_BADGE_BAKER_PREFIX_URL = 'obi_badge_baker_prefix_url';
  const FORM_FIELD_BADGE_ISSUER_INFO_URL = 'obi_badge_issuer_info_url';
  const FORM_FIELD_BADGE_ISSUER_SCRIPT_URL = 'obi_badge_issuer_script_url';
  const FORM_FIELD_AUTO_PUBLISH_MEMO = 'auto_publish_badge_memo';
  const FORM_FIELD_JSON_ALLOW_DATA_URI = 'obi_json_allow_data_uri';

  // Define the identifiers of buttons that trigger submission of data,
  // for example, "ClearCache" maps to validateTriggerClearCache,
  // submitTriggerClearCache, ajaxTriggerClearCache, etc.
  const FORM_BUTTON_SAVE_DATA = 'SaveData';
  const FORM_BUTTON_CLEAR_CACHE = 'ClearCache';

  // Define the HTML identifiers for AJAX-replacement contents.
  const AJAX_CONTENT_ACTIONS = 'config_action_group';
  const AJAX_CONTENT_CACHE = 'cache_settings';

  // Define the HTML identifiers for AJAX-replacement wrappers.
  const AJAX_WRAPPER_ACTIONS = 'config_action_group_wrapper';
  const AJAX_WRAPPER_CACHE = 'cache_settings_wrapper';

  public function buildDefault($form, &$form_state) {

    $cache_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Clear Cache'),
      '#description' => t('Remove all data currently cached by the Badge Depot module, such as badge assertions from remote sites.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_CACHE),
      '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
    );
    $form[self::AJAX_CONTENT_CACHE] = &$cache_fieldset;
    $cache_fieldset[self::FORM_BUTTON_CLEAR_CACHE] = array(
      '#type' => 'submit',
      '#name' => self::FORM_BUTTON_CLEAR_CACHE,
      '#value' => t('Clear badge caches'),
      '#ajax' => array(
        'callback' => '_badge_depot_form_proxy_ajax',
        'wrapper' => self::AJAX_WRAPPER_CACHE,
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Clearing cache...',
        ),
      ),
    );

    $inspector_default_url = url(BADGE_DEPOT_INSPECTOR_DEFAULT_PATH);
    $inspector_backpack_url = url(BADGE_DEPOT_INSPECTOR_BACKPACK_PATH);
    $baker_service_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Mozilla OBI Badge Baker'),
      '#description' => t(
        '<a target="_blank" href="!bakerInfoUrl">Badge Baking</a> is used by the <a target="_blank" href="!inspectorUrl">Backpack Inspector</a> tool and the badge-issuer service to create personalized PNG images when Open Badges are awarded to users.',
        array(
          '!bakerInfoUrl' => BadgeDepotGlobalSettings::getObiBadgeBakerInfoUrl(),
          '!inspectorUrl' => $inspector_backpack_url,
        )
      ),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form[self::FORM_CONTENT_BADGE_BAKER] = &$baker_service_fieldset;
    $baker_service_fieldset[self::FORM_FIELD_BADGE_BAKER_PREFIX_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Baker Service URL'),
      '#description' => t('The prefix URL of the web-service that is used to bake badges.  The badge\'s assertion-URL will be appended to this value as an encoded URI component when badge baking is requested, such that the prefix URL must also included a query-string name when needed, for example, "http://beta.openbadges.org/baker?assertion=".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiBadgeBakerPrefixUrl(),
      '#required' => TRUE,
    );

    $displayer_api_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Mozilla OBI Displayer API'),
      '#description' => t(
        'The <a target="_blank" href="!displayerInfoUrl">Displayer API</a> is used by the <a target="_blank" href="!inspectorUrl">Backpack Inspector</a> tool to view a user\'s shared Open Badges.',
        array(
          '!displayerInfoUrl' => BadgeDepotGlobalSettings::getObiDisplayerApiInfoUrl(),
          '!inspectorUrl' => $inspector_backpack_url,
        )
      ),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form[self::FORM_CONTENT_DISPLAYER_API] = &$displayer_api_fieldset;
    $displayer_api_fieldset[self::FORM_FIELD_DISPLAYER_API_BASE_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Backpack Service URL'),
      '#description' => t('The base URL of the web-service that is used to load public backpacks.  The URL includes a trailing slash character, for example, "http://beta.openbadges.org/displayer/".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiDisplayerApiBackpackUrl(),
      '#required' => TRUE,
    );
    $displayer_api_fieldset[self::FORM_FIELD_DISPLAYER_API_USER_ID_URL] = array(
      '#type' => 'textfield',
      '#title' => t('User Lookup URL'),
      '#description' => t('The complete URL of the web-service that converts an email address to a Mozilla user ID, for example, "http://beta.openbadges.org/displayer/convert/email".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiDisplayerApiUserUrl(),
      '#required' => TRUE,
    );

    $assertion_proxy_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Badge Assertion Proxy'),
      '#description' => t(
        'Configuration settings for the proxy that is used by the <a target="_blank" href="!badgeInspectorUrl">Badge Inspector</a> and <a target="_blank" href="!backpackInspectorUrl">Backpack Inspector</a> tools.',
        array(
          '!badgeInspectorUrl' => $inspector_default_url,
          '!backpackInspectorUrl' => $inspector_backpack_url,
        )
      ),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['assertion_proxy_fieldset'] = &$assertion_proxy_fieldset;
    $assertion_proxy_fieldset[self::FORM_FIELD_AUTO_PUBLISH_MEMO] = array(
      '#attributes' => array('name' => self::FORM_FIELD_AUTO_PUBLISH_MEMO),
      '#type' => 'checkbox',
      '#title' => t('Auto-publish discovered badges'),
      '#description' => t('Specify whether badge info should be published automatically when discovered by the assertion proxy.'),
      '#default_value' => (BadgeDepotGlobalSettings::getAutoPublishMemoFlag() ? 1 : 0),
    );

    $assertion_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Badge Issuing'),
      '#description' => t('Specify the options for default behaviour when badges are awarded through the creation of assertion data.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form[self::FORM_CONTENT_ASSERTION_OPTIONS] = &$assertion_fieldset;
    $assertion_fieldset[self::FORM_FIELD_BADGE_ISSUER_SCRIPT_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Issuer API Script URL'),
      '#description' => t(
        'The URL of the remote JavaScript file that must be referenced for access to the <a href="!issuerApiInfoUrl" target="_blank">Mozilla Issuer API</a>. This enables badges to be uploaded to the Mozilla Backpack. For example, "http://beta.openbadges.org/issuer.js".',
        array(
          '!issuerApiInfoUrl' => BadgeDepotGlobalSettings::getObiBadgeIssuerApiInfoUrl(),
        )
      ),
      '#default_value' => BadgeDepotGlobalSettings::getObiBadgeIssuerApiScriptUrl(),
      '#required' => FALSE,
    );
    $assertion_fieldset[self::FORM_FIELD_ASSERTIONS_AUTO_SAVE_EMAIL] = array(
      '#attributes' => array('name' => self::FORM_FIELD_ASSERTIONS_AUTO_SAVE_EMAIL),
      '#type' => 'checkbox',
      '#title' => t('Save recipient email-address by default'),
      '#description' => t('Specify whether a badge assertion should, by default, also save the email address of the badge recipient when the badge is awarded, if available.'),
      '#default_value' => (BadgeDepotGlobalSettings::getAssertionSaveEmailFlag() ? 1 : 0),
    );

    $json_data_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('OBI JSON-Data Formatting'),
      '#description' => t('Specify general settings that control how the back-end web-service data is being formatted.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['json_data_fieldset'] = &$json_data_fieldset;
    $json_data_fieldset[self::FORM_FIELD_ASSERTIONS_INCLUDE_DYNAMIC_IMAGE] = array(
      '#attributes' => array('name' => self::FORM_FIELD_ASSERTIONS_INCLUDE_DYNAMIC_IMAGE),
      '#type' => 'checkbox',
      '#title' => t('Include dynamic-image URLs'),
      '#description' => t('Specify whether badge assertions should automatically include a dynamic URL for image generation when no pre-baked image URL is specified.  Such URLs generally redirect to the badge-baker service with a dynamic image-creation request.'),
      '#default_value' => (BadgeDepotGlobalSettings::getAssertionIncludeDynamicImageFlag() ? 1 : 0),
    );
    $json_data_fieldset[self::FORM_FIELD_JSON_ALLOW_DATA_URI] = array(
      '#attributes' => array('name' => self::FORM_FIELD_JSON_ALLOW_DATA_URI),
      '#type' => 'checkbox',
      '#title' => t('Allow Data URIs for images'),
      '#description' => t('Specify whether OBI JSON data is allowed to include Data URIs for images.<br /><strong>Warning</strong>: Before you enable the option, ensure that <a target="_blank" href="!bakerBugUrl">this issue</a> is resolved so that the Mozilla Badge Baking Service supports Data URIs as per release 1.0 of the OBI Assertion specification.', array('!bakerBugUrl' => 'https://github.com/mozilla/openbadges-bakery-service/issues/4')),
      '#default_value' => (BadgeDepotGlobalSettings::getObiJsonAllowDataUriFlag() ? 1 : 0),
    );

    $wiki_info_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Open Badges Documentation'),
      '#description' => t('Define the URLs that are provided for additional information about OBI APIs.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form[self::FORM_CONTENT_WIKI_INFO] = &$wiki_info_fieldset;
    $wiki_info_fieldset[self::FORM_FIELD_ASSERTIONS_INFO_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Assertions Wiki URL'),
      '#description' => t('The URL that references documentation about the Mozilla Assertions API, for example, "https://github.com/mozilla/openbadges/wiki/Assertions".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiAssertionsInfoUrl(),
      '#required' => TRUE,
    );
    $wiki_info_fieldset[self::FORM_FIELD_DISPLAYER_API_INFO_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Displayer API Wiki URL'),
      '#description' => t('The URL that references documentation about the Mozilla OBI Displayer API, for example, "https://github.com/mozilla/openbadges/wiki/Displayer-API".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiDisplayerApiInfoUrl(),
      '#required' => TRUE,
    );
    $wiki_info_fieldset[self::FORM_FIELD_BADGE_BAKER_INFO_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Badge Baking Wiki URL'),
      '#description' => t('The URL that references documentation about the Mozilla Badge Baker, for example, "https://github.com/mozilla/openbadges/wiki/Badge-Baking".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiBadgeBakerInfoUrl(),
      '#required' => TRUE,
    );
    $wiki_info_fieldset[self::FORM_FIELD_BADGE_ISSUER_INFO_URL] = array(
      '#type' => 'textfield',
      '#title' => t('Issuer API Wiki URL'),
      '#description' => t('The URL that references documentation about the Mozilla Issuer API that is used for uploading badges to the Mozilla Backpack, for example, "https://github.com/mozilla/openbadges/wiki/Issuer-API".'),
      '#default_value' => BadgeDepotGlobalSettings::getObiBadgeIssuerApiInfoUrl(),
      '#required' => TRUE,
    );

    $action_group = array(
      //'#markup' => '',
      '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_ACTIONS),
      '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
    );
    $form[self::AJAX_CONTENT_ACTIONS] = &$action_group;
    $action_group[self::FORM_BUTTON_SAVE_DATA] = array(
      '#type' => 'submit',
      '#name' => self::FORM_BUTTON_SAVE_DATA,
      '#value' => t('Save Settings'),
    );

    return parent::buildDefault($form, $form_state);
  }

  /**
   * Implements a validation-handler to be used when the configuration options are saved.
   */
  public function validateTriggerSaveData($form, &$form_state) {
    $values = &$form_state['values'];

    // Check that all URL fields appear to be actual URLs.
    $url_fields = array(
      self::FORM_FIELD_ASSERTIONS_INFO_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_WIKI_INFO]),
      self::FORM_FIELD_DISPLAYER_API_INFO_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_WIKI_INFO]),
      self::FORM_FIELD_DISPLAYER_API_BASE_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_DISPLAYER_API]),
      self::FORM_FIELD_DISPLAYER_API_USER_ID_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_DISPLAYER_API]),
      self::FORM_FIELD_BADGE_BAKER_INFO_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_WIKI_INFO]),
      self::FORM_FIELD_BADGE_BAKER_PREFIX_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_BADGE_BAKER]),
      self::FORM_FIELD_BADGE_ISSUER_INFO_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_WIKI_INFO]),
      self::FORM_FIELD_BADGE_ISSUER_SCRIPT_URL => array('isAbsoluteUrl' => TRUE, 'formContainer' => &$form[self::FORM_CONTENT_ASSERTION_OPTIONS]),
    );
    foreach ($url_fields as $url_field_name => $field_data) {
      $url = $values[$url_field_name];
      $form_field = &$field_data['formContainer'][$url_field_name];
      $is_required = (isset($form_field['#required']) ? $form_field['#required'] : FALSE);
      if ((!isset($url)) || (($is_required || ($url != '')) && (!valid_url($url, $field_data['isAbsoluteUrl'])))) {
        form_set_error($url_field_name, t('The %label value is invalid (%url).', array('%url' => $url, '%label' => $form_field['#title'])));
      }
    }
  }

  // Implements the submit handler that gets called once
  // the data-saving request is completely validated.
  // NOTE: This method gets called only if the data that was
  //       submitted for the data-saving request was valid.
  protected function submitTriggerSaveData($form, &$form_state) {
    $values = &$form_state['values'];

    // Persist the current configuration settings.
    $assertions_info_url = $values[self::FORM_FIELD_ASSERTIONS_INFO_URL];
    $displayer_api_info_url = $values[self::FORM_FIELD_DISPLAYER_API_INFO_URL];
    $displayer_api_base_url = $values[self::FORM_FIELD_DISPLAYER_API_BASE_URL];
    $displayer_api_user_id_url = $values[self::FORM_FIELD_DISPLAYER_API_USER_ID_URL];
    $baker_service_info_url = $values[self::FORM_FIELD_BADGE_BAKER_INFO_URL];
    $baker_service_base_url = $values[self::FORM_FIELD_BADGE_BAKER_PREFIX_URL];
    $issuer_api_info_url = $values[self::FORM_FIELD_BADGE_ISSUER_INFO_URL];
    $issuer_api_script_url = $values[self::FORM_FIELD_BADGE_ISSUER_SCRIPT_URL];
    $auto_publish_proxy_memo = $values[self::FORM_FIELD_AUTO_PUBLISH_MEMO];
    $allow_image_data_uri = $values[self::FORM_FIELD_JSON_ALLOW_DATA_URI];
    $include_dynamic_image = $values[self::FORM_FIELD_ASSERTIONS_INCLUDE_DYNAMIC_IMAGE];
    $save_recipient_email = $values[self::FORM_FIELD_ASSERTIONS_AUTO_SAVE_EMAIL];
    BadgeDepotGlobalSettings::setObiAssertionsInfoUrl($assertions_info_url);
    BadgeDepotGlobalSettings::setObiDisplayerApiInfoUrl($displayer_api_info_url);
    BadgeDepotGlobalSettings::setObiDisplayerApiBackpackUrl($displayer_api_base_url);
    BadgeDepotGlobalSettings::setObiDisplayerApiUserUrl($displayer_api_user_id_url);
    BadgeDepotGlobalSettings::setObiBadgeBakerInfoUrl($baker_service_info_url);
    BadgeDepotGlobalSettings::setObiBadgeBakerPrefixUrl($baker_service_base_url);
    BadgeDepotGlobalSettings::setObiBadgeIssuerApiInfoUrl($issuer_api_info_url);
    BadgeDepotGlobalSettings::setObiBadgeIssuerApiScriptUrl($issuer_api_script_url);
    BadgeDepotGlobalSettings::setAutoPublishMemoFlag($auto_publish_proxy_memo);
    BadgeDepotGlobalSettings::setObiJsonAllowDataUriFlag($allow_image_data_uri);
    BadgeDepotGlobalSettings::setAssertionIncludeDynamicImageFlag($include_dynamic_image);
    BadgeDepotGlobalSettings::setAssertionSaveEmailFlag($save_recipient_email);

    // Display a successful form-submission message.
    drupal_set_message(t('The configuration settings were saved successfully.'));

    // Rebuild the form with the current settings.
    $form_state['rebuild'] = TRUE;
  }

  // Implements the submit handler that gets called once
  // the clear-cache request is completely validated.
  // NOTE: This method gets called only if the data that was
  //       submitted for the clear-cache request was valid.
  protected function submitTriggerClearCache($form, &$form_state) {
    $cache_identifier_wildcard = (BADGE_DEPOT_MODULE_NAME . ':');
    $cache_bin = 'cache';
    cache_clear_all($cache_identifier_wildcard, $cache_bin, TRUE);
    drupal_set_message(t('Badge Depot caches cleared.'));
    $form_state['rebuild'] = TRUE;
  }

  // Implements the AJAX-callback handler that gets called
  // once the clear-cache web-request completes.
  // NOTE: This method gets called for every AJAX clear-cache request,
  //       irrespective of success; however, this method is not called
  //       if the cache request was made without JavaScript support (in
  //       which case the full form from the rebuild step is returned).
  protected function ajaxTriggerClearCache($form, &$form_state) {
    // Select the region to be replaced by the AJAX response.
    $output = $form[self::AJAX_CONTENT_CACHE];

    // If any validation errors occurred, only handle those.
    $errors = form_get_errors();
    if (!empty($errors)) {
      // Include default error handling (i.e., display messages).
      return $output;
    }

    // Render the AJAX content on the form.
    $commands = array();
    $commands[] = ajax_command_insert(NULL, drupal_render($output));

    // Ensure that any status messages get displayed.
    $commands[] = ajax_command_append(NULL, theme('status_messages'));

    return array('#type' => 'ajax', '#commands' => $commands);
  }
}
