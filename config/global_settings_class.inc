<?php
/**
 * @file
 * Contains BadgeDepotGlobalSettings.
 */

class BadgeDepotGlobalSettings {

  // Specifies the keys used for storing the global settings
  const OBI_ASSERTIONS_INFO_URL_KEY = 'badge_depot_obi_assertions_info_url';
  const OBI_DISPLAYER_API_INFO_URL_KEY = 'badge_depot_obi_displayer_api_info_url';
  const OBI_DISPLAYER_API_USER_ID_URL_KEY = 'badge_depot_obi_displayer_api_user_id_url';
  const OBI_DISPLAYER_API_BASE_URL_KEY = 'badge_depot_obi_displayer_api_base_url';
  const OBI_BADGE_BAKER_INFO_URL_KEY = 'badge_depot_obi_badge_baker_info_url';
  const OBI_BADGE_BAKER_PREFIX_URL_KEY = 'badge_depot_obi_badge_baker_prefix_url';
  const OBI_BADGE_ISSUER_INFO_URL_KEY = 'badge_depot_obi_badge_issuer_info_url';
  const OBI_BADGE_ISSUER_SCRIPT_URL_KEY = 'badge_depot_obi_badge_issuer_script_url';
  const OBI_JSON_ALLOW_DATA_URI_KEY = 'badge_depot_obi_json_allow_data_uri';
  const AUTO_PUBLISH_BADGE_MEMO_KEY = 'badge_depot_auto_publish_badge_memo';
  const ASSERTIONS_INCLUDE_DYNAMIC_IMAGE_KEY = 'badge_depot_assertions_include_dynamic_image';
  const ASSERTIONS_AUTO_SAVE_EMAIL_KEY = 'badge_depot_assertions_auto_save_email';

  /**
   * Removes the variables that store the values of the global settings.  This
   * methods gets called when the module is uninstalled by the administrator.
   */
  public static function uninstall() {
    variable_del(self::OBI_ASSERTIONS_INFO_URL_KEY);
    variable_del(self::OBI_DISPLAYER_API_INFO_URL_KEY);
    variable_del(self::OBI_DISPLAYER_API_USER_ID_URL_KEY);
    variable_del(self::OBI_DISPLAYER_API_BASE_URL_KEY);
    variable_del(self::OBI_BADGE_BAKER_INFO_URL_KEY);
    variable_del(self::OBI_BADGE_BAKER_PREFIX_URL_KEY);
    variable_del(self::OBI_BADGE_ISSUER_INFO_URL_KEY);
    variable_del(self::OBI_BADGE_ISSUER_SCRIPT_URL_KEY);
    variable_del(self::OBI_JSON_ALLOW_DATA_URI_KEY);
    variable_del(self::AUTO_PUBLISH_BADGE_MEMO_KEY);
    variable_del(self::ASSERTIONS_INCLUDE_DYNAMIC_IMAGE_KEY);
    variable_del(self::ASSERTIONS_AUTO_SAVE_EMAIL_KEY);
  }

  /**
   * Gets the URL of the Mozilla OBI Displayer API that converts an email
   * address to a user ID.  For details, refer to the API documentation at...
   *   https://github.com/mozilla/openbadges/wiki/Displayer-API
   */
  public static function getObiDisplayerApiUserUrl() {
    $url = variable_get(self::OBI_DISPLAYER_API_USER_ID_URL_KEY, NULL);
    if (!isset($url)) {
      // Example: "http://beta.openbadges.org/displayer/convert/email"
      $url = (static::getObiDisplayerApiBackpackUrl() . 'convert/email');
    }

    return $url;
  }

  /**
   * Sets the URL of the Mozilla OBI Displayer API used for user lookups.
   */
  public static function setObiDisplayerApiUserUrl($url) {
    variable_set(self::OBI_DISPLAYER_API_USER_ID_URL_KEY, $url);
  }

  /**
   * Gets the base URL of the Mozilla OBI Displayer API that is
   * used to obtain a badge user's shared backpacks. When specified,
   * the URL will always include the trailing slash character.
   * For details, refer to the API documentation at...
   *   https://github.com/mozilla/openbadges/wiki/Displayer-API
   */
  public static function getObiDisplayerApiBackpackUrl() {
    return variable_get(self::OBI_DISPLAYER_API_BASE_URL_KEY, 'http://beta.openbadges.org/displayer/');
  }

  /**
   * Sets the base URL of the Mozilla OBI Displayer API that is used to
   * obtain a badge user's shared backpacks.  The URL should include the
   * trailing slash character, otherwise it will be appended automatically.
   */
  public static function setObiDisplayerApiBackpackUrl($url) {
    if ((!empty($url)) && (!DataUriCreatorString::endsWith($url, '/'))) {
      // Ensure that the URL ends with a slash character,
      // since derived URLs are not expected to check for this
      $url .= '/';
    }

    variable_set(self::OBI_DISPLAYER_API_BASE_URL_KEY, $url);
  }

  /**
   * Gets the URL of the Mozilla Badge Baker that takes an assertion URL and
   * bakes it into a badge PNG image. The assertion URL will be appended to the
   * baker prefix as an encoded URI component when requesting the baked badge.
   * For details, refer to the API documentation at...
   *   https://github.com/mozilla/openbadges/wiki/Badge-Baking
   */
  public static function getObiBadgeBakerPrefixUrl() {
    return variable_get(self::OBI_BADGE_BAKER_PREFIX_URL_KEY, 'http://beta.openbadges.org/baker?assertion=');
  }

  /**
   * Sets the URL of the Mozilla Badge Baker.
   */
  public static function setObiBadgeBakerPrefixUrl($url) {
    variable_set(self::OBI_BADGE_BAKER_PREFIX_URL_KEY, $url);
  }

  /**
   * Gets the URL of the Mozilla Badge Issuer API script that takes
   * a badge's assertion URL places the badge in the user's Backpack.
   * For details, refer to the API documentation at...
   *   https://github.com/mozilla/openbadges/wiki/Issuer-API
   */
  public static function getObiBadgeIssuerApiScriptUrl() {
    return variable_get(self::OBI_BADGE_ISSUER_SCRIPT_URL_KEY, 'http://beta.openbadges.org/issuer.js');
  }

  /**
   * Sets the URL of the Mozilla Badge Issuer API script.
   */
  public static function setObiBadgeIssuerApiScriptUrl($url) {
    variable_set(self::OBI_BADGE_ISSUER_SCRIPT_URL_KEY, $url);
  }

  /**
   * Determines whether badge memo's should be published automatically when
   * discovered through the assertion proxy (e.g., found by Badge Inspector).
   */
  public static function getAutoPublishMemoFlag() {
    return variable_get(self::AUTO_PUBLISH_BADGE_MEMO_KEY, TRUE);
  }

  /**
   * Specifies whether badge memo's should be published automatically when
   * discovered through the assertion proxy (e.g., found by Badge Inspector).
   */
  public static function setAutoPublishMemoFlag($is_published) {
    $is_published = ($is_published ? TRUE : FALSE);
    variable_set(self::AUTO_PUBLISH_BADGE_MEMO_KEY, $is_published);
  }

  /**
   * Determines whether badge assertions should automatically include a
   * dynamic URL for image generation when no pre-baked image URL is specified
   * (e.g., by default the exposed URL will redirect to the badge-baker service).
   */
  public static function getAssertionIncludeDynamicImageFlag() {
    return variable_get(self::ASSERTIONS_INCLUDE_DYNAMIC_IMAGE_KEY, FALSE);
  }

  /**
   * Specifies whether badge assertions should automatically include a dynamic
   * URL for image generation when no pre-baked image URL is specified.
   */
  public static function setAssertionIncludeDynamicImageFlag($is_included) {
    $is_included = ($is_included ? TRUE : FALSE);
    variable_set(self::ASSERTIONS_INCLUDE_DYNAMIC_IMAGE_KEY, $is_included);
  }

  /**
   * Determines whether a badge assertion should, by default, also save
   * the email address of the badge recipient when a badge is awarded.
   */
  public static function getAssertionSaveEmailFlag() {
    return variable_get(self::ASSERTIONS_AUTO_SAVE_EMAIL_KEY, TRUE);
  }

  /**
   * Specify whether a badge assertion should, by default, also save
   * the email address of the badge recipient when a badge is awarded.
   */
  public static function setAssertionSaveEmailFlag($is_auto_saved) {
    $is_auto_saved = ($is_auto_saved ? TRUE : FALSE);
    variable_set(self::ASSERTIONS_AUTO_SAVE_EMAIL_KEY, $is_auto_saved);
  }

  /**
   * Determines whether OBI JSON data is allowed to include Data URIs.
   */
  public static function getObiJsonAllowDataUriFlag() {
    return variable_get(self::OBI_JSON_ALLOW_DATA_URI_KEY, FALSE);
  }

  /**
   * Specify whether OBI JSON data is allowed to include Data URIs.
   *
   * This provides a global setting to toggle a workaround for the issue
   * where the Mozilla Badge Baking Service apparently does not yet fully
   * support the OBI 1.0 release of the Assertion specification that allows
   * a PNG image URL to be specified as a Data URI. For details, see...
   *   https://github.com/mozilla/openbadges-bakery-service/issues/4
   */
  public static function setObiJsonAllowDataUriFlag($allow_data_uri) {
    $allow_data_uri = ($allow_data_uri ? TRUE : FALSE);
    variable_set(self::OBI_JSON_ALLOW_DATA_URI_KEY, $allow_data_uri);
  }

  /**
   * Gets the URL that references documentation about the Mozilla Assertions API.
   */
  public static function getObiAssertionsInfoUrl() {
    return variable_get(self::OBI_ASSERTIONS_INFO_URL_KEY, 'https://github.com/mozilla/openbadges/wiki/Assertions');
  }

  /**
   * Sets the URL that references documentation about the Mozilla Assertions API.
   */
  public static function setObiAssertionsInfoUrl($url) {
    variable_set(self::OBI_ASSERTIONS_INFO_URL_KEY, $url);
  }

  /**
   * Gets the URL that references documentation about the Mozilla OBI Displayer API.
   */
  public static function getObiDisplayerApiInfoUrl() {
    return variable_get(self::OBI_DISPLAYER_API_INFO_URL_KEY, 'https://github.com/mozilla/openbadges/wiki/Displayer-API');
  }

  /**
   * Sets the URL that references documentation about the Mozilla OBI Displayer API.
   */
  public static function setObiDisplayerApiInfoUrl($url) {
    variable_set(self::OBI_DISPLAYER_API_INFO_URL_KEY, $url);
  }

  /**
   * Gets the URL that references documentation about the Mozilla Badge Baker.
   */
  public static function getObiBadgeBakerInfoUrl() {
    return variable_get(self::OBI_BADGE_BAKER_INFO_URL_KEY, 'https://github.com/mozilla/openbadges/wiki/Badge-Baking');
  }

  /**
   * Sets the URL that references documentation about the Mozilla Badge Baker.
   */
  public static function setObiBadgeBakerInfoUrl($url) {
    variable_set(self::OBI_BADGE_BAKER_INFO_URL_KEY, $url);
  }

  /**
   * Gets the URL that references documentation about the Mozilla Badge Issuer API.
   */
  public static function getObiBadgeIssuerApiInfoUrl() {
    return variable_get(self::OBI_BADGE_ISSUER_INFO_URL_KEY, 'https://github.com/mozilla/openbadges/wiki/Issuer-API');
  }

  /**
   * Sets the URL that references documentation about the Mozilla Badge Issuer API.
   */
  public static function setObiBadgeIssuerApiInfoUrl($url) {
    variable_set(self::OBI_BADGE_ISSUER_INFO_URL_KEY, $url);
  }
}
