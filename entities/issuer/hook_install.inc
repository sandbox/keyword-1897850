<?php
/**
 * @file
 * Contains additional hook_schema() information for installation.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_schema database info for the badge-issuer entity.
 */
function _badge_depot_append_entities_issuer_schema(&$schema) {
  $schema[BADGE_DEPOT_ENTITY_TABLE_ISSUER] = array(
    'description' => t('The base record for the registry of OBI badge-issuer organizations.'),
    'fields' => array(
      'issuer_id' => array(
        'description' => t('The unique identifier of this issuer-organization entry.'),
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'issuer_machine_name' => array(
        'description' => t('The persistent unique-identifier of this issuer-organization entry, which is a machine-readable name in Drupal parlance.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      //'version_id' => array(
      //  'description' => t('The current version identifier of the issuer-organization data.'),
      //  'type' => 'int',
      //  'not null' => TRUE,
      //  'unsigned' => TRUE,
      //  'default' => 0,
      //),
      'owner_user_id' => array(
        'description' => t('A reference to the user that owns this issuer-organization for data-management purposes.'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'owner_user_name' => array(
        'description' => t('The name of the user that owns this issuer-organization for data-management purposes; used primarily for enhanced security when exporting/importing the data entity.'),
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => t('The entity type of this issuer entry (used by entity sub-type bundles).'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => t('The name of this issuer organization (required; treated as non-markup plain text).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => t('A short description of this issuer-organization entry (treated as non-markup plain text).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'body' => array(
        'description' => t('The detailed description of this issuer-organization entry (treated as HTML markup).'),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'is_published' => array(
        'description' => t('A flag indicating whether the issuer-organization entry is publicly visible.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'is_dormant' => array(
        'description' => t('A flag indicating that the issuer-organization is currently not issuing badges.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => t('The Unix timestamp of when the issuer-organization entry was created.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => t('The Unix timestamp of when the issuer-organization entry was most recently saved.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'url' => array(
        'description' => t('The URL that links to the badge-issuing agent (required). This will often be the URL of the issuer institution, although it may be a different URL.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'email' => array(
        // Required since March 2013 for OBI specification 0.5.0 (initially
        // optional, though), but apparently again optional for version 1.0.
        'description' => t('The email address of a contact person associated with the badge-issuer organization.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'image_url' => array(
        'description' => t('The URL that links to an image that represents the institution (can be a regular URL or <a href="!url" target="_blank" title="Tool to create a Data URI">Data URI</a>).', array('!url' => url(DATA_URI_CREATOR_PAGE_PATH))),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'public_key_url' => array(
        'description' => t('An optional URL that links to an external public-key for the badge-issuer organization.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'public_key_pem' => array(
        'description' => t('An optional Public Key (PEM format) for the badge-issuer organization to be hosted by the Badge Depot module and used by signed badges (e.g., PEM-formatted content of an OpenSSL .pem file; see RFC 1421 through 1424).'),
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'private_key_pem' => array(
        'description' => t('An optional Private Key (PEM format) for the badge-issuer organization to be used for signed badges (e.g., PEM-formatted content of an OpenSSL .key file).'),
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'revocation_list_url' => array(
        'description' => t('An optional endpoint URL that links to an external Badge Revocation List for when the default service will not be used.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'revocation_list_extras' => array(
        'description' => t('Any optional JSON data to be merged with the default Badge Revocation List of this issuer.'),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('issuer_id'),
    'unique keys' => array(
      'issuer_key' => array('issuer_id'),
      //'issuer_version_key' => array('issuer_id', 'version_id'),
    ),
    'indexes' => array(
      'changed_index' => array('changed'),
      'created_index' => array('created'),
      'machine_index' => array('issuer_machine_name'),
      'owner_user_index' => array('owner_user_id'),
    ),
  );
}
