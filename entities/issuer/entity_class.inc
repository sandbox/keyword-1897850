<?php
/**
 * @file
 * Contains BadgeIssuerEntity.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Main class for badge-issuer entities.
 */
class BadgeIssuerEntity extends BadgeDepotObiBaseEntity {

  /**
   * Overrides the base method that returns the default name of the
   * database table for this entity type.
   */
  protected static function getDefaultEntityType() {
    return BADGE_DEPOT_ENTITY_TABLE_ISSUER;
  }

  /**
   * Overrides the base method that returns the name of the database
   * column that stores the machine-readable name of the entity.
   */
  protected static function getDefaultMachineNameKey() {
    return BADGE_DEPOT_ENTITY_COLUMN_ISSUER_MACHINE_NAME;
  }

  /**
   * Overrides the base method that returns the base URL path for all instances
   * of this entity type (that is not for a single specific entity instance).
   */
  protected static function getEntityMainRootPath() {
    return BADGE_DEPOT_MAIN_ISSUER_PATH;
  }

  /**
   * Overrides the base method that returns the base URL path to administer
   * this entity type (that is not for a single entity instance).
   */
  protected static function getEntityAdminRootPath() {
    return BADGE_DEPOT_ADMIN_ISSUER_PATH;
  }

  /**
   * Overrides the base method that lists all the not-null text fields.
   */
  protected function getNonNullableTextFieldNames() {
    return (parent::getNonNullableTextFieldNames() +
      array(
        'body',
        'image_url',
        'public_key_pem',
        'private_key_pem',
        'revocation_list_extras',
      )
    );
  }

  // Define fields that map to specific database columns of this entity:
  public $issuer_id;

  public $issuer_machine_name;

  // Title is the name of the issuer organization;
  // a required field (OBI specification 1.0).
  public $title;

  public $description;

  public $body;

  public $is_dormant;

  // URL is a required field (OBI specification 1.0).
  public $url;

  public $email;

  public $image_url;

  public $public_key_url;

  public $public_key_pem;

  public $private_key_pem;

  public $revocation_list_url;

  public $revocation_list_extras;

  public function __construct(array $values = array(), $entity_type = NULL) {
    parent::__construct($values, $entity_type);

    // Publish issuer-organizations by default.
    $this->is_published = TRUE;
  }

  /**
   * Gets the path of the public key when served locally.
   *
   * @param $allow_blank_pem
   *   FALSE to return NULL if no local PEM data is available, or
   *   TRUE to return the URL even when no public-key data is set.
   *
   * @return
   *   The local path where the public-key PEM data is being served.
   */
  public function getLocalPublicKeyPath($allow_blank_pem = FALSE) {
    $url = NULL;
    if ($allow_blank_pem || (!empty($this->public_key_pem))) {
      $url = $this->getEntityViewPath();
      if (!empty($url)) {
        $url .= '/public-key';
      }
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the issuer-organization's
   * public-key signature, which can be a local or remote URL.
   *
   * @param $allow_blank_pem
   *   FALSE to return NULL if no remote URL has been specified and no local
   *   PEM data is available; otherwise TRUE to return the URL irrespectively.
   *
   * @return
   *   The URL where the public key can be found.
   */
  public function getPublicKeyUrl($allow_blank_pem = FALSE) {
    $url = $this->public_key_url;
    if (empty($url)) {
      $url = $this->getLocalPublicKeyPath($allow_blank_pem);
    }

    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the endpoint where the OBI Badge Issuer data is hosted.
   *
   * @return
   *   The local path where the issuer data is being served in JSON format.
   */
  public function getLocalJsonDataPath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/obi-data';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the issuer JSON data.
   *
   * The format of the linked data is as specified for OBI "IssuerOrganization".
   *
   * @return
   *   The URL where the badge issuer-organization data can be found.
   */
  public function getJsonDataUrl() {
    $url = $this->getLocalJsonDataPath();
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the Badge Revocation List when served locally.
   *
   * @return
   *   The local path where the revocation-list JSON data is being served.
   */
  public function getLocalRevocationListPath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/revocation-list';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the Badge Revocation List,
   * which can be a local or remote endpoint.
   *
   * @return
   *   The URL where the badge's Revocation List JSON data can be found.
   */
  public function getRevocationListUrl() {
    $url = NULL;
    if (!empty($this->revocation_list_url)) {
      $url = $this->revocation_list_url;
    }
    else {
      $url = $this->getLocalRevocationListPath();
    }

    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the badge-issuer image when served locally.
   *
   * @return
   *   The local path where the image is being served.
   */
  public function getLocalImagePath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/image';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the organization's image,
   * which can be a local or remote URL, or even a Data URI.
   *
   * @param $allow_data_uri
   *   TRUE to indicate that the URL may be a Data URI, or FALSE if a hosted
   *   alternative URL should be provided when the image URL is a Data URI.
   *
   * @return
   *   The URL where the raw badge-image can be found.
   */
  public function getImageUrl($allow_data_uri = TRUE) {
    $url = $this->image_url;
    if ((!$allow_data_uri) && DataUriCreator::isDataUri($url)) {
      $url = $this->getLocalImagePath();
    }

    return DataUriCreator::getDataUriOrQualifiedUrl($url);
  }

  /**
   * Create a JSON Web Signature (JWS) representation of the specified data
   * using the issuer's digital signature to signify the origin of the data.
   *
   * @param $data_payload
   *   The data payload to be signed.  The data should be specified as a
   *   string, otherwise it will be converted to a JSON string before signing.
   *
   * @return
   *   The JWS string that represents the data payload; otherwise NULL,
   *   for example if the the badge-issuer signature's private key is unknown.
   */
  public function getJsonWebSignature($data_payload) {
    return BadgeDepotJsonWebSignature::getCompactSerialization($data_payload, $this->private_key_pem);
  }

  /**
   * Gets the JSON representation of this badge-issuer
   * entity, formatted as required by the OBI Badge Assertion
   * specification, version 1.0 (see section "IssuerOrganization").
   *
   * @return
   *   An array containing the entity's JSON data.
   */
  public function getJsonData() {
    $json_data = array(
      'name' => $this->title,
      'url' => url($this->url, array('absolute' => TRUE)),
    );

    if (!empty($this->description)) {
      $json_data['description'] = $this->description;
    }

    if (!empty($this->email)) {
      $json_data['email'] = $this->email;
    }

    $revocation_list_url = $this->getRevocationListUrl();
    if (!empty($revocation_list_url)) {
      $json_data['revocationList'] = $revocation_list_url;
    }

    $image_url = $this->getImageUrl(BadgeDepotGlobalSettings::getObiJsonAllowDataUriFlag());
    if (!empty($image_url)) {
      $json_data['image'] = $image_url;
    }

    return $json_data;
  }

  /**
   * Implements a web-service endpoint for serving a JSON representation
   * of this entity's data that is formatted as per version 1.0 of the
   * OBI Badge Assertion specification (see section "IssuerOrganization").
   *
   * @return
   *   Upon success, an array containing the entity's data for JSON-rendering
   *   purposes; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED).
   *   The return value should be processed by this module's JSON delivery
   *   callback function (that is badge_depot_deliver_json).
   */
  public function requestJsonData() {
    if (!$this->isAccessibleByUser('view')) {
      return MENU_ACCESS_DENIED;
    }

    return $this->getJsonData();
  }

  /**
   * Implements a web-service endpoint for serving the badge revocation list
   * of this issuer, which is formatted as per version 1.0 of the OBI Badge
   * Assertion specification (see the sections on "Revoking", "JSON Examples",
   * and the "revocationList" property of "IssuerOrganization").
   *
   * @return
   *   Upon success, an associative-array or string-representation of the
   *   entity's revocation; otherwise, an integer error-code (for example,
   *   MENU_ACCESS_DENIED).  The return value should be processed by this
   *   module's JSON delivery callback (that is badge_depot_deliver_json).
   */
  public function requestJsonRevocations() {
    if (!$this->isAccessibleByUser('view')) {
      return MENU_ACCESS_DENIED;
    }

    $revocations = array();

    // Query all the badge assertions that have been awarded by this issuer but
    // has since been revoked, and populate the initial revocations-list data.
    $issuer_id = $this->issuer_id;

    // Get a list of all the manifest IDs for this issuer-organization.
    $manifest_ids = BadgeManifestEntity::loadMultipleThroughFilter($issuer_id);
    if (empty($manifest_ids)) {
      $manifest_ids = NULL;
    }
    else {
      $delegate = function(&$manifest, $key) {
        $manifest = $manifest->manifest_id;
      };

      if (!array_walk($manifest_ids, $delegate)) {
        $manifest_ids = NULL;
      }
    }

    // Do not list any awards that should not be published.
    $is_published = TRUE;

    // Do not specify this as a criterium, since all revoked
    // badges must also have their expiration-time set.
    $has_revoke_reason = NULL;
    $deadline_time = time();
    $assertions = BadgeAssertionEntity::loadMultipleThroughFilter($manifest_ids, $is_published, NULL, $has_revoke_reason, $deadline_time);
    if (!empty($assertions)) {
      foreach ($assertions as $assertion) {
        if (($assertion->isRevoked($deadline_time)) && ((!empty($manifest_ids) || ($issuer_id == $assertion->getManifest()->issuer_id)))) {
          $revocations[$assertion->assertion_machine_name] = $assertion->getRevokeReason($deadline_time);
        }
      }
    }

    // Merge with any extra badge-revocation reasons specified by this entity.
    if (!empty($this->revocation_list_extras)) {
      if (!empty($revocations)) {
        $revocations += drupal_json_decode($this->revocation_list_extras);
      }
      else {
        $revocations = $this->revocation_list_extras;
      }
    }

    return $revocations;
  }

  /**
   * Implements a web-service endpoint for serving the public key of
   * this badge-issuer organization, which is formatted as PEM data.
   *
   * @return
   *   Upon success, a string containing the public-key in PEM format is
   *   returned; otherwise, an integer error-code is returned (for example,
   *   MENU_ACCESS_DENIED).  The return value should be processed by this
   *   module's PEM delivery callback function (badge_depot_deliver_pem).
   */
  public function requestSignature() {
    if (!$this->isAccessibleByUser('view')) {
      return MENU_ACCESS_DENIED;
    }

    return $this->public_key_pem;
  }

  /**
   * Returns the page-request response for the image of the badge issuer.
   *
   * @return
   *   Upon success, the Data URI or regular URL of the image.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestImage() {
    return $this->image_url;
  }
}
