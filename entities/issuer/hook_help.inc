<?php
/**
 * @file
 * Contains additional hook_help() information.
 */

/**
 * Provides hook_help info for the badge-issuer entity.
 */
function _badge_depot_entities_issuer_help($path, $arg) {
  if ($path === BADGE_DEPOT_ADMIN_ISSUER_PATH) {
    $output = ('<p>' . t('Badge Issuer information is used to implement services for affiliated organizations that award Open Badges.') . '</p>');
    if ((count($arg) <= 4) || empty($arg[4])) {
      $output .= ('<p>' . t('Configure the badge issuers that are stored as Entity data using this administration page.') . '</p>');
    }

    return $output;
  }

  if ($path === (BADGE_DEPOT_ADMIN_ISSUER_PATH . '/manage/%')) {
    $output = _badge_depot_entities_issuer_help(BADGE_DEPOT_ADMIN_ISSUER_PATH, $arg);
    $machine_name = $arg[5];
    $entity = BadgeIssuerEntity::loadFromMachineName($machine_name);
    if (isset($entity)) {
      $url_options = array('absolute' => TRUE);
      $output .= '<p>' . t('For example, once configured, the Badge Depot module is able to:') . '</p>';
      $output .= '<ul>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Publish a human-readable page</a> describing the organization,', array('@url' => url($entity->getEntityViewPath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Provide OBI issuer data</a>, in JSON format, that gets linked to from badges issued by the organization,', array('@url' => url($entity->getLocalJsonDataPath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Host an image</a> (optional) representing the organization,', array('@url' => url($entity->getLocalImagePath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Host a public-key</a> (optional), in PEM format, that is used for signature verification of signed badges, and', array('@url' => url($entity->getLocalPublicKeyPath(TRUE), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Serve a Badge Revocation List</a>, in JSON format, of badges that have been revoked for some reason.', array('@url' => url($entity->getLocalRevocationListPath(), $url_options))) . '</li>';
      $output .= '</ul>';
    }

    return $output;
  }
}
