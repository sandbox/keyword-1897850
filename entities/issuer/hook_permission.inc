<?php
/**
 * @file
 * Contains additional hook_permission() information.
 */

/**
 * Adds the hook_permission data for the badge-issuer entity.
 */
function _badge_depot_append_entities_issuer_permission(&$permissions) {
  $permissions['administer badge issuer entities'] = array(
    'title' => t('Administer badge-issuer data'),
    'description' => t('Provides full access to all administrative tasks related to badge_issuer entities.'),
    'restrict access' => TRUE,
  );

  $permissions['create badge issuer entities'] = array(
    'title' => t('Create badge-issuer data'),
    'description' => t('Allows users to create badge_issuer entities.'),
    'restrict access' => TRUE,
  );

  $permissions['delete badge issuer entities'] = array(
    'title' => t('Delete badge-issuer data'),
    'description' => t('Allows users to delete badge_issuer entities.'),
    'restrict access' => TRUE,
  );

  $permissions['update badge issuer entities'] = array(
    'title' => t('Update badge-issuer data'),
    'description' => t('Allows users to update badge_issuer entities.'),
    'restrict access' => TRUE,
  );

  $permissions['view badge issuer entities'] = array(
    'title' => t('View badge-issuer data'),
    'description' => t('Allows users to view badge_issuer entities.'),
  );
}
