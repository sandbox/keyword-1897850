<?php
/**
 * @file
 * Contains additional hook_views_data_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes the hook_views_data_alter data for the badge-issuer entity.
 */
function _badge_depot_entities_issuer_views_data_alter(&$info) {
  // This modifies the formatting for the body field of the badge_issuer
  // entity to be rendered as HTML.
  $info[BADGE_DEPOT_ENTITY_TABLE_ISSUER]['body']['field']['handler'] = 'BadgeDepotViewsFieldHandlerHtml';
}
