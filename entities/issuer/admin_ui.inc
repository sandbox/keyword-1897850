<?php
/**
 * @file
 * Implements the administrative user-interface functions for the issuer entity.
 */

class BadgeIssuerAdminPage extends BadgeDepotEntityAdminPage {

  // Define the identifiers of the form fields that contain submission data.
  const FORM_FIELD_EMAIL = 'email';
  const FORM_FIELD_REVOCATIONS = 'revocation_list_extras';
  const FORM_FIELD_PUBLIC_KEY = 'public_key_pem';
  const FORM_FIELD_PRIVATE_KEY = 'private_key_pem';

  // Define the identifiers of buttons that trigger submission of data,
  // for example, "CreateSignature" maps to validateCreateSignature,
  // submitCreateSignature, ajaxCreateSignature, etc.
  const FORM_BUTTON_CREATE_SIGNATURE = 'CreateSignature';

  // Define the HTML identifiers for AJAX-replacement contents.
  const AJAX_CONTENT_SIGNATURE = 'signing_key';

  // Define the HTML identifiers for AJAX-replacement wrappers.
  const AJAX_WRAPPER_SIGNATURE = 'signing_key_wrapper';

  /**
   * Overrides the default build-handler to implement the administrative
   * user-interface form for the badge-issuer entity.
   */
  public function buildDefault($form, &$form_state) {
    $this->getBuildFormExtraArgs($entity, $op);
    $entity_type = $entity->entityType();

    // Includes super-user access.
    $is_admin = user_access('administer ' . $entity_type . ' entities');
    if ($op === 'clone') {
      // Modify the title of the clone.
      $entity->title .= ' (cloned)';

      // Create a new machine-readable ID for the clone.
      $entity->setMachineName();
    }

    $properties = NULL;
    $this->buildEntityFieldAdmin($form, 'title', $properties, $entity, array('#title' => t('Name of Badge-Issuer'), '#required' => TRUE, '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'url', $properties, $entity, array('#required' => TRUE, '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, self::FORM_FIELD_EMAIL, $properties, $entity, array('#title' => t('Contact Email'), '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'description', $properties, $entity, array('#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'body', $properties, $entity, array('#type' => 'textarea'));
    $this->buildEntityFieldAdmin($form, 'is_published', $properties, $entity);
    $this->buildEntityFieldAdmin($form, 'is_dormant', $properties, $entity);

    $owner_user_id = $entity->owner_user_id;
    if ($is_admin) {
      // Administrators have full control over changing
      // the current owner of this badge entity.
      $users = entity_load('user');
      $user_select = array();
      foreach ($users as $user_item) {
        $user_select[$user_item->uid] = $user_item->name;
      }

      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'select', '#options' => $user_select));
    }
    elseif (!empty($owner_user_id)) {
      // Regular users can only see who is the owner of this
      // badge entity, but only when it currently has an owner.
      $users = entity_load('user', array($owner_user_id));
      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'item', '#markup' => theme('username', array('account' => $users[$owner_user_id]))));
    }

    $form['issuer_image'] = array('#type' => 'fieldset', '#title' => t('Institution Image'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['issuer_image'], 'image_url', $properties, $entity, array('#maxlength' => 350208));
    $form['issuer_image']['activity_launch'] = array(
      '#type' => 'button',
      '#value' => t('View Image'),
      '#attributes' => array(
        // Override client-side JavaScript action and disable form submission.
        'onclick' => $this->buildLaunchJavaScript(
          'image_url',
          'jQuery("#issuer_image_preview").each(function() { this.src = url; }); jQuery("#issuer_image_container").show();',
          t('The image URL is not specified.')
        ),
      ),
      '#suffix' => '<span id="issuer_image_container" style="display:none;"><br /><br /><img id="issuer_image_preview" /></span>',
    );

    $signature_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Digital Signature'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_SIGNATURE),
      '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
    );
    $form[self::AJAX_CONTENT_SIGNATURE] = &$signature_fieldset;
    $this->buildEntityFieldAdmin($signature_fieldset, 'public_key_url', $properties, $entity, array('#maxlength' => 255));
    $signature_fieldset['criteria_launch'] = $this->buildLaunchButton('public_key_url', t('Download External Key'));
    $public_key_pem = $this->buildEntityFieldAdmin($signature_fieldset, self::FORM_FIELD_PUBLIC_KEY, $properties, $entity, array('#type' => 'textarea'));
    $public_key_path = $entity->getLocalPublicKeyPath();
    if (!empty($public_key_path)) {
      $signature_fieldset[self::FORM_FIELD_PUBLIC_KEY]['#description'] .= ' ' . t('If specified, the public key will be served from <a href="!url" target="_blank">this local endpoint</a>.', array('!url' => url($public_key_path)));
    }

    $private_key_pem = $this->buildEntityFieldAdmin($signature_fieldset, self::FORM_FIELD_PRIVATE_KEY, $properties, $entity, array('#type' => 'textarea'));

    if (isset($this->createdPublicKeyPem)) {
      $public_key_pem = $this->createdPublicKeyPem;
      $signature_fieldset[self::FORM_FIELD_PUBLIC_KEY]['#value'] = $public_key_pem;
    }

    if (isset($this->createdPrivateKeyPem)) {
      $private_key_pem = $this->createdPrivateKeyPem;
      $signature_fieldset[self::FORM_FIELD_PRIVATE_KEY]['#value'] = $private_key_pem;
    }

    if (BadgeDepotOpenSsl::isAvailable()) {
      // Add a helper button to create a temporary signature's
      // public and private keys (e.g., for testing purposes).
      $signature_fieldset[self::FORM_BUTTON_CREATE_SIGNATURE] = array(
        '#type' => 'submit',
        '#name' => self::FORM_BUTTON_CREATE_SIGNATURE,
        '#value' => t('Create new signature'),
        '#ajax' => array(
          'callback' => '_badge_depot_form_proxy_ajax',
          'wrapper' => self::AJAX_WRAPPER_SIGNATURE,
          'progress' => array(
            'type' => 'throbber',
            'message' => 'Generating signature...',
          ),
        ),
      );
      if ((!empty($public_key_pem)) || (!empty($private_key_pem))) {
        $signature_fieldset[self::FORM_FIELD_PRIVATE_KEY]['#description'] .= t('<br /><strong>Warning</strong>: The digital signature of the issuer organization should not be changed after it was used to issue badges to users, since this may invalidate the issuer\'s existing badge awards.');
      }
      $signature_fieldset[self::FORM_FIELD_PRIVATE_KEY]['#description'] .= t('<br /><strong>Hint</strong>: You can generate a new signature certificate to be used for testing purposes with the button below.');
    }

    $form['revocation_list'] = array('#type' => 'fieldset', '#title' => t('Revocation List'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['revocation_list'], 'revocation_list_url', $properties, $entity, array('#title' => t('Remote URL'), '#maxlength' => 255));
    $form['revocation_list']['revocation_launch'] = $this->buildLaunchButton('revocation_list_url', t('Download External List'));
    $revocation_extras = $this->buildEntityFieldAdmin($form['revocation_list'], self::FORM_FIELD_REVOCATIONS, $properties, $entity, array('#title' => t('Extra JSON'), '#type' => 'textarea'));
    if ((!empty($revocation_extras)) && ($revocation_extras !== '{}')) {
      $form['revocation_list'][self::FORM_FIELD_REVOCATIONS]['#rows'] = 10;
    }
    $form['revocation_list'][self::FORM_FIELD_REVOCATIONS]['#description'] .= t(
      ' Refer to the <a href="!url" target="_blank">OBI Assertions specification</a>' .
      ' for formatting requirements. The data must be entered as a JSON object;' .
      ' each property represents a revoked badge, where the property\'s key' .
      ' corresponds with the revoked badge\'s unique identifier and the' .
      ' property\'s value is a string describing the reason for revocation.' .
      ' For example: <em>{"id-1": "Issued in error", "id-2": "Issued as a test"}</em>',
      array(
        '!url' => url(BadgeDepotGlobalSettings::getObiAssertionsInfoUrl() . '#revoking-1'),
      )
    );

    field_attach_form($entity_type, $entity, $form, $form_state);

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t(($op === 'add') ? 'Create badge issuer' : 'Save data'),
      '#weight' => 50,
    );

    return parent::buildDefault($form, $form_state);
  }

  /**
   * Overrides the default validation-handler of the administrative form.
   */
  public function validateDefault($form, &$form_state) {
    $values = &$form_state['values'];

    // Validate the digital signature, if specified.
    $public_key_pem_value = &$values[self::FORM_FIELD_PUBLIC_KEY];
    $public_key_pem_value = ltrim($public_key_pem_value);
    $public_key_pem = trim($public_key_pem_value);
    $private_key_pem_value = &$values[self::FORM_FIELD_PRIVATE_KEY];
    $private_key_pem_value = ltrim($private_key_pem_value);
    $private_key_pem = trim($private_key_pem_value);
    if (($public_key_pem !== '') || ($private_key_pem !== '')) {
      if (!BadgeDepotOpenSsl::isAvailable()) {
        $not_supported = t('The PHP system is not configured to support OpenSSL features.');
        if ($public_key_pem !== '') {
          form_set_error(self::FORM_FIELD_PUBLIC_KEY, $not_supported);
        }

        if ($private_key_pem !== '') {
          form_set_error(self::FORM_FIELD_PRIVATE_KEY, $not_supported);
        }
      }
      else {
        if ($public_key_pem !== '') {
          if ((!DataUriCreatorString::startsWith($public_key_pem, '-----BEGIN ')) || (!DataUriCreatorString::endsWith($public_key_pem, '-----'))) {
            form_set_error(self::FORM_FIELD_PUBLIC_KEY, t('The public key is not enclosed properly.'));
          }
          elseif (openssl_pkey_get_public($public_key_pem_value) === FALSE) {
            form_set_error(self::FORM_FIELD_PUBLIC_KEY, t('The public key is not formatted properly.'));
          }
        }

        if ($private_key_pem !== '') {
          if ((!DataUriCreatorString::startsWith($private_key_pem, '-----BEGIN ')) || (!DataUriCreatorString::endsWith($private_key_pem, '-----'))) {
            form_set_error(self::FORM_FIELD_PRIVATE_KEY, t('The private key is not enclosed properly.'));
          }
          elseif (openssl_pkey_get_private($private_key_pem_value) === FALSE) {
            form_set_error(self::FORM_FIELD_PRIVATE_KEY, t('The private key is not formatted properly.'));
          }
        }
      }
    }

    // Check that a valid email-address was specified.
    $email = (isset($values[self::FORM_FIELD_EMAIL]) ? $values[self::FORM_FIELD_EMAIL] : NULL);
    if (isset($email) && ($email != '') && (!valid_email_address($email))) {
      form_set_error(self::FORM_FIELD_EMAIL, t('%recipient is an invalid e-mail address.', array('%recipient' => $email)));
    }

    // Check that any revocation-list data is formatted correctly.
    $revocations_value = &$values[self::FORM_FIELD_REVOCATIONS];
    if (empty($revocations_value)) {
      // Synchronized with form state.
      $revocations_value = '';
    }
    else {
      $revocations = drupal_json_decode($revocations_value);
      $is_valid = is_array($revocations);
      if ($is_valid) {
        foreach ($revocations as $assertion_id => $revoke_reason) {
          if ((!is_string($revoke_reason)) || empty($revoke_reason) || ((!is_string($assertion_id)) && (!is_int($assertion_id)))) {
            $is_valid = FALSE;
            break;
          }
        }
      }

      if ($is_valid) {
        // Attempt to format the JSON data to remain
        // constant after entity-export round-trips.
        $pretty_json = (empty($revocations) ? '{}' : _badge_depot_json_encode_pretty($revocations));
        if (is_string($pretty_json)) {
          // Synchronized with form state.
          $revocations_value = $pretty_json;
        }
      }
      else {
        form_set_error(self::FORM_FIELD_REVOCATIONS, t('Invalid data was specified for the revocation-list. Refer to the <a href="!url" target="_blank">OBI Assertions specification</a> for details.', array('!url' => url(BadgeDepotGlobalSettings::getObiAssertionsInfoUrl() . '#revoking-1'))));
      }
    }

    return parent::validateDefault($form, $form_state);
  }

  // Implements the submit handler that gets called once
  // the signature-creation request is completely validated.
  // NOTE: This method gets called only if the data that was
  //       submitted for the signature-creation request was valid.
  protected function submitTriggerCreateSignature($form, &$form_state) {
    // Create the signature data
    if (BadgeDepotOpenSsl::createKey($public_key_pem, $private_key_pem)) {
      // Upon success, display a success message.
      drupal_set_message(t('Signature certificate was generated.'));

      // Remember the newly generated signature
      // data to be used when the form is re-built.
      $this->createdPublicKeyPem = $public_key_pem;
      $this->createdPrivateKeyPem = $private_key_pem;
    }
    else {
      // Upon failure, display an error message.
      $errors = BadgeDepotOpenSsl::getErrors();
      $errors = (empty($errors) ? '' : ('<ul><li>' . implode('</li><li>', $errors) . '</li><ul>'));
      drupal_set_message(filter_xss(t('Sorry, but a new signature certificate could not be created.') . $errors), 'warning');
    }

    $form_state['rebuild'] = TRUE;
  }

  // Implements the AJAX-callback handler that gets called
  // once the signature-creation web-request completes.
  // NOTE: This method gets called for every AJAX signature-creation request,
  //       irrespective of success; however, this method is not called
  //       if the signature request was made without JavaScript support
  //       (in which case the full form from the rebuild step is returned).
  protected function ajaxTriggerCreateSignature($form, &$form_state) {
    // Select the region to be replaced by the AJAX response.
    $output = $form[self::AJAX_CONTENT_SIGNATURE];

    // If any validation errors occurred, only handle those.
    $errors = form_get_errors();
    if (!empty($errors)) {
      // Include default error handling (i.e., display messages).
      return $output;
    }

    // Render the AJAX content on the form.
    $commands = array();
    $commands[] = ajax_command_insert(NULL, drupal_render($output));

    // Ensure that any status messages get displayed.
    $commands[] = ajax_command_append(NULL, theme('status_messages'));

    return array('#type' => 'ajax', '#commands' => $commands);
  }
}
