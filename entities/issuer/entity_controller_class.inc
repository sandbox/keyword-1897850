<?php
/**
 * @file
 * Contains BadgeDepotIssuerEntityController.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Controller class for badge-issuer entities.
 */
class BadgeDepotIssuerEntityController extends BadgeDepotObiBaseEntityController {

  /**
   * Overrides the base method that builds a structured array
   * representing the entity's content for the supported view-modes.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    if (!is_array($content)) {
      $content = array();
    }

    // LRMI microdata for the badge Issuer-Organization HTML page
    $content['#prefix'] = &$lrmi_page_prefix;
    $content['#suffix'] = &$lrmi_page_suffix;
    $lrmi_page_prefix =
      '<div id="lrmiBadgeIssuer" itemscope="itemscope" itemtype="http://schema.org/WebPage/BadgeIssuer">' . PHP_EOL
      . '<meta itemprop="additionalType" content="http://schema.org/Organization" />' . PHP_EOL
      . '<meta itemprop="additionalType" content="http://starlitebadges.com/schemas/Badge/Issuer/" />' . PHP_EOL
      . '<meta itemprop="name" content="' . htmlspecialchars($entity->title) . '" />' . PHP_EOL
      . '<meta itemprop="url" content="' . htmlspecialchars(url($entity->url, array('absolute' => TRUE))) . '" />' . PHP_EOL;
    $lrmi_page_suffix = '</div>';

    $properties = NULL;
    $wrapper = NULL;
    $image_url = $this->buildEntityFieldContent($content, 'image_url', $properties, $wrapper, $entity, array('#title' => '', '#theme' => 'badge_depot_image', '#path' => $entity->image_url, '#alt' => check_plain($entity->title)));
    if (!empty($image_url)) {
      $lrmi_page_prefix .= '<meta itemprop="image" content="' . htmlspecialchars($entity->getImageUrl(FALSE)) . '" />' . PHP_EOL;
    }

    $content['title'] = array(
      '#theme_wrappers' => array('form_element'),
      '#title' => t(check_plain($entity->title)),
      '#markup' => l($entity->url, $entity->url, array('attributes' => array('target' => '_blank'))),
    );

    $description = $this->buildEntityFieldContent($content, 'description', $properties, $wrapper, $entity, array('#title' => '', '#theme' => NULL, '#markup' => check_plain($entity->description)));
    if (!empty($description)) {
      $lrmi_page_prefix .= '<meta itemprop="description" content="' . htmlspecialchars($entity->description) . '" />' . PHP_EOL;
    }

    if ($view_mode === 'full') {
      $this->buildEntityFieldContent($content, 'body', $properties, $wrapper, $entity, array('#title' => '', '#theme' => NULL, '#markup' => check_markup($entity->body, 'full_html', $langcode)));
      $this->buildEntityFieldContent($content, 'email', $properties, $wrapper, $entity, array('#title' => 'Contact person'));

      // Reference the public-key being hosted remotely, if specified
      $public_key_label = t('Public Key');
      $link_options = array('attributes' => array('target' => '_blank'));
      $public_key_url = $this->buildEntityFieldContent(
        $content, 'public_key_url', $properties, $wrapper, $entity,
        array(
          '#title' => $public_key_label,
          '#theme' => NULL,
          '#markup' => l(t('Remotely published digital signature of issuer organization (PEM)'), $entity->public_key_url, $link_options)
        )
      );
      if (empty($public_key_url)) {
        // Alternatively reference the public-key being hosted locally, if specified
        $public_key_path = $entity->getLocalPublicKeyPath();
        if (!empty($public_key_path)) {
          $url_options = array('absolute' => TRUE);
          $public_key_url = url($public_key_path, $url_options);
          $content['public_key_link_wrapper'] = array(
            '#title' => $public_key_label,
            '#theme_wrappers' => array('form_element'),
            'public_key_link' => array(
              '#type' => 'link',
              '#object' => $entity,
              '#title' => t('Published digital signature of issuer organization (PEM)'),
              '#href' => $public_key_url,
              '#options' => $link_options,
            )
          );
        }
      }

      $badge_items = array();
      $issuer_id = $entity->issuer_id;
      $manifests = BadgeManifestEntity::loadMultipleThroughFilter($issuer_id);
      if (isset($manifests)) {
        foreach ($manifests as $manifest) {
          if (($issuer_id === $manifest->issuer_id) && (!$manifest->is_dormant)) {
            $badge_markup = l(
              check_plain($manifest->title),
              url($manifest->getEntityViewPath(), array('absolute' => TRUE)),
              array(
                'attributes' => array(
                  'title' => check_plain($manifest->description),
                  'target' => '_blank',
                ),
              )
            );
            $badge_items[] = array(
              'data' => $badge_markup,
            );
          }
        }
      }

      if (!empty($badge_items)) {
        $content['badge_list'] = array(
          '#title' => t('Available Badges'),
          '#theme_wrappers' => array('form_element'),
          'content' => array('#theme' => 'item_list', '#items' => $badge_items, '#type' => 'ul'),
        );
      }

      // Reference the revocation-list being hosted remotely, if specified
      $revocations_label = t('Revocation List');
      $link_options = array('attributes' => array('target' => '_blank'));
      $revocations_url = $this->buildEntityFieldContent(
        $content, 'revocation_list_url', $properties, $wrapper, $entity,
        array(
          '#title' => $revocations_label,
          '#theme' => NULL,
          '#markup' => l(t('Remotely published list of badges revoked for some reason (JSON)'), $entity->revocation_list_url, $link_options)
        )
      );
      if (empty($revocations_url)) {
        // Alternatively reference the revocation-list being hosted locally
        $revocations_path = $entity->getLocalRevocationListPath();
        if (!empty($revocations_path)) {
          $url_options = array('absolute' => TRUE);
          $revocations_url = url($revocations_path, $url_options);
          $content['revocation_list_link_wrapper'] = array(
            '#title' => $revocations_label,
            '#theme_wrappers' => array('form_element'),
            'revocation_list_link' => array(
              '#type' => 'link',
              '#object' => $entity,
              '#title' => t('Published list of badges revoked for some reason (JSON)'),
              '#href' => $revocations_url,
              '#options' => $link_options,
            )
          );
        }
      }

      if ($entity->is_dormant) {
        $content['is_dormant'] = array(
          '#markup' => ('<p>' . t('Note: The organization is apparently no longer awarding badges using this identity.') . '</p>'),
        );
      }

      if (!$entity->is_published) {
        drupal_set_message(filter_xss(t('The data for "@title" is currently not published publicly. <a href="!url">Edit this badge-issuer</a> entity if you want to change this.', array('@title' => $entity->title, '!url' => url($entity->getEntityEditPath())))), 'warning');
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Overrides the base method that tweaks the extracted data to be exported
   * for an entity; it modifies the formatting of JSON types of data.
   */
  protected function exportCleanup(array &$vars, $entity) {
    // Converts the extra JSON data of the Revocation List to something that looks nicer for exporting
    $this->exportCleanupForJsonString('revocation_list_extras', $vars, $entity);

    parent::exportCleanup($vars, $entity);
  }

  /**
   * Overrides the base method that tweaks the deserialized data to be imported
   * into an entity; it modifies the formatting of JSON types of data.
   */
  protected function importCleanup(array &$vars, $export) {
    // Converts the extra JSON data of the Revocation List to the string format being used internally
    $this->importCleanupForJsonString('revocation_list_extras', $vars, $export);

    parent::importCleanup($vars, $export);
  }
}
