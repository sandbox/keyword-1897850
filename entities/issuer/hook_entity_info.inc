<?php
/**
 * @file
 * Contains additional hook_entity_info() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_entity_info data for the badge-issuer entity.
 */
function _badge_depot_append_entities_issuer_entity_info(&$info) {
  $info[BADGE_DEPOT_ENTITY_TABLE_ISSUER] = array(
    'label' => t('Badge Issuer Entity'),
    'plural label' => t('Badge Issuer Entities'),
    'description' => t('An entity type used as a base record for the registry of OBI badge-issuer organizations.'),
    'entity class' => 'BadgeIssuerEntity',
    // Based on EntityAPIControllerExportable that
    // is the default for exportable entities.
    'controller class' => 'BadgeDepotIssuerEntityController',
    'views controller class' => 'EntityDefaultViewsController',
    // Defaults to EntityDefaultMetadataController.
    'metadata controller class' => 'BadgeDepotEntityMetadataController',
    'base table' => BADGE_DEPOT_ENTITY_TABLE_ISSUER,
    'fieldable' => TRUE,
    'exportable' => TRUE,
    'entity keys' => array(
      'label' => 'title',
      'id' => 'issuer_id',
      'name' => BADGE_DEPOT_ENTITY_COLUMN_ISSUER_MACHINE_NAME,
      // TODO: The bundle value should be a column name that can override the
      //       table name where the entity data is stored, maybe the current
      //       'type' column? (line 110 of entity.inc with base Entity class).
      //'bundle' => BADGE_DEPOT_ENTITY_COLUMN_ISSUER_MACHINE_NAME,
      //'revision' => 'version_id',
    ),
    'access callback' => '_badge_depot_entity_access',
    // Use the class' label() implementation by default.
    'label callback' => '_badge_depot_entity_class_label',
    // Use the class' uri() implementation by default.
    'uri callback' => 'entity_class_uri',
    'bundles' => array(),
    //'bundle keys' => array(
    //  'bundle' => BADGE_DEPOT_ENTITY_COLUMN_ISSUER_MACHINE_NAME,
    //),
    'module' => BADGE_DEPOT_MODULE_NAME,
    'admin ui' => array(
      'path' => BADGE_DEPOT_ADMIN_ISSUER_PATH,
      'file' => 'entities/issuer/admin_ui.inc',
    ),
    'view modes' => array(
      'full' => array(
        'label' => t('Full View'),
        'custom settings' => FALSE,
      ),
      'teaser' => array(
        'label' => t('Teaser View'),
        'custom settings' => FALSE,
      ),
    ),
  );
}
