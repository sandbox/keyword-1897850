<?php
/**
 * @file
 * Contains additional hook_entity_property_info_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes the hook_entity_property_info_alter data for the badge-issuer entity.
 */
function _badge_depot_entities_issuer_entity_property_info_alter(&$info) {
  $properties = &$info[BADGE_DEPOT_ENTITY_TABLE_ISSUER]['properties'];

  $properties['issuer_id'] = array_merge(((array) ($properties['issuer_id'])), array(
    'description' => t('The unique identifier of this issuer-organization entry.'),
  ));

  $properties['issuer_machine_name'] = array_merge(((array) ($properties['issuer_machine_name'])), array(
    'label' => t('Persistent ID'),
    'description' => t('The persistent unique-identifier of this issuer-organization that can be used for exporting and importing data between machines.'),
  ));

  $properties['owner_user_id'] = array_merge(((array) ($properties['owner_user_id'])), array(
    'label' => t('Owner User ID'),
    'description' => t('A reference to the user that owns this issuer-organization for data-management purposes.'),
    'type' => 'user',
  ));

  $properties['owner_user_name'] = array_merge(((array) ($properties['owner_user_name'])), array(
    'label' => t('Owner User Name'),
    'description' => t('The name of the user that owns this issuer-organization for data-management purposes; used primarily for enhanced security when exporting/importing the data entity.'),
  ));

  $properties['type'] = array_merge(((array) ($properties['type'])), array(
    'description' => t('The entity type of this issuer-organization entry (used by entity sub-type bundles).'),
  ));

  $properties['title'] = array_merge(((array) ($properties['title'])), array(
    'label' => t('Name'),
    'description' => t('The name of this issuer organization (required; treated as non-markup plain text; the entity\'s "title" property).'),
  ));

  $properties['description'] = array_merge(((array) ($properties['description'])), array(
    'label' => t('Description (short)'),
    'description' => t('A short description of this issuer-organization entry (treated as non-markup plain text; the entity\'s "description" property).'),
  ));

  $properties['body'] = array_merge(((array) ($properties['body'])), array(
    'label' => t('Description (long)'),
    'description' => t('The detailed description of this issuer-organization entry (treated as HTML markup; the entity\'s "body" property).'),
  ));

  $properties['is_published'] = array_merge(((array) ($properties['is_published'])), array(
    'label' => t('Is Published?'),
    'description' => t('A boolean flag indicating whether the issuer-organization entry is publicly visible (the entity\'s "is_published" property).'),
    'type' => 'boolean',
  ));

  $properties['is_dormant'] = array_merge(((array) ($properties['is_dormant'])), array(
    'label' => t('Is Dormant?'),
    'description' => t('A boolean flag indicating that the issuer-organization is currently not issuing badges (the entity\'s "is_dormant" property).'),
    'type' => 'boolean',
  ));

  $properties['created'] = array_merge(((array) ($properties['created'])), array(
    'description' => t('The full date and time, as a timestamp, of when the issuer-organization entry was created.'),
    'type' => 'date',
  ));

  $properties['changed'] = array_merge(((array) ($properties['changed'])), array(
    'description' => t('The full date and time, as a timestamp, of when the issuer-organization entry was most recently saved.'),
    'type' => 'date',
  ));

  $properties['url'] = array_merge(((array) ($properties['url'])), array(
    'label' => t('Issuer URL'),
    'description' => t('The URL that links to the badge-issuing agent (required; the entity\'s "url" property). This will often be the origin URL of the issuer institution, although it may be a different URL.'),
    'type' => 'uri',
  ));

  $properties['email'] = array_merge(((array) ($properties['email'])), array(
    'label' => t('Issuer Email'),
    // Required since March 2013 for OBI specification 0.5.0 (initially
    // optional), but apparently again optional for version 1.0.
    'description' => t('The email address of a contact person associated with the badge-issuer organization (the entity\'s "email" property).'),
  ));

  $properties['image_url'] = array_merge(((array) ($properties['image_url'])), array(
    'label' => t('Image URL'),
    'description' => t('The URL that links to an image that represents the institution (can be a regular URL or <a href="!url" target="_blank" title="Tool to create a Data URI">Data URI</a>; the entity\'s "image_url" property).', array('!url' => url(DATA_URI_CREATOR_PAGE_PATH))),
    'type' => 'uri',
  ));

  $properties['public_key_url'] = array_merge(((array) ($properties['public_key_url'])), array(
    'label' => t('Public-Key URL'),
    'description' => t('An optional URL that links to a remote public-key for the badge-issuer organization (the entity\'s "public_key_url" property).'),
    'type' => 'uri',
  ));

  $properties['public_key_pem'] = array_merge(((array) ($properties['public_key_pem'])), array(
    'label' => t('Public-Key Data'),
    'description' => t('An optional Public Key for the badge-issuer organization to be hosted by the Badge Depot module and used by signed badges (e.g., PEM-formatted content of an OpenSSL .pem file; the entity\'s "public_key_pem" property).  Refer to <a href="!urlRFC1421" target="_blank" title="RFC 1421">RFC 1421</a> through <a href="!urlRFC1424" target="_blank" title="RFC 1424">1424</a> for formatting details.', array('!urlRFC1421' => 'http://www.ietf.org/rfc/rfc1421.txt', '!urlRFC1424' => 'http://www.ietf.org/rfc/rfc1424.txt')),
  ));

  $properties['private_key_pem'] = array_merge(((array) ($properties['private_key_pem'])), array(
    'label' => t('Private-Key Data'),
    'description' => t('An optional Private Key for the badge-issuer organization to be used for signed badges (e.g., PEM-formatted content of an OpenSSL .key file; the entity\'s "private_key_pem" property).'),
  ));

  $properties['revocation_list_url'] = array_merge(((array) ($properties['revocation_list_url'])), array(
    'label' => t('Remote Revocation List URL'),
    'description' => t('An optional endpoint URL that links to a remote Badge Revocation List for when the default service will not be used (the entity\'s "revocation_list_url" property).'),
    'type' => 'uri',
  ));

  $properties['revocation_list_extras'] = array_merge(((array) ($properties['revocation_list_extras'])), array(
    'label' => t('Revocation List Extras'),
    'description' => t('Any optional JSON data to be merged with the default Badge Revocation List of this issuer (the entity\'s "revocation_list_extras" property).'),
  ));
}
