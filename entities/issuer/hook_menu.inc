<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_menu info for the badge-issuer entity.
 */
function _badge_depot_append_entities_issuer_menu(&$items) {
  $entity_type = BADGE_DEPOT_ENTITY_TABLE_ISSUER;

  // Index in the URL path where the entity wildcard is specified.
  $entity_path_index = 2;

  $entity_wildcard_path = (BADGE_DEPOT_MAIN_ISSUER_PATH . '/%badge_depot_entities_issuer');

  // By naming convention, implies badge_depot_entities_issuer_load
  // is to be used for data loading.
  $items[$entity_wildcard_path] = array(
    'title callback' => '_badge_depot_entity_class_label',
    'title arguments' => array($entity_path_index, t('View badge-issuer entity') /* default title */),
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeIssuerEntity::requestView',
    'page arguments' => array($entity_path_index, 'full' /* view mode */),
    'access callback' => '_badge_depot_entity_access',
    'access arguments' => array('view' /* operation */, $entity_path_index, NULL /* account */, $entity_type),
  );

  $items[$entity_wildcard_path . '/public-key'] = array(
    'title' => 'Web service to provide a badge issuer\'s public-key (PEM)',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeIssuerEntity::invokeMethod',
    'page arguments' => array($entity_path_index, 'requestSignature'),
    'file' => 'core/delivery/pem.inc',
    'delivery callback' => 'badge_depot_deliver_pem',
    // Public access, but security is checked by the callback
    // so that more descriptive status codes can be returned.
    'access callback' => TRUE,
  );

  $items[$entity_wildcard_path . '/revocation-list'] = array(
    'title' => 'Web service to provide a badge issuer\'s revocation list (JSON)',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeIssuerEntity::invokeMethod',
    'page arguments' => array($entity_path_index, 'requestJsonRevocations'),
    'file' => 'core/delivery/json.inc',
    'delivery callback' => 'badge_depot_deliver_json',
    // Public access, but security is checked by the callback
    // so that more descriptive status codes can be returned.
    'access callback' => TRUE,
  );

  $items[$entity_wildcard_path . '/image'] = array(
    'title' => 'Badge-issuer image',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeIssuerEntity::invokeMethod',
    'page arguments' => array($entity_path_index, 'requestImage'),
    'file' => 'core/delivery/data_uri.inc',
    'delivery callback' => 'badge_depot_deliver_data_uri',
    'access callback' => '_badge_depot_entity_access',
    'access arguments' => array('view' /* operation */, $entity_path_index, NULL /* account */, $entity_type),
  );

  $items[$entity_wildcard_path . '/obi-data'] = array(
    'title' => 'Web service to provide a badge issuer\'s OBI data (JSON) being linked to by awarded badges',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeIssuerEntity::invokeMethod',
    'page arguments' => array($entity_path_index, 'requestJsonData'),
    'file' => 'core/delivery/json.inc',
    'delivery callback' => 'badge_depot_deliver_json',
    // Public access, but security is checked by the callback
    // so that more descriptive status codes can be returned.
    'access callback' => TRUE,
  );
}

/**
 * Adds the hook_menu_alter info for the badge-issuer entity.
 */
function _badge_depot_append_entities_issuer_menu_alter(&$items) {
  $items[BADGE_DEPOT_ADMIN_ISSUER_PATH]['description'] = 'Create and manage the affiliated organizations that can issue Open Badges.';
}
