<?php
/**
 * @file
 * Contains various theme functions used by data entities of this module.
 */

/**
 * Implements the badge_depot_entity_field_data theme-function
 * to be used as a "#theme" value for elements in render arrays.
 */
function _badge_depot_theme_entity_field_data($variables) {
  // extract($variables);
  $element = $variables['element'];
  $data_wrapper = $element['#data_wrapper'];

  // Output safe HTML.
  return $data_wrapper->value(array('sanitize' => TRUE));
}

/**
 * Implements the badge_depot_image theme-function to be used as a
 * "#theme" value for elements in render arrays.  This theme is almost
 * identical to Drupal's built-in "image" theme, except that it also
 * allows a Data URI to be specified as the image path.  Refer to
 * the theme_image function in Drupal's "theme.inc" file, which is
 * the original souce-code on which this implementation is based.
 */
function _badge_depot_theme_image($variables) {
  $attributes = $variables['attributes'];
  $attributes['src'] = DataUriCreator::getDataUriOrQualifiedUrl($variables['path'], TRUE /* $is_stream_allowed */);
  foreach (array('width', 'height', 'alt', 'title') as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}
