<?php
/**
 * @file
 * Contains BadgeDepotObiBaseEntity.
 */

/**
 * Base class for OBI entities that can have a Drupal user that own them.
 */
class BadgeDepotObiBaseEntity extends BadgeDepotExportableEntity {

  /**
   * Override this in a subclass to return the base URL path for all instances
   * of this entity type (that is not for a single specific entity instance).
   */
  protected static function getEntityMainRootPath() {
    return NULL;
  }

  /**
   * Override this in a subclass to return the base URL path for administrative
   * purposes of this entity type (that is not for a single entity instance).
   */
  protected static function getEntityAdminRootPath() {
    return NULL;
  }

  // Define fields that map to specific database columns of this entity
  public $owner_user_id;
  public $owner_user_name;
  //public $version_id;
  public $type;
  public $is_published;

  /**
   * Overrides isAccessibleByUser().
   *
   * Determines whether this entity can be accessed by the specified user.
   */
  public function isAccessibleByUser($operation, $account = NULL) {
    if (($operation === 'view') && ($this->is_published)) {
      return TRUE;
    }

    return parent::isAccessibleByUser($operation, $account);
  }

  /**
   * Overrides the base method that permanently saves the entity.
   */
  public function save() {
    if (!isset($this->owner_user_id)) {
      $this->owner_user_id = 0;
    }

    // Keep the user-name and user-ID fields synchronized
    $user = NULL;
    if ($this->owner_user_id !== 0) {
      $users = entity_load('user', array($this->owner_user_id));
      $user = (isset($users[$this->owner_user_id]) ? $users[$this->owner_user_id] : NULL);
    }

    $this->owner_user_name = (isset($user->name) ? $user->name : '');

    return parent::save();
  }

  /**
   * Gets the base path where the Open Badge entity data can be viewed.
   *
   * @return
   *   The base path where the OBI data is being served.
   */
  public function getEntityViewPath() {
    if ($this->hasMachineName()) {
      $url = static::getEntityMainRootPath();
      if (!empty($url)) {
        return ($url . '/' . $this->getMachineName());
      }
    }

    return NULL;
  }

  /**
   * Gets the base path where the Open Badge entity data can be edited.
   *
   * @return
   *   The base path where the administrative GUI for the entity data is hosted.
   */
  public function getEntityEditPath() {
    if ($this->hasMachineName()) {
      $url = static::getEntityAdminRootPath();
      if (!empty($url)) {
        return ($url . '/manage/' . $this->getMachineName());
      }
    }

    return NULL;
  }

  /**
   * Overrides the base method that specifies the default URI for this entity.
   */
  protected function defaultUri() {
    $url = static::getEntityMainRootPath();
    if (!empty($url)) {
      return array('path' => ($url . '/' . $this->identifier()));
    }

    return array();
  }

  /**
   * Determines whether this entity is owned by the specified user.
   */
  public function isOwnerUser($account = NULL) {
    global $user;
    $account = (isset($account) ? $account : $user);
    $is_owner = (($this->owner_user_id === $account->uid) && (empty($this->owner_user_name) || _badge_depot_string_equals($this->owner_user_name, $account->name, TRUE)));
    return $is_owner;
  }
}
