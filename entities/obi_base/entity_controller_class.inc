<?php
/**
 * @file
 * Contains BadgeDepotObiBaseEntityController.
 */

/**
 * Base controller-class for OBI entities that can have a Drupal user that own them.
 */
class BadgeDepotObiBaseEntityController extends BadgeDepotExportableEntityController {

  /**
   * Overrides the base method that tweaks the extracted data to be exported
   * for an entity; it filters out more variables that should not be exported.
   */
  protected function exportCleanup(array &$vars, $entity) {
    // Remove the owner user's ID, since the ID may be
    // different on the importing Drupal installation
    unset($vars['owner_user_id']);

    parent::exportCleanup($vars, $entity);
  }

  /**
   * Overrides the base method that tweaks the deserialized data to be imported
   * into an entity; it adds or modifies variables that should also be imported.
   */
  protected function importCleanup(array &$vars, $export) {
    // Attempt to locate the owner user's ID
    $owner_user_id = 0;
    $owner_user_name = (isset($vars['owner_user_name']) ? $vars['owner_user_name'] : NULL);
    $user = (empty($owner_user_name) ? NULL : user_load_by_name($owner_user_name));
    if (isset($user->uid)) {
      $owner_user_id = $user->uid;
    }

    $vars['owner_user_id'] = $owner_user_id;

    parent::importCleanup($vars, $export);
  }
}
