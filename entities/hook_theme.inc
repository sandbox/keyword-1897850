<?php
/**
 * @file
 * Contains additional hook_theme() information.
 */

/**
 * Adds the hook_theme data for data entities of this module in general.
 */
function _badge_depot_append_entities_theme(&$themes, &$existing, $type, $theme, $path) {
  $themes['badge_depot_entity_field_data'] = array(
    'render element' => 'element',
    'file' => 'entities/theme_callback.inc',
    'function' => '_badge_depot_theme_entity_field_data',
  );

  $themes['badge_depot_image'] = array(
    'file' => 'entities/theme_callback.inc',
    'function' => '_badge_depot_theme_image',
    // Same as for the 'image' entry of drupal_common_theme in "common.inc".
    'variables' => array('path' => NULL, 'width' => NULL, 'height' => NULL, 'alt' => '', 'title' => NULL, 'attributes' => array()),
  );
}
