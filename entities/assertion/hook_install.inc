<?php
/**
 * @file
 * Contains additional hook_schema() information for installation.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_schema database info for the badge-assertion entity.
 */
function _badge_depot_append_entities_assertion_schema(&$schema) {
  $schema[BADGE_DEPOT_ENTITY_TABLE_ASSERTION] = array(
    'description' => t('The base record for the registry of OBI badge assertions (i.e., awarded badges).'),
    'fields' => array(
      'assertion_id' => array(
        'description' => t('The unique identifier of this badge assertion.'),
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'assertion_machine_name' => array(
        'description' => t('Corresponding with the OBI "uid" property, this is the persistent unique-identifier of the awarded badge, which is a machine-readable name in Drupal parlance.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      //'version_id' => array(
      //  'description' => t('The current version identifier of the assertion data.'),
      //  'type' => 'int',
      //  'not null' => TRUE,
      //  'unsigned' => TRUE,
      //  'default' => 0,
      //),
      'owner_user_id' => array(
        'description' => t('A reference to the user that owns this assertion for data-management purposes, which should not be confused with the user that the badge is awarded to.'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'owner_user_name' => array(
        'description' => t('The name of the user that owns this assertion for data-management purposes; used primarily for enhanced security when exporting/importing the data entity. This is generally not the same as user that received the badge.'),
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
      ),
      'manifest_id' => array(
        'description' => t('A reference to the badge definition (i.e., badge-class manifest) that describes general properties of this badge award.'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'manifest_machine_name' => array(
        'description' => t('The machine-readable name of the badge-class manifest that defines this badge award.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => t('The entity type of this badge assertion (used by entity sub-type bundles).'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'revoke_reason' => array(
        'description' => t('If the badge has been revoked, specify the reason of revocation; otherwise leave blank.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => t('The name of this badge (treated as non-markup plain text).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'notes' => array(
        'description' => t('Optional notes for internal use by the badge issuer (treated as HTML markup).'),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'is_published' => array(
        'description' => t('A flag indicating whether the badge assertion is publicly visible.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => t('The Unix timestamp of when the badge was issued (i.e., when the badge assertion was created).'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => t('The Unix timestamp of when the badge assertion was most recently saved.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'expires' => array(
        'description' => t('The Unix timestamp of when the badge expires.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'image_url' => array(
        'description' => t('An optional URL that links to the baked badge image containing the hosted assertion URL; it should have a square size (can be a regular URL or <a href="!url" target="_blank" title="Tool to create a Data URI">Data URI</a>; the entity\'s "image_url" property). If not specified, a dynamic URL can be provided using the Badge Baker Service.', array('!url' => url(DATA_URI_CREATOR_PAGE_PATH))),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'evidence_url' => array(
        'description' => t('An optional URL that links to the badge\'s evidence page.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'recipient_email' => array(
        'description' => t('The email address of the user that receives the badge award, which may be omitted for security, privacy or other reasons, but only after the badge instance had been created.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'recipient_json' => array(
        'description' => t('The recipient of the badge achievement (i.e., the JSON for the OBI "IdentityObject" value).'),
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('assertion_id'),
    'unique keys' => array(
      'assertion_key' => array('assertion_id'),
      //'assertion_version_key' => array('assertion_id', 'version_id'),
    ),
    'indexes' => array(
      'changed_index' => array('changed'),
      'created_index' => array('created'),
      'expires_index' => array('expires'),
      'machine_index' => array('assertion_machine_name'),
      'owner_user_index' => array('owner_user_id'),
      'manifest_id_index' => array('manifest_id'),
      'manifest_machine_index' => array('manifest_machine_name'),
      'revoke_index' => array('revoke_reason'),
    ),
  );
}
