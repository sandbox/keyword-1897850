<?php
/**
 * @file
 * Contains additional hook_permission() information.
 */

/**
 * Adds the hook_permission data for the badge-assertion entity.
 */
function _badge_depot_append_entities_assertion_permission(&$permissions) {
  $permissions['administer badge assertion entities'] = array(
    'title' => t('Administer badge-assertion data'),
    'description' => t('Provides full access to all administrative tasks related to badge_assertion entities.'),
    'restrict access' => TRUE,
  );

  $permissions['create badge assertion entities'] = array(
    'title' => t('Create badge-assertion data'),
    'description' => t('Allows users to create badge_assertion entities.'),
    'restrict access' => TRUE,
  );

  $permissions['delete badge assertion entities'] = array(
    'title' => t('Delete badge-assertion data'),
    'description' => t('Allows users to delete badge_assertion entities.'),
    'restrict access' => TRUE,
  );

  $permissions['update badge assertion entities'] = array(
    'title' => t('Update badge-assertion data'),
    'description' => t('Allows users to update badge_assertion entities.'),
    'restrict access' => TRUE,
  );

  $permissions['view badge assertion entities'] = array(
    'title' => t('View badge-assertion data'),
    'description' => t('Allows users to view badge_assertion entities.'),
  );
}
