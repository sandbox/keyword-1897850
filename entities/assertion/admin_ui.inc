<?php
/**
 * @file
 * Contains BadgeAssertionAdminPage.
 */

/**
 * Implements the administrative user-interface functions for the assertion entity.
 */
class BadgeAssertionAdminPage extends BadgeDepotEntityAdminPage {

  // Define the identifiers of the form fields that contain submission data.
  const FORM_CONTENT_EVIDENCE = 'evidence_page';
  const FORM_CONTENT_IMAGE = 'assertion_image';
  const FORM_CONTENT_RECIPIENT = 'recipient';
  const FORM_FIELD_MANIFEST_ID = 'manifest_id';
  const FORM_FIELD_EXPIRES = 'expires';
  const FORM_FIELD_IMAGE_URL = 'image_url';
  const FORM_FIELD_EVIDENCE_URL = 'evidence_url';
  const FORM_FIELD_RECIPIENT_EMAIL = 'recipient_email';
  const FORM_FIELD_RECIPIENT_JSON = 'recipient_json';

  /**
   * Overrides the default build-handler to implement the administrative
   * user-interface form for the badge-assertion entity.
   */
  public function buildDefault($form, &$form_state) {
    $this->getBuildFormExtraArgs($entity, $op);
    $is_new_entity = ($op === 'add');
    $entity_type = $entity->entityType();

    // Including super-user access.
    $is_admin = user_access('administer ' . $entity_type . ' entities');

    if ($op === 'clone') {
      // Modify the title of the clone.
      $entity->title .= ' (cloned)';

      // Create a new machine-readable ID for the clone.
      $entity->setMachineName();
    }

    $properties = NULL;
    $this->buildEntityFieldAdmin($form, 'title', $properties, $entity, array('#title' => t('Name of Badge Award'), '#maxlength' => 255));

    $form[self::FORM_CONTENT_RECIPIENT] = array(
      '#type' => 'fieldset',
      '#title' => t('Badge Recipient'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $this->buildEntityFieldAdmin($form[self::FORM_CONTENT_RECIPIENT], self::FORM_FIELD_RECIPIENT_EMAIL, $properties, $entity, array('#title' => t('Email'), '#maxlength' => 255));
    if ($is_new_entity) {
      $form[self::FORM_CONTENT_RECIPIENT][self::FORM_FIELD_RECIPIENT_EMAIL]['#description'] .= ' ' . t('Whether the email address is saved after creation is determined by the Badge Depot <a href="!url" target="_blank">configuration settings</a>.', array('!url' => url(BADGE_DEPOT_CONFIG_PATH_PREFIX . 'badge-depot')));
      $form[self::FORM_CONTENT_RECIPIENT]['#description'] .= ' ' . t('To award a badge you must specify either the recipient\'s email address or create the identity JSON manually.');
    }

    $this->buildEntityFieldAdmin($form[self::FORM_CONTENT_RECIPIENT], self::FORM_FIELD_RECIPIENT_JSON, $properties, $entity, array('#title' => t('Identity Object JSON'), '#type' => 'textarea', '#rows' => 7));
    $form[self::FORM_CONTENT_RECIPIENT][self::FORM_FIELD_RECIPIENT_JSON]['#description'] .= t(
      ' Refer to the <a href="!url" target="_blank">OBI Assertions specification</a> for detais on the data format.' .
      '<br /><b>Hint</b>: The JSON identity data can be recreated automatically by clearing this field, entering a recipient email-address, and then saving the entity.',
      array(
        '!url' => url(BadgeDepotGlobalSettings::getObiAssertionsInfoUrl() . '#identityobject'),
      )
    );

    $manifest_select = array();
    $manifests = BadgeManifestEntity::loadMultiple();
    if (isset($manifests)) {
      foreach ($manifests as $manifest_item) {
        if ($is_admin || $manifest_item->isOwnerUser()) {
          $is_dormant = $manifest_item->is_dormant;
          if ((!$is_new_entity) || (!$is_dormant)) {
            $title = $manifest_item->title;
            if ($is_dormant) {
              $title .= ' (dormant)';
            }

            $manifest_select[$manifest_item->manifest_id] = $title;
          }
        }
      }
    }

    $this->buildEntityFieldAdmin($form, self::FORM_FIELD_MANIFEST_ID, $properties, $entity, array('#title' => t('Badge Manifest'), '#required' => TRUE, '#type' => 'select', '#options' => $manifest_select));
    if (empty($manifest_select)) {
      drupal_set_message(
        t(
          'To award a badge and create an assertion for it, your user must be associated with at least one <a href="!url">badge-class manifest</a>.',
          array('!url' => filter_xss(url(BADGE_DEPOT_ADMIN_MANIFEST_PATH)))
        ),
        'warning'
      );
    }
    elseif (count($manifest_select) === 1) {
      // Select the only organization that the user belongs to (the first key).
      reset($manifest_select);
      $form[self::FORM_FIELD_MANIFEST_ID]['#default_value'] = key($manifest_select);
    }

    $owner_user_id = $entity->owner_user_id;
    if ($is_admin) {
      // Administrators have full control over changing
      // the current owner of this badge entity.
      $users = entity_load('user');
      $user_select = array();
      foreach ($users as $user_item) {
        $user_select[$user_item->uid] = $user_item->name;
      }

      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'select', '#options' => $user_select));
    }
    elseif (!empty($owner_user_id)) {
      // Regular users can only see who is the owner of this
      // badge entity, but only when it currently has an owner.
      $users = entity_load('user', array($owner_user_id));
      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'item', '#markup' => theme('username', array('account' => $users[$owner_user_id]))));
    }

    $expires_value = $this->buildEntityFieldAdmin($form, self::FORM_FIELD_EXPIRES, $properties, $entity, array('#title' => 'Expiration Date'));
    if (empty($expires_value)) {
      $form[self::FORM_FIELD_EXPIRES]['#default_value'] = '';
    }
    elseif (is_numeric($expires_value)) {
      // ISO 8601 date (e.g., "2009-02-13T17:31:30-06:00").
      $form[self::FORM_FIELD_EXPIRES]['#default_value'] = format_date($expires_value, 'custom', 'c');
    }

    $this->buildEntityFieldAdmin($form, 'revoke_reason', $properties, $entity, array('#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'is_published', $properties, $entity);
    $form['is_published']['#description'] .= t('<br /><b>Warning</b>: Once a badge is awarded and published, it generally should not be unpublished afterwards; instead, if needed, revoke the badge by providing a specific reason in the appropriate field.');
    if ($is_new_entity) {
      $form['is_published']['#description'] .= t('<br /><b>Note</b>: To specify a custom Baked Badge Image, a two-step process is needed, since the Assertion URL required for baking becomes available only after creating the current assertion instance; for the first step you may want to not publish the assertion, and only do so after the custom images is specified by updating the assertion during the second step.');
    }
    else {
      $form[self::FORM_CONTENT_IMAGE] = array('#type' => 'fieldset', '#title' => t('Baked Badge Image'), '#collapsible' => TRUE, '#collapsed' => FALSE);
      $this->buildEntityFieldAdmin($form[self::FORM_CONTENT_IMAGE], self::FORM_FIELD_IMAGE_URL, $properties, $entity, array('#maxlength' => 350208));
      $form[self::FORM_CONTENT_IMAGE][self::FORM_FIELD_IMAGE_URL]['#description'] .= t('<br /><b>Warning</b>: If specified, the badge image <em>must</em> be baked with the !assertionRef of the current badge instance.', array('!assertionRef' => t(($is_new_entity ? '<em>@text</em>' : '<a href="!url" target="_blank">@text</a>'), array('@text' => t('Assertion URL'), '!url' => $entity->getJsonDataUrl()))));
      $form[self::FORM_CONTENT_IMAGE]['activity_launch'] = array(
        '#type' => 'button',
        '#value' => t('View Image'),
        '#attributes' => array(
          // Override client-side JavaScript action and disable form submission.
          'onclick' => $this->buildLaunchJavaScript(
            self::FORM_FIELD_IMAGE_URL,
            'jQuery("#assertion_image_preview").each(function() { this.src = url; }); jQuery("#assertion_image_container").show();',
            t('The image URL is not specified.')
          ),
        ),
        '#suffix' => '<span id="assertion_image_container" style="display:none;"><br /><br /><img id="assertion_image_preview" /></span>',
      );
    }

    $form[self::FORM_CONTENT_EVIDENCE] = array(
      '#type' => 'fieldset',
      '#title' => t('Evidence Page'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $this->buildEntityFieldAdmin($form[self::FORM_CONTENT_EVIDENCE], self::FORM_FIELD_EVIDENCE_URL, $properties, $entity, array('#maxlength' => 255));
    $form[self::FORM_CONTENT_EVIDENCE]['evidence_launch'] = $this->buildLaunchButton(self::FORM_FIELD_EVIDENCE_URL);

    $this->buildEntityFieldAdmin($form, 'notes', $properties, $entity, array('#type' => 'textarea'));

    field_attach_form($entity_type, $entity, $form, $form_state);

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t($is_new_entity ? 'Create badge award' : 'Save data'),
      '#weight' => 50,
    );

    return parent::buildDefault($form, $form_state);
  }

  /**
   * Overrides the default validation-handler of the administrative form.
   */
  public function validateDefault($form, &$form_state) {
    $values = &$form_state['values'];

    // Check that any expiry data is set correctly.
    $expire_value = &$values[self::FORM_FIELD_EXPIRES];
    $expire_timestamp = BadgeAssertionEntity::parseTimestamp($expire_value);
    if (isset($expire_timestamp)) {
      // Synchronized with form state.
      $expire_value = $expire_timestamp;
    }
    else {
      form_set_error(self::FORM_FIELD_EXPIRES, t('Unknown expiry date (%expires).', array('%expires' => $expire_value)));
    }

    // Check that a valid email-address was specified.
    $email = $values[self::FORM_FIELD_RECIPIENT_EMAIL];
    $recipient_json_value = &$values[self::FORM_FIELD_RECIPIENT_JSON];
    $recipient_json = trim($recipient_json_value);
    if (($email != '') && (!valid_email_address($email))) {
      form_set_error(self::FORM_FIELD_RECIPIENT_EMAIL, t('%recipient is an invalid e-mail address.', array('%recipient' => $email)));
    }
    else {
      $email = trim($email);
      if (empty($email) && empty($recipient_json)) {
        form_set_error(self::FORM_FIELD_RECIPIENT_EMAIL, t('You must specify either the recipient\'s email address or create the identity JSON manually.'));
      }
    }

    // Verify any recipient JSON data.
    if (empty($recipient_json)) {
      // Synchronized with form state.
      $recipient_json_value = '';
    }
    else {
      $recipient_json = drupal_json_decode($recipient_json);
      $is_valid_json = is_array($recipient_json);
      $validation_error = '';
      if ($is_valid_json) {
        if (empty($recipient_json['identity']) || (!is_string($recipient_json['identity']))) {
          $validation_error .= ' ' . t('The <em>identity</em> value specified in the identity JSON data must be a valid string.');
        }

        if (empty($recipient_json['type']) || (!is_string($recipient_json['type']))) {
          $validation_error .= ' ' . t('The <em>type</em> specified in the identity JSON data must be a valid string.');
        }

        if ((!isset($recipient_json['hashed'])) || (!is_bool($recipient_json['hashed']))) {
          $validation_error .= ' ' . t('The <em>hashed</em> value specified in the identity JSON data must be a valid boolean.');
        }

        if (isset($recipient_json['salt']) && (!is_string($recipient_json['salt']))) {
          $validation_error .= ' ' . t('When the <em>salt</em> is specified in the identity JSON data, the value must be a valid string.');
        }
      }

      if ($is_valid_json && empty($validation_error)) {
        // Attempt to format the JSON data to remain
        // constant after entity-export round-trips.
        $pretty_json = (empty($recipient_json) ? '{}' : _badge_depot_json_encode_pretty($recipient_json));
        if (is_string($pretty_json)) {
          // Synchronized with form state.
          $recipient_json_value = $pretty_json;
        }
      }
      else {
        if (empty($validation_error)) {
          $validation_error = t('Invalid data was specified for the identity JSON data.');
        }

        form_set_error(self::FORM_FIELD_RECIPIENT_JSON, ($validation_error . ' ' . t('Refer to the <a href="!url" target="_blank">OBI Assertions specification</a> for details.', array('!url' => url(BadgeDepotGlobalSettings::getObiAssertionsInfoUrl() . '#identityobject')))));
      }
    }

    // TODO: If a badge image is specified, verify that it is baked with
    //       the current entity's assertion URL (similar to the logic in
    //       BadgeManifestAdminPage's validateDefault method).

    // Check that all URL fields appear to be actual URLs.
    $url_fields = array(
      self::FORM_FIELD_EVIDENCE_URL => array('isAbsoluteUrl' => FALSE, 'formContainer' => &$form[self::FORM_CONTENT_EVIDENCE]),
    );
    if (isset($form[self::FORM_CONTENT_IMAGE])) {
      $url_fields[self::FORM_FIELD_IMAGE_URL] = array('isAbsoluteUrl' => FALSE, 'formContainer' => &$form[self::FORM_CONTENT_IMAGE]);
    }
    foreach ($url_fields as $url_field_name => $field_data) {
      $url = $values[$url_field_name];
      $form_field = &$field_data['formContainer'][$url_field_name];
      $is_required = (isset($form_field['#required']) ? $form_field['#required'] : FALSE);
      if ((!isset($url)) || (($is_required || ($url != '')) && (!valid_url($url, $field_data['isAbsoluteUrl'])))) {
        form_set_error($url_field_name, t('The %label value is invalid (%url).', array('%url' => $url, '%label' => $form_field['#title'])));
      }
    }

    return parent::validateDefault($form, $form_state);
  }
}
