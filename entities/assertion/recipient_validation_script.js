/**
 * @file
 * Contains the client-side browser-code to verify a badge recipient.
 */

/**
 * Performs an AJAX call to the server to verify a badge recipient.
 */
function ValidateBadgeAssertionRecipient() {
  var validationSalt = jQuery('#RecipientValidationSalt').val();
  var validationHash = jQuery('#RecipientValidationHash').val();
  var validationEmail = jQuery('#RecipientValidationEmail').val();
  EnableBadgeValidationElements(false);
  SetBadgeValidationStatus('Checking...');
  var queryString = ('email=' + encodeURIComponent(validationEmail) +
    '&recipient=' + encodeURIComponent(validationHash) +
    '&salt=' + encodeURIComponent(validationSalt));
  var proxyURL = (Drupal.settings.badge_depot.obi_recipient_validation_url + '?' + queryString);
  jQuery.ajax({
    dataType: 'json',
    url: proxyURL,
    // Success handler.
    success: function (data) {
      EnableBadgeValidationElements(true);
      if (data.match) {
        SetBadgeValidationStatus('Yes, the badge was awarded to ' + validationEmail + '!');
      }
      else {
        SetBadgeValidationStatus('No, it looks like the badge was awarded to someone else!');
      }
    },
    // Error handler.
    error: function (jqXHR, textStatus, errorThrown) {
      EnableBadgeValidationElements(true);
      SetBadgeValidationStatus('Problem encountered while validating recipient (' + textStatus + ').');
    }
  });
}

/**
 * Updates the dynamic HTML with the result after checking a badge recipient.
 */
function SetBadgeValidationStatus(message) {
  if ((typeof message !== 'undefined') && (message !== null) && (message !== '')) {
    jQuery('#RecipientValidationResultText').text(message).show();
  }
  else {
    jQuery('#RecipientValidationResultText').hide();
  }
}

/**
 * Enables or disables the GUI when checking a badge recipient.
 */
function EnableBadgeValidationElements(isEnabled) {
  jQuery('#RecipientValidationButton').each(function () { this.disabled = !isEnabled; });
  jQuery('#RecipientValidationEmail').each(function () { this.disabled = !isEnabled; });
}
