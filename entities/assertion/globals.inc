<?php
/**
 * @file
 * Provides global functions and other definitions, within the context of this
 * entity, which must always be available when this module is installed, such
 * as for various callback functions.
 */

// Include all dependency files.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';
require_once dirname(__DIR__) . '/globals.inc';

// Specify the database table name that is used for storing the assertion entities.
define('BADGE_DEPOT_ENTITY_TABLE_ASSERTION', 'badge_assertion');

// Specify the database column name that is used for storing the badge-assertion's machine-readable name.
define('BADGE_DEPOT_ENTITY_COLUMN_ASSERTION_MACHINE_NAME', 'assertion_machine_name');

// Specify the URLs for accessing data related to badge-assertion entities.
define('BADGE_DEPOT_MAIN_ASSERTION_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'assertion'));
define('BADGE_DEPOT_ADMIN_ASSERTION_PATH', (BADGE_DEPOT_ADMIN_PATH_PREFIX . 'assertion'));

// Implements a general loader for badge-assertion entities from URL path parameters.
// NOTE: Based on naming convention that Drupal follows for wildcard segments
//       in URL paths, this function will automatically be called to load data
//       for path keys that contain %badge_depot_entities_assertion as a segment.
function badge_depot_entities_assertion_load($assertion_id = NULL) {
  return BadgeAssertionEntity::autoLoad($assertion_id);
}
