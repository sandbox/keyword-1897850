<?php
/**
 * @file
 * Contains BadgeDepotAssertionEntityController.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Controller class for badge-assertion entities.
 */
class BadgeDepotAssertionEntityController extends BadgeDepotObiBaseEntityController {

  /**
   * Overrides the base method that builds a structured array
   * representing the entity's content for the supported view-modes.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    if (!is_array($content)) {
      $content = array();
    }

    $properties = NULL;
    $wrapper = NULL;
    $manifest = $entity->getManifest();

    // LRMI microdata for the badge Assertion HTML page
    $content['#prefix'] = &$lrmi_page_prefix;
    $content['#suffix'] = &$lrmi_page_suffix;
    $lrmi_page_prefix =
      '<div id="lrmiBadgeAssertion" itemscope="itemscope" itemtype="http://schema.org/WebPage/BadgeAssertion">' . PHP_EOL
      . '<meta itemprop="additionalType" content="http://starlitebadges.com/schemas/Badge/Assertion/" />' . PHP_EOL
      . '<meta itemprop="additionalType" content="http://www.lrmi.net/the-specification" />' . PHP_EOL
      . '<meta itemprop="name" content="' . htmlspecialchars($entity->title) . '" />' . PHP_EOL;
    $lrmi_page_suffix = '</div>';

    $image_url = $entity->getImageUrl(FALSE, FALSE);
    if (!empty($image_url)) {
      $lrmi_page_prefix .= '<meta itemprop="image" content="' . htmlspecialchars($image_url) . '" />' . PHP_EOL;
    }

    $image_url = $entity->getImageUrl(TRUE, FALSE);
    if (empty($image_url) && isset($manifest)) {
      $image_url = $manifest->getImageUrl();
    }
    if (!empty($image_url)) {
      // Do not specify "#alt" text, since this could look like a second heading
      // in Firefox if something goes wrong with a dynamic badge-baker image.
      $content['image_url'] = array(
        '#theme_wrappers' => array('form_element'),
        '#title' => '',
        '#theme' => 'badge_depot_image',
        '#width' => 90,
        '#path' => $image_url,
      );
    }

    $this->buildEntityFieldContent($content, 'title', $properties, $wrapper, $entity, array('#title' => ''));
    $link_options = array('attributes' => array('target' => '_blank'));
    if ($view_mode === 'full') {
      $entity_type = $entity->entityType();

      // Includes super-user access.
      $is_internal_user = ($entity->isOwnerUser() || user_access('administer ' . $entity_type . ' entities') || user_access('update ' . $entity_type . ' entities'));
      if ($is_internal_user) {
        if (!empty($entity->recipient_email)) {
          $this->buildEntityFieldContent($content, 'recipient_email', $properties, $wrapper, $entity);
        }
      }

      if (isset($manifest)) {
        $manifest_markup = l($manifest->title, $manifest->getEntityViewPath(), $link_options);
        $content['manifest'] = array(
          '#theme_wrappers' => array('form_element'),
          '#theme' => NULL,
          '#object' => $entity,
          '#data' => $manifest,
          '#title' => t('Badge Definition'),
          '#markup' => $manifest_markup,
        );
      }

      $issuer = $entity->getIssuer();
      if (isset($issuer)) {
        // LRMI microdata for the badge-issuer organization name; expected
        // within an itemscope of itemtype "http://schema.org/Organization".
        $issuer_markup = '<meta itemprop="name" content="' . htmlspecialchars($issuer->title) . '" />';
        $issuer_markup .= l(
          check_plain($issuer->title),
          $issuer->getEntityViewPath(),
          array(
            // LRMI microdata for the badge-issuer organization URL; expected
            // within an itemscope of itemtype "http://schema.org/Organization".
            'attributes' => array(
              'target' => '_blank',
              'itemprop' => 'url',
              'rel' => 'publisher',
            ),
          )
        );
        if (!$issuer->isAccessibleByUser('view')) {
          $issuer_markup .= ' ' . t('(access restricted)');
        }
        elseif (!$issuer->is_published) {
          $issuer_markup .= ' ' . t('(no public access)');
          if ($entity->is_published) {
            drupal_set_message(
              t(
                'The <a href="!viewUrl">link to the Issuer</a> is currently not published publicly. <a href="!editUrl">Edit the badge-issuer</a> entity to change this.',
                array(
                  '@title' => check_plain($issuer->title),
                  '!editUrl' => url($issuer->getEntityEditPath()),
                  '!viewUrl' => url($issuer->getEntityViewPath()),
                )
              ),
              'warning'
            );
          }
        }
      }
      else {
        $issuer_markup = t('The issuer organization for this badge could not be found!');
      }

      $content['issuer'] = array(
        '#theme_wrappers' => array('form_element'),
        '#theme' => NULL,
        '#object' => $entity,
        '#data' => $issuer,
        '#title' => t('Issuer'),
        '#markup' => $issuer_markup,
        // LRMI microdata for the badge-issuer organization.
        '#prefix' => '<div id="lrmiBadgeIssuer" itemprop="publisher" itemscope="itemscope" itemtype="http://schema.org/Organization">',
        '#suffix' => '</div>',
      );

      $this->buildEntityFieldContent($content, 'created', $properties, $wrapper, $entity, array('#title' => 'Issued On', '#theme' => NULL, '#markup' => check_plain(format_date($entity->created, 'long'))));
      if (!empty($entity->expires)) {
        $this->buildEntityFieldContent($content, 'expires', $properties, $wrapper, $entity, array('#title' => 'Expiration Date', '#theme' => NULL, '#markup' => check_plain(format_date($entity->expires, 'long'))));
        //$this->buildEntityFieldContent($content, 'expires', $properties, $wrapper, $entity, array('#title' => 'Expiration Date'));
      }
    }

    if ($entity->isRevoked()) {
      $revoke_reason = $entity->getRevokeReason();
      $content['revoke_reason'] = array(
        '#markup' => ('<p>' . t('<u>Note</u>: The badge has been revoked. %reason', array('%reason' => t($revoke_reason))) . '</p>'),
      );
      $content['revoke_reason'] = array(
        '#theme_wrappers' => array('form_element'),
        '#title' => t('Cancellation'),
        '#markup' => t('The badge has been revoked. %reason', array('%reason' => t($revoke_reason))),
      );
    }

    if ($view_mode === 'full') {
      $evidence_url = $entity->getEvidenceUrl();
      if (!empty($evidence_url)) {
        $content['evidence_url'] = array(
          '#theme_wrappers' => array('form_element'),
          '#title' => t('Evidence Page'),
          '#markup' => l($evidence_url, $evidence_url, $link_options),
        );
      }

      $assertion_url = $entity->getJsonDataUrl();
      $recipient = $entity->getRecipientAsArray();
      if (!empty($recipient['identity'])) {
        $content['identity'] = array(
          '#theme_wrappers' => array('form_element'),
          '#title' => t('Recipient ID'),
          '#markup' => check_plain($recipient['identity']),
        );

        // Add a "Check Recipient" button, similar to the feature
        // in the Badge Inspector and Backpack Inspector.
        // Get access to the current Drupal user, if any.
        global $user;
        $validation_user_email = (isset($user->mail) ? ($user->mail) : 'verify@example.com');
        $content['RecipientValidationEmail'] = array(
          '#id' => 'RecipientValidationEmail',
          '#type' => 'textfield',
          '#title' => '',
          '#description' => t('Specify the email address of the person you believe received the badge award to validate the recipient of the badge.'),
          '#value' => $validation_user_email,
          '#suffix' => '<div id="RecipientValidationResultText"></div>',
        );
        $content['RecipientValidationHash'] = array(
          '#attributes' => array('id' => 'RecipientValidationHash'),
          '#type' => 'hidden',
          '#value' => $recipient['identity'],
        );
        $content['RecipientValidationSalt'] = array(
          '#attributes' => array('id' => 'RecipientValidationSalt'),
          '#type' => 'hidden',
          // $entity->getRecipientSalt())
          '#value' => (empty($recipient['salt']) ? '' : $recipient['salt']),
        );
        $content['RecipientValidationButton'] = array(
          '#id' => 'RecipientValidationButton',
          '#type' => 'button',
          '#value' => t('Check Recipient Email'),
          // Override client-side JavaScript action and disable form submission.
          '#attributes' => array('onclick' => 'ValidateBadgeAssertionRecipient(); return false;'),
          '#attached' => array(
            'js' => array(
              array(
                'data' => array(BADGE_DEPOT_MODULE_NAME => array(
                  'obi_recipient_validation_url' => url(BADGE_DEPOT_INSPECTOR_CHECK_RECIPIENT_PATH, array('absolute' => TRUE)),
                )),
                'type' => 'setting',
              ),
              (_badge_depot_get_module_path() . '/entities/assertion/recipient_validation_script.js'),
            ),
          ),
        );
      }

      // Add a button that links to the Mozilla Backpack for uploading this badge.
      $allow_unknown_signature = FALSE;
      $signed_badge_url = (BadgeDepotOpenSsl::isAvailable() ? $entity->getSignedBadgeUrl($allow_unknown_signature) : NULL);

      // For example, "http://beta.openbadges.org/issuer.js".
      $issuer_api_script_url = BadgeDepotGlobalSettings::getObiBadgeIssuerApiScriptUrl();
      if (!empty($issuer_api_script_url)) {
        $signed_badge_jws = NULL;
        if (!empty($signed_badge_url) && $is_internal_user) {
          // Generate the signed-badge JSON Web Signature (JWS), if appropriate.
          $signed_badge_jws = $entity->getJsonWebSignature($allow_unknown_signature);
        }

        $upload_button_text = 'Upload Badge to Backpack';
        if (!empty($assertion_url)) {
          $upload_button_text_ex = $upload_button_text;
          if (!empty($signed_badge_jws)) {
            $upload_button_text_ex .= ' (Baked)';
          }
          $content['badge_backpack_upload'] = array(
            '#type' => 'button',
            '#value' => t($upload_button_text_ex),
            // Override client-side JavaScript action and disable form submission.
            '#attributes' => array('onclick' => 'OpenBadges.issue([ jQuery("#obiBadgeAssertionUrl").attr("href") ], function(errors, successes) { }); return false;'),
            '#attached' => array(
              'js' => array(
                $issuer_api_script_url => array('type' => 'external'),
              ),
            ),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          );
        }

        // Add an upload button for the signed badge.
        if (!empty($signed_badge_jws)) {
          $content['signed_badge_jws'] = array(
            '#attributes' => array('id' => 'signed_badge_jws'),
            '#type' => 'hidden',
            '#value' => $signed_badge_jws,
          );
          $upload_button_text_ex = $upload_button_text . ' (Signed)';
          $content['signed_badge_upload'] = array(
            '#type' => 'button',
            '#value' => t($upload_button_text_ex),
            // Override client-side JavaScript action and disable form submission.
            '#attributes' => array('onclick' => 'OpenBadges.issue([ jQuery("#signed_badge_jws").val() ], function(errors, successes) { }); return false;'),
            '#attached' => array(
              'js' => array(
                $issuer_api_script_url => array('type' => 'external'),
              ),
            ),
            '#prefix' => '<p>',
            '#suffix' => '</p>',
          );
        }
      }

      // Add a download button for the signed-badge JSON Web Signature (JWS).
      if (!empty($signed_badge_url)) {
        $content['signed_badge_url'] = array(
          '#attributes' => array('id' => 'signed_badge_url'),
          '#type' => 'hidden',
          '#value' => $signed_badge_url,
        );
        $content['signed_badge_download'] = array(
          '#type' => 'button',
          '#value' => t('Download Signed Badge (JWS)'),
          // Override client-side JavaScript action and disable form submission.
          '#attributes' => array('onclick' => 'window.open(jQuery("#signed_badge_url").val(), "_blank"); return false;'),
        );
      }

      // Add a download button for the baked badge image.
      $image_url = $entity->getImageUrl(FALSE, TRUE);
      if (!empty($image_url)) {
        $content['baked_image_url'] = array(
          '#attributes' => array('id' => 'baked_image_url'),
          '#type' => 'hidden',
          '#value' => $image_url,
        );
        $content['baked_badge_download'] = array(
          '#type' => 'button',
          '#value' => t('Download Baked Badge Image'),
          // Override client-side JavaScript action and disable form submission.
          '#attributes' => array('onclick' => 'window.open(jQuery("#baked_image_url").val(), "_blank"); return false;'),
        );
      }

      $content['assertion_url'] = array(
        '#theme_wrappers' => array('form_element'),
        '#title' => t('Assertion Data'),
        '#markup' => l($assertion_url, $assertion_url, array_merge_recursive($link_options, array('attributes' => array('id' => 'obiBadgeAssertionUrl')))),
      );
    }

    $this->buildEntityFieldContent($content, 'assertion_machine_name', $properties, $wrapper, $entity, array('#title' => 'Unique Award ID'));

    if ($view_mode === 'full') {
      if ($is_internal_user && (!empty($entity->notes))) {
        $this->buildEntityFieldContent($content, 'notes', $properties, $wrapper, $entity, array(
          '#title' => t('Notes'),
          '#theme' => NULL,
          '#markup' => check_markup($entity->notes, 'full_html', $langcode),
        ));
      }

      if (!$entity->is_published) {
        drupal_set_message(
          t(
            'The data for "@title" is currently not published publicly. <a href="!url">Edit this badge-assertion</a> entity if you want to change this.',
            array(
              '@title' => check_plain($entity->title),
              '!url' => url($entity->getEntityEditPath()),
            )
          ),
          'warning'
        );
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Overrides the base method that tweaks the extracted data to be exported
   * for an entity; it filters out more variables that should not be exported.
   */
  protected function exportCleanup(array &$vars, $entity) {
    // Remove the badge definition's ID, since the ID may
    // be different on the importing Drupal installation.
    unset($vars['manifest_id']);

    // Converts JSON data into something that looks nicer for exporting.
    $this->exportCleanupForJsonString('recipient_json', $vars, $entity);

    parent::exportCleanup($vars, $entity);
  }

  /**
   * Overrides the base method that tweaks the deserialized data to be imported
   * into an entity; it adds or modifies variables that should also be imported.
   */
  protected function importCleanup(array &$vars, $export) {
    // Attempt to locate the manifest's ID.
    $manifest_id = 0;
    $manifest_machine_name = (isset($vars['manifest_machine_name']) ? $vars['manifest_machine_name'] : NULL);
    $manifest = (empty($manifest_machine_name) ? NULL : BadgeManifestEntity::loadFromMachineName($manifest_machine_name));
    if (isset($manifest->manifest_id)) {
      $manifest_id = $manifest->manifest_id;
    }

    $vars['manifest_id'] = $manifest_id;

    // Converts JSON data to the string formats being used internally.
    $this->importCleanupForJsonString('recipient_json', $vars, $export);

    parent::importCleanup($vars, $export);
  }
}
