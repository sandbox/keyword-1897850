<?php
/**
 * @file
 * Contains BadgeAssertionEntity.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Main class for badge-assertion entities (that is OBI badge definitions).
 */
class BadgeAssertionEntity extends BadgeDepotObiBaseEntity {

  /**
   * Overrides the base method that returns the default name of the
   * database table for this entity type.
   */
  protected static function getDefaultEntityType() {
    return BADGE_DEPOT_ENTITY_TABLE_ASSERTION;
  }

  /**
   * Overrides the base method that returns the name of the database
   * column that stores the machine-readable name of the entity.
   */
  protected static function getDefaultMachineNameKey() {
    return BADGE_DEPOT_ENTITY_COLUMN_ASSERTION_MACHINE_NAME;
  }

  /**
   * Overrides the base method that returns the base URL path for all instances
   * of this entity type (that is not for a single specific entity instance).
   */
  protected static function getEntityMainRootPath() {
    return BADGE_DEPOT_MAIN_ASSERTION_PATH;
  }

  /**
   * Overrides the base method that returns the base URL path to administer
   * this entity type (that is not for a single entity instance).
   */
  protected static function getEntityAdminRootPath() {
    return BADGE_DEPOT_ADMIN_ASSERTION_PATH;
  }

  /**
   * Overrides the base method that lists all the not-null text fields.
   */
  protected function getNonNullableTextFieldNames() {
    return (parent::getNonNullableTextFieldNames() +
      array(
        'notes',
        'image_url',
        'recipient_json',
      )
    );
  }

  // Define fields that map to specific database columns of this entity:
  public $assertion_id;

  // Also used as the OBI BadgeAssertion "uid" property.
  public $assertion_machine_name;

  // The title is the name of the awarded badge as presented in lists.
  public $title;

  // Used internally by the issuer, if needed.
  public $notes;

  public $expires;

  // Leave blank if not revoked; when revoking, set the "expires" time-stamp
  public $revoke_reason;

  public $manifest_id;

  public $manifest_machine_name;

  public $image_url;

  public $evidence_url;

  public $recipient_email;

  public $recipient_json;

  public function __construct(array $values = array(), $entity_type = NULL) {
    parent::__construct($values, $entity_type);

    // Publish assertions by default.
    $this->is_published = TRUE;
  }

  /**
   * Convert the specified value to its equivalent Unix time-stamp.
   *
   * @param $date_time
   *   A date-time string or number value to be converted.
   *
   * @return
   *   Upon success, the integer number representing the Unix time-stamp
   *   that corresponds with the specified date-time value; otherwise NULL.
   */
  public static function parseTimestamp($date_time) {
    if ((!isset($date_time)) || ($date_time === '')) {
      $timestamp = 0;
    }
    elseif (is_numeric($date_time)) {
      $timestamp = (int) $date_time;
    }
    else {
      $default_timezone = date_default_timezone_get();
      date_default_timezone_set('UTC');
      $timestamp = strtotime($date_time);
      date_default_timezone_set($default_timezone);
      if ($timestamp === FALSE) {
        $timestamp = NULL;
      }
    }

    return $timestamp;
  }

  /**
   * Overrides the base method that permanently saves the entity.
   */
  public function save() {
    if (!isset($this->manifest_id)) {
      // WARNING: No badge may exist without a badge-class manifest!
      $this->manifest_id = 0;
    }

    // Keep the manifest's machine-name and ID fields synchronized.
    $manifest = $this->getManifest();
    $this->manifest_machine_name = (isset($manifest->manifest_machine_name) ? $manifest->manifest_machine_name : '');
    if (isset($manifest) && empty($this->title)) {
      // Add a default title consisting of the badge-class name and a
      // timestamp; ISO 8601 date (e.g., "2009-02-13T17:31:30-06:00").
      $this->title = ($manifest->title . ' (' . format_date(time(), 'custom', 'c') . ')');
    }

    // Ensure that the expiration date is saved as a number.
    if (isset($this->expires) && (!is_numeric($this->expires))) {
      $this->expires = static::parseTimestamp($this->expires);
    }

    // Let this badge expire when it is revoked.
    if ((!empty($this->revoke_reason)) && empty($this->expires)) {
      $this->expires = time();
    }

    // Filter out a whitespace string for the recipient JSON.
    $recipient_json_empty_check = $this->recipient_json;
    if (is_string($recipient_json_empty_check)) {
      $recipient_json_empty_check = trim($recipient_json_empty_check);
    }

    // Ensure that recipient JSON data exists when the recipient's email address is known.
    if (empty($recipient_json_empty_check) && (!empty($this->recipient_email))) {
      $this->setRecipient($this->recipient_email);
    }

    // Ensure that the recipient JSON data is saved as a string.
    if (isset($this->recipient_json) && (!is_string($this->recipient_json))) {
      $json_string = _badge_depot_json_encode_pretty($this->recipient_json);
      if (is_string($json_string)) {
        $this->recipient_json = $json_string;
      }
    }

    return parent::save();
  }

  /**
   * Gets the entity object representing the issuer-organization that is associated with this badge.
   *
   * @return
   *   The issuer entity of this badge.
   */
  public function getIssuer() {
    $issuer = NULL;
    $manifest = $this->getManifest();
    if (isset($manifest)) {
      $issuer = $manifest->getIssuer();
    }

    return $issuer;
  }

  /**
   * Gets the entity object representing the badge definition (that is OBI
   * badge-class manifest) that is associated with this awarded badge.
   *
   * @return
   *   The manifest entity of this badge.
   */
  public function getManifest() {
    static $manifest = NULL;
    static $manifest_id = NULL;
    if ((!isset($manifest)) || ($manifest_id !== $this->manifest_id)) {
      $manifest = NULL;
      $manifest_id = $this->manifest_id;
      if (isset($manifest_id) && ($manifest_id !== 0)) {
        $manifest = BadgeManifestEntity::load($manifest_id);
      }
    }

    return $manifest;
  }

  /**
   * Gets the path of the endpoint where the OBI badge-assertion data is hosted.
   *
   * @return
   *   The local path where the assertion data is being served in JSON format.
   */
  public function getLocalJsonDataPath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/obi-data';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the assertion JSON data.
   *
   * The format of the linked data is as specified for OBI "BadgeAssertion".
   *
   * @return
   *   The URL where the badge's assertion data can be found.
   */
  public function getJsonDataUrl() {
    $url = $this->getLocalJsonDataPath();
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the endpoint where the OBI signed-badge JWS is hosted.
   *
   * @param $allow_unknown_signature
   *   FALSE to return NULL if no signature is available, or
   *   TRUE to return the URL even when the signature is unknown.
   *
   * @return
   *   The local path where the signed badge is being served in JSON Web Signature (JWS) format.
   */
  public function getLocalSignedBadgePath($allow_unknown_signature = TRUE) {
    $url = NULL;
    $is_path_requested = $allow_unknown_signature;
    if (!$is_path_requested) {
      $issuer = $this->getIssuer();
      if (isset($issuer)) {
        $is_path_requested = (!empty($issuer->private_key_pem));
      }
    }

    if ($is_path_requested) {
      $url = $this->getEntityViewPath();
      if (!empty($url)) {
        $url .= '/jws-data';
      }
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the OBI signed-badge JWS.
   *
   * @param $allow_unknown_signature
   *   FALSE to return NULL if no signature is available, or
   *   TRUE to return the URL even when the signature is unknown.
   *
   * @return
   *   The URL where the signed-badge data can be found.
   */
  public function getSignedBadgeUrl($allow_unknown_signature = TRUE) {
    $url = $this->getLocalSignedBadgePath($allow_unknown_signature);
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the local endpoint where the OBI Badge-Class Manifest data is served.
   *
   * @return
   *   The local path where the badge definition data is being served in JSON format.
   */
  public function getLocalManifestPath() {
    $url = NULL;
    $manifest = $this->getManifest();
    if (isset($manifest)) {
      $url = $manifest->getLocalJsonDataPath();
    }
    elseif (!empty($this->manifest_machine_name)) {
      $url = (BADGE_DEPOT_MAIN_MANIFEST_PATH . '/' . $this->manifest_machine_name . '/obi-data');
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the badge-manifest JSON data.
   *
   * The format of the linked data is as specified for OBI "BadgeClass".
   *
   * @return
   *   The URL where the badge's manifest data can be found.
   */
  public function getManifestUrl() {
    $url = $this->getLocalManifestPath();
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the badge image when served locally.
   *
   * @return
   *   The local path where the image is being served.
   */
  public function getLocalImagePath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/image';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the baked badge image,
   * which can be a local or remote URL, or even a Data URI.
   *
   * @param $allow_data_uri
   *   TRUE to indicate that the URL may be a Data URI, or FALSE if a hosted
   *   alternative URL should be provided when the image URL is a Data URI.
   * @param $allow_baker_url
   *   TRUE to indicate that the URL may reference the badge-baker service
   *   directly, or FALSE to prevent a dynamic URL from being returned.
   *
   * @return
   *   The URL where the issued badge image can be found.
   */
  public function getImageUrl($allow_data_uri = TRUE, $allow_baker_url = FALSE) {
    $url = $this->image_url;
    if ($allow_baker_url && empty($url)) {
      // Use a dynamic URL involving the Badge Baker Service, if needed.
      $baker_url_prefix = BadgeDepotGlobalSettings::getObiBadgeBakerPrefixUrl();
      $assertion_url = $this->getJsonDataUrl();
      if (isset($baker_url_prefix) && isset($assertion_url)) {
        $url = ($baker_url_prefix . urlencode($assertion_url));
      }
    }
    elseif ((!$allow_data_uri) && DataUriCreator::isDataUri($url)) {
      $url = $this->getLocalImagePath();
    }

    return DataUriCreator::getDataUriOrQualifiedUrl($url);
  }

  /**
   * Gets the permalink path of the Badge Evidence page that is served
   * locally, which in turn redirects to the actual Badge Evidence
   * page that may be served locally or remotely.
   *
   * @return
   *   The local path where the evidence HTML page is being served.
   */
  public function getLocalEvidencePermalink() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/evidence';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the Badge Evidence page, which can be a local or remote page.
   *
   * @return
   *   The URL where the badge's evidence page can be found.
   */
  public function getEvidenceUrl() {
    return (empty($this->evidence_url) ? NULL : url($this->evidence_url, array('absolute' => TRUE)));
  }

  /**
   * Gets the permalink path of the Badge Issuer-Organization data that
   * is served locally, which in turn redirects to the actual endpoint.
   *
   * @return
   *   The local path where the badge-issuer data is being served.
   */
  public function getLocalIssuerPermalink() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/issuer';
    }

    return $url;
  }

  /**
   * Gets the permalink path of the Badge-Class Manifest data that is
   * served locally, which in turn redirects to the actual endpoint.
   *
   * @return
   *   The local path where the badge-definition JSON data is being served.
   */
  public function getLocalManifestPermalink() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/manifest';
    }

    return $url;
  }

  /**
   * Determines whether this awarded badge has expired.
   *
   * @param $deadline_time
   *   The Unix timestamp to be used as the cut-off time for expiration.
   *
   * @return
   *   TRUE if the badge has expired; otherwise FALSE.
   */
  public function isExpired($deadline_time = NULL) {
    if (!isset($deadline_time)) {
      $deadline_time = time();
    }

    return ((!empty($this->expires)) && ($this->expires <= $deadline_time));
  }

  /**
   * Determines whether this awarded badge has been revoked,
   * which is logically also the case for a badge that has expired.
   *
   * @param $deadline_time
   *   The Unix timestamp to be used as the cut-off time for expiration.
   *
   * @return
   *   TRUE if the badge has expired or has been revoked; otherwise FALSE.
   */
  public function isRevoked($deadline_time = NULL) {
    return ($this->isExpired($deadline_time) || (isset($this->revoke_reason) && ($this->revoke_reason != '')));
  }

  /**
   * Gets the revocation reason text.
   */
  public function getRevokeReason($deadline_time = NULL) {
    $revoke_reason = $this->revoke_reason;
    if (((!isset($revoke_reason)) || ($revoke_reason === '')) && ($this->isRevoked($deadline_time))) {
      if ($this->isExpired($deadline_time)) {
        $revoke_reason = 'The withdrawal was due to badge expiration.';
      }
      else {
        $revoke_reason = 'The reason for withdrawal is unknown.';
      }
    }

    return $revoke_reason;
  }

  /**
   * Sets the awarded badge's recipient JSON-data, for example,
   * by specifying an email address of the recipient user.
   *
   * @param $identity_data
   *   The data that identifies the recipient, as required for the specified
   *   identity type.  Generally this should be an associative array, but for
   *   the "email" identity-type, the data can simply be a string to specify
   *   the email address of the badge recipient.
   * @param $identity_type
   *   The type of identity data used to specify the recipient, for example, "email".
   *
   * @return
   *   Upon success, an associative array representing the recipient JSON-data; otherwise NULL.
   */
  public function setRecipient($identity_data, $identity_type = 'email') {
    if (!is_array($identity_data)) {
      // For example, array('email' => 'someone@example.com').
      $identity_data = array($identity_type => $identity_data);
    }

    $this->recipient_json = '';
    $this->recipient_email = '';
    $email = NULL;
    $recipient = NULL;
    switch ($identity_type) {
      case 'email':
        $identity = NULL;
        $email = trim($identity_data['email']);
        $salt = NULL;
        $hashed = (isset($identity_data['hashed']) ? (bool) $identity_data['hashed'] : TRUE);
        if ($hashed) {
          $hash_type = (isset($identity_data['hashType']) ? strtolower($identity_data['hashType']) : 'sha256');
          if (in_array($hash_type, hash_algos(), TRUE) && in_array($hash_type, array('sha1', 'sha256', 'sha512', 'md5'), TRUE)) {
            $salt = (isset($identity_data['salt']) ? (string) $identity_data['salt'] : NULL);
            if (!isset($salt)) {
              $salt = (BADGE_DEPOT_MODULE_NAME . ':' . hash('md5', (uniqid('saline', TRUE) . rand())));
            }

            $identity = ($hash_type . '$' . hash($hash_type, ($email . $salt)));
          }
        }
        else {
          $identity = $email;
        }

        if (isset($identity)) {
          $recipient = array(
            'identity' => $identity,
            'type' => 'email',
            'hashed' => $hashed,
          );

          if ($hashed && isset($salt) && ($salt !== '')) {
            $recipient['salt'] = $salt;
          }
        }
        else {
          // Email address was not used and may be invalid.
          $email = NULL;
        }

        break;
    }

    if ((!empty($email)) && BadgeDepotGlobalSettings::getAssertionSaveEmailFlag()) {
      $this->recipient_email = $email;
    }

    if (!empty($recipient)) {
      $this->recipient_json = $recipient;
    }

    return $recipient;
  }

  /**
   * Gets the recipient JSON-data as an associative array.
   */
  public function getRecipientAsArray() {
    $recipient = $this->recipient_json;
    if (!is_array($recipient)) {
      if (empty($recipient) || ($recipient === '{}') || (!is_string($recipient))) {
        // Invalid recipient data.
        $recipient = NULL;
      }
      else {
        $recipient = drupal_json_decode($recipient);
      }
    }

    return $recipient;
  }

  /**
   * Extracts the identity value from the recipient JSON-data, which is often a hashed value of the recipient's email address.
   */
  public function getRecipientIdentity() {
    $salt = NULL;
    $recipient = $this->getRecipientAsArray();
    if (isset($recipient['identity'])) {
      $salt = $recipient['identity'];
    }

    return $salt;
  }

  /**
   * Extracts the salt value from the recipient JSON-data, if provided, which is often used to hash the identity email-address.
   */
  public function getRecipientSalt() {
    $salt = NULL;
    $recipient = $this->getRecipientAsArray();
    if (isset($recipient['salt'])) {
      $salt = $recipient['salt'];
    }

    return $salt;
  }

  /**
   * Formats the specified date-time for JSON output.
   *
   * @param $date_time
   *   A date-time string or Unix-timestamp number to be formatted.
   *
   * @return
   *   Upon success, the timestamp's value to be used in JSON output; otherwise NULL.
   */
  protected function formatJsonTimestamp($date_time) {
    $timestamp = static::parseTimestamp($date_time);

    // TODO: Make this configurable, perhaps using BadgeDepotGlobalSettings.
    $human_friendly = TRUE;
    if (isset($timestamp) && $human_friendly) {
      // ISO 8601 date (e.g., "2009-02-13T17:31:30-06:00").
      $timestamp = format_date($timestamp, 'custom', 'c');
    }

    return $timestamp;
  }

  /**
   * Gets the signed-badge representation of this assertion entity,
   * formatted as a JSON Web Signature (JWS) as defined in the OBI Badge
   * Assertion specification, version 1.0 (see section "Signed Badges").
   *
   * @param $allow_blank_pem
   *   FALSE to return NULL if the issuer's signing PEM-data is unknown,
   *   or TRUE to return the JWS even when no public-key data is set.
   *
   * @return
   *   The JWS string that represents the signed badge; otherwise NULL,
   *   for example if the badge has been revoked or if the issuer's signing
   *   key is unknown.
   */
  public function getJsonWebSignature($allow_blank_pem = FALSE) {
    $signed_badge_jws = NULL;
    if (!$this->isRevoked()) {
      $issuer = $this->getIssuer();
      if (isset($issuer)) {
        $json_data = $this->getJsonData('signed', array('allowBlankPEM' => (bool) $allow_blank_pem));
        if (is_array($json_data) && (!empty($json_data))) {
          $signed_badge_jws = $issuer->getJsonWebSignature($json_data);
        }
      }
    }

    return $signed_badge_jws;
  }

  /**
   * Gets the JSON representation of this assertion entity,
   * formatted as required by the OBI Badge Assertion
   * specification, version 1.0 (see section "BadgeAssertion").
   *
   * @param $options
   *   An optional associative array containing settings, such
   *   as additional formatting requirements for signed badges.
   *
   * @return
   *   An array containing the entity's JSON data.
   */
  public function getJsonData($verify_type = 'hosted', $options = NULL) {
    if ($this->isRevoked()) {
      // Process revoked badges according to OBI specification 1.0 requirements,
      // for example, '{"revoked": true}'.
      $json_data = array('revoked' => TRUE);
    }
    else {
      // Process other badges according to OBI specification 1.0 requirements...
      $verify = NULL;
      switch ($verify_type) {
        case 'signed':
          // If the type is "signed", link to the issuer's public key.
          $issuer = $this->getIssuer();
          if (isset($issuer)) {
            $allow_blank_pem = (isset($options['allowBlankPEM']) ? (bool) $options['allowBlankPEM'] : TRUE);

            // Signature URL.
            $verify = array(
              'type' => 'signed',
              'url' => $issuer->getPublicKeyUrl($allow_blank_pem),
            );
          }

          break;
        case NULL:
        case '':
        case 'hosted':
          // For the "hosted" type of verification, link to
          // the assertion data on this server (Assertion URL).
          $verify = array(
            'type' => 'hosted',
            'url' => $this->getJsonDataUrl(),
          );

          break;
      }

      if (empty($verify)) {
        $json_data = NULL;
      }
      else {
        $json_data = array(
          'uid' => $this->assertion_machine_name,
          'badge' => $this->getManifestUrl(),
          'issuedOn' => $this->formatJsonTimestamp($this->created),
          'recipient' => $this->getRecipientAsArray(),
          'verify' => $verify,
        );

        $evidence = $this->getEvidenceUrl();
        if (!empty($evidence)) {
          $json_data['evidence'] = $evidence;
        }

        $expires = $this->expires;
        if (!empty($expires)) {
          $json_data['expires'] = $this->formatJsonTimestamp($expires);
        }

        $image_url = $this->getImageUrl(BadgeDepotGlobalSettings::getObiJsonAllowDataUriFlag());
        if (empty($image_url) && BadgeDepotGlobalSettings::getAssertionIncludeDynamicImageFlag()) {
          $image_url = $this->getLocalImagePath();
          if (!empty($image_url)) {
            $image_url = url($image_url, array('absolute' => TRUE));
          }
        }

        if (!empty($image_url)) {
          $json_data['image'] = $image_url;
        }
      }
    }

    return $json_data;
  }

  /**
   * Implements a web-service endpoint for serving a JSON representation
   * of this entity's data that is formatted as per version 1.0 of the
   * OBI Badge Assertion specification (see section "BadgeAssertion").
   *
   * @return
   *   Upon success, an array containing the entity's data for JSON-rendering
   *   purposes; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED).
   *   The return value should be processed by this module's JSON delivery
   *   callback function (that is badge_depot_deliver_json).
   */
  public function requestJsonData() {
    if (!$this->isAccessibleByUser('view')) {
      return MENU_ACCESS_DENIED;
    }

    $json_data = $this->getJsonData();
    if (isset($json_data['revoked']) && ($json_data['revoked'] == TRUE)) {
      // Process revoked badges according to OBI specification 1.0 requirements.
      drupal_add_http_header('Status', '410 Gone');
    }

    return $json_data;
  }

  /**
   * Implements a web-service endpoint for serving a signed-badge representation
   * of this entity's data that is formatted as per version 1.0 of the
   * OBI Badge Assertion specification (see section "BadgeAssertion").
   *
   * @return
   *   Upon success, a string containing the JSON Web Signature (JWS) of the
   *   signed badge; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED)
   *   or NULL, for example if the badge has been revoked or if the issuer's
   *   signing key is unknown.  The return value should be processed by this
   *   module's JWS delivery callback function (badge_depot_deliver_jws).
   */
  public function requestJsonWebSignature() {
    if (!$this->isAccessibleByUser('view')) {
      return MENU_ACCESS_DENIED;
    }

    return $this->getJsonWebSignature();
  }

  /**
   * Returns the URL for the badge-evidence page's redirection request;
   * this is the implementation of the permalink page that redirects to
   * the actual Evidence HTML page that may be served locally or remotely.
   *
   * @return
   *   Upon success, the endpoint URL of the evidence page for redirection.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestEvidence() {
    return $this->getEvidenceUrl();
  }

  /**
   * Returns the page-request response for the image of the badge assertion.
   *
   * @return
   *   Upon success, the Data URI or regular URL of the image.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestImage() {
    $allow_data_uri = TRUE;

    // Redirect to the baker service, if needed.
    $allow_baker_url = TRUE;

    return $this->getImageUrl($allow_data_uri, $allow_baker_url);
  }

  /**
   * Returns the URL for the badge-issuer page's redirection request;
   * this is the implementation of a convenience permalink page that
   * redirects to the actual Issuer JSON data related to this badge.
   *
   * @return
   *   Upon success, endpoint URL of the issuer JSON to be redirected to.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestIssuer() {
    $url = NULL;
    $manifest = $this->getManifest();
    if (isset($manifest)) {
      $url = $manifest->getLocalIssuerPath();
    }

    return $url;
  }

  /**
   * Returns the URL for the badge-manifest page's redirection request;
   * this is the implementation of a convenience permalink page that
   * redirects to the actual BadgeClass JSON data related to this badge.
   *
   * @return
   *   Upon success, endpoint URL of the manifest JSON to be redirected to.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestManifest() {
    return $this->getLocalManifestPath();
  }

  /**
   * Loads all badge awards for the specified badge-class manifest.
   *
   * @param $manifest_id
   *   The identifier of the badge-class manifest to be matched.
   *
   * @return
   *   Upon success, an array containing the requested assertion entities;
   *   otherwise NULL.
   */
  public static function loadMultipleFromManifestID($manifest_id) {
    if (!isset($manifest_id)) {
      return NULL;
    }

    $get_all_values = TRUE;
    $safe_manifest_id = (integer) $manifest_id;
    return static::loadFromValueMatch('manifest_id', $safe_manifest_id, $get_all_values);
  }

  /**
   * Loads all badge awards for the specified list of manifest IDs.
   *
   * @param $manifest_ids
   *   An array of manifest identifiers to be matched.  All badge awards
   *   that are instances of any of the specified IDs will be returned.
   *
   * @return
   *   Upon success, an array containing the requested assertion entities;
   *   otherwise NULL.
   */
  public static function loadMultipleFromManifestIDs($manifest_ids) {
    // Get all the badge definitions for the specified manifests.
    $assertions = NULL;
    if (!empty($manifest_ids)) {
      $manifest_ids = array_unique($manifest_ids);
      $get_all_values = TRUE;
      $assertions = static::loadFromValueMatch(NULL, NULL, $get_all_values, function($query) {
        $query->propertyCondition('manifest_id', $manifest_ids, 'IN');
      });
    }

    return $assertions;
  }

  /**
   * Loads all badge awards for the specified list of manifests.
   *
   * @param $manifests
   *   An array of manifest entities to be matched.  All badge awards that
   *   are instances of any of the specified manifests will be returned.
   *   The entities to be matched are generally obtained through a call to
   *   the BadgeManifestEntity::loadMultipleThroughFilter function.
   *
   * @return
   *   Upon success, an array containing the requested assertion entities;
   *   otherwise NULL.
   */
  public static function loadMultipleFromManifests($manifests) {
    $assertions = NULL;

    // Get all the IDs of the manifests.
    $manifest_ids = array();
    if (!empty($manifests)) {
      foreach ($manifests as $manifest) {
        $manifest_ids[] = $manifest->manifest_id;
      }
    }

    return static::loadMultipleFromManifestIDs($manifest_ids);
  }

  /**
   * Loads all badge awards that match the specified filter criteria,
   * such as ones that are marked as revoked or published.
   *
   * @param $manifest_id
   *   The optional identifier of the badge-class manifest to be matched,
   *   or NULL to ignore.  An array with multiple IDs can also be specified.
   *
   * @param $is_published
   *   An optional boolean flag that indicates whether badge awards that
   *   are published should be included or not.  Specify NULL to include
   *   awards irrespetive of their publication state, or alternatively
   *   specify TRUE or FALSE to match the award's current publication state.
   *
   * @param $owner_user_id
   *   The user identifier of the entity's owner to be matched, or NULL
   *   to ignore.  An array with multiple IDs may also be specified.
   *
   * @param $has_revoke_reason
   *   An optional boolean flag that indicates whether badge awards that have
   *   a revoke reason should be included or not.  Specify NULL to include
   *   awards irrespetive of their revoke-reason value, or alternatively specify
   *   TRUE or FALSE to specify the existance of the award's current revoke-reason.
   *
   * @param $expiration_time
   *   An optional time-stamp to be compared against the assertion's expiration time.
   *   Specify NULL to include awards irrespetive of their expiration value, specify
   *   0 to include assertions that have no expiry time, or alternatively specify
   *   an actual time-stamp to compare against the award's current expiration value.
   *
   * @return
   *   Upon success, an array containing the requested assertion entities;
   *   otherwise NULL.
   */
  public static function loadMultipleThroughFilter($manifest_id = NULL, $is_published = NULL, $owner_user_id = NULL, $has_revoke_reason = NULL, $expiration_time = NULL) {
    if ((!isset($manifest_id)) && (!isset($is_published)) && (!isset($owner_user_id)) && (!isset($has_revoke_reason)) && (!isset($expiration_time))) {
      return NULL;
    }

    $context_data = array();
    if (isset($manifest_id)) {
      $context_data['manifest_id'] = (is_array($manifest_id) ? $manifest_id : (integer) $manifest_id);
    }

    if (isset($is_published)) {
      $context_data['is_published'] = ($is_published ? 1 : 0);
    }

    if (isset($owner_user_id)) {
      $context_data['owner_user_id'] = (is_array($owner_user_id) ? $owner_user_id : (integer) $owner_user_id);
    }

    if (isset($has_revoke_reason)) {
      $context_data['@hasRevokeReason'] = ((bool) $has_revoke_reason);
    }

    if (isset($expiration_time)) {
      $expiration_time = static::parseTimestamp($expiration_time);
      if (isset($expiration_time)) {
        $context_data['@expireTime'] = $expiration_time;
      }
    }

    $get_all_values = TRUE;
    return static::loadFromValueMatch(
      NULL,
      NULL,
      $get_all_values,
      function($query, $context_data) {
        foreach ($context_data as $property_name => $property_value) {
          switch ($property_name) {
            case '@expireTime':
              if ($property_value == 0) {
                $query->propertyCondition('expires', 0, '=');
              }
              else {
                $query->propertyCondition('expires', 0, '<>');
                $query->propertyCondition('expires', $property_value, '<=');
              }

              break;
            case '@hasRevokeReason':
              if ($property_value) {
                $query->propertyCondition('revoke_reason', '', '<>');
              }
              else {
                $query->propertyCondition('revoke_reason', '', '=');
              }

              break;
            default:
              $query->propertyCondition($property_name, $property_value);
              break;
          }
        }
      },
      $context_data
    );
  }
}
