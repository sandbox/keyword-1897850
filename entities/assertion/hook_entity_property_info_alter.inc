<?php
/**
 * @file
 * Contains additional hook_entity_property_info_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes hook_entity_property_info_alter data for the badge-assertion entity.
 */
function _badge_depot_entities_assertion_entity_property_info_alter(&$info) {
  $properties = &$info[BADGE_DEPOT_ENTITY_TABLE_ASSERTION]['properties'];

  $properties['assertion_id'] = array_merge(((array) ($properties['assertion_id'])), array(
    'label' => t('Badge-Assertion ID'),
    'description' => t('The unique identifier of this badge assertion entry.'),
  ));

  $properties['assertion_machine_name'] = array_merge(((array) ($properties['assertion_machine_name'])), array(
    'label' => t('Persistent ID'),
    'description' => t('Corresponding with the OBI "uid" property, this is the persistent unique-identifier of the awarded badge, which is also used for exporting and importing data between machines.'),
  ));

  $properties['owner_user_id'] = array_merge(((array) ($properties['owner_user_id'])), array(
    'label' => t('Owner User ID'),
    'description' => t('A reference to the user that owns this assertion for data-management purposes, which should not be confused with the user that the badge is awarded to.'),
    'type' => 'user',
  ));

  $properties['owner_user_name'] = array_merge(((array) ($properties['owner_user_name'])), array(
    'label' => t('Owner User Name'),
    'description' => t('The name of the user that owns this assertion for data-management purposes; used primarily for enhanced security when exporting/importing the data entity. This is generally not the same as user that received the badge.'),
  ));

  $properties['manifest_id'] = array_merge(((array) ($properties['manifest_id'])), array(
    'label' => t('Badge Manifest ID'),
    'description' => t('A reference to the badge definition (i.e., badge-class manifest) that describes general properties of this badge award (the entity\'s "manifest_id" property).'),
    'type' => BADGE_DEPOT_ENTITY_TABLE_ISSUER,
  ));

  $properties['manifest_machine_name'] = array_merge(((array) ($properties['manifest_machine_name'])), array(
    'label' => t('Issuer machine-readable ID'),
    'description' => t('The machine-readable name of the badge-class manifest that defines this badge award (the entity\'s "manifest_machine_name" property).'),
    'type' => 'token',
  ));

  $properties['type'] = array_merge(((array) ($properties['type'])), array(
    'description' => t('The entity type of this assertion entry (used by entity sub-type bundles).'),
  ));

  $properties['revoke_reason'] = array_merge(((array) ($properties['revoke_reason'])), array(
    'label' => t('Revocation Reason'),
    'description' => t('If the badge has been revoked, specify the reason of revocation; otherwise leave blank (the entity\'s "revoke_reason" property).'),
  ));

  $properties['title'] = array_merge(((array) ($properties['title'])), array(
    'label' => t('Name'),
    'description' => t('The name of this OBI badge assertion (treated as non-markup plain text; the entity\'s "title" property).'),
  ));

  $properties['notes'] = array_merge(((array) ($properties['notes'])), array(
    'label' => t('Notes'),
    'description' => t('Optional notes for internal use by the badge issuer (treated as HTML markup; the entity\'s "notes" property).'),
  ));

  $properties['is_published'] = array_merge(((array) ($properties['is_published'])), array(
    'label' => t('Is Published?'),
    'description' => t('A boolean flag indicating whether the assertion entry is publicly visible (the entity\'s "is_published" property).'),
    'type' => 'boolean',
  ));

  $properties['created'] = array_merge(((array) ($properties['created'])), array(
    'description' => t('The full date and time, as a timestamp, of when the badge was issued (i.e., when the assertion entry was created).'),
    'type' => 'date',
  ));

  $properties['changed'] = array_merge(((array) ($properties['changed'])), array(
    'description' => t('The full date and time, as a timestamp, of when the assertion entry was most recently saved.'),
    'type' => 'date',
  ));

  $properties['expires'] = array_merge(((array) ($properties['expires'])), array(
    'description' => t('The optional full date and time, as a timestamp, of when the badge is to expire. If the achievment has some notion of expiry, this indicates when a badge should no longer be considered valid.'),
    'type' => 'date',
  ));

  $properties['image_url'] = array_merge(((array) ($properties['image_url'])), array(
    'label' => t('Image URL'),
    'description' => t('An optional URL that links to the baked badge image containing the hosted assertion URL; it should have a square size (can be a regular URL or <a href="!url" target="_blank" title="Tool to create a Data URI">Data URI</a>; the entity\'s "image_url" property). If not specified, a dynamic URL can be generated using the Badge Baker Service.', array('!url' => url(DATA_URI_CREATOR_PAGE_PATH))),
    'type' => 'uri',
  ));

  $properties['evidence_url'] = array_merge(((array) ($properties['evidence_url'])), array(
    'label' => t('Evidence URL'),
    'description' => t('An optional URL that links to the badge\'s evidence page (the entity\'s "evidence_url" property).'),
    'type' => 'uri',
  ));

  $properties['recipient_email'] = array_merge(((array) ($properties['recipient_email'])), array(
    'label' => t('Recipient Email'),
    'description' => t('The email address of the user that receives the badge award, which may be omitted for security, privacy or other reasons, but only after the badge instance had been created (the entity\'s "recipient_email" property).'),
  ));

  $properties['recipient_json'] = array_merge(((array) ($properties['recipient_json'])), array(
    'label' => t('Badge Recipient'),
    'description' => t('The recipient of the badge achievement (i.e., the JSON for the OBI "IdentityObject" value; the entity\'s "recipient_json" property).'),
    // TODO: 'type' => 'list<text>',
  ));
}
