<?php
/**
 * @file
 * Contains additional hook_help() information.
 */

/**
 * Provides hook_help info for the badge-assertion entity.
 */
function _badge_depot_entities_assertion_help($path, $arg) {
  if ($path === BADGE_DEPOT_ADMIN_ASSERTION_PATH) {
    $output = ('<p>' . t('Badge Assertion information is used to provide proof that a badge instance was awarded to a specific user by an affiliated issuer-organization.') . '</p>');
    if ((count($arg) <= 4) || empty($arg[4])) {
      $output .= ('<p>' . t('Configure the badge assertions that are stored as Entity data using this administration page.') . '</p>');
    }

    return $output;
  }

  if ($path === (BADGE_DEPOT_ADMIN_ASSERTION_PATH . '/manage/%')) {
    $output = _badge_depot_entities_assertion_help(BADGE_DEPOT_ADMIN_ASSERTION_PATH, $arg);
    $machine_name = $arg[5];
    $entity = BadgeAssertionEntity::loadFromMachineName($machine_name);
    if (isset($entity)) {
      $url_options = array('absolute' => TRUE);
      $output .= '<p>' . t('Once configured, the Badge Depot module is also able to provide these assertion-related services:') . '</p>';
      $output .= '<ul>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Publish a human-readable page</a> describing the awarded badge,', array('@url' => url($entity->getEntityViewPath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Host OBI badge-assertion data</a>, in JSON format, that gets linked to from baked badges-images,', array('@url' => url($entity->getLocalJsonDataPath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Provide a baked image</a> representing the awarded badge,', array('@url' => url($entity->getLocalImagePath(), $url_options))) . '</li>';
      if ((!$entity->isRevoked()) && BadgeDepotOpenSsl::isAvailable()) {
        $issuer = $entity->getIssuer();
        if (isset($issuer) && (!empty($issuer->private_key_pem))) {
          $output .= '<li>' . t('Provide a <a href="@url" target="_blank">signed badge (JWS)</a> representation of the award,', array('@url' => url($entity->getLocalSignedBadgePath(), $url_options))) . '</li>';
        }
      }

      $output .= '<li>' . t('Link to the badge award\'s OBI <a href="@issuerUrl" target="_blank">Issuer-Organization</a> and <a href="@manifestUrl" target="_blank">Badge-Class Manifest</a> data, expressed in JSON format, and', array('@issuerUrl' => url($entity->getLocalIssuerPermalink(), $url_options), '@manifestUrl' => url($entity->getLocalManifestPermalink(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Link to evidence</a> (optional) of the work that the recipient did to earn the achievement.', array('@url' => url($entity->getLocalEvidencePermalink(), $url_options))) . '</li>';
      $output .= '</ul>';
    }

    return $output;
  }
}
