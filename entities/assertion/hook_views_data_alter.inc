<?php
/**
 * @file
 * Contains additional hook_views_data_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes the hook_views_data_alter data for the badge-assertion entity.
 */
function _badge_depot_entities_assertion_views_data_alter(&$info) {
  // This modifies the formatting for the body field of the badge_assertion
  // entity to be rendered as HTML.
  $info[BADGE_DEPOT_ENTITY_TABLE_ASSERTION]['notes']['field']['handler'] = 'BadgeDepotViewsFieldHandlerHtml';
}
