<?php
/**
 * @file
 * Provides global functions and other definitions for all badge entities.
 */

/**
 * Fixes a problem with the default entity_class_label() function.
 *
 * It can happen that a null entity is passed during entity importing to
 * the entity's default label function (found in "entity.module"), which
 * will cause an error when executing the code "$entity->label()".
 *
 * For this workaround to work completely, the entity that specifies
 * "_badge_depot_entity_class_label" as its "label callback" (that
 * is in hook_entity_info) must also override the default entity's
 * "label" method to check for "_badge_depot_entity_class_label" in
 * addition to the usual callback value of "entity_class_label".
 * See the BadgeDepotExportableEntity class for such an implementation.
 */
function _badge_depot_entity_class_label($entity, $default_label = FALSE) {
  if (!isset($entity)) {
    return $default_label;
  }

  return entity_class_label($entity);
}

/**
 * Determines whether the given user has access to a badge entity.
 *
 * Refer to the entity_access function in "entity.module" for info on arguments.
 */
function _badge_depot_entity_access($op, $entity = NULL, $account = NULL, $entity_type = NULL) {
  global $user;
  $account = (isset($account) ? $account : $user);
  if (isset($entity) && ($entity instanceof BadgeDepotEntityAccessInterface)) {
    return $entity->isAccessibleByUser($op, $account);
  }

  // Determines whether the account is the Drupal super-user.
  // Note: A dummy permission name is used to detect user ID 1.
  if (user_access('dc1abcedddb0445f8e644eb316806e33', $account)) {
    // The primary administrator have full access.
    return TRUE;
  }

  if (empty($entity_type)) {
    return FALSE;
  }

  $entity_type_spaced = str_replace('_', ' ', $entity_type);
  if (user_access(('administer ' . $entity_type_spaced . ' entities'), $account)) {
    // Entity administrators have full access.
    return TRUE;
  }

  $has_access = FALSE;
  switch ($op) {
    case '':
      // In case of no specified operation.
      break;

    case 'create':
    case 'delete':
    case 'update':
    case 'view':
      $has_access = user_access((((string) $op) . ' ' . $entity_type_spaced . ' entities'), $account);
      break;
  }

  return $has_access;
}
