<?php
/**
 * @file
 * Contains BadgeDepotExportableEntity.
 */

/**
 * Base class for data entities of this module.
 */
class BadgeDepotExportableEntity extends Entity implements BadgeDepotEntityAccessInterface {

  // Define fields that map to general database columns of this entity.
  public $created;
  public $changed;
  public $module;

  /**
   * Override this in a subclass to return the default table name of
   * the entity in the database.
   */
  protected static function getDefaultEntityType() {
    return NULL;
  }

  /**
   * Override this in a subclass to return the name of the database
   * column that stores the machine-readable name of the entity, if
   * defined for this entity type.
   */
  protected static function getDefaultMachineNameKey() {
    $machine_name_key = NULL;
    $entity_type = static::getDefaultEntityType();
    if (isset($entity_type) && ($entity_type !== '')) {
      $entity_info = entity_get_info($entity_type);
      if (isset($entity_info['entity keys']['name'])) {
        // For example, "badge_machine_name" or simply "name".
        $machine_name_key = $entity_info['entity keys']['name'];
      }
    }

    return $machine_name_key;
  }

  public function __construct(array $values = array(), $entity_type = NULL) {
    static::ensureEntityType($entity_type);
    if (!isset($values)) {
      $values = array();
    }

    parent::__construct($values, $entity_type);
    if (!isset($this->{$this->statusKey})) {
      // Ensure that we have a bit-mask field (by default called "status")
      // with values defined in "entity.module", such as ENTITY_CUSTOM:
      // undefined (0), custom (1), default (2), overridden (3) or fixed (6).
      // Exportable entities will almost always be custom data by default.
      $this->{$this->statusKey} = ENTITY_CUSTOM;
    }
  }

  protected static function ensureEntityType(&$entity_type) {
    if ((!isset($entity_type)) || ($entity_type === '')) {
      $entity_type = static::getDefaultEntityType();
    }
  }

  protected static function ensureMachineNameKey(&$machine_name_key) {
    if ((!isset($machine_name_key)) || ($machine_name_key === '')) {
      $machine_name_key = static::getDefaultMachineNameKey();
    }
  }

  public static function loadFromMachineName($machine_name) {
    // Get a single value only.
    $get_all_values = FALSE;
    return static::loadMultipleFromMachineName($machine_name, $get_all_values);
  }

  public static function loadMultipleFromMachineName($machine_name, $get_all_values = TRUE) {
    if (!isset($machine_name)) {
      return NULL;
    }

    $machine_name_key = static::getDefaultMachineNameKey();
    return static::loadFromValueMatch($machine_name_key, $machine_name, $get_all_values);
  }

  public static function load($id) {
    if (is_numeric($id) && (/* not a machine name */ strlen($id) <= (1 + strlen(PHP_INT_MAX)))) {
      $entity_type = static::getDefaultEntityType();
      if (!empty($entity_type)) {
        $entities = entity_load($entity_type, array($id));
        if (is_array($entities) && (!empty($entities))) {
          // Extract the first entity -- there should be one at most.
          return reset($entities);
        }
      }
    }

    return NULL;
  }

  public static function loadMultiple() {
    $column_name = NULL;
    $match_value = NULL;
    $get_all_values = TRUE;
    return static::loadFromValueMatch($column_name, $match_value, $get_all_values);
  }

  protected static function loadFromValueMatch($column_name, $match_value, $get_all_values = FALSE, $delegate_before_execute = NULL, $delegate_context_data = NULL) {
    $entity_type = static::getDefaultEntityType();
    if (empty($entity_type) || ((!empty($column_name)) && (!isset($match_value)))) {
      return NULL;
    }

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $entity_type, '=');
    if (!empty($column_name)) {
      $query->propertyCondition($column_name, (string) $match_value, '=');
    }

    if (isset($delegate_before_execute)) {
      $delegate_before_execute($query, $delegate_context_data);
    }

    $result = $query->execute();
    $entities = NULL;
    if (!empty($result[$entity_type])) {
      $entities = entity_load($entity_type, array_keys($result[$entity_type]));
    }

    if (!$get_all_values) {
      if (is_array($entities) && (!empty($entities))) {
        // Extract the first entity.
        return reset($entities);
      }

      // No entity of this type exists.
      return NULL;
    }

    return $entities;
  }

  /**
   * Provides a helper method to implement the special "auto-loader"
   * functions of entities that are used as URL path wildcard components.
   *
   * Refer to Drupal's hook_menu documentation for additional details.
   */
  public static function autoLoad($identifier_or_machine_name = NULL) {
    $entity = FALSE;
    if (!empty($identifier_or_machine_name)) {
      $entity = static::load($identifier_or_machine_name);
      if (!$entity) {
        $entity = static::loadFromMachineName($identifier_or_machine_name);
      }
    }

    return ($entity ? $entity : FALSE);
  }

  /**
   * Retrieves the name of the field that store the machine-name.
   */
  protected function getMachineNameKey() {
    return (isset($this->entityInfo['entity keys']['name']) ? ($this->entityInfo['entity keys']['name']) : NULL);
  }

  /**
   * Determines whether this entity defines a machine-name field that should be populated.
   */
  public function isMachineNameRequired() {
    $machine_name_key = $this->getMachineNameKey();
    return (!empty($machine_name_key));
  }

  /**
   * Determines whether this entity has an associated machine-name that is populated.
   */
  public function hasMachineName() {
    $machine_name = $this->getMachineName();
    return (!empty($machine_name));
  }

  /**
   * Retrieves this entity's machine-name.
   */
  public function getMachineName() {
    $machine_name_key = $this->getMachineNameKey();
    return (empty($machine_name_key) ? NULL : $this->$machine_name_key);
  }

  /**
   * Updates this entity's machine-name to the specified value,
   * if specified, or otherwise with an auto-generated name.
   */
  public function setMachineName($machine_name = NULL) {
    $machine_name_key = $this->getMachineNameKey();
    if (!empty($machine_name_key)) {
      // Generate a new machine-name if none was specified.
      if (empty($machine_name)) {
        $machine_name = hash('md5', uniqid(($this->entityType() . ':' . $this->title), TRUE));
      }

      // Set this entity's machine-name.
      $this->$machine_name_key = $machine_name;

      // Returns the current machine name.
      return $machine_name;
    }

    // A machine name is not supported by this entity.
    return NULL;
  }

  /**
   * Overrides the base method that permanently saves the entity.
   */
  public function save() {
    if ($this->isMachineNameRequired() && (!$this->hasMachineName())) {
      $this->setMachineName();
    }

    // Set all not-null text fields without values to empty strings.
    foreach ($this->getNonNullableTextFieldNames() as $property_name) {
      if (!isset($this->$property_name)) {
        $this->$property_name = '';
      }
    }

    // Set the creation time-stamp, if not yet initialized.
    if (empty($this->created)) {
      $this->created = time();
    }

    // Set the update time-stamp.
    $this->changed = time();
    return parent::save();
  }

  /**
   * Returns an array containing the names of all the text fields
   * that may not be null.  Subclasses are to implement this method
   * and append the names of any such fields defined for the entity.
   * NOTE: The varchar type fields should generally not be included;
   *       only fields with "type" set to "text" and with "not null"
   *       set to TRUE should be included.
   */
  protected function getNonNullableTextFieldNames() {
    // TODO: Determine whether this default implementation cannot simply
    //       determine the names directly from the entity's schema-info.
    return array();
  }

  /**
   * Overrides the base method that specifies the default label for this entity.
   * NOTE: This override is done primarily to accommodate the workaround for the
   *       entity-import error solved through _badge_depot_entity_class_label.
   */
  public function label() {
    if (isset($this->entityInfo['label callback']) && $this->entityInfo['label callback'] == '_badge_depot_entity_class_label') {
      return $this->defaultLabel();
    }

    return parent::label();
  }

  /**
   * Determines whether this entity is owned by the specified user.
   */
  public function isOwnerUser($account = NULL) {
    return FALSE;
  }

  /**
   * Implements BadgeDepotEntityAccessInterface.
   *
   * Determines whether this entity can be accessed by the specified user.
   */
  public function isAccessibleByUser($operation, $account = NULL) {
    global $user;
    $account = (isset($account) ? $account : $user);
    $entity_type_spaced = str_replace('_', ' ', $this->entityType());

    // Includes super-user access.
    $is_administrator = user_access(('administer ' . $entity_type_spaced . ' entities'), $account);
    if ($is_administrator) {
      // Administrators have full access.
      return TRUE;
    }

    $is_owner = $this->isOwnerUser($account);
    $has_access = FALSE;
    switch ($operation) {
      case '':
        // In case of no specified operation.
        break;
      case 'create':
      case 'delete':
      case 'update':
      case 'view':
        $has_access = ($is_owner || user_access((((string) $operation) . ' ' . $entity_type_spaced . ' entities'), $account));
        break;
    }

    return $has_access;
  }

  /**
   * Invokes a method on an instance of an entity class.
   *
   * @param $entity
   *   The entity object in which the method should be invoked.
   * @param $method_name
   *   The name of the method to be invoked.
   *
   * @return
   *   The result that was returned by the called method, if any.
   */
  public static function invokeMethod($entity, $method_name) {
    $result = NULL;
    if (is_a($entity, get_called_class()) && method_exists($entity, $method_name)) {
      if (func_num_args() > 2) {
        // Forward the specified additional arguments to the method.
        $args = array_slice(func_get_args(), 2);
        $result = call_user_func_array(array($entity, $method_name), $args);
      }
      else {
        // Call the method without any arguments.
        $result = $entity->$method_name();
      }
    }

    return $result;
  }

  /**
   * Implements a page callback that prepares an entity for rendering on an
   * HTML page; however, the callback can also be used to degrade gracefully
   * for the case when no entity data is available.
   */
  public static function requestView($entity = NULL, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    if (!$entity) {
      drupal_set_message(t('No entity is available for rendering, such that processing could not complete.'), 'error');

      // Render a blank page with the error message from above.
      return array();
    }

    return $entity->view($view_mode, $langcode, $page);
  }
}
