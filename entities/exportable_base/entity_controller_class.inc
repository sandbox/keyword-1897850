<?php
/**
 * @file
 * Contains BadgeDepotExportableEntityController.
 */

/**
 * Base controller class for data entities of this module.
 */
class BadgeDepotExportableEntityController extends EntityAPIControllerExportable {

  /**
   * Implements a helper method to build content for an entity
   * that can be called from the entity's buildContent method.
   */
  protected function buildEntityFieldContent(&$content, $property_name, &$properties, &$wrapper, $entity, $custom_values = NULL) {
    $data = NULL;
    if (!empty($entity->$property_name)) {
      $data = $entity->$property_name;

      if (!isset($properties)) {
        $property_info = entity_get_property_info($entity->entityType());
        $properties = $property_info['properties'];
      }

      if (!isset($wrapper)) {
        $wrapper = entity_metadata_wrapper($entity->entityType(), $entity);
      }

      $content[$property_name] = array(
        '#theme' => 'badge_depot_entity_field_data',
        '#theme_wrappers' => array('form_element'),
        '#title' => $properties[$property_name]['label'],
        '#object' => $entity,
        '#field_name' => $property_name,
        '#data' => $data,
        '#data_wrapper' => $wrapper->$property_name,
        //'#description' => $properties[$property_name]['description'],
      );

      if (isset($custom_values)) {
        $content[$property_name] = array_merge($content[$property_name], $custom_values);
      }
    }

    return $data;
  }

  /**
   * Overrides the current EntityAPIControllerInterface implementation
   * that exports an entity as a serialized string in JSON format.  This
   * overridden implementation, in conjunction with the exportCleanup method,
   * provides equivalent functionality as the base class, but exportCleanup
   * allows for additional filtering to be provided by derived classes.
   *
   * @param $entity
   *   The entity to export.
   * @param $prefix
   *   An optional prefix for each line.
   *
   * @return
   *   A serialized string in JSON format suitable for the import() method.
   */
  public function export($entity, $prefix = '') {
    $vars = get_object_vars($entity);
    $this->exportCleanup($vars, $entity);
    return entity_var_json_export($vars, $prefix);
  }

  /**
   * Tweaks the extracted data to be exported for an entity; for
   * example, it filters out variables that should not be exported.
   * This implementation, in conjunction with the overridden export
   * method, provides equivalent functionality as the base class, but it
   * allows for additional filtering to be provided by derived classes.
   *
   * @param array $vars
   *   The extracted properties of the entity, which will be serialized.
   *   Modify the content of this associative array to change the data
   *   that will be serialized to a JSON string.
   * @param $entity
   *   The original entity that is being exported.
   */
  protected function exportCleanup(array &$vars, $entity) {
    unset($vars[$this->statusKey], $vars[$this->moduleKey], $vars['is_new']);
    if ($this->nameKey != $this->idKey) {
      unset($vars[$this->idKey]);
    }
  }

  /**
   * Overrides the current EntityAPIControllerInterface implementation
   * that imports an entity from a serialized string in JSON format.  This
   * overridden implementation, in conjunction with the importCleanup method,
   * provides equivalent functionality as the base class, but importCleanup
   * allows for additional filtering to be provided by derived classes.
   *
   * @param string $export
   *   An exported entity, serialized as a string in JSON format,
   *   as produced by the export() method.
   *
   * @return
   *   An entity object that is not yet saved.
   */
  public function import($export) {
    $vars = drupal_json_decode($export);
    if (is_array($vars)) {
      $this->importCleanup($vars, $export);
      return $this->create($vars);
    }

    return FALSE;
  }

  /**
   * Tweaks the deserialized data to be imported into an entity; for
   * example, it can add or modify variables that should be imported.
   * This implementation, in conjunction with the overridden import
   * method, provides equivalent functionality as the base class, but it
   * allows for additional editing to be provided by derived classes.
   *
   * @param array $vars
   *   The deserialized properties of the entity, which will be imported.
   *   Modify the content of this associative array to change the data
   *   that will be used to create a new entity object.
   * @param $export
   *   The original JSON string representing the serialized entity
   *   that is being imported.
   */
  protected function importCleanup(array &$vars, $export) {
  }

  /**
   * Provides a helper method to convert JSON string-valued variables
   * during exporting to something that looks nicer in the exported data.
   *
   * @param string $json_variable_name
   *   The name of the JSON type variable to be cleaned up for exporting.
   * @param array $vars
   *   The extracted properties of the entity, which will be serialized.
   *   If successful, this method will modify the named variable in this array.
   * @param $entity
   *   The original entity that is being exported.
   */
  protected function exportCleanupForJsonString($json_variable_name, array &$vars, $entity) {
    $json_variable = &$vars[$json_variable_name];
    if (is_string($json_variable) && (!empty($json_variable))) {
      $json_data = drupal_json_decode($json_variable);
      if (isset($json_data)) {
        $json_variable = $json_data;
      }
    }
  }

  /**
   * Provides a helper method to convert JSON string-valued variables
   * during importing from something that looks nicer in the exported
   * data to the original string format being used internally.
   *
   * @param string $json_variable_name
   *   The name of the JSON type variable to be cleaned up for importing.
   * @param array $vars
   *   The deserialized properties of the entity, which will be imported.
   *   If successful, this method will modify the named variable in this array.
   * @param $export
   *   The original JSON string representing the serialized entity
   *   that is being imported.
   */
  protected function importCleanupForJsonString($json_variable_name, array &$vars, $export) {
    $json_variable = &$vars[$json_variable_name];
    if (isset($json_variable) && (!is_string($json_variable))) {
      $json_string = _badge_depot_json_encode_pretty($json_variable);
      if ($json_string !== FALSE) {
        $json_variable = $json_string;
      }
      else {
        drupal_set_message(t('Failed to import JSON variable %variableName.', array('%variableName' => $json_variable_name)), 'warning');
        $json_variable = '';
      }
    }
  }
}
