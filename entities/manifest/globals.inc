<?php
/**
 * @file
 * Provides global functions and other definitions, within the context of this
 * entity, which must always be available when this module is installed, such
 * as for various callback functions.
 */

// Include all dependency files.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';
require_once dirname(__DIR__) . '/globals.inc';

// Specify the database table name that is used for storing the badge-manifest entities.
define('BADGE_DEPOT_ENTITY_TABLE_MANIFEST', 'badge_manifest');

// Specify the database column name that is used for storing the badge-manifest's machine-readable name.
define('BADGE_DEPOT_ENTITY_COLUMN_MANIFEST_MACHINE_NAME', 'manifest_machine_name');

// Specify the URLs for accessing data related to badge-manifest entities.
define('BADGE_DEPOT_MAIN_MANIFEST_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'manifest'));
define('BADGE_DEPOT_ADMIN_MANIFEST_PATH', (BADGE_DEPOT_ADMIN_PATH_PREFIX . 'manifest'));

// Implements a general loader for badge-manifest entities from URL path parameters.
// NOTE: Based on naming convention that Drupal follows for wildcard segments
//       in URL paths, this function will automatically be called to load data
//       for path keys that contain %badge_depot_entities_manifest as a segment.
function badge_depot_entities_manifest_load($manifest_id = NULL) {
  return BadgeManifestEntity::autoLoad($manifest_id);
}
