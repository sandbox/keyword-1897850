<?php
/**
 * @file
 * Contains BadgeManifestEntity.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Main class for badge-class manifest entities (that is OBI badge definitions).
 */
class BadgeManifestEntity extends BadgeDepotObiBaseEntity {

  /**
   * Overrides the base method that returns the default name of the
   * database table for this entity type.
   */
  protected static function getDefaultEntityType() {
    return BADGE_DEPOT_ENTITY_TABLE_MANIFEST;
  }

  /**
   * Overrides the base method that returns the name of the database
   * column that stores the machine-readable name of the entity.
   */
  protected static function getDefaultMachineNameKey() {
    return BADGE_DEPOT_ENTITY_COLUMN_MANIFEST_MACHINE_NAME;
  }

  /**
   * Overrides the base method that returns the base URL path for all instances
   * of this entity type (that is not for a single specific entity instance).
   */
  protected static function getEntityMainRootPath() {
    return BADGE_DEPOT_MAIN_MANIFEST_PATH;
  }

  /**
   * Overrides the base method that returns the base URL path to administer
   * this entity type (that is not for a single entity instance).
   */
  protected static function getEntityAdminRootPath() {
    return BADGE_DEPOT_ADMIN_MANIFEST_PATH;
  }

  /**
   * Overrides the base method that lists all the not-null text fields.
   */
  protected function getNonNullableTextFieldNames() {
    return (parent::getNonNullableTextFieldNames() +
      array(
        'body',
        'image_url',
        'tags',
        'alignments',
      )
    );
  }

  // Define fields that map to specific database columns of this entity:
  public $manifest_id;

  public $manifest_machine_name;

  // Title is the name of the badge-class (that is badge manifest).
  public $title;

  public $description;

  public $body;

  public $is_dormant;

  public $is_stem_aligned;

  public $issuer_id;

  public $issuer_machine_name;

  public $image_url;

  public $criteria_url;

  public $activity_url;

  public $tags;

  public $alignments;

  /**
   * Overrides the base method that permanently saves the entity.
   */
  public function save() {
    if (!isset($this->issuer_id)) {
      $this->issuer_id = 0;
    }

    // Keep the issuer's machine-name and ID fields synchronized.
    $issuer = $this->getIssuer();
    $this->issuer_machine_name = (isset($issuer->issuer_machine_name) ? $issuer->issuer_machine_name : '');

    return parent::save();
  }

  /**
   * Gets the entity object representing the issuer-organization that is associated with this badge.
   *
   * @return
   *   The issuer entity of this badge.
   */
  public function getIssuer() {
    static $issuer = NULL;
    static $issuer_id = NULL;
    if ((!isset($issuer)) || ($issuer_id !== $this->issuer_id)) {
      $issuer = NULL;
      $issuer_id = $this->issuer_id;
      if (isset($issuer_id) && ($issuer_id !== 0)) {
        $issuer = BadgeIssuerEntity::load($issuer_id);
      }
    }

    return $issuer;
  }

  /**
   * Gets the path of the endpoint where the OBI badge-class manifest data is hosted.
   *
   * @return
   *   The local path where the manifest data is being served in JSON format.
   */
  public function getLocalJsonDataPath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/obi-data';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the badge-definition JSON data.
   *
   * The format of the linked data is as specified for OBI "BadgeClass".
   *
   * @return
   *   The URL where the badge-class manifest data can be found.
   */
  public function getJsonDataUrl() {
    $url = $this->getLocalJsonDataPath();
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the local endpoint where the OBI Badge Issuer data is served.
   *
   * @return
   *   The local path where the issuer data is being served in JSON format.
   */
  public function getLocalIssuerPath() {
    $url = NULL;
    $issuer = $this->getIssuer();
    if (isset($issuer)) {
      $url = $issuer->getLocalJsonDataPath();
    }
    elseif (!empty($this->issuer_machine_name)) {
      $url = (BADGE_DEPOT_MAIN_ISSUER_PATH . '/' . $this->issuer_machine_name . '/obi-data');
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the badge-issuer JSON data.
   *
   * The format of the linked data is as specified for OBI "IssuerOrganization".
   *
   * @return
   *   The URL where the badge's issuer data can be found.
   */
  public function getIssuerUrl() {
    $url = $this->getLocalIssuerPath();
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the path of the badge image when served locally.
   *
   * @return
   *   The local path where the image is being served.
   */
  public function getLocalImagePath() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/image';
    }

    return $url;
  }

  /**
   * Gets the fully-qualified URL of the unbaked badge image,
   * which can be a local or remote URL, or even a Data URI.
   *
   * @param $allow_data_uri
   *   TRUE to indicate that the URL may be a Data URI, or FALSE if a hosted
   *   alternative URL should be provided when the image URL is a Data URI.
   *
   * @return
   *   The URL where the badge raw image can be found.
   */
  public function getImageUrl($allow_data_uri = TRUE) {
    $url = $this->image_url;
    if ((!$allow_data_uri) && DataUriCreator::isDataUri($url)) {
      $url = $this->getLocalImagePath();
    }

    return DataUriCreator::getDataUriOrQualifiedUrl($url);
  }

  /**
   * Gets the permalink path of the Badge Criteria page that is served
   * locally, which in turn redirects to the actual Badge Criteria
   * page that may be served locally or remotely.
   *
   * @return
   *   The local path where the criteria HTML page is being served.
   */
  public function getLocalCriteriaPermalink() {
    $url = $this->getEntityViewPath();
    if (!empty($url)) {
      $url .= '/criteria';
    }

    return $url;
  }

  /**
   * Gets the path of the Badge Criteria page that is served locally.
   *
   * @return
   *   The local path where the criteria HTML page is being served.
   */
  public function getLocalCriteriaPath() {
    // By default, the manifest entity's regular
    // view can also serve as a badge-criteria page.
    return $this->getEntityViewPath();
  }

  /**
   * Gets the fully-qualified URL of the Badge Criteria page,
   * which can be a local or remote page.
   *
   * @return
   *   The URL where the badge's criteria page can be found.
   */
  public function getCriteriaUrl() {
    $url = ((!empty($this->criteria_url)) ? $this->criteria_url : $this->getLocalCriteriaPath());
    if (!empty($url)) {
      $url = url($url, array('absolute' => TRUE));
    }

    return $url;
  }

  /**
   * Gets the tags as an array of strings.
   */
  public function getTagsAsArray() {
    $tags = $this->tags;
    if (isset($tags) && (!is_array($tags))) {
      if (is_string($tags)) {
        // Reduce "\r\n" to "\n" only.
        $tags = str_replace("\r", '', $tags);
        if (empty($tags)) {
          $tags = array();
        }
        else {
          // Split on newlines (that is one tag per line).
          $tags = explode("\n", $tags);
        }
      }
      else {
        // Unhandled data type.
        $tags = NULL;
      }
    }

    if (!isset($tags)) {
      // Always return an array, never NULL.
      $tags = array();
    }

    return $tags;
  }

  /**
   * Gets the JSON representation of this badge-class manifest,
   * formatted as required by the OBI Badge Assertion
   * specification, version 1.0 (see section "BadgeClass").
   *
   * @return
   *   An array containing the entity's JSON data.
   */
  public function getJsonData() {
    $json_data = array(
      'name' => $this->title,
      'description' => $this->description,
      'criteria' => $this->getCriteriaUrl(),
      'issuer' => $this->getIssuerUrl(),
    );

    if (!empty($this->alignments)) {
      $alignments = drupal_json_decode($this->alignments);
      if (is_array($alignments) && (!empty($alignments))) {
        $json_data['alignment'] = $alignments;
      }
    }

    $tags = $this->getTagsAsArray();
    if (!empty($tags)) {
      $json_data['tags'] = $tags;
    }

    $image_url = $this->getImageUrl(BadgeDepotGlobalSettings::getObiJsonAllowDataUriFlag());
    $json_data['image'] = $image_url;
    return $json_data;
  }

  /**
   * Implements a web-service endpoint for serving a JSON representation
   * of this entity's data that is formatted as per version 1.0 of the
   * OBI Badge Assertion specification (see section "BadgeClass").
   *
   * @return
   *   Upon success, an array containing the entity's data for JSON-rendering
   *   purposes; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED).
   *   The return value should be processed by this module's JSON delivery
   *   callback function (that is badge_depot_deliver_json).
   */
  public function requestJsonData() {
    if (!$this->isAccessibleByUser('view')) {
      return MENU_ACCESS_DENIED;
    }

    return $this->getJsonData();
  }

  /**
   * Returns the URL for the badge-criteria page's redirection request;
   * this is the implementation of the permalink page that redirects to
   * the actual Criteria HTML page that may be served locally or remotely.
   *
   * @return
   *   Upon success, the endpoint URL of the criteria page for redirection.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestCriteria() {
    return $this->getCriteriaUrl();
  }

  /**
   * Returns the page-request response for the image of the badge manifest.
   *
   * @return
   *   Upon success, the Data URI or regular URL of the image.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestImage() {
    return $this->image_url;
  }

  /**
   * Returns the URL for the badge-issuer page's redirection request;
   * this is the implementation of a convenience permalink page that
   * redirects to the actual Issuer JSON data related to this badge.
   *
   * @return
   *   Upon success, endpoint URL of the issuer JSON to be redirected to.
   *   The return value should be processed by this module's Data URI
   *   delivery callback function (that is badge_depot_deliver_data_uri).
   */
  public function requestIssuer() {
    return $this->getLocalIssuerPath();
  }

  /**
   * Loads all badge manifests that match the specified filter criteria,
   * such as for a specific issuer-organization.
   *
   * @param $issuer_id
   *   The identifier of the issuer-organization to be matched, or
   *   NULL to ignore.  An array with multiple IDs can also be specified.
   *
   * @param $is_dormant
   *   An optional boolean flag that indicates whether badge manifests that
   *   are dormant should be included or not.  Specify NULL to include
   *   manifests irrespetive of their dormancy state, or alternatively
   *   specify TRUE or FALSE to match the manifest's current dormancy state.
   *
   * @param $is_published
   *   An optional boolean flag that indicates whether badge manifests that
   *   are published should be included or not.  Specify NULL to include
   *   manifests irrespetive of their publication state, or alternatively
   *   specify TRUE or FALSE to match the manifest's current publication state.
   *
   * @param $is_stem_aligned
   *   An optional boolean flag that indicates whether badge manifests that
   *   are STEM-aligned should be included or not.  Specify NULL to include
   *   manifests irrespetive of their alignment state, or alternatively
   *   specify TRUE or FALSE to match the manifest's current alignment state.
   *
   * @param $owner_user_id
   *   The user identifier of the entity's owner to be matched, or NULL
   *   to ignore.  An array with multiple IDs may also be specified.
   *
   * @return
   *   Upon success, an array containing the requested manifests entities;
   *   otherwise NULL.
   */
  public static function loadMultipleThroughFilter($issuer_id = NULL, $is_dormant = NULL, $is_published = NULL, $is_stem_aligned = NULL, $owner_user_id = NULL) {
    if ((!isset($issuer_id)) && (!isset($is_dormant)) && (!isset($is_published)) && (!isset($is_stem_aligned)) && (!isset($owner_user_id))) {
      return NULL;
    }

    $context_data = array();
    if (isset($issuer_id)) {
      $context_data['issuer_id'] = (is_array($issuer_id) ? $issuer_id : (integer) $issuer_id);
    }

    if (isset($is_dormant)) {
      $context_data['is_dormant'] = ($is_dormant ? 1 : 0);
    }

    if (isset($is_published)) {
      $context_data['is_published'] = ($is_published ? 1 : 0);
    }

    if (isset($is_stem_aligned)) {
      $context_data['is_stem_aligned'] = ($is_stem_aligned ? 1 : 0);
    }

    if (isset($owner_user_id)) {
      $context_data['owner_user_id'] = (is_array($owner_user_id) ? $owner_user_id : (integer) $owner_user_id);
    }

    $get_all_values = TRUE;
    return static::loadFromValueMatch(
      NULL,
      NULL,
      $get_all_values,
      function($query, $context_data) {
        foreach ($context_data as $property_name => $property_value) {
          $query->propertyCondition($property_name, $property_value);
        }
      },
      $context_data
    );
  }
}
