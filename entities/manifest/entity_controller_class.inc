<?php
/**
 * @file
 * Contains BadgeDepotManifestEntityController.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Controller class for badge-manifest entities.
 */
class BadgeDepotManifestEntityController extends BadgeDepotObiBaseEntityController {

  /**
   * Overrides the base method that builds a structured array
   * representing the entity's content for the supported view-modes.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    if (!is_array($content)) {
      $content = array();
    }

    // LRMI microdata for the badge Criteria HTML page.
    $content['#prefix'] = &$lrmi_page_prefix;
    $content['#suffix'] = &$lrmi_page_suffix;
    $lrmi_page_prefix =
      '<div id="lrmiBadgeCriteria" itemscope="itemscope" itemtype="http://schema.org/WebPage/BadgeCriteria">' . PHP_EOL
      . '<meta itemprop="additionalType" content="http://starlitebadges.com/schemas/Badge/" />' . PHP_EOL
      . '<meta itemprop="additionalType" content="http://www.lrmi.net/the-specification" />' . PHP_EOL
      . '<meta itemprop="name" content="' . htmlspecialchars($entity->title) . '" />' . PHP_EOL
      . '<meta itemprop="description" content="' . htmlspecialchars($entity->description) . '" />' . PHP_EOL
      . '<meta itemprop="image" content="' . htmlspecialchars($entity->getImageUrl(FALSE)) . '" />' . PHP_EOL;
    $lrmi_page_suffix = '</div>';

    $properties = NULL;
    $wrapper = NULL;
    $this->buildEntityFieldContent($content, 'image_url', $properties, $wrapper, $entity, array('#title' => '', '#theme' => 'badge_depot_image', '#width' =>  90, '#path' => $entity->image_url, '#alt' => check_plain($entity->title)));
    $this->buildEntityFieldContent($content, 'title', $properties, $wrapper, $entity, array('#title' => ''));
    $this->buildEntityFieldContent($content, 'description', $properties, $wrapper, $entity, array('#title' => '', '#theme' => NULL, '#markup' => check_plain($entity->description)));
    if ($view_mode === 'full') {
      $this->buildEntityFieldContent($content, 'body', $properties, $wrapper, $entity, array('#title' => '', '#theme' => NULL, '#markup' => check_markup($entity->body, 'full_html', $langcode)));

      if ($entity->is_stem_aligned) {
        $content['is_stem_aligned'] = array(
          '#markup' => ('<p>' . t('The badge appears to be aligned with STEM content (that is Science, Technology, Engineering and Mathematics).') . '</p>'),
        );
      }

      $issuer = $entity->getIssuer();
      if (isset($issuer)) {
        // LRMI microdata for the badge-issuer organization name; expected
        // within an itemscope of itemtype "http://schema.org/Organization".
        $issuer_markup = '<meta itemprop="name" content="' . htmlspecialchars($issuer->title) . '" />';
        $issuer_markup .= l(
          check_plain($issuer->title),
          $issuer->getEntityViewPath(),
          array(
            // LRMI microdata for the badge-issuer organization URL; expected
            // within an itemscope of itemtype "http://schema.org/Organization".
            'attributes' => array(
              'target' => '_blank',
              'itemprop' => 'url',
              'rel' => 'publisher',
            ),
          )
        );
        if (!$issuer->isAccessibleByUser('view')) {
          $issuer_markup .= ' ' . t('(access restricted)');
        }
        elseif (!$issuer->is_published) {
          $issuer_markup .= ' ' . t('(no public access)');
          if ($entity->is_published) {
            drupal_set_message(filter_xss(t('The <a href="!viewUrl">link to the Issuer</a> is currently not published publicly. <a href="!editUrl">Edit the badge-issuer</a> entity to change this.', array('@title' => $issuer->title, '!editUrl' => url($issuer->getEntityEditPath()), '!viewUrl' => url($issuer->getEntityViewPath())))), 'warning');
          }
        }
      }
      else {
        $issuer_markup = t('The issuer organization for this badge could not be found!');
      }

      $content['issuer'] = array(
        '#theme_wrappers' => array('form_element'),
        '#theme' => NULL,
        '#object' => $entity,
        '#data' => $issuer,
        '#title' => t('Issuer'),
        '#markup' => $issuer_markup,
        // LRMI microdata for the badge-issuer organization.
        '#prefix' => '<div id="lrmiBadgeIssuer" itemprop="publisher" itemscope="itemscope" itemtype="http://schema.org/Organization">',
        '#suffix' => '</div>',
      );

      $this->buildEntityFieldContent(
        $content,
        'activity_url',
        $properties,
        $wrapper,
        $entity,
        array(
          '#title' => t('Activity Page'),
          '#theme' => NULL,
          '#markup' => l(
            check_plain($entity->activity_url),
            $entity->activity_url,
            array(
              // LRMI microdata for the badge activity-page
              // URL; expected within an itemscope of itemtype
              // "http://schema.org/WebPage/BadgeCriteria".
              'attributes' => array(
                'id' => 'lrmiBadgeActivity',
                'target' => '_blank',
                'itemprop' => 'url',
              ),
            )
          ),
        )
      );

      $badge_criteria_url = $entity->getCriteriaUrl();
      if (!empty($badge_criteria_url)) {
        $content['criteria_url'] = array(
          '#theme_wrappers' => array('form_element'),
          '#title' => t('Criteria Page'),
          '#markup' => l($badge_criteria_url, $badge_criteria_url, array('attributes' => array('target' => '_blank'))),
        );

        // Add a button that links to Google's Structured Data Testing Tool
        // to analyze the the criteria page.
        $content['criteria_url_inspect'] = array(
          '#type' => 'button',
          '#value' => t('Inspect with Google Tool'),
          // Override client-side JavaScript action and disable form submission.
          '#attributes' => array('onclick' => 'window.open("' . BADGE_DEPOT_GOOGLE_STRUCTURED_DATA_TESTING_TOOL_URL_PREFIX . urlencode($badge_criteria_url) . '", "_blank"); return false;'),
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );
      }

      // Render alignments.
      if (!empty($entity->alignments)) {
        $alignments = drupal_json_decode($entity->alignments);
        if (is_array($alignments) && (!empty($alignments))) {
          $alignment_items = array();
          foreach ($alignments as $alignment) {
            // Required AlignmentObject property for OBI Assertions 1.0 spec.
            $alignment_name = $alignment['name'];

            // Required AlignmentObject property for OBI Assertions 1.0 spec.
            $alignment_url = $alignment['url'];

            // Optional AlignmentObject property for OBI Assertions 1.0 spec.
            $alignment_description = (isset($alignment['description']) ? $alignment['description'] : NULL);

            unset($alignment['name']);
            unset($alignment['url']);
            unset($alignment['description']);
            $alignment_markup = l(
              check_plain($alignment_name),
              url($alignment_url, array('absolute' => TRUE)),
              array(
                'attributes' => array('target' => '_blank'),
              )
            );

            if (isset($alignment_description)) {
              $alignment_markup .= (': ' . check_plain($alignment_description));
            }

            // Extend OBI AlignmentObject with LRMI Alignment Object properties.
            // NOTE: For these almost-standard extensions, we do not require
            //       the namespace prefixes to be include as recommended by
            //       OBI for additional properties, but for any other extra
            //       less-standard extensions we definitely should.
            $target_description = (isset($alignment['lrmi.net/the-specification/alignment-object:targetDescription']) ? $alignment['lrmi.net/the-specification/alignment-object:targetDescription'] : isset($alignment['lrmi.net:targetDescription']) ? $alignment['lrmi.net:targetDescription'] : isset($alignment['targetDescription']) ? $alignment['targetDescription'] : $alignment_description);
            $target_name = (isset($alignment['lrmi.net/the-specification/alignment-object:targetName']) ? $alignment['lrmi.net/the-specification/alignment-object:targetName'] : isset($alignment['lrmi.net:targetName']) ? $alignment['lrmi.net:targetName'] : isset($alignment['targetName']) ? $alignment['targetName'] : $alignment_name);
            $target_url = (isset($alignment['lrmi.net/the-specification/alignment-object:targetUrl']) ? $alignment['lrmi.net/the-specification/alignment-object:targetUrl'] : isset($alignment['lrmi.net:targetUrl']) ? $alignment['lrmi.net:targetUrl'] : isset($alignment['targetUrl']) ? $alignment['targetUrl'] : $alignment_url);
            $educational_framework = (isset($alignment['lrmi.net/the-specification/alignment-object:educationalFramework']) ? $alignment['lrmi.net/the-specification/alignment-object:educationalFramework'] : isset($alignment['lrmi.net:educationalFramework']) ? $alignment['lrmi.net:educationalFramework'] : isset($alignment['educationalFramework']) ? $alignment['educationalFramework'] : NULL);
            $alignment_type = (isset($alignment['lrmi.net/the-specification/alignment-object:alignmentType']) ? $alignment['lrmi.net/the-specification/alignment-object:alignmentType'] : isset($alignment['lrmi.net:alignmentType']) ? $alignment['lrmi.net:alignmentType'] : isset($alignment['alignmentType']) ? $alignment['alignmentType'] : NULL);

            // Allow a common-core-state-standard suggestions
            // (that is for corestandards.org purposes).
            $suggested_ccss = (isset($alignment['astronautacademy.org:suggestedCCSS']) ? $alignment['astronautacademy.org:suggestedCCSS'] : isset($alignment['starlitebadges.com:suggestedCCSS']) ? $alignment['starlitebadges.com:suggestedCCSS'] : isset($alignment['projectwhitecard.com:suggestedCCSS']) ? $alignment['projectwhitecard.com:suggestedCCSS'] : isset($alignment['suggestedCCSS']) ? $alignment['suggestedCCSS'] : NULL);

            if (isset($target_description)) {
              $alignment_markup .= PHP_EOL . '<meta itemprop="targetDescription" content="' . htmlspecialchars($target_description) . '" />';
            }

            if (isset($target_name)) {
              $alignment_markup .= PHP_EOL . '<meta itemprop="targetName" content="' . htmlspecialchars($target_name) . '" />';
            }

            if (isset($target_url)) {
              $alignment_markup .= PHP_EOL . '<meta itemprop="targetUrl" content="' . htmlspecialchars(url($target_url, array('absolute' => TRUE))) . '" />';
            }

            if (isset($educational_framework)) {
              $alignment_markup .= PHP_EOL . '<meta itemprop="educationalFramework" content="' . htmlspecialchars($educational_framework) . '" />';
            }

            if (isset($alignment_type)) {
              $alignment_markup .= PHP_EOL . '<meta itemprop="alignmentType" content="' . htmlspecialchars($alignment_type) . '" />';
            }

            if (isset($suggested_ccss)) {
              $alignment_markup .= PHP_EOL . '<meta itemprop="suggestedCCSS" content="' . htmlspecialchars($suggested_ccss) . '" />';
            }

            $alignment_items[] = array(
              'itemprop' => 'educationalAlignment',
              'itemscope' => 'itemscope',
              'itemtype' => 'http://www.lrmi.net/the-specification/alignment-object',
              'data' => $alignment_markup,
            );
          }

          $content['alignments'] = array(
            '#title' => t('Educational Alignments'),
            '#theme_wrappers' => array('form_element'),
            'content' => array('#theme' => 'item_list', '#items' => $alignment_items, '#type' => 'ul'),
          );
        }
      }

      // Render tags.
      $tags = $entity->getTagsAsArray();
      if (!empty($tags)) {
        $lrmi_page_prefix .= '<meta itemprop="keywords" content="' . htmlspecialchars(implode(',', $tags)) . '" />' . PHP_EOL;

        // Sanitize the tags for HTML output.
        $delegate = function(&$item, $key) {
          $item = check_plain($item);
        };
        array_walk($tags, $delegate);

        $content['badge_tags'] = array(
          '#title' => t('Tags'),
          '#theme_wrappers' => array('form_element'),
          'content' => array('#theme' => 'item_list', '#items' => $tags, '#type' => 'ul'),
        );
      }

      if ($entity->is_dormant) {
        $content['is_dormant'] = array(
          '#markup' => ('<p>' . t('Note: The badge is currently not being awarded.') . '</p>'),
        );
      }

      if (!$entity->is_published) {
        drupal_set_message(filter_xss(t('The data for "@title" is currently not published publicly. <a href="!url">Edit this badge-class manifest</a> entity if you want to change this.', array('@title' => $entity->title, '!url' => url($entity->getEntityEditPath())))), 'warning');
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Overrides the base method that tweaks the extracted data to be exported
   * for an entity; it filters out more variables that should not be exported.
   */
  protected function exportCleanup(array &$vars, $entity) {
    // Remove the owner issuer's ID, since the ID may be
    // different on the importing Drupal installation.
    unset($vars['issuer_id']);

    // Export the tags as an array of string, not as a multi-line text block.
    $vars['tags'] = $entity->getTagsAsArray();

    // Converts JSON data into something that looks nicer for exporting.
    $this->exportCleanupForJsonString('alignments', $vars, $entity);

    parent::exportCleanup($vars, $entity);
  }

  /**
   * Overrides the base method that tweaks the deserialized data to be imported
   * into an entity; it adds or modifies variables that should also be imported.
   */
  protected function importCleanup(array &$vars, $export) {
    // Attempt to locate the issuer's ID.
    $issuer_id = 0;
    $issuer_machine_name = (isset($vars['issuer_machine_name']) ? $vars['issuer_machine_name'] : NULL);
    $issuer = (empty($issuer_machine_name) ? NULL : BadgeIssuerEntity::loadFromMachineName($issuer_machine_name));
    if (isset($issuer->issuer_id)) {
      $issuer_id = $issuer->issuer_id;
    }

    $vars['issuer_id'] = $issuer_id;

    // Convert imported tag arrays to the internal
    // format of a multi-line text block.
    if (is_array($vars['tags'])) {
      $vars['tags'] = implode("\n", $vars['tags']);
    }

    // Converts JSON data to the string formats being used internally.
    $this->importCleanupForJsonString('alignments', $vars, $export);

    parent::importCleanup($vars, $export);
  }
}
