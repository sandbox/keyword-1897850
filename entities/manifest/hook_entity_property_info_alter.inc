<?php
/**
 * @file
 * Contains additional hook_entity_property_info_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes hook_entity_property_info_alter data for the badge-manifest entity.
 */
function _badge_depot_entities_manifest_entity_property_info_alter(&$info) {
  $properties = &$info[BADGE_DEPOT_ENTITY_TABLE_MANIFEST]['properties'];

  $properties['manifest_id'] = array_merge(((array) ($properties['manifest_id'])), array(
    'label' => t('Badge-Manifest ID'),
    'description' => t('The unique identifier of this badge manifest entry.'),
  ));

  $properties['manifest_machine_name'] = array_merge(((array) ($properties['manifest_machine_name'])), array(
    'label' => t('Persistent ID'),
    'description' => t('The persistent unique-identifier of this manifest that can be used for exporting and importing data between machines.'),
  ));

  $properties['owner_user_id'] = array_merge(((array) ($properties['owner_user_id'])), array(
    'label' => t('Owner User ID'),
    'description' => t('A reference to the user that owns this manifest for data-management purposes.'),
    'type' => 'user',
  ));

  $properties['owner_user_name'] = array_merge(((array) ($properties['owner_user_name'])), array(
    'label' => t('Owner User Name'),
    'description' => t('The name of the user that owns this manifest for data-management purposes; used primarily for enhanced security when exporting/importing the data entity.'),
  ));

  $properties['issuer_id'] = array_merge(((array) ($properties['issuer_id'])), array(
    'label' => t('Issuer ID'),
    'description' => t('A reference to the issuer-organization that awards this badge and that is ultimately repsonsible for maintaining the definition\'s accuracy (the entity\'s "issuer_id" property).'),
    'type' => BADGE_DEPOT_ENTITY_TABLE_ISSUER,
  ));

  $properties['issuer_machine_name'] = array_merge(((array) ($properties['issuer_machine_name'])), array(
    'label' => t('Issuer machine-readable ID'),
    'description' => t('The machine-readable name of the issuer-organization that awards this badge (the entity\'s "issuer_machine_name" property).'),
    'type' => 'token',
  ));

  $properties['type'] = array_merge(((array) ($properties['type'])), array(
    'description' => t('The entity type of this manifest entry (used by entity sub-type bundles).'),
  ));

  $properties['title'] = array_merge(((array) ($properties['title'])), array(
    'label' => t('Name'),
    'description' => t('The name of this OBI badge-class manifest (required; treated as non-markup plain text; the entity\'s "title" property).'),
  ));

  $properties['description'] = array_merge(((array) ($properties['description'])), array(
    'label' => t('Description (short)'),
    'description' => t('A short description of this manifest entry (treated as non-markup plain text; the entity\'s "description" property).'),
  ));

  $properties['body'] = array_merge(((array) ($properties['body'])), array(
    'label' => t('Description (long)'),
    'description' => t('The detailed description of this manifest entry (treated as HTML markup; the entity\'s "body" property).'),
  ));

  $properties['is_published'] = array_merge(((array) ($properties['is_published'])), array(
    'label' => t('Is Published?'),
    'description' => t('A boolean flag indicating whether the manifest entry is publicly visible (the entity\'s "is_published" property).'),
    'type' => 'boolean',
  ));

  $properties['is_dormant'] = array_merge(((array) ($properties['is_dormant'])), array(
    'label' => t('Is Dormant?'),
    'description' => t('A boolean flag indicating that the manifest is currently not issuing badges (the entity\'s "is_dormant" property).'),
    'type' => 'boolean',
  ));

  $properties['is_stem_aligned'] = array_merge(((array) ($properties['is_stem_aligned'])), array(
    'label' => t('Is STEM Content?'),
    'description' => t('A boolean flag indicating that the badge content is aligned with STEM education, i.e., Science, Technology, Engineering and Mathematics (the entity\'s "is_stem_aligned" property).'),
    'type' => 'boolean',
  ));

  $properties['created'] = array_merge(((array) ($properties['created'])), array(
    'description' => t('The full date and time, as a timestamp, of when the manifest entry was created.'),
    'type' => 'date',
  ));

  $properties['changed'] = array_merge(((array) ($properties['changed'])), array(
    'description' => t('The full date and time, as a timestamp, of when the manifest entry was most recently saved.'),
    'type' => 'date',
  ));

  $properties['image_url'] = array_merge(((array) ($properties['image_url'])), array(
    'label' => t('Image URL'),
    'description' => t('The URL that links to the unbaked (a.k.a. raw) badge image; it should have a square size (can be a regular URL or <a href="!url" target="_blank" title="Tool to create a Data URI">Data URI</a>; the entity\'s "image_url" property).', array('!url' => url(DATA_URI_CREATOR_PAGE_PATH))),
    'type' => 'uri',
  ));

  $properties['criteria_url'] = array_merge(((array) ($properties['criteria_url'])), array(
    'label' => t('Criteria URL'),
    'description' => t('The URL that links to the badge\'s criteria page (the entity\'s "criteria_url" property). Specify this only if you want an alternative non-default page to be used.'),
    'type' => 'uri',
  ));

  $properties['activity_url'] = array_merge(((array) ($properties['activity_url'])), array(
    'label' => t('Activity URL'),
    'description' => t('The URL that links to the content of the badge (that is, the activity that can be performed to obtain the badge; the entity\'s "activity_url" property).'),
    'type' => 'uri',
  ));

  $properties['tags'] = array_merge(((array) ($properties['tags'])), array(
    'label' => t('Tag List'),
    'description' => t('Optional tags for the badge manifest, which describe the type of achievement.'),
    // TODO: 'type' => 'list<text>',
  ));

  $properties['alignments'] = array_merge(((array) ($properties['alignments'])), array(
    'label' => t('Alignment List'),
    'description' => t('Optional LRMI metadata for this manifest (the entity\'s "alignments" property).'),
    // TODO: 'type' => 'list<struct>', or 'list<text>' or 'text' with additional custom encoding/decoding
    // TODO: 'property info' => array('name' => ('type' => 'text', 'description' => t('Name of the alignment.'), ...), 'url' => ('type' => 'uri', 'description' => t('URL linking to the official description of the standard.'), ...), 'description' => ('type' => 'text', 'description' => t('Short description of the standard.'), ...)),
  ));
}
