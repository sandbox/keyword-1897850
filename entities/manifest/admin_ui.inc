<?php
/**
 * @file
 * Implements the administrative user-interface functions for the manifest entity.
 */

class BadgeManifestAdminPage extends BadgeDepotEntityAdminPage {

  // Define the identifiers of the form fields that contain submission data.
  const FORM_FIELD_TAGS = 'tags';
  const FORM_FIELD_ALIGNMENTS = 'alignments';
  const FORM_FIELD_IMAGE_URL = 'image_url';

  /**
   * Overrides the default build-handler to implement the administrative
   * user-interface form for the badge-manifest entity.
   */
  public function buildDefault($form, &$form_state) {
    $this->getBuildFormExtraArgs($entity, $op);
    $is_new_entity = ($op === 'add');
    $entity_type = $entity->entityType();

    // Includes super-user access.
    $is_admin = user_access('administer ' . $entity_type . ' entities');
    if ($op === 'clone') {
      // Modify the title of the clone.
      $entity->title .= ' (cloned)';

      // Create a new machine-readable ID for the clone.
      $entity->setMachineName();
    }

    $properties = NULL;
    $this->buildEntityFieldAdmin($form, 'title', $properties, $entity, array('#title' => t('Name of Badge'), '#required' => TRUE, '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'description', $properties, $entity, array('#required' => TRUE, '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'body', $properties, $entity, array('#type' => 'textarea'));
    $this->buildEntityFieldAdmin($form, 'is_published', $properties, $entity);
    $this->buildEntityFieldAdmin($form, 'is_dormant', $properties, $entity);
    $this->buildEntityFieldAdmin($form, 'is_stem_aligned', $properties, $entity);

    $owner_user_id = $entity->owner_user_id;
    if ($is_admin) {
      // Administrators have full control over changing
      // the current owner of this badge entity.
      $users = entity_load('user');
      $user_select = array();
      foreach ($users as $user_item) {
        $user_select[$user_item->uid] = $user_item->name;
      }

      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'select', '#options' => $user_select));
    }
    elseif (!empty($owner_user_id)) {
      // Regular users can only see who is the owner of this
      // badge entity, but only when it currently has an owner.
      $users = entity_load('user', array($owner_user_id));
      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'item', '#markup' => theme('username', array('account' => $users[$owner_user_id]))));
    }

    $issuer_select = array();
    $issuers = BadgeIssuerEntity::loadMultiple();
    if (isset($issuers)) {
      foreach ($issuers as $issuer_item) {
        if ($is_admin || $issuer_item->isOwnerUser()) {
          $is_dormant = $issuer_item->is_dormant;
          if ((!$is_new_entity) || (!$is_dormant)) {
            $title = $issuer_item->title;
            if ($is_dormant) {
              $title .= ' (dormant)';
            }

            $issuer_select[$issuer_item->issuer_id] = $title;
          }
        }
      }
    }

    $this->buildEntityFieldAdmin($form, 'issuer_id', $properties, $entity, array('#title' => t('Issuer'), '#required' => TRUE, '#type' => 'select', '#options' => $issuer_select));
    if (empty($issuer_select)) {
      drupal_set_message(filter_xss(t('To create a new badge, your user must be associated with at least one <a href="!url">issuer organization</a>.', array('!url' => url(BADGE_DEPOT_ADMIN_ISSUER_PATH)))), 'warning');
    }
    elseif (count($issuer_select) === 1) {
      // Select the only organization that the user belongs to (the first key).
      reset($issuer_select);
      $form['issuer_id']['#default_value'] = key($issuer_select);
    }

    $form['manifest_image'] = array('#type' => 'fieldset', '#title' => t('Raw Badge Image'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['manifest_image'], self::FORM_FIELD_IMAGE_URL, $properties, $entity, array('#required' => TRUE, '#maxlength' => 350208));
    $form['manifest_image']['activity_launch'] = array(
      '#type' => 'button',
      '#value' => t('View Image'),
      '#attributes' => array(
        // Override client-side JavaScript action and disable form submission.
        'onclick' => $this->buildLaunchJavaScript(
          self::FORM_FIELD_IMAGE_URL,
          'jQuery("#manifest_image_preview").each(function() { this.src = url; }); jQuery("#manifest_image_container").show();',
          t('The image URL is not specified.')
        ),
      ),
      '#suffix' => '<span id="manifest_image_container" style="display:none;"><br /><br /><img id="manifest_image_preview" /></span>',
    );

    $criteria_path = $entity->getLocalCriteriaPath();
    $criteria_path = (empty($criteria_path) ? $criteria_path : url($criteria_path, array('absolute' => TRUE)));
    $form['criteria_page'] = array(
      '#type' => 'fieldset',
      '#title' => t('Criteria Page'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#suffix' => '<input type="hidden" id="criteria_url_default" value="' . htmlspecialchars($criteria_path) . '" />',
    );
    $this->buildEntityFieldAdmin($form['criteria_page'], 'criteria_url', $properties, $entity, array('#maxlength' => 255));
    $form['criteria_page']['criteria_url']['#description'] .= t(
      ' If not specified, the criteria page will be available at ' .
      (empty($criteria_path) ? 'a default location.' : '<a href="!url" target="_blank">the default location</a>.'),
      array('!url' => $criteria_path)
    );
    $form['criteria_page']['criteria_url']['#description'] .= t(
      ' If the badge represents an educational achievement, consider marking up this page with <a href="!url" target="_blank" title="The Learning Resource Metadata Initiative">LRMI</a>.',
      array('!url' => 'http://www.lrmi.net/')
    );
    $form['criteria_page']['criteria_url']['#description'] .= t(
      ' Remember that advanced badge-criteria pages with LRMI markup <a href="!url" target="_blank" title="Badge Criteria page setup">can be created</a> with the Badge Depot module.',
      array('!url' => url('node/add/badge-criteria-page', array('absolute' => TRUE)))
    );
    $form['criteria_page']['criteria_launch'] = $this->buildLaunchButton(array('criteria_url', '#criteria_url_default'));
    $form['criteria_page']['criteria_microdata'] = array(
      '#type' => 'button',
      '#value' => t('Launch Google Tool'),
      '#attributes' => array(
        // Override client-side JavaScript action and disable form submission.
        'onclick' => $this->buildLaunchJavaScript(
          array('criteria_url', '#criteria_url_default'),
          'window.open(("' . BADGE_DEPOT_GOOGLE_STRUCTURED_DATA_TESTING_TOOL_URL_PREFIX . '" + encodeURI(url)), "_blank");',
          t('The URL is not specified.')
        ),
      ),
    );

    $form['activity_page'] = array('#type' => 'fieldset', '#title' => t('Badge Activity Page'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['activity_page'], 'activity_url', $properties, $entity, array('#maxlength' => 255));
    $form['activity_page']['activity_launch'] = $this->buildLaunchButton('activity_url');

    $this->buildEntityFieldAdmin($form, self::FORM_FIELD_ALIGNMENTS, $properties, $entity, array('#type' => 'textarea', '#rows' => 10));
    $form[self::FORM_FIELD_ALIGNMENTS]['#description'] .= t(
      ' The alignment data is incorporated as LRMI markup in the badge\'s default' .
      ' Criteria page by the Badge Depot module. The data must be entered as a' .
      ' JSON array, where each element describes an Alignment Object as defined' .
      ' in the <a href="!url" target="_blank">OBI Assertions specification</a>,' .
      ' which contains at least the <em>name</em> and <em>url</em> properties.' .
      ' For example: <em>[{"name": "Alignment #1", "url": "http://example.com/standard-1"}, {"name": "Alignment #2", "url": "http://example.com/standard-2"}]</em>',
      array(
        '!url' => url(BadgeDepotGlobalSettings::getObiAssertionsInfoUrl() . '#alignmentobject'),
      )
    );

    $this->buildEntityFieldAdmin($form, self::FORM_FIELD_TAGS, $properties, $entity, array('#title' => t('Tags'), '#type' => 'textarea'));
    $form[self::FORM_FIELD_TAGS]['#description'] .= ' ' . t('Specify one tag per line.');

    field_attach_form($entity_type, $entity, $form, $form_state);

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t($is_new_entity ? 'Create badge definition' : 'Save data'),
      '#weight' => 50,
    );

    return parent::buildDefault($form, $form_state);
  }

  /**
   * Overrides the default validation-handler of the administrative form.
   */
  public function validateDefault($form, &$form_state) {
    $values = &$form_state['values'];

    // Check that the badge image is not already baked.
    $image_url = $values[self::FORM_FIELD_IMAGE_URL];
    $image_data = NULL;
    $image_mime = NULL;
    if (!isset($image_url)) {
      form_set_error(self::FORM_FIELD_IMAGE_URL, t('The image URL must be specified.'));
    }
    else {
      if (DataUriCreator::isDataUri($image_url)) {
        // A Data URI is specified for the badge image...
        $image_data = DataUriCreator::decode($image_url, $image_mime);
        if (is_null($image_data)) {
          form_set_error(self::FORM_FIELD_IMAGE_URL, t('The specified image URL is an invalid Data URI.'));
        }
        elseif ((!is_string($image_data)) || (!is_string($image_mime)) || ($image_mime === '')) {
          form_set_error(self::FORM_FIELD_IMAGE_URL, t('The badge-image Data URI does not contain the expected content.'));
        }
      }
      else {
        // A traditional URL is specified for the badge image...
        if (valid_url($image_url, FALSE)) {
          $image_url = url($image_url, array('absolute' => TRUE));
        }

        if (!valid_url($image_url, TRUE)) {
          form_set_error(self::FORM_FIELD_IMAGE_URL, t('The image URL is invalid.'));
        }
        else {
          $web_data = drupal_http_request($image_url);
          if (isset($web_data->data)) {
            $image_data = $web_data->data;
          }

          if (isset($web_data->headers['content-type'])) {
            $image_mime = $web_data->headers['content-type'];
          }

          if ((!isset($image_data)) || (!isset($image_mime))) {
            $image_error = 'The downloaded badge-image is invalid.';
            if (isset($web_data->error)) {
              $image_error .= (' ' . $web_data->error);
            }

            form_set_error(self::FORM_FIELD_IMAGE_URL, t($image_error));
          }
        }
      }

      // Check that the badge image is not already baked.
      if (isset($image_data) && isset($image_mime)) {
        if (!DataUriCreatorString::startsWith($image_mime, 'image/')) {
          form_set_error(self::FORM_FIELD_IMAGE_URL, t('The image URL is apparently not for an image; the specified MIME type is "@mimeType".', array('@mimeType' => $image_mime)));
        }
        elseif ($image_mime != 'image/png') {
          form_set_error(self::FORM_FIELD_IMAGE_URL, t('The image URL must refer to a PNG type of file.'));
        }
        else {
          $temp_image_file = drupal_tempnam('temporary://', 'badge_depot_image_');
          if (($temp_image_file === FALSE) || (file_put_contents($temp_image_file, $image_data) === FALSE)) {
            form_set_error(self::FORM_FIELD_IMAGE_URL, t('Failed to validate badge image.'));
          }
          else {
            $badge_assertion_url = BadgeDepotImagePngMetadata::getBadgeAssertionUrl($temp_image_file);
            if (isset($badge_assertion_url)) {
              form_set_error(self::FORM_FIELD_IMAGE_URL, t('The image URL refers to a baked badge; only raw images are allowed (the embedded assertion URL is <a href="!url" target="_blank">@title</a>).', array('!url' => url($badge_assertion_url), '@title' => $badge_assertion_url)));
            }
            elseif (getimagesize($temp_image_file) === FALSE) {
              form_set_error(self::FORM_FIELD_IMAGE_URL, t('The image file could not be decoded.'));
            }
          }
        }
      }
    }

    // Check that any alignment-list data is formatted correctly.
    $alignments_value = &$values[self::FORM_FIELD_ALIGNMENTS];
    if (empty($alignments_value)) {
      // Synchronized with form state.
      $alignments_value = '';
    }
    else {
      $alignments = drupal_json_decode($alignments_value);
      $is_valid = is_array($alignments);
      if ($is_valid) {
        foreach ($alignments as $alignment_index => $alignment_item) {
          if ((!is_int($alignment_index)) || (!is_array($alignment_item))) {
            $is_valid = FALSE;
            break;
          }

          $alignment_has_name = FALSE;
          $alignment_has_url = FALSE;
          foreach ($alignment_item as $alignment_key => $alignment_value) {
            if ((!$is_valid) || (!is_string($alignment_key))) {
              $is_valid = FALSE;
              break;
            }

            switch ($alignment_key) {
              case 'name':
                $alignment_has_name = TRUE;
                $is_valid = (is_string($alignment_value) && (!empty($alignment_value)));
                break;
              case 'url':
                $alignment_has_url = TRUE;
                $is_valid = (is_string($alignment_value) && (!empty($alignment_value)) && valid_url($alignment_value, TRUE));
                break;
            }
          }

          if ((!$is_valid) || (!$alignment_has_name) || (!$alignment_has_url)) {
            $is_valid = FALSE;
            break;
          }
        }
      }

      if ($is_valid) {
        // Attempt to format the JSON data to remain
        // constant after entity-export round-trips.
        $pretty_json = (empty($alignments) ? '[]' : _badge_depot_json_encode_pretty($alignments));
        if (is_string($pretty_json)) {
          // Synchronized with form state.
          $alignments_value = $pretty_json;
        }
      }
      else {
        form_set_error(self::FORM_FIELD_ALIGNMENTS, t('Invalid data was specified for the alignment list. Refer to the <a href="!url" target="_blank">OBI Assertions specification</a> for details.', array('!url' => url(BadgeDepotGlobalSettings::getObiAssertionsInfoUrl() . '#alignmentobject'))));
      }
    }

    return parent::validateDefault($form, $form_state);
  }
}
