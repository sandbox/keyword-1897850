<?php
/**
 * @file
 * Contains additional hook_help() information.
 */

/**
 * Provides hook_help info for the badge-manifest entity.
 */
function _badge_depot_entities_manifest_help($path, $arg) {
  if ($path === BADGE_DEPOT_ADMIN_MANIFEST_PATH) {
    $output = ('<p>' . t('Badge-class Manifest information is used to define the badges that can be issued by affiliated organizations.') . '</p>');
    if ((count($arg) <= 4) || empty($arg[4])) {
      $output .= ('<p>' . t('Configure the badge definitions that are stored as Entity data using this administration page.') . '</p>');
    }

    return $output;
  }

  if ($path === (BADGE_DEPOT_ADMIN_MANIFEST_PATH . '/manage/%')) {
    $output = _badge_depot_entities_manifest_help(BADGE_DEPOT_ADMIN_MANIFEST_PATH, $arg);
    $machine_name = $arg[5];
    $entity = BadgeManifestEntity::loadFromMachineName($machine_name);
    if (isset($entity)) {
      $url_options = array('absolute' => TRUE);
      $output .= '<p>' . t('Once configured, the Badge Depot module is also able to provide these manifest-related services:') . '</p>';
      $output .= '<ul>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Publish a human-readable page</a> describing the badge definition, which can also be used as the default <a href="@criteriaUrl" target="_blank">badge Criteria page</a>,', array('@url' => url($entity->getEntityViewPath(), $url_options), '@criteriaUrl' => url($entity->getLocalCriteriaPermalink(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Provide OBI badge-class manifest data</a>, in JSON format, that gets linked to from awarded badges,', array('@url' => url($entity->getLocalJsonDataPath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Host an image</a> representing the badge, and', array('@url' => url($entity->getLocalImagePath(), $url_options))) . '</li>';
      $output .= '<li>' . t('<a href="@url" target="_blank">Link to the OBI Issuer-Organization</a>, expressed in JSON format.', array('@url' => url($entity->getLocalIssuerPath(), $url_options))) . '</li>';
      $output .= '</ul>';
    }

    return $output;
  }
}
