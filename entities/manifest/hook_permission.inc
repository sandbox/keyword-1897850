<?php
/**
 * @file
 * Contains additional hook_permission() information.
 */

/**
 * Adds the hook_permission data for the badge-manifest entity.
 */
function _badge_depot_append_entities_manifest_permission(&$permissions) {
  $permissions['administer badge manifest entities'] = array(
    'title' => t('Administer badge-manifest data'),
    'description' => t('Provides full access to all administrative tasks related to badge_manifest entities.'),
    'restrict access' => TRUE,
  );

  $permissions['create badge manifest entities'] = array(
    'title' => t('Create badge-manifest data'),
    'description' => t('Allows users to create badge_manifest entities.'),
    'restrict access' => TRUE,
  );

  $permissions['delete badge manifest entities'] = array(
    'title' => t('Delete badge-manifest data'),
    'description' => t('Allows users to delete badge_manifest entities.'),
    'restrict access' => TRUE,
  );

  $permissions['update badge manifest entities'] = array(
    'title' => t('Update badge-manifest data'),
    'description' => t('Allows users to update badge_manifest entities.'),
    'restrict access' => TRUE,
  );

  $permissions['view badge manifest entities'] = array(
    'title' => t('View badge-manifest data'),
    'description' => t('Allows users to view badge_manifest entities.'),
  );
}
