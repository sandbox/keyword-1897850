<?php
/**
 * @file
 * Contains additional hook_schema() information for installation.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_schema database info for the badge-manifest entity.
 */
function _badge_depot_append_entities_manifest_schema(&$schema) {
  $schema[BADGE_DEPOT_ENTITY_TABLE_MANIFEST] = array(
    'description' => t('The base record for the registry of OBI badge-class manifests (i.e., badge definitions).'),
    'fields' => array(
      'manifest_id' => array(
        'description' => t('The unique identifier of this badge-class manifest.'),
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'manifest_machine_name' => array(
        'description' => t('The persistent unique-identifier of this badge-class manifest, a.k.a. badge definition, which is a machine-readable name in Drupal parlance.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      //'version_id' => array(
      //  'description' => t('The current version identifier of the manifest data.'),
      //  'type' => 'int',
      //  'not null' => TRUE,
      //  'unsigned' => TRUE,
      //  'default' => 0,
      //),
      'owner_user_id' => array(
        'description' => t('A reference to the user that owns this manifest for data-management purposes.'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'owner_user_name' => array(
        'description' => t('The name of the user that owns this manifest for data-management purposes; used primarily for enhanced security when exporting/importing the data entity.'),
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
      ),
      'issuer_id' => array(
        'description' => t('A reference to the issuer-organization that awards this badge and that is ultimately repsonsible for maintaining the definition\'s accuracy.'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'issuer_machine_name' => array(
        'description' => t('The machine-readable name of the issuer-organization that awards this badge.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => t('The entity type of this badge-class manifest (used by entity sub-type bundles).'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => t('The name of this badge (required; treated as non-markup plain text).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => t('A short description of this achievement (required; treated as non-markup plain text).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'body' => array(
        'description' => t('The detailed description of this badge-class manifest (treated as HTML markup).'),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'is_published' => array(
        'description' => t('A flag indicating whether the badge-class manifest is publicly visible.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'is_dormant' => array(
        'description' => t('A flag indicating that instances of this badge is currently not being issued.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => t('The Unix timestamp of when the badge-class manifest was created.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => t('The Unix timestamp of when the badge-class manifest was most recently saved.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'image_url' => array(
        'description' => t('The URL that links to the unbaked (a.k.a. raw) image that represents the badge achievement (required; can be a regular URL or <a href="!url" target="_blank" title="Tool to create a Data URI">Data URI</a>).', array('!url' => url(DATA_URI_CREATOR_PAGE_PATH))),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'criteria_url' => array(
        'description' => t('The optional URL that links to the badge\'s criteria page. When not specified, a default URL will be used.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'activity_url' => array(
        'description' => t('The URL that links to the content of the badge (that is, the activity that can be performed to obtain the badge).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'tags' => array(
        'description' => t('A list of tags that describe the type of achievement.'),
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'alignments' => array(
        'description' => t('An optional list of <a href="!url" target="_blank" title="Learning Resource Metadata Initiative">LRMI metadata</a> describing which educational standards this badge aligns with, if any.', array('!url' => 'http://www.lrmi.net/')),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'is_stem_aligned' => array(
        'description' => t('A flag indicating that the badge is aligned with content for STEM (i.e., Science, Technology, Engineering and Mathematics).'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('manifest_id'),
    'unique keys' => array(
      'manifest_key' => array('manifest_id'),
      //'manifest_version_key' => array('manifest_id', 'version_id'),
    ),
    'indexes' => array(
      'changed_index' => array('changed'),
      'created_index' => array('created'),
      'machine_index' => array('manifest_machine_name'),
      'owner_user_index' => array('owner_user_id'),
      'issuer_id_index' => array('issuer_id'),
      'issuer_machine_index' => array('issuer_machine_name'),
    ),
  );
}
