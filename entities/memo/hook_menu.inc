<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_menu info for the badge-memo entity.
 */
function _badge_depot_append_entities_memo_menu(&$items) {
  $entity_type = BADGE_DEPOT_ENTITY_TABLE_MEMO;

  // Index in the URL path where the entity wildcard is specified.
  $entity_path_index = 2;

  // By naming convention, implies badge_depot_entities_memo_load
  // is to be used for data loading.
  $items[BADGE_DEPOT_MAIN_MEMO_PATH . '/%badge_depot_entities_memo'] = array(
    'title callback' => '_badge_depot_entity_class_label',
    'title arguments' => array($entity_path_index, t('View badge-memo entity') /* default title */),
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeMemoEntity::requestView',
    'page arguments' => array($entity_path_index, 'full' /* view mode */),
    'access callback' => '_badge_depot_entity_access',
    'access arguments' => array('view' /* operation */, $entity_path_index, NULL /* account */, $entity_type),
  );
}

/**
 * Adds the hook_menu_alter info for the badge-memo entity.
 */
function _badge_depot_append_entities_memo_menu_alter(&$items) {
  $items[BADGE_DEPOT_ADMIN_MEMO_PATH]['description'] = 'Create and manage records of Open Badges that are available from various organizations.';
}
