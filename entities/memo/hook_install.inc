<?php
/**
 * @file
 * Contains additional hook_schema() information for installation.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Adds the hook_schema database info for the badge-memo entity.
 */
function _badge_depot_append_entities_memo_schema(&$schema) {
  $schema[BADGE_DEPOT_ENTITY_TABLE_MEMO] = array(
    'description' => t('The base record for the registry of OBI badge activities (i.e., an entry that typically at least has a badge-criteria page).'),
    'fields' => array(
      'badge_id' => array(
        'description' => t('The unique identifier of this badge entry.'),
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'badge_machine_name' => array(
        'description' => t('The persistent unique-identifier of this badge entry, which is a machine-readable name in Drupal parlance.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      //'version_id' => array(
      //  'description' => t('The current version identifier of the badge data.'),
      //  'type' => 'int',
      //  'not null' => TRUE,
      //  'unsigned' => TRUE,
      //  'default' => 0,
      //),
      'owner_user_id' => array(
        'description' => t('A reference to the user that owns this badge for management purposes.'),
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'owner_user_name' => array(
        'description' => t('The name of the user that owns this badge for management purposes; used primarily for enhanced security when exporting/importing the data entity.'),
        'type' => 'varchar',
        'length' => 60,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => t('The entity type of this badge entry (used by entity sub-type bundles).'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => t('The title of this badge entry (treated as non-markup plain text).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'body' => array(
        'description' => t('The detailed description of this badge entry (treated as HTML markup).'),
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'is_published' => array(
        'description' => t('A flag indicating whether the badge entry is publicly visible.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'is_dormant' => array(
        'description' => t('A flag indicating that the badge is currently not live and not being issued.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => t('The Unix timestamp of when the badge entry was created.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => t('The Unix timestamp of when the badge entry was most recently saved.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'issuer_name' => array(
        'description' => t('The name of the badge\'s issuing agent.'),
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'issuer_url' => array(
        'description' => t('The URL that links to the badge\'s issuing agent. This will often be the origin URL of the issuer, although it may be a different URL.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'issuer_email' => array(
        'description' => t('The email address of a human associated with the badge issuer (required since March 2013).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'issuer_organization' => array(
        'description' => t('Specifies an optional organization for which the badge is being issued.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'activity_url' => array(
        'description' => t('The URL that links to the content of the badge (that is, the activity that can be performed to obtain the badge).'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'image_url' => array(
        'description' => t('The URL that links to an image that represents the badge; this can be source image of the badge or another image associated with the badge.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'criteria_url' => array(
        'description' => t('The URL that links to the criteria page of the badge, which should contain LRMI and CCSS microdata.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'criteria_meta' => array(
        'description' => t('The last known JSON representation of microdata that is contained in the badge\'s criteria page.'),
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'source_assertion_url' => array(
        'description' => t('The URL of the original badge-assertion that was used to create this database entry.'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'source_assertion_json' => array(
        'description' => t('The JSON data of the original badge-assertion that was used to create this database entry.'),
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('badge_id'),
    'unique keys' => array(
      'badge_key' => array('badge_id'),
      //'badge_version_key' => array('badge_id', 'version_id'),
    ),
    'indexes' => array(
      'criteria_index' => array('criteria_url'),
      'changed_index' => array('changed'),
      'created_index' => array('created'),
      'machine_index' => array('badge_machine_name'),
      'owner_user_index' => array('owner_user_id'),
    ),
  );
}
