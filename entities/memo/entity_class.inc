<?php
/**
 * @file
 * Contains BadgeMemoEntity.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Main class for badge-memo entities.
 */
class BadgeMemoEntity extends BadgeDepotObiBaseEntity {

  /**
   * Overrides the base method that returns the default name of the
   * database table for this entity type.
   */
  protected static function getDefaultEntityType() {
    return BADGE_DEPOT_ENTITY_TABLE_MEMO;
  }

  /**
   * Overrides the base method that returns the name of the database
   * column that stores the machine-readable name of the entity.
   */
  protected static function getDefaultMachineNameKey() {
    return BADGE_DEPOT_ENTITY_COLUMN_MEMO_MACHINE_NAME;
  }

  /**
   * Overrides the base method that returns the base URL path for all instances
   * of this entity type (i.e., not for a single specific entity instance).
   */
  protected static function getEntityMainRootPath() {
    return BADGE_DEPOT_MAIN_MEMO_PATH;
  }

  /**
   * Overrides the base method that returns the base URL path to administer
   * this entity type (i.e., not for a single entity instance).
   */
  protected static function getEntityAdminRootPath() {
    return BADGE_DEPOT_ADMIN_MEMO_PATH;
  }

  /**
   * Overrides the base method that lists all the not-null text fields.
   */
  protected function getNonNullableTextFieldNames() {
    return (parent::getNonNullableTextFieldNames() +
      array(
        'body',
        'criteria_meta',
        'source_assertion_json',
      )
    );
  }

  // Define fields that map to specific database columns of this entity.
  public $badge_id;
  public $badge_machine_name;
  public $title;
  public $body;
  public $is_dormant;
  public $issuer_name;
  public $issuer_url;
  public $issuer_email;
  public $issuer_organization;
  public $activity_url;
  public $image_url;
  public $criteria_url;
  public $criteria_meta;
  public $source_assertion_url;
  public $source_assertion_json;

  /**
   * Loads the badge-memo entity that corresponds with the specified criteria-URL.
   */
  public static function loadFromCriteriaURL($criteria_url) {
    // Get a single value only.
    $get_all_values = FALSE;
    return static::loadMultipleFromCriteriaURL($criteria_url, $get_all_values);
  }

  /**
   * Loads all badge-memo entities that corresponds with the specified criteria-URL.
   */
  public static function loadMultipleFromCriteriaURL($criteria_url, $get_all_values = TRUE) {
    if (!isset($criteria_url)) {
      return NULL;
    }

    $safe_criteria_url = check_url($criteria_url);
    return static::loadFromValueMatch('criteria_url', $safe_criteria_url, $get_all_values);
  }
}
