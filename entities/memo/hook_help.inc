<?php
/**
 * @file
 * Contains additional hook_help() information.
 */

/**
 * Provides hook_help info for the badge-memo entity.
 */
function _badge_depot_entities_memo_help($path, $arg) {
  if ($path === BADGE_DEPOT_ADMIN_MEMO_PATH) {
    $output = ('<p>' . t('The Badge Depot module keeps records of Open Badges that are issued by various organizations across the web.  The Badge Memo entity data are used to store these records that reach out beyond the badges that are issued by the module itself.') . '</p>');
    if ((count($arg) <= 4) || empty($arg[4])) {
      $url_options = array('absolute' => TRUE);
      $output .= '<p>';
      $output .= t('Configure the badge definitions that are stored as Entity data using this administration page.');
      $output .= ' ';
      $output .= t(
        'Additional memo\'s can also be created using the <a href="!badgeInspectorUrl" target="_blank">Badge Inspector</a> and <a href="!backpackInspectorUrl" target="_blank">Backpack Inspector</a> tools.',
        array(
          '!badgeInspectorUrl' => url(BADGE_DEPOT_INSPECTOR_DEFAULT_PATH, $url_options),
          '!backpackInspectorUrl' => url(BADGE_DEPOT_INSPECTOR_BACKPACK_PATH, $url_options),
        )
      );
      $output .= '</p>';
    }

    return $output;
  }

  if ($path === (BADGE_DEPOT_ADMIN_MEMO_PATH . '/manage/%')) {
    $output = _badge_depot_entities_memo_help(BADGE_DEPOT_ADMIN_MEMO_PATH, $arg);
    $machine_name = $arg[5];
    $entity = BadgeMemoEntity::loadFromMachineName($machine_name);
    if (isset($entity)) {
      $output .= '<p>' . t('Once this memo is configured and published, <a href="@url" target="_blank">a human-readable page</a> describing this badge can be viewed publicly.', array('@url' => url($entity->getEntityViewPath(), array('absolute' => TRUE)))) . '</p>';
    }

    return $output;
  }
}
