<?php
/**
 * @file
 * Contains BadgeDepotMemoEntityController.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Controller class for badge-memo entities.
 */
class BadgeDepotMemoEntityController extends BadgeDepotObiBaseEntityController {

  /**
   * Overrides the base method that builds a structured array
   * representing the entity's content for the supported view-modes.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    if (!is_array($content)) {
      $content = array();
    }

    $properties = NULL;
    $wrapper = NULL;
    $this->buildEntityFieldContent($content, 'image_url', $properties, $wrapper, $entity, array('#title' => '', '#theme' => 'image', '#width' =>  90, '#height' => 90, '#path' => $entity->image_url));
    $this->buildEntityFieldContent($content, 'title', $properties, $wrapper, $entity, array('#title' => ''));
    $this->buildEntityFieldContent($content, 'body', $properties, $wrapper, $entity, array('#title' => '', '#theme' => NULL, '#markup' => check_markup($entity->body, 'full_html', $langcode)));
    if ($view_mode === 'full') {
      $link_options = array('attributes' => array('target' => '_blank'));
      $this->buildEntityFieldContent($content, 'issuer_url', $properties, $wrapper, $entity, array('#title' => t('Issuer'), '#theme' => NULL, '#markup' => l($entity->issuer_name, $entity->issuer_url, $link_options)));
      $this->buildEntityFieldContent($content, 'issuer_email', $properties, $wrapper, $entity, array('#title' => ''));
      $this->buildEntityFieldContent($content, 'activity_url', $properties, $wrapper, $entity, array('#title' => t('Activity Page'), '#theme' => NULL, '#markup' => l($entity->activity_url, $entity->activity_url, $link_options)));
      $badge_criteria_url = $this->buildEntityFieldContent($content, 'criteria_url', $properties, $wrapper, $entity, array('#title' => t('Criteria Page'), '#theme' => NULL, '#markup' => l($entity->criteria_url, $entity->criteria_url, $link_options)));
      if (isset($badge_criteria_url)) {
        // Add a button that links to Google's Structured Data Testing Tool to analyze the the criteria page.
        $button = array(
          '#type' => 'button',
          '#value' => t('Inspect with Google Tool'),
          // Override client-side JavaScript action and disable form submission.
          '#attributes' => array('onclick' => 'window.open("' . BADGE_DEPOT_GOOGLE_STRUCTURED_DATA_TESTING_TOOL_URL_PREFIX . urlencode($badge_criteria_url) . '", "_blank"); return false;'),
          '#prefix' => '<br />',
        );
        $content['criteria_url']['#markup'] .= render($button);
      }

      if ($entity->is_dormant) {
        $content['is_dormant'] = array(
          '#markup' => ('<p>' . t('Note: The badge is apparently no longer live.') . '</p>'),
        );
      }

      if (!$entity->is_published) {
        drupal_set_message(
          t(
            'The data for "@title" is currently not published publicly. <a href="!url">Edit this badge-memo</a> entity if you want to change this.',
            array(
              '@title' => check_plain($entity->title),
              '!url' => url($entity->getEntityEditPath()),
            )
          ),
          'warning'
        );
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Overrides the base method that tweaks the extracted data to be exported
   * for an entity; it modifies the formatting of JSON types of data.
   */
  protected function exportCleanup(array &$vars, $entity) {
    // Converts JSON data into something that looks nicer for exporting.
    $this->exportCleanupForJsonString('criteria_meta', $vars, $entity);
    $this->exportCleanupForJsonString('source_assertion_json', $vars, $entity);

    parent::exportCleanup($vars, $entity);
  }

  /**
   * Overrides the base method that tweaks the deserialized data to be imported
   * into an entity; it modifies the formatting of JSON types of data.
   */
  protected function importCleanup(array &$vars, $export) {
    // Converts JSON data to the string formats being used internally.
    $this->importCleanupForJsonString('criteria_meta', $vars, $export);
    $this->importCleanupForJsonString('source_assertion_json', $vars, $export);

    parent::importCleanup($vars, $export);
  }
}
