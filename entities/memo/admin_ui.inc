<?php
/**
 * @file
 * Implements the administrative user-interface functions for the memo entity.
 */

// Include all dependency files.
require_once 'globals.inc';

class BadgeMemoAdminPage extends BadgeDepotEntityAdminPage {

  /**
   * Overrides the default build-handler to implement the administrative
   * user-interface form for the badge-memo entity.
   */
  public function buildDefault($form, &$form_state) {
    $this->getBuildFormExtraArgs($entity, $op);
    $entity_type = $entity->entityType();

    // Includes super-user access.
    $is_admin = user_access('administer ' . $entity_type . ' entities');
    if ($op === 'clone') {
      // Modify the title of the clone.
      $entity->title .= ' (cloned)';

      // Create a new machine-readable ID for the clone.
      $entity->setMachineName();

      // Wipe source-data for clones.
      $entity->source_assertion_url = '';
      $entity->source_assertion_json = '';
    }

    $properties = NULL;
    $this->buildEntityFieldAdmin($form, 'title', $properties, $entity, array('#title' => t('Name of Badge'), '#required' => TRUE, '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form, 'body', $properties, $entity, array('#type' => 'textarea', '#required' => TRUE));
    $this->buildEntityFieldAdmin($form, 'is_published', $properties, $entity);
    $this->buildEntityFieldAdmin($form, 'is_dormant', $properties, $entity);

    $owner_user_id = $entity->owner_user_id;
    if ($is_admin) {
      // Administrators have full control over changing
      // the current owner of this badge entity.
      $users = entity_load('user');
      $user_select = array();
      foreach ($users as $user_item) {
        $user_select[$user_item->uid] = $user_item->name;
      }

      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'select', '#options' => $user_select));
    }
    elseif (!empty($owner_user_id)) {
      // Regular users can only see who is the owner of this badge entity,
      // but only when it currently has an owner.
      $users = entity_load('user', array($owner_user_id));
      $this->buildEntityFieldAdmin($form, 'owner_user_id', $properties, $entity, array('#type' => 'item', '#markup' => theme('username', array('account' => $users[$owner_user_id]))));
    }

    $form['badge_image'] = array('#type' => 'fieldset', '#title' => t('Badge Image'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['badge_image'], 'image_url', $properties, $entity, array('#required' => TRUE, '#maxlength' => 255));
    $form['badge_image']['activity_launch'] = array(
      '#type' => 'button',
      '#value' => t('View Image'),
      '#attributes' => array(
        // Override client-side JavaScript action and disable form submission.
        'onclick' => $this->buildLaunchJavaScript(
          'image_url',
          'jQuery("#badge_image_preview").each(function() { this.src = url; }); jQuery("#badge_image_container").show();',
          t('The image URL is not specified.')
        ),
      ),
      '#suffix' => '<span id="badge_image_container" style="display:none;"><br /><br /><img id="badge_image_preview" /></span>',
    );

    $form['activity_page'] = array('#type' => 'fieldset', '#title' => t('Badge Activity Page'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['activity_page'], 'activity_url', $properties, $entity, array('#maxlength' => 255));
    $form['activity_page']['activity_launch'] = $this->buildLaunchButton('activity_url');

    $form['issuer'] = array('#type' => 'fieldset', '#title' => t('Badge Issuer'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['issuer'], 'issuer_name', $properties, $entity, array('#title' => t('Name'), '#required' => TRUE, '#maxlength' => 128));
    $this->buildEntityFieldAdmin($form['issuer'], 'issuer_email', $properties, $entity, array('#title' => t('Email'), '#maxlength' => 255));
    $this->buildEntityFieldAdmin($form['issuer'], 'issuer_organization', $properties, $entity, array('#title' => t('Organization'), '#maxlength' => 255));

    $form['criteria_page'] = array('#type' => 'fieldset', '#title' => t('Criteria Page'), '#collapsible' => TRUE, '#collapsed' => FALSE);
    $this->buildEntityFieldAdmin($form['criteria_page'], 'criteria_url', $properties, $entity, array('#required' => TRUE, '#maxlength' => 255));
    $form['criteria_page']['criteria_launch'] = $this->buildLaunchButton('criteria_url');
    $form['criteria_page']['criteria_microdata'] = array(
      '#type' => 'button',
      '#value' => t('Launch Google Tool'),
      '#attributes' => array(
        // Override client-side JavaScript action and disable form submission.
        'onclick' => $this->buildLaunchJavaScript(
          'criteria_url',
          'window.open(("' . BADGE_DEPOT_GOOGLE_STRUCTURED_DATA_TESTING_TOOL_URL_PREFIX . '" + encodeURI(url)), "_blank");',
          t('The URL is not specified.')
        ),
      ),
    );
    $criteria_meta = $this->buildEntityFieldAdmin($form['criteria_page'], 'criteria_meta', $properties, $entity, array('#type' => 'textarea'));
    if ((!empty($criteria_meta)) && ($criteria_meta !== '{}')) {
      $form['criteria_page']['criteria_meta']['#rows'] = 15;
    }

    if (($op !== 'add') && ((!empty($entity->source_assertion_url)) || (!empty($entity->source_assertion_json)))) {
      $form['source_assertion'] = array('#type' => 'fieldset', '#title' => t('Source Badge'), '#collapsible' => TRUE, '#collapsed' => TRUE);
      $this->buildEntityFieldAdmin($form['source_assertion'], 'source_assertion_url', $properties, $entity, array('#title' => t('Assertion URL'), '#disabled' => TRUE, '#maxlength' => 255));
      if (!empty($entity->source_assertion_url)) {
        $form['source_assertion']['badge_assertion_download'] = $this->buildLaunchButton('source_assertion_url', t('Download JSON'));
      }

      $this->buildEntityFieldAdmin($form['source_assertion'], 'source_assertion_json', $properties, $entity, array('#title' => t('Assertion JSON'), '#type' => 'textarea', '#rows' => 20, '#disabled' => TRUE));
    }

    field_attach_form($entity_type, $entity, $form, $form_state);

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t(($op === 'add') ? 'Create badge memo' : 'Save data'),
      '#weight' => 50,
    );

    return parent::buildDefault($form, $form_state);
  }
}
