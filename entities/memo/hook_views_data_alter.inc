<?php
/**
 * @file
 * Contains additional hook_views_data_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes the hook_views_data_alter data for the badge-memo entity.
 */
function _badge_depot_entities_memo_views_data_alter(&$info) {
  // This modifies the formatting for the body field of the badge_memo
  // entity to be rendered as HTML.
  $info[BADGE_DEPOT_ENTITY_TABLE_MEMO]['body']['field']['handler'] = 'BadgeDepotViewsFieldHandlerHtml';
}
