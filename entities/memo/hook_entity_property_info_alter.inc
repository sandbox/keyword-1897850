<?php
/**
 * @file
 * Contains additional hook_entity_property_info_alter() information.
 */

// Include all dependency files.
require_once 'globals.inc';

/**
 * Changes the hook_entity_property_info_alter data for the badge-memo entity.
 */
function _badge_depot_entities_memo_entity_property_info_alter(&$info) {
  $properties = &$info[BADGE_DEPOT_ENTITY_TABLE_MEMO]['properties'];

  $properties['badge_id'] = array_merge(((array) ($properties['badge_id'])), array(
    'description' => t('The unique identifier of this badge entry.'),
  ));

  $properties['badge_machine_name'] = array_merge(((array) ($properties['badge_machine_name'])), array(
    'label' => t('Persistent ID'),
    'description' => t('The persistent unique-identifier of this badge that can be used for exporting and importing data between machines.'),
  ));

  $properties['owner_user_id'] = array_merge(((array) ($properties['owner_user_id'])), array(
    'label' => t('Owner User ID'),
    'description' => t('A reference to the user that owns this badge for management purposes.'),
    'type' => 'user',
  ));

  $properties['owner_user_name'] = array_merge(((array) ($properties['owner_user_name'])), array(
    'label' => t('Owner User Name'),
    'description' => t('The name of the user that owns this badge for management purposes; used primarily for enhanced security when exporting/importing the data entity.'),
  ));

  $properties['type'] = array_merge(((array) ($properties['type'])), array(
    'description' => t('The entity type of this badge entry (used by entity sub-type bundles).'),
  ));

  $properties['title'] = array_merge(((array) ($properties['title'])), array(
    'label' => t('Title'),
    'description' => t('The title of this badge entry (treated as non-markup plain text).'),
  ));

  $properties['body'] = array_merge(((array) ($properties['body'])), array(
    'label' => t('Description'),
    'description' => t('The detailed description of this badge entry (treated as HTML markup; the entity\'s "body" property).'),
  ));

  $properties['is_published'] = array_merge(((array) ($properties['is_published'])), array(
    'label' => t('Is Published?'),
    'description' => t('A boolean flag indicating whether the badge entry is publicly visible (the entity\'s "is_published" property).'),
    'type' => 'boolean',
  ));

  $properties['is_dormant'] = array_merge(((array) ($properties['is_dormant'])), array(
    'label' => t('Is Dormant?'),
    'description' => t('A boolean flag indicating that the badge is currently not live and not being issued (the entity\'s "is_dormant" property).'),
    'type' => 'boolean',
  ));

  $properties['created'] = array_merge(((array) ($properties['created'])), array(
    'description' => t('The full date and time, as a timestamp, of when the badge entry was created.'),
    'type' => 'date',
  ));

  $properties['changed'] = array_merge(((array) ($properties['changed'])), array(
    'description' => t('The full date and time, as a timestamp, of when the badge entry was most recently saved.'),
    'type' => 'date',
  ));

  $properties['issuer_name'] = array_merge(((array) ($properties['issuer_name'])), array(
    'label' => t('Issuer Name'),
    'description' => t('The name of the badge\'s issuing agent (the entity\'s "issuer_name" property).'),
  ));

  $properties['issuer_url'] = array_merge(((array) ($properties['issuer_url'])), array(
    'label' => t('Issuer URL'),
    'description' => t('The URL that links to the badge\'s issuing agent (the entity\'s "issuer_url" property). This will often be the origin URL of the issuer, although it may be a different URL.'),
    'type' => 'uri',
  ));

  $properties['issuer_email'] = array_merge(((array) ($properties['issuer_email'])), array(
    'label' => t('Issuer Email'),
    'description' => t('The email address of a human associated with the badge issuer (required since March 2013; the entity\'s "issuer_email" property).'),
  ));

  $properties['issuer_organization'] = array_merge(((array) ($properties['issuer_organization'])), array(
    'label' => t('Issuer Organization'),
    'description' => t('Specifies an optional organization for which the badge is being issued (the entity\'s "issuer_organization" property).'),
  ));

  $properties['activity_url'] = array_merge(((array) ($properties['activity_url'])), array(
    'label' => t('Activity URL'),
    'description' => t('The URL that links to the content of the badge (that is, the activity that can be performed to obtain the badge; the entity\'s "activity_url" property).'),
    'type' => 'uri',
  ));

  $properties['image_url'] = array_merge(((array) ($properties['image_url'])), array(
    'label' => t('Image URL'),
    'description' => t('The URL that links to an image that represents the badge; this can be source image of the badge or another image associated with the badge (the entity\'s "image_url" property).'),
    'type' => 'uri',
  ));

  $properties['criteria_url'] = array_merge(((array) ($properties['criteria_url'])), array(
    'label' => t('Criteria URL'),
    'description' => t('The URL that links to the criteria page of the badge, which should contain LRMI and CCSS microdata (the entity\'s "criteria_url" property).'),
    'type' => 'uri',
  ));

  $properties['criteria_meta'] = array_merge(((array) ($properties['criteria_meta'])), array(
    'label' => t('Criteria JSON (LRMI/CCSS)'),
    'description' => t('The last known JSON representation of microdata that is contained in the badge\'s criteria page (the entity\'s "criteria_meta" property).'),
  ));

  $properties['source_assertion_url'] = array_merge(((array) ($properties['source_assertion_url'])), array(
    'label' => t('Source Assertion URL'),
    'description' => t('The URL of the original badge-assertion that was used to create this database entry (the entity\'s "source_assertion_url" property).'),
    'type' => 'uri',
  ));

  $properties['source_assertion_json'] = array_merge(((array) ($properties['source_assertion_json'])), array(
    'label' => t('Source Assertion JSON'),
    'description' => t('The JSON data of the original badge-assertion that was used to create this database entry (the entity\'s "source_assertion_json" property).'),
  ));
}
