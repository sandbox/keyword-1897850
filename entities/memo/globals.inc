<?php
/**
 * @file
 * Provides global functions and other definitions, within the context of this
 * entity, which must always be available when this module is installed, such
 * as for various callback functions.
 */

// Include all dependency files.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';
require_once dirname(__DIR__) . '/globals.inc';

// Specify the database table name that is used for storing the badge-memo entities.
define('BADGE_DEPOT_ENTITY_TABLE_MEMO', 'badge_memo');

// Specify the database column name that is used for storing the badge-memo's machine-readable name.
define('BADGE_DEPOT_ENTITY_COLUMN_MEMO_MACHINE_NAME', 'badge_machine_name');

// Specify the URLs for accessing data related to badge-memo entities.
define('BADGE_DEPOT_MAIN_MEMO_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'memo'));
define('BADGE_DEPOT_ADMIN_MEMO_PATH', (BADGE_DEPOT_ADMIN_PATH_PREFIX . 'memo'));

// Define some URLs of external tools
define('BADGE_DEPOT_GOOGLE_STRUCTURED_DATA_TESTING_TOOL_URL_PREFIX', 'http://www.google.com/webmasters/tools/richsnippets?url=');

// Implements a general loader for badge-memo entities from URL path parameters.
// NOTE: Based on naming convention that Drupal follows for wildcard segments
//       in URL paths, this function will automatically be called to load data
//       for path keys that contain %badge_depot_entities_memo as a segment.
function badge_depot_entities_memo_load($badge_id = NULL) {
  return BadgeMemoEntity::autoLoad($badge_id);
}
