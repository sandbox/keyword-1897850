<?php
/**
 * @file
 * Contains additional hook_permission() information.
 */

/**
 * Adds the hook_permission data for the badge-memo entity.
 */
function _badge_depot_append_entities_memo_permission(&$permissions) {
  $permissions['administer badge memo entities'] = array(
    'title' => t('Administer badge-memo data'),
    'description' => t('Provides full access to all administrative tasks related to badge_memo entities.'),
    'restrict access' => TRUE,
  );

  $permissions['create badge memo entities'] = array(
    'title' => t('Create badge-memo data'),
    'description' => t('Allows users to create badge_memo entities.'),
    'restrict access' => TRUE,
  );

  $permissions['delete badge memo entities'] = array(
    'title' => t('Delete badge-memo data'),
    'description' => t('Allows users to delete badge_memo entities.'),
    'restrict access' => TRUE,
  );

  $permissions['update badge memo entities'] = array(
    'title' => t('Update badge-memo data'),
    'description' => t('Allows users to update badge_memo entities.'),
    'restrict access' => TRUE,
  );

  $permissions['view badge memo entities'] = array(
    'title' => t('View badge-memo data'),
    'description' => t('Allows users to view badge_memo entities.'),
  );
}
