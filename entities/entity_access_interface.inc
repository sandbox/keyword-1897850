<?php
/**
 * @file
 * Contains BadgeDepotEntityAccessInterface.
 */

/**
 * Defines an interface that allows data Entity classes to implement their
 * own logic for user-access authorization.
 */
interface BadgeDepotEntityAccessInterface {

  /**
   * Determines whether the entity can be accessed by the specified user
   * with regards to a specific operation such as to view the data.
   *
   * @param $operation
   *   A string that specifies the operation to be authorized, for example,
   *   "view", "create", "update", "delete", and so on.
   * @param $account
   *   (optional) The account to check; if not specified, the currently
   *   logged-in user will be used.
   *
   * @return
   *   Boolean TRUE if the current user has the requested permission;
   *   otherwise FALSE.
   */
  public function isAccessibleByUser($operation, $account = NULL);
}
