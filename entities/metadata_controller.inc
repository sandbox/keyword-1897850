<?php
/**
 * @file
 * Contains BadgeDepotEntityMetadataController.
 */

/**
 * Customized controller for generating basic metadata for CRUD entity types.
 *
 * See the comments in "modules/entity/entity.api.php", as well as the function
 * named entity_entity_property_info and the EntityDefaultMetadataController
 * class implemented in "modules/entity/entity.info.inc".
 */
class BadgeDepotEntityMetadataController extends EntityDefaultMetadataController {

  public function __construct($type) {
    parent::__construct($type);
  }

  public function entityPropertyInfo() {
    $entity_property_info = parent::entityPropertyInfo();
    //$properties = &$entity_property_info[$this->type]['properties'];
    return $entity_property_info;
  }

  /**
   * Return a set of properties for an entity based on the schema definition.
   * EntityDefaultMetadataController's entityPropertyInfo method call this.
   */
  protected function convertSchema() {
    $properties = parent::convertSchema();

    // Determine which property names should be renamed
    foreach ($properties as $name => &$property_info) {
      //$camel_case_name = static::toCamelCase($name);
      //if ($camel_case_name !== $name) {
      if (strstr($name, '_') !== FALSE) {
        // Improve the label for properties with underscores in names
        $property_info['label'] = str_replace('_', ' ', $properties[$name]['label']);
      }
    }

    return $properties;
  }

  /**
   * Return a name in camel-case format, while removing underscores.
   */
  private static function toCamelCase($name) {
    $parts = explode('_', $name);
    $is_success = array_walk($parts, function(&$part, $key) {
        if ($key !== 0) {
          $part = drupal_ucfirst($part);
        }
      });

    return ($is_success ? implode('', $parts) : $name);
  }
}
