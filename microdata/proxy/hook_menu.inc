<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

/**
 * Adds the hook_menu info for the HTML-microdata proxy web-service.
 */
function _badge_depot_append_microdata_proxy_menu(&$items) {
  $items[BADGE_DEPOT_PROXY_MICRODATA_PATH] = array(
    'title' => 'Web service for proxying of HTML microdata (such as LRMI data)',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeDepotMicrodataProxy::requestJsonResponse',
    'file' => 'core/delivery/json.inc',
    'delivery callback' => 'badge_depot_deliver_json',
    'access callback' => TRUE,  /* Public access */
  );
}
