<?php
/**
 * @file
 * Contains BadgeDepotMicrodataProxy.
 */

// Include the dependency files, for example to define BADGE_DEPOT_ROOT.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';

class BadgeDepotMicrodataProxy extends BadgeDepotWebProxy {

  /**
   * Implements a proxy web-service for HTML-microdata retrieval.
   *
   * @return
   *   Upon success, an array containing the proxied data for JSON-rendering
   *   purposes; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED).
   *   The return value should be processed by this module's JSON delivery
   *   callback function (i.e., badge_depot_deliver_json).
   */
  public static function requestJsonResponse($document_id = NULL) {
    $proxy = new BadgeDepotMicrodataProxy();
    $result_json = $proxy->getJsonResponse(
      $document_id,
      'badge_depot_microdata_proxy',
      (BADGE_DEPOT_MODULE_NAME . ':microdata_proxy:'),
      '#m1crodataC@CHE',
      'BadgeMicrodataUpdate');
    return $result_json;
  }

  /**
   * Overrides the base method to implement completion actions
   * that need to be performed when a queued proxy-task finish
   * downloading any requested HTML-microdata (e.g., LRMI data).
   */
  protected function queueItemDownloadComplete(&$queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents) {
    $rule_result = FALSE;
    switch ($completion_rule) {
      case 'BadgeMicrodataUpdate':
        // For successfully downloaded HTML microdata (e.g., with
        // LRMI and CCSS info), update the badge-memo entity-data.
        if ($download_status === TRUE) {
          $rule_result = $this->updateMemoEntity($document_contents, $document_url);
        }

        break;
      default:
        $rule_result = parent::queueItemDownloadComplete($queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents);
        break;
    }

    return $rule_result;
  }

  // Implements a method for data validation of downloaded HTML with microdata.
  protected function validateData($data, $source_url, $http_status_code) {
    // Check for HTML microdata.
    $is_debug_mode = FALSE;
    $is_valid_data = FALSE;
    require_once BADGE_DEPOT_ROOT . '/includes/MicrodataPHP/MicrodataPhp.inc';
    try {
      $parser = new MicrodataPhp(NULL, $data);
      $microdata = $parser->obj();
      if (isset($microdata)) {
        $data = _badge_depot_json_encode_pretty($microdata, $is_debug_mode);
        $is_valid_data = isset($data);
        if (!$is_valid_data) {
          watchdog(BADGE_DEPOT_MODULE_NAME, 'Invalid HTML microdata detected (#@httpCode) at !source.', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank')))), WATCHDOG_WARNING);
        }
      }
      else {
        watchdog(BADGE_DEPOT_MODULE_NAME, 'No HTML microdata detected (#@httpCode) at !source.', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank')))), WATCHDOG_WARNING);
      }
    }
    catch (Exception $e) {
      $is_valid_data = FALSE;
      watchdog(BADGE_DEPOT_MODULE_NAME, 'Very bad HTML microdata detected (#@httpCode) at !source (@error).', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank'))), '@error' => $e->getMessage()), WATCHDOG_WARNING);
    }

    if (!$is_valid_data) {
      // Replace invalid data with default content.
      $data = '{}';
    }

    return $data;
  }

  // Update the badge-memo entity-data with HTML microdata (such as LRMI and CCSS info).
  protected function updateMemoEntity($badge_criteria_json, $badge_criteria_url) {
    $rule_result = FALSE;
    // Ignore unsafe URLs.
    if ((!empty($badge_criteria_json)) && ($badge_criteria_json != '{}') && (!empty($badge_criteria_url)) && ($badge_criteria_url === drupal_strip_dangerous_protocols($badge_criteria_url))) {
      // Extract some criteria data.
      $badge_criteria_data = drupal_json_decode($badge_criteria_json);
      $activity_url = '';
      if (isset($badge_criteria_data['items'][0]['type'][0]) && (strcasecmp(($badge_criteria_data['items'][0]['type'][0]), 'http://schema.org/WebPage/BadgeCriteria') === 0) && isset($badge_criteria_data['items'][0]['properties']['url'][0])) {
        $activity_url = static::getAbsoluteUrl(($badge_criteria_data['items'][0]['properties']['url'][0]), $badge_criteria_url);
      }

      // Attempt to format the JSON data to remain
      // constant after entity-export round-trips.
      $pretty_json = (empty($badge_criteria_data) ? '{}' : _badge_depot_json_encode_pretty($badge_criteria_data));
      if (is_string($pretty_json)) {
        $badge_criteria_json = $pretty_json;
      }

      // Load all badge-memo entities associated
      // with this this criteria URL (if any).
      $memo_entities = BadgeMemoEntity::loadMultipleFromCriteriaURL($badge_criteria_url);
      if (!empty($memo_entities)) {
        foreach ($memo_entities as $memo_entity_id => $memo_entity) {
          // Synchronize the criteria metadata with all the entities.
          $is_modified = FALSE;
          $current_criteria_json = $memo_entity->criteria_meta;
          $current_activity_url = $memo_entity->activity_url;

          if ($current_criteria_json !== $badge_criteria_json) {
            $is_modified = TRUE;
            $memo_entity->criteria_meta = $badge_criteria_json;
          }

          if (empty($current_activity_url) && (!empty($activity_url))) {
            $is_modified = TRUE;
            $memo_entity->activity_url = $activity_url;
          }

          // Upon success, update the result counter.
          if ($is_modified && ($memo_entity->save() !== FALSE)) {
            if (is_bool($rule_result)) {
              $rule_result = 1;
            }
            else {
              $rule_result++;
            }
          }
        }
      }
    }

    return $rule_result;
  }

  /**
   * Retrieves a fully-qualified URL, given a base URL to be used if needed.
   */
  private static function getAbsoluteUrl($relative_url, $base_url) {
    // No processing needed if already an absolute URL.
    if (parse_url($relative_url, PHP_URL_SCHEME) != '') {
      return $relative_url;
    }

    if (!isset($relative_url)) {
      // Replace NULL with empty string.
      $relative_url = '';
    }

    // Handle queries and anchors.
    $first_relative_character = substr($relative_url, 0, 1);
    if (($first_relative_character === '#') || ($first_relative_character === '?')) {
      return ($base_url . $relative_url);
    }

    // Extract the various path components from the base URL.
    $base_parts = parse_url($base_url);

    // Remove non-directory element from path.
    if (isset($base_parts['path'])) {
      $path = preg_replace('#/[^/]*$#', '', $base_parts['path']);
    }
    else {
      $path = '';
    }

    // Clear path if relative URL is rooted.
    if ($first_relative_character === '/') {
      $path = '';
    }

    // Build the initial absolute URL that may still be dirty.
    $absolute_url = '';
    if (isset($base_parts['host'])) {
      $absolute_url = $base_parts['host'];
    }

    $port = (isset($base_parts['port']) ? ($base_parts['port']) : '');
    if (!empty($port)) {
      $absolute_url .= (':' . $port);
    }

    $absolute_url .= ($path . '/' . $relative_url);

    // Cleanup the URL by substituting '/' for '//' or '/./' or '/foo/../'.
    $regex = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    $replacement_count = 0;
    do {
      $absolute_url = preg_replace($regex, '/', $absolute_url, -1, $replacement_count);
    } while ($replacement_count > 0);

    // Build the final absolute URL.
    $scheme = (isset($base_parts['scheme']) ? ($base_parts['scheme']) : '');
    $user = (isset($base_parts['user']) ? ($base_parts['user']) : '');
    $pass = (isset($base_parts['pass']) ? ($base_parts['pass']) : '');
    if ((!empty($user)) || (!empty($pass))) {
      $absolute_url = ('@' . $absolute_url);
      if (!empty($pass)) {
        $absolute_url = (':' . $pass . $absolute_url);
      }

      if (!empty($user)) {
        $absolute_url = ($user . $absolute_url);
      }
    }

    if (!empty($scheme)) {
      $absolute_url = ($scheme . '://' . $absolute_url);
    }

    return $absolute_url;
  }
}
