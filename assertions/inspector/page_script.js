/**
 * @file
 * Contains the client-side browser-code to inspect data of a badge.
 */

function IsBadgeDetailShown() {
  if (IsValueDefined(Drupal.settings.badge_depot.showBadgeDetail)) {
    return Drupal.settings.badge_depot.showBadgeDetail;
  }

  return false;
}

function GetBadgeBakerPrefixUrl() {
  if (IsValueDefined(Drupal.settings.badge_depot.badgeBakerUrlPrefix)) {
    return Drupal.settings.badge_depot.badgeBakerUrlPrefix;
  }

  return 'http://beta.openbadges.org/baker?assertion=';
}

function GetBadgeRecipientValidatorUrl() {
  if (IsValueDefined(Drupal.settings.badge_depot.recipientValidatorUrl)) {
    return Drupal.settings.badge_depot.recipientValidatorUrl;
  }

  return '../assertions/check_recipient';
}

function GetBadgeAssertionsProxyUrl() {
  if (IsValueDefined(Drupal.settings.badge_depot.assertionsProxyUrl)) {
    return Drupal.settings.badge_depot.assertionsProxyUrl;
  }

  return '../assertions/proxy';
}

function GetBadgeMicrodataProxyUrl() {
  if (IsValueDefined(Drupal.settings.badge_depot.microdataProxyUrl)) {
    return Drupal.settings.badge_depot.microdataProxyUrl;
  }

  return '../microdata/proxy';
}

function GetDefaultEmailEncoded() {
  if (IsValueDefined(Drupal.settings.badge_depot.defaultEmailEncoded)) {
    return Drupal.settings.badge_depot.defaultEmailEncoded;
  }

  return '';
}

function GetResultRowCount() {
  if (IsValueDefined(Drupal.settings.badge_depot.resultRowCount)) {
    return Drupal.settings.badge_depot.resultRowCount;
  }

  if (IsValueDefined(window.resultRowCount)) {
    return window.resultRowCount;
  }

  return 0;
}

function IncrementResultRowCount(deltaCount) {
  if (IsValueDefined(Drupal.settings.badge_depot.resultRowCount)) {
    Drupal.settings.badge_depot.resultRowCount += deltaCount;
  }
  else if (typeof window.resultRowCount === 'undefined') {
    window.resultRowCount = deltaCount;
  }
  else {
    window.resultRowCount = deltaCount += deltaCount;
  }
}

function IsValueDefined(value) {
  return ((typeof value !== 'undefined') && (value !== null) && (value !== ''));
}

function GetBadgeIssuerResult() {
  return jQuery('#BadgeIssuerResult').text();
}

function SetBadgeIssuerResult(message) {
  jQuery('#BadgeIssuerResult').text(message);
}

function SetBadgeValidationResult(message) {
  if (IsValueDefined(message)) {
    jQuery('#RecipientValidationResultText').text(message);
    jQuery('#RecipientValidationResultRow').show();
  }
  else {
    jQuery('#RecipientValidationResultRow').hide();
  }
}

function GetBadgeMicrodataResult() {
  return jQuery('#BadgeMicrodataResult').text();
}

function SetBadgeMicrodataResult(message) {
  jQuery('#BadgeMicrodataResult').text(message);
}

function EncodeHtmlEntities(text) {
  return jQuery('<div />').text(text).html();  // e.g., escapes "&", "<" and ">", but not the single or double quotes
}

function EncodeHtmlAttributes(text) {
  return text.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;');
}

function AddBadgeTableRowRaw(label, valueHtml, rowIdentifier, stripeKind) {
  if (IsValueDefined(valueHtml)) {
    var labelEncoded;
    if (label === null) {
      labelEncoded = '&nbsp;';
    }
    else {
      labelEncoded = ('<label>' + EncodeHtmlEntities(label) + ':</label>');
    }

    var rowAttributes = '';
    var isDetailRow = false;
    if (!IsValueDefined(stripeKind)) {
      stripeKind = (IsValueDefined(label) ? true : 'same');  // Apply default stiping
    }
    else if (stripeKind === 'detail') {
      isDetailRow = true;
      stripeKind = ((IsValueDefined(label) && IsBadgeDetailShown()) ? true : 'same');  // Apply default stiping
    }

    switch (String(stripeKind).toLowerCase()) {
      case 'odd':
        IncrementResultRowCount(((GetResultRowCount() % 2) == 1) ? 0 : 1);  // Ensure row count is odd
        rowAttributes += ' class="odd"';
        break;
      case 'even':
        IncrementResultRowCount(((GetResultRowCount() % 2) == 1) ? 1 : 0);  // Ensure row count is even
        rowAttributes += ' class="even"';
        break;
      case 'true':
        IncrementResultRowCount(1);
        // Fall through to "same" logic
      case 'same':
        rowAttributes += (' class="' + (((GetResultRowCount() % 2) == 1) ? 'odd' : 'even') + '"');
        break;
      case 'false':  // i.e., none
      default:
        break;
    }

    if (IsValueDefined(rowIdentifier)) {
      rowAttributes += (' id="' + rowIdentifier + '"');
    }

    jQuery('#ResultsTable').append('<tr' + rowAttributes + '><td>' + labelEncoded + '</td><td>' + valueHtml + '</td></tr>');
    if (isDetailRow && (!IsBadgeDetailShown()) && IsValueDefined(rowIdentifier)) {
      jQuery('#' + rowIdentifier).hide();
    }
  }
}

function AddBadgeTableRow(label, value, rowIdentifier, stripeKind) {
  if (IsValueDefined(value)) {
    var valueEncoded = EncodeHtmlEntities(value);
    AddBadgeTableRowRaw(label, valueEncoded, rowIdentifier, stripeKind);
  }

  return value;
}

function AddBadgeTableUrl(label, url, baseUrl, linkText, rowIdentifier, stripeKind) {
  var qualifiedUrl;
  if (IsValueDefined(url)) {
    var valueEncoded;
    if (IsValueDefined(linkText)) {
      valueEncoded = EncodeHtmlEntities(linkText);
    }
    else {
      valueEncoded = EncodeHtmlEntities(url);
    }

    qualifiedUrl = jQuery.trim(String(url));
    if (qualifiedUrl.indexOf('http') !== 0) {
      baseUrl = jQuery.trim(String(baseUrl));
      if ((baseUrl !== '') && (baseUrl.indexOf('/', (baseUrl.length - 1)) < 0) && (qualifiedUrl.indexOf('/') !== 0)) {
        qualifiedUrl = ('/' + qualifiedUrl);
      }

      qualifiedUrl = (baseUrl + qualifiedUrl);
    }

    var valueHtml = ('<a target="_blank" href="' + EncodeHtmlAttributes(qualifiedUrl) + '">' + valueEncoded + '</a>');
    AddBadgeTableRowRaw(label, valueHtml, rowIdentifier, stripeKind);
  }

  return qualifiedUrl;
}

function AddBadgeTableEmail(label, email, rowIdentifier, stripeKind) {
  if (IsValueDefined(email)) {
    var valueEncoded = EncodeHtmlEntities(email);
    var valueHtml = ('<a href="mailto:' + EncodeHtmlAttributes(jQuery.trim(email)) + '">' + valueEncoded + '</a>');
    AddBadgeTableRowRaw(label, valueHtml, rowIdentifier, stripeKind);
  }
}

function HandleRecipientValidationJSON() {
  var validationSalt = jQuery('#RecipientValidationSalt').val();
  var validationHash = jQuery('#RecipientValidationHash').val();
  var validationEmail = jQuery('#RecipientValidationEmail').val();
  jQuery('#RecipientValidationButton').each(function() { this.disabled = true; });
  jQuery('#RecipientValidationEmail').each(function() { this.disabled = true; });
  SetBadgeValidationResult('Checking...');
  var queryString = ('email=' + encodeURIComponent(validationEmail) +
    '&recipient=' + encodeURIComponent(validationHash) +
    '&salt=' + encodeURIComponent(validationSalt));
  var proxyURL = (GetBadgeRecipientValidatorUrl() + '?' + queryString);
  jQuery.ajax({
    dataType: 'json',
    url: proxyURL,
    success: function(data) {  // Success handler
      jQuery('#RecipientValidationButton').each(function() { this.disabled = false; });
      jQuery('#RecipientValidationEmail').each(function() { this.disabled = false; });
      if (data.match) {
        SetBadgeValidationResult('Yes, the badge was awarded to ' + validationEmail + '!');
      }
      else {
        SetBadgeValidationResult('No, it looks like the badge was awarded to someone else!');
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {  // Error handler
      jQuery('#RecipientValidationButton').each(function() { this.disabled = false; });
      jQuery('#RecipientValidationEmail').each(function() { this.disabled = false; });
      SetBadgeValidationResult('Problem encountered while validating recipient (' + textStatus + ').');
    }
  });
}

function UpdateBadgeIssuerResult() {
  SetBadgeIssuerResult(GetBadgeIssuerResult() + '.');
}

function HandleAssertionJSON(assertionURL, asyncState) {
  var maxRetryCount = 15;
  asyncState = asyncState || {};
  if (!IsValueDefined(asyncState.assertion)) {
    asyncState.assertion = {
      retryCount: 0,
      error: '',
      assertionURL: assertionURL,
      queryString: ('url=' + encodeURIComponent(assertionURL)),
      badgeBakerURL: (GetBadgeBakerPrefixUrl() + encodeURIComponent(assertionURL)),
    };
  }
  else if (++asyncState.assertion.retryCount >= maxRetryCount) {
    asyncState.assertion.retryCount = -1;
    if (asyncState.assertion.error === '') {
      asyncState.assertion.error = 'Timeout encountered while loading assertion data. Please try again later.';
    }

    SetBadgeIssuerResult(asyncState.assertion.error);
    asyncState.assertion.error = '';
    return;
  }

  var proxyBaseURL = (GetBadgeAssertionsProxyUrl() + '?');
  var proxyURL = (proxyBaseURL + asyncState.assertion.queryString);
  jQuery.ajax({
    dataType: 'json',
    url: proxyURL,
    success: function(data) {  // Success handler
      if (data.proxy) {
        asyncState.assertion.proxy = data.proxy;
        UpdateBadgeIssuerResult();
        asyncState.assertion.queryString = ('id=' + encodeURIComponent(data.proxy.id));
        window.setTimeout(function() { HandleAssertionJSON(null, asyncState); }, 1000);
        window.setTimeout(UpdateBadgeIssuerResult, 750);
        if (asyncState.assertion.retryCount === 0) {
          jQuery.ajax({
            dataType: 'json',
            url: (proxyBaseURL + 'async=1'),
            success: function(data) {  // Success handler
              if (data.error) {
                asyncState.assertion.error = ('Problem encountered while loading assertion data. ' + data.error);
                asyncState.assertion.retryCount = maxRetryCount;
              }
              else if (data.errors) {
                asyncState.assertion.error = ('Problems encountered while loading assertion data. ' + data.errors.toString());
                asyncState.assertion.retryCount = maxRetryCount;
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {  // Error handler
              asyncState.assertion.error = ('Problem encountered while loading assertion data (' + textStatus + ').');
              asyncState.assertion.retryCount = maxRetryCount;
            }
          });
        }
      }
      else if (data.badge) {
        asyncState.assertion.data = data;
        var originPrefix = '';
        if ((data.badge.issuer) && IsValueDefined(data.badge.issuer.origin)) {
          originPrefix = data.badge.issuer.origin;
        }

        SetBadgeIssuerResult('');
        AddBadgeTableRow('Badge\xA0Name', data.badge.name);
        AddBadgeTableRow('Description', data.badge.description);
        if (IsBadgeDetailShown()) {
          AddBadgeTableRow('OBI\xA0Version', data.badge.version);
        }

        if (IsValueDefined(AddBadgeTableRow('Recipient', data.recipient, 'BadgeRecipientRow', 'detail'))) {
          var checkEmailHtml =
            '<input type="hidden" id="RecipientValidationHash" value="" />' +
            '<input type="hidden" id="RecipientValidationSalt" value="" />' +
            '<input type="input" id="RecipientValidationEmail" size="60" value="' + GetDefaultEmailEncoded() + '" />' +
            '<input type="button" id="RecipientValidationButton" value="Check Recipient" />';
          AddBadgeTableRowRaw('Validate\xA0Email', checkEmailHtml);
          jQuery('#RecipientValidationHash').val(data.recipient);
          AddBadgeTableRowRaw(null, '<span id="RecipientValidationResultText"></span>', 'RecipientValidationResultRow', 'same');
          SetBadgeValidationResult(null);
          jQuery('#RecipientValidationButton').click(
            function()
            {
              HandleRecipientValidationJSON();
            });
        }

        if (IsValueDefined(AddBadgeTableRow('Validation\xA0Salt', data.salt, 'BadgeValidationSaltRow', 'detail'))) {
          jQuery('#RecipientValidationSalt').val(data.salt);
        }

        var criteriaPageUrl = AddBadgeTableUrl('Criteria', data.badge.criteria, originPrefix);
        if (IsValueDefined(criteriaPageUrl)) {
          if (IsBadgeDetailShown()) {
            AddBadgeTableUrl(null, ('http://www.google.com/webmasters/tools/richsnippets?url=' + encodeURIComponent(criteriaPageUrl)), '', 'View any microdata using Google\'s Structured Data Testing Tool');
          }

          AddBadgeTableRowRaw(null, '<span id="BadgeMicrodataResult"></span>', 'BadgeMicrodataResultRow', 'same');
          SetBadgeMicrodataResult('Scanning for Common Core State Standards data...');
          HandleMicrodataJSON(criteriaPageUrl, asyncState);
        }

        var sourceImageUrl = AddBadgeTableUrl('Source\xA0Image', data.badge.image, originPrefix, null, 'BadgeSourceImageUrlRow', 'detail');
        if (sourceImageUrl) {
          var prefix = (IsValueDefined(jQuery('#BadgeImages').html()) ? 'awarded vs. source ' : '');
          jQuery('#BadgeImages').append(prefix + '<img width="90" title="Source badge image" src="' + EncodeHtmlAttributes(sourceImageUrl) + '" />');
          jQuery('#BadgeImagesRow').show();
          AddBadgeTableUrl(null, asyncState.assertion.badgeBakerURL, '', 'Recreate and download the personalized badge', 'BadgeImageBakeRow', false);
          jQuery('#BadgeImageBakeRow').insertAfter('#BadgeImagesRow');
          jQuery('#BadgeImageBakeRow').each(function() { this.className = jQuery('#BadgeImagesRow')[0].className; });  // Use same theme as the row above
        }

        AddBadgeTableUrl('Evidence', data.evidence, originPrefix);
        if (data.badge.issuer) {
          AddBadgeTableUrl('Issuer\xA0Origin', data.badge.issuer.origin, '');
          AddBadgeTableRow('Issuer\xA0Name', data.badge.issuer.name);
          AddBadgeTableRow('Issuer\xA0Organization', data.badge.issuer.org);
          AddBadgeTableEmail('Issuer\xA0Contact', data.badge.issuer.contact);
        }

        AddBadgeTableRow('Issued\xA0On', data.issued_on);
        AddBadgeTableRow('Expires', data.expires);
      }
      else if (data.revoked) {
        SetBadgeIssuerResult('Important: This badge has been revoked!');
      }
      else if (jQuery.isEmptyObject(data)) {
        SetBadgeIssuerResult('Oops, the badge-assertion data appears to be missing! The badge may have been revoked.');
      }
      else {
        SetBadgeIssuerResult('Finished, but strange badge-assertion data was encountered! Please contact the badge issuer to investigate further.');
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {  // Error handler
      SetBadgeIssuerResult('Problem encountered while processing assertion data (' + textStatus + ').');
    }
  });
}

function HandleMicrodataJSON(criteriaPageUrl, asyncState) {
  asyncState = asyncState || {};
  if (!IsValueDefined(asyncState.microdata)) {
    asyncState.microdata = {
      retryCount: 0,
      criteriaPageUrl: criteriaPageUrl,
      queryString: ('url=' + encodeURIComponent(criteriaPageUrl)),
    };
  }
  else if (++asyncState.microdata.retryCount >= 15) {
    asyncState.microdata.retryCount = -1;
    SetBadgeMicrodataResult('Timeout encountered while loading criteria microdata. Please try again later.');
    return;
  }

  var proxyBaseURL = (GetBadgeMicrodataProxyUrl() + '?');
  var proxyURL = (proxyBaseURL + asyncState.microdata.queryString);
  jQuery.ajax({
    dataType: 'json',
    url: proxyURL,
    success: function(data) {  // Success handler
      if (data.proxy) {
        asyncState.microdata.proxy = data.proxy;
        SetBadgeMicrodataResult(GetBadgeMicrodataResult() + '.');
        asyncState.microdata.queryString = ('id=' + encodeURIComponent(data.proxy.id));
        window.setTimeout(function() { HandleMicrodataJSON(null, asyncState); }, 1000);
        if (asyncState.microdata.retryCount === 0) {
          jQuery.ajax({dataType: 'json', url: (proxyBaseURL + 'async=1')});
        }
      }
      else if (data.items) {
        asyncState.microdata.items = data.items;
        var alignmentHtml = '';
        var alignmentCount = 0;
        var ccssCount = 0;
        try {
          var itemCount = data.items.length;
          for (var i = 0; i < itemCount; i++) {
            var properties = data.items[i]['properties'];
            for (var propertyName in properties) {
              if (propertyName.toLowerCase() === 'educationalalignment') {
                var eduItems = properties[propertyName];
                var eduCount = eduItems.length;
                for (var j = 0; j < eduCount; j++) {
                  var eduItem = eduItems[j];
                  var eduType = eduItem['type'][0];
                  if (eduType.toLowerCase().indexOf('http://www.lrmi.net/the-specification/alignment-object') === 0) {
                    alignmentCount++;
                    var eduProps = eduItem['properties'];
                    var targetCCSS = '';
                    var targetName = '';
                    var targetDescription = '';
                    var targetUrl = '';
                    for (var eduPropName in eduProps) {
                      switch (eduPropName.toLowerCase()) {
                        case 'suggestedccss':
                          ccssCount++;
                          targetCCSS = eduProps[eduPropName][0];
                          break;
                        case 'targetname':
                          targetName = eduProps[eduPropName][0];
                          break;
                        case 'targetdescription':
                          targetDescription = eduProps[eduPropName][0];
                          break;
                        case 'targeturl':
                          targetUrl = jQuery.trim(eduProps[eduPropName][0]);
                          break;
                      }
                    }

                    if ((targetDescription != '') || (targetUrl != '') || (targetName != '') || (targetCCSS != '')) {
                      alignmentHtml += '<li>';
                      if (targetDescription !== '') {
                        alignmentHtml += (EncodeHtmlEntities(targetDescription) + ' ');
                      }

                      var targetText = '';
                      if ((targetName != '') || (targetCCSS != '')) {
                        if (targetName != '') {
                          targetText += targetName;
                        }

                        if ((targetName != '') && (targetCCSS != '')) {
                          targetText += '; ';
                        }

                        if (targetCCSS != '') {
                          targetText += ('CCSS: ' + targetCCSS);
                        }
                      }

                      if ((targetUrl != '') || (targetText != '')) {
                        alignmentHtml += '(';
                        if (targetUrl == '') {
                          alignmentHtml += EncodeHtmlEntities(targetText);
                        }
                        else {
                          if (targetText == '') {
                            targetText = 'link';
                          }

                          alignmentHtml += ('<a target="_blank" href="' + EncodeHtmlAttributes(targetUrl) + '">' + EncodeHtmlEntities(targetText) + '</a>');
                        }

                        alignmentHtml += ')';
                      }

                      alignmentHtml += '</li>';
                    }
                  }
                }
              }
            }
          }

          if ((alignmentCount === 0) && (ccssCount === 0)) {
            SetBadgeMicrodataResult('We cannot find any Common Core State Standards (CCSS) or Learning Resource Metadata Initiative (LRMI) microdata for the badge! If you are the creator of the badge\'s educational content, please include this information on your criteria page.');
          }
          else if (ccssCount === 0) {
            SetBadgeMicrodataResult('We found ' + alignmentCount + ' alignment(s) with the Learning Resource Metadata Initiative (LRMI), but no reference to Common Core State Standards (CCSS) was identified. If you are the creator of the badge\'s educational content, please include CCSS information on your criteria page.');
          }
          else {
            SetBadgeMicrodataResult('We found ' + alignmentCount + ' alignment(s) with the Learning Resource Metadata Initiative (LRMI), including ' + ccssCount + ' reference(s) to Common Core State Standards (CCSS)!');
          }
        }
        catch (err) {
          SetBadgeMicrodataResult('Problem encountered while reading criteria microdata (' + err.message + ').');
        }

        if (alignmentHtml !== '') {
          jQuery('#BadgeMicrodataResult').append('<ul>' + alignmentHtml + '</ul>');
        }
      }
      else {
        SetBadgeMicrodataResult('Finished, but strange criteria microdata was encountered!');
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {  // Error handler
      SetBadgeMicrodataResult('Problem encountered while processing criteria microdata (' + textStatus + ').');
    }
  });
}

// Defines a jQuery method that can be invoked by Drupal AJAX commands to start
// the downloading of assertion data on pages such as the Backpack Inspector.
jQuery.fn.BadgeDepotInitiateAssertionJSON = function(assertionURL) {
  if (!IsValueDefined(assertionURL)) {
    assertionURL = jQuery('#BadgeAssertionLink').attr('href');
  }

  SetBadgeIssuerResult('Loading...');
  HandleAssertionJSON(assertionURL);
}

// Automatically start downloading of assertion data for pages that include this
// script during the initial page-loading sequence (i.e., as opposed to through
// AJAX requests that need to call BadgeDepotInitiateAssertionJSON explicitly).
jQuery(window).ready(function() {
  if (document.readyState != 'complete') {
    jQuery.fn.BadgeDepotInitiateAssertionJSON();
  }
});
