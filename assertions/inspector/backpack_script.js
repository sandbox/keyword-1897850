/**
 * @file
 * Contains the client-side browser-code to inspect Mozilla Backpack badges.
 */

function GetBadgeDepotBaseUrl() {
  if (typeof Drupal.settings.badge_depot.base_url !== 'undefined') {
    return Drupal.settings.badge_depot.base_url;
  }

  return '';
}

function GetCurrentBadgeUserEmail() {
  if (typeof Drupal.settings.badge_depot.obi_user_email !== 'undefined') {
    return Drupal.settings.badge_depot.obi_user_email;
  }

  return '';
}

function GetCurrentBadgeUserID() {
  if (typeof Drupal.settings.badge_depot.obi_user_id !== 'undefined') {
    return Drupal.settings.badge_depot.obi_user_id;
  }

  return 0;
}

function GetBadgeDisplayerApiBaseUrl() {
  if (typeof Drupal.settings.badge_depot.obi_displayer_api !== 'undefined') {
    return Drupal.settings.badge_depot.obi_displayer_api;
  }

  return '';
}

function GetBadgeDisplayerApiGroupsUrl() {
  // Example: "http://beta.openbadges.org/displayer/21/groups.json".
  return (GetBadgeDisplayerApiBaseUrl() + GetCurrentBadgeUserID() + '/groups.json');
}

function GetBadgeDisplayerApiGroupUrl(groupID) {
  // Example: "http://beta.openbadges.org/displayer/5797/group/1957.json".
  return (GetBadgeDisplayerApiBaseUrl() + GetCurrentBadgeUserID() + '/group/' + groupID + '.json');
}

function SetBadgeDisplayerApiStatus(message) {
  jQuery('#badge_obi_api_status').text(message);
}

function BadgeDepotDisableSelectButton(isDisabled) {
  jQuery('[name=BadgeSelect]').hide().each(function() { this.disabled = isDisabled; });
}

jQuery.fn.BadgeDepotGetUserGroups = function() {
  SetBadgeDisplayerApiStatus('Loading backpack data...');
  BadgeDepotDisableSelectButton(true);
  var url = GetBadgeDisplayerApiGroupsUrl();
  jQuery.ajax({dataType: 'json', url: url, success: BadgeDepotGetUserGroupsSuccess, error: BadgeDepotGetUserGroupsFailure});
};

function BadgeDepotGetUserGroupsSuccess(data) {
  if (data.groups) {
    try {
      var groupCount = data.groups.length;
      if (groupCount <= 0) {
        SetBadgeDisplayerApiStatus('Sorry, this user has no public backpack groups.');
      }
      else {
        SetBadgeDisplayerApiStatus('');
        var formatBadgeLabel = function(group) {
          var countDescription;
          if (group.badges == 1) {
            countDescription = '1 badge';
          }
          else {
            countDescription = ('' + group.badges + ' badges');
          }

          return ('' + group.name + ' (' + countDescription + ')');
        }

        var selectedGroupID = 0;
        if (groupCount == 1) {
          // Automatically use this user's one-and-only shared group.
          var group = data.groups[0];
          var label = formatBadgeLabel(group);
          jQuery('#badge_obi_groups_wrapper').text('Shared Group: ' + label);
          if (group.badges > 0) {
            selectedGroupID = group.groupId;
          }
        }
        else {
          // Create a drop-down selection-box for the badge groups.
          jQuery('#badge_obi_groups_wrapper').html('Shared Groups: <select id="badge_obi_group_select"></select>');

          // Populate the drop-down selection-box with options for each group.
          for (var i = 0; i < groupCount; i++) {
            var group = data.groups[i];
            var label = formatBadgeLabel(group);
            var option = jQuery('<option></option>').attr('value', group.groupId).text(label);
            if ((selectedGroupID <= 0) && (group.badges > 0)) {
              option.each(function() { this.selected = true; });
              selectedGroupID = group.groupId;
            }

            jQuery('#badge_obi_group_select').append(option);
          }

          // Attach a handler for the selection-change event.
          jQuery('#badge_obi_group_select').change(
            function() {
              jQuery("option:selected", this).each(
                function() {
                  BadgeDepotGetGroupData(jQuery(this).val());
                }
              );
            }
          );
        }

        // Select the first group to be loaded.
        if (selectedGroupID > 0) {
          BadgeDepotGetGroupData(selectedGroupID);
        }
      }
    }
    catch (err) {
      SetBadgeDisplayerApiStatus('Problem encountered while reading the backpack data (' + err.message + ').');
    }
  }
  else {
    SetBadgeDisplayerApiStatus('Finished loading the backpack, but strange data was encountered!');
  }
}

function BadgeDepotGetUserGroupsFailure(jqXHR, textStatus, errorThrown) {
  SetBadgeDisplayerApiStatus('Problem encountered while loading the backpack data (' + textStatus + ').');
}

function BadgeDepotGetGroupData(groupID) {
  SetBadgeDisplayerApiStatus('Loading backpack group...');
  BadgeDepotDisableSelectButton(true);

  // Store the group ID for submission.
  jQuery('[name=badgeGroupID]').val(groupID);

  var url = GetBadgeDisplayerApiGroupUrl(groupID);
  jQuery.ajax({dataType: 'json', url: url, success: BadgeDepotGetGroupDataSuccess, error: BadgeDepotGetGroupDataFailure});
};

function BadgeDepotGetGroupDataSuccess(data) {
  if (data.badges) {
    try {
      SetBadgeDisplayerApiStatus('');
      var badgeCount = data.badges.length;
      if (badgeCount <= 0) {
        jQuery('#badge_obi_badges_wrapper').html('<p>This group contains no badges.</p>');
      }
      else {
        BadgeDepotDisableSelectButton(false);

        // Create a list for selecting a specific badge from the group.
        jQuery('#badge_obi_badges_wrapper').html('');

        // Populate the drop-down selection-box with options for each badge.
        for (var i = 0; i < badgeCount; i++) {
          var badge = data.badges[i];
          var imageUrl;
          try {
            imageUrl = badge.imageUrl;
          }
          catch (err) {
            imageUrl = '';
          }

          var hostedUrl;
          var hostedUrl;
          try {
            hostedUrl = badge.hostedUrl;
          }
          catch (err) {
            hostedUrl = '';
          }

          var label;
          try {
            label = ('Last validated: ' + new Date(Date.parse(badge.lastValidated)).toLocaleString());
          }
          catch (err) {
            label = '';
          }

          var badgeImage = jQuery('<img></img>').each(function() { this.src = imageUrl; this.width = 90; this.height = 90; this.border = 0; this.style.margin = '10px'; });
          var badgeItem = jQuery('<a target="_blank"></a>').each(function() { this.href = hostedUrl; this.title = label; }).append(badgeImage).click(function(event) { event.preventDefault(); BadgeDepotGetAssertionInfo(jQuery(this).attr('href'), this, event); return false; });
          jQuery('#badge_obi_badges_wrapper').append(badgeItem);
        }
      }
    }
    catch (err) {
      SetBadgeDisplayerApiStatus('Problem encountered while reading the backpack group (' + err.message + ').');
    }
  }
  else {
    SetBadgeDisplayerApiStatus('Finished loading the backpack group, but strange data was encountered!');
  }
}

function BadgeDepotGetGroupDataFailure(jqXHR, textStatus, errorThrown) {
  SetBadgeDisplayerApiStatus('Problem encountered while loading the backpack group (' + textStatus + ').');
}

function BadgeDepotGetAssertionInfo(assertionURL, badgeImageElement, clickEvent) {
  if ((typeof assertionURL === 'undefined') || (assertionURL === null) || (assertionURL === '')) {
    alert('Sorry, but the selected badge does not have an assertion URL to follow.');
    return;
  }

  // Store the assertion URL for submission.
  jQuery('[name=badgeAssertion]').val(assertionURL);

  // Trigger automatic submission of the form for badge selection.
  jQuery('[name=BadgeSelect]').trigger('mousedown');
  //var badgeSelectTriggerQuery = jQuery('[name=BadgeSelect]');
  //var badgeSelectTriggerElement = badgeSelectTriggerQuery[0];
  //var badgeSelectTriggerID = badgeSelectTriggerQuery.attr('id');
  //Drupal.ajax[badgeSelectTriggerID].eventResponse(badgeSelectTriggerElement, clickEvent);
  //badgeSelectTriggerQuery.trigger('mousedown');
  //jQuery('form[id$="inspector-backpack-form"]').submit();  // _triggering_element_name = BadgeSelect ???
}
