<?php
/**
 * @file
 * Contains BadgeDepotInspectorBackpackPage.
 */

/**
 * Implements a form page for the Badge Inspector that takes a Mozilla
 * Backpack user's badges as input (that is the "backpack" inspector).
 */
class BadgeDepotInspectorBackpackPage extends BadgeDepotInspectorPageBase {

  public function __construct() {
    parent::__construct();
  }

  // Define the identifiers of the form fields that contain submission data.
  const FORM_FIELD_EMAIL = 'badgeUserEmail';
  const FORM_FIELD_USER_ID = 'badgeUserID';
  const FORM_FIELD_GROUP_ID = 'badgeGroupID';
  const FORM_FIELD_ASSERTION_URL = 'badgeAssertion';
  const FORM_FIELD_DETAIL = 'detail';

  // Define the identifiers of buttons that trigger submission of data,
  // for example, "UserLookup" maps to validateTriggerUserLookup,
  // submitTriggerUserLookup, ajaxTriggerUserLookup, etc.
  const FORM_BUTTON_USER_LOOKUP = 'UserLookup';
  const FORM_BUTTON_BADGE_SELECT = 'BadgeSelect';

  // Define the HTML identifiers for AJAX-replacement contents.
  const AJAX_CONTENT_IDENTIFY_USER = 'badge_backpack_fieldset';
  const AJAX_CONTENT_SELECT_BADGE = 'badge_select_fieldset';
  const AJAX_CONTENT_OBI_GROUPS = 'badge_obi_groups';
  const AJAX_CONTENT_OBI_BADGES = 'badge_obi_badges';
  const AJAX_CONTENT_OBI_STATUS = 'badge_obi_api_status';

  // Define the HTML identifiers for AJAX-replacement wrappers.
  const AJAX_WRAPPER_IDENTIFY_USER = 'badge_backpack_fieldset_wrapper';
  const AJAX_WRAPPER_SELECT_BADGE = 'badge_select_fieldset_wrapper';
  const AJAX_WRAPPER_OBI_GROUPS = 'badge_obi_groups_wrapper';
  const AJAX_WRAPPER_OBI_BADGES = 'badge_obi_badges_wrapper';
  const AJAX_WRAPPER_OBI_STATUS = 'badge_obi_api_status_wrapper';

  // Define the identifiers of the various steps/stages of this form:
  // Obtain the user's email address and OBI user-ID.
  const FORM_STEP_IDENTIFY_USER = 1;
  // Got the user's email and ID; next select the group, ID and assertion-URL of a badge.
  const FORM_STEP_SELECT_BADGE = 2;
  // Selected one badge; finally display the badge's information.
  const FORM_STEP_COMPLETION = 3;

  protected function &getStoredEmail(&$form_state, $default = NULL) {
    $storage_value = &$this->getStorageValue($form_state, self::FORM_FIELD_EMAIL, $default);
    return $storage_value;
  }

  protected function &setStoredEmail(&$form_state, $value) {
    $storage_value = &$this->setStorageValue($form_state, self::FORM_FIELD_EMAIL, $value);
    return $storage_value;
  }

  protected function &getStoredUserID(&$form_state, $default = NULL) {
    $storage_value = &$this->getStorageValue($form_state, self::FORM_FIELD_USER_ID, $default);
    return $storage_value;
  }

  protected function &setStoredUserID(&$form_state, $value) {
    $storage_value = &$this->setStorageValue($form_state, self::FORM_FIELD_USER_ID, $value);
    return $storage_value;
  }

  protected function &getStoredGroupID(&$form_state, $default = NULL) {
    $storage_value = &$this->getStorageValue($form_state, self::FORM_FIELD_GROUP_ID, $default);
    return $storage_value;
  }

  protected function &setStoredGroupID(&$form_state, $value) {
    $storage_value = &$this->setStorageValue($form_state, self::FORM_FIELD_GROUP_ID, $value);
    return $storage_value;
  }

  protected function &getStoredAssertionURL(&$form_state, $default = NULL) {
    $storage_value = &$this->getStorageValue($form_state, self::FORM_FIELD_ASSERTION_URL, $default);
    return $storage_value;
  }

  protected function &setStoredAssertionURL(&$form_state, $value) {
    $storage_value = &$this->setStorageValue($form_state, self::FORM_FIELD_ASSERTION_URL, $value);
    return $storage_value;
  }

  protected function &getStoredDetailFlag(&$form_state, $default = NULL) {
    $storage_value = &$this->getStorageValue($form_state, self::FORM_FIELD_DETAIL, $default);
    return $storage_value;
  }

  protected function &setStoredDetailFlag(&$form_state, $value) {
    $storage_value = &$this->setStorageValue($form_state, self::FORM_FIELD_DETAIL, $value);
    return $storage_value;
  }

  // Builds the initial form and rebuilds the form for the various subsequent steps/states.
  public function buildForm($form, &$form_state) {
    // Reference the current step of the form.
    $step_id = &$this->getFormStepID($form_state, self::FORM_STEP_IDENTIFY_USER);

    // Retrieve the data of the user-identification stage.
    $badge_user_email = $this->getStoredEmail($form_state);
    $badge_user_id = $this->getStoredUserID($form_state);
    if (($step_id > self::FORM_STEP_IDENTIFY_USER) && (empty($badge_user_email) || empty($badge_user_id))) {
      // Synch'ed with storage.
      $step_id = self::FORM_STEP_IDENTIFY_USER;
    }

    if (empty($badge_user_email)) {
      // Get access to the current Drupal user, if any.
      global $user;
      $values = &$form_state['values'];
      // Synch'ed with storage.
      $badge_user_email = (isset($values[self::FORM_FIELD_EMAIL]) ? $values[self::FORM_FIELD_EMAIL] : (isset($user->mail) ? ((string) ($user->mail)) : ''));
      $badge_user_id = NULL;
    }

    // Retrieve the data of the badge-selection stage.
    $badge_group_id = $this->getStoredGroupID($form_state);
    $badge_assertion_url = $this->getStoredAssertionURL($form_state);

    // Details shown by default when logged in as a Drupal administrator.
    $detail_box_checked = $this->getStoredDetailFlag($form_state, user_access('access administration pages'));
    if (($step_id > self::FORM_STEP_SELECT_BADGE) && (empty($badge_group_id) || empty($badge_assertion_url) || (!isset($detail_box_checked)))) {
      // Synch'ed with storage.
      $step_id = self::FORM_STEP_SELECT_BADGE;
    }

    // Start building the form.
    $form['intro'] = array('#markup' => ('<p>' . t('Use the tool below to view the details of badges in an OBI badge backpack.') . '</p>'));

    $is_collapsed = ($step_id > self::FORM_STEP_SELECT_BADGE);
    $backpack_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Badge Backpack'),
      '#description' => t('The following steps will guide you through selecting a badge.'),
      '#collapsible' => $is_collapsed,
      '#collapsed' => $is_collapsed,
      '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_IDENTIFY_USER),
      '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
    );
    $form[self::AJAX_CONTENT_IDENTIFY_USER] = &$backpack_fieldset;
    $backpack_fieldset[self::FORM_FIELD_USER_ID] = array(
      '#type' => 'hidden',
      '#default_value' => '',
      '#value' => $badge_user_id
    );
    $backpack_fieldset[self::FORM_FIELD_GROUP_ID] = array(
      '#type' => 'hidden',
      '#default_value' => '',
      '#value' => $badge_group_id,
    );
    $backpack_fieldset[self::FORM_FIELD_ASSERTION_URL] = array(
      '#type' => 'hidden',
      '#default_value' => '',
      '#value' => $badge_assertion_url,
    );

    $is_collapsed = ($step_id > self::FORM_STEP_IDENTIFY_USER);
    $user_fieldset = array(
      '#type' => 'fieldset',
      '#title' => t('Step 1 of 2: Identify the backpack user'),
      '#description' => t('To start, enter the email address of a backpack user.'),
      '#collapsible' => $is_collapsed,
      '#collapsed' => $is_collapsed,
      '#prefix' => '<br />',
    );
    $backpack_fieldset['user_fieldset'] = &$user_fieldset;
    $user_fieldset[self::FORM_FIELD_EMAIL] = array(
      '#type' => 'textfield',
      '#title' => t('Badge Recipient Email'),
      '#description' => t('Someone that shared his/her badge collection from a !url.', array('!url' => l(t('Mozilla Backpack'), 'http://backpack.openbadges.org/backpack', array('attributes' => array('target' => '_blank'))))),
      '#default_value' => $badge_user_email,
      '#required' => TRUE,
    );
    $user_fieldset[self::FORM_BUTTON_USER_LOOKUP] = array(
      '#type' => 'submit',
      '#name' => self::FORM_BUTTON_USER_LOOKUP,
      '#value' => t('View Backpack'),
      '#ajax' => array(
        'callback' => '_badge_depot_form_proxy_ajax',
        'wrapper' => self::AJAX_WRAPPER_IDENTIFY_USER,
      ),
    );

    if ($step_id >= self::FORM_STEP_SELECT_BADGE) {
      $is_collapsed = ($step_id > self::FORM_STEP_SELECT_BADGE);
      $badge_select_fieldset = array(
        '#type' => 'fieldset',
        '#title' => t('Step 2 of 2: Select a badge'),
        '#description' => t('Click the specific badge that you want to learn more about.  Badges are organized by group.'),
        '#collapsible' => TRUE,
        '#collapsed' => $is_collapsed,
        '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_SELECT_BADGE),
        '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
        '#attached' => array(
          'js' => array(
            array(
              'data' => array(BADGE_DEPOT_MODULE_NAME => array(
                'base_url' => _badge_depot_get_module_url(),
                'obi_displayer_api' => BadgeDepotGlobalSettings::getObiDisplayerApiBackpackUrl(),
                'obi_user_id' => $badge_user_id,
                'obi_user_email' => $badge_user_email,
              )),
              'type' => 'setting',
            ),
            (_badge_depot_get_module_path() . '/assertions/inspector/backpack_script.js'),
          ),
        ),
      );
      $backpack_fieldset[self::AJAX_CONTENT_SELECT_BADGE] = &$badge_select_fieldset;
      $badge_select_fieldset[self::AJAX_CONTENT_OBI_GROUPS] = array(
        '#markup' => '',
        '#prefix' => ('<br />' . $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_OBI_GROUPS)),
        '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
      );
      $badge_select_fieldset[self::AJAX_CONTENT_OBI_BADGES] = array(
        '#markup' => '',
        '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_OBI_BADGES),
        '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
      );
      $badge_select_fieldset[self::AJAX_CONTENT_OBI_STATUS] = array(
        '#markup' => format_string(
          '!prefix<b>@content</b>!suffix',
          array(
            '@content' => t('To download the backpack data, JavaScript must be enabled.'),
            '!prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_CONTENT_OBI_STATUS),
            '!suffix' => $this->getAjaxWrapperMarkupSuffix(),
          )
        ),
        '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_OBI_STATUS),
        '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
      );
      $badge_select_fieldset[self::FORM_FIELD_DETAIL] = array(
        '#type' => 'checkbox',
        '#title' => t('Include advanced details upon selection'),
        '#default_value' => ($detail_box_checked ? 1 : 0),
      );
      $badge_select_fieldset[self::FORM_BUTTON_BADGE_SELECT] = array(
        '#type' => 'submit',
        '#name' => self::FORM_BUTTON_BADGE_SELECT,
        '#value' => t('Proceed with Selected Badge'),
        '#ajax' => array(
          'callback' => '_badge_depot_form_proxy_ajax',
          'wrapper' => self::AJAX_WRAPPER_COMPLETION,
          'progress' => array(
            'type' => 'throbber',
            'message' => 'Loading badge detail...',
          ),
        ),
        //'#prefix' => '<div class="element-hidden">',
        //'#suffix' => '</div>',
      );
    }

    if ($step_id >= self::FORM_STEP_COMPLETION) {
      $this->renderBadgeInfo($form, $detail_box_checked, 0, NULL, $badge_assertion_url, $badge_user_email);
    }
    else {
      $this->renderBadgeInfoWrapper($form);
    }

    return parent::buildForm($form, $form_state);
  }

  // Implements the validation handler for a Mozilla user-identifier, given an email address.
  protected function validateTriggerUserLookup($form, &$form_state) {
    // Synchronize the current step with the one submitted from the browser.
    $step_id = &$this->setFormStepID($form_state, self::FORM_STEP_IDENTIFY_USER);
    $values = &$form_state['values'];

    // Check that the a valid email-address was provided.
    // NOTE: The email's "required" requirement is automatically checked by Drupal.
    $email = (isset($values[self::FORM_FIELD_EMAIL]) ? $values[self::FORM_FIELD_EMAIL] : NULL);
    if (!empty($email)) {
      if (!valid_email_address($email)) {
        // Reject email addresses that are not formatted well.
        form_set_error(self::FORM_FIELD_EMAIL, t('%recipient is an invalid e-mail address.', array('%recipient' => $email)));
      }
      else {
        // Maps the specified email-address to a Mozilla user-ID.
        $user_id = self::getMozillaUserID($email);
        if (empty($user_id)) {
          // Reject email addresses that are not known to the Mozilla OBI API.
          form_set_error(self::FORM_FIELD_EMAIL, t('%recipient does not have a Mozilla Backpack.', array('%recipient' => $email)));
        }
        else {
          // Now that we have a valid email-address and Mozilla user-ID, save this info for later use.
          $email = &$this->setStoredEmail($form_state, $email);
          $user_id = &$this->setStoredUserID($form_state, $user_id);

          // Move to the next step, now that valid data exists for the current step.
          // Synch'ed with storage.
          $step_id++;
        }
      }
    }
  }

  // Implements the submit handler that gets called once
  // the email-lookup step is completely validated.
  // NOTE: This method gets called only if the data that was submitted
  //       for the email-lookup step was valid and placed in storage.
  protected function submitTriggerUserLookup($form, &$form_state) {
    // Rebuild the form with the current valid
    // user-info that are available in storage.
    $form_state['rebuild'] = TRUE;
  }

  // Implements the AJAX-callback handler that gets called
  // once the email-lookup step's web-request completes.
  // NOTE: This method gets called for every AJAX email-lookup request,
  //       irrespective of success; however, this method is not called
  //       if the lookup request was made without JavaScript support (in
  //       which case the full form from the rebuild step is returned).
  protected function ajaxTriggerUserLookup($form, &$form_state) {
    // Select the region to be replaced by the AJAX response.
    $output = $form[self::AJAX_CONTENT_IDENTIFY_USER];

    // If any validation errors occurred, only handle those.
    $errors = form_get_errors();
    if (!empty($errors)) {
      // Includes default error handling (to display messages).
      return $output;
    }

    // Render the next step on the form.
    $commands = array();
    $commands[] = ajax_command_insert(NULL, drupal_render($output));
    //$commands[] = ajax_command_replace(('#' + self::AJAX_WRAPPER_COMPLETION), drupal_render($form[self::AJAX_CONTENT_COMPLETION]));

    // Ensure that any error and status messages get displayed.
    $commands[] = ajax_command_prepend(NULL, theme('status_messages'));

    // Continue with the current user info, if no error occurred.
    $commands[] = ajax_command_invoke(NULL, 'BadgeDepotGetUserGroups');

    //$email = $this->getStoredEmail($form_state);
    //$user_id = $this->getStoredUserID($form_state);
    //$status_message = "$email => $user_id";
    //$commands[] = ajax_command_alert($status_message);
    //$commands[] = ajax_command_invoke(NULL, 'BadgeDepotGetUserGroups', array($email));

    return array('#type' => 'ajax', '#commands' => $commands);
  }

  // Implements the validation handler for a badge selection, given an assertion URL.
  protected function validateTriggerBadgeSelect($form, &$form_state) {
    // Synchronize the current step with the one submitted from the browser.
    $step_id = &$this->setFormStepID($form_state, self::FORM_STEP_SELECT_BADGE);
    $input = &$form_state['input'];
    $values = &$form_state['values'];

    // Check that the a valid assertion-URL was provided.
    $assertion_url = (isset($input[self::FORM_FIELD_ASSERTION_URL]) ? $input[self::FORM_FIELD_ASSERTION_URL] : NULL);
    $group_id = (isset($input[self::FORM_FIELD_GROUP_ID]) ? $input[self::FORM_FIELD_GROUP_ID] : NULL);
    $detail_box_checked = (isset($values[self::FORM_FIELD_DETAIL]) ? $values[self::FORM_FIELD_DETAIL] : NULL);
    if (empty($assertion_url)) {
      form_set_error(self::FORM_FIELD_ASSERTION_URL, t('The assertion URL is missing.'));
    }
    elseif (!valid_url($assertion_url, TRUE)) {
      // Reject assertion URLs that are not formatted correctly.
      form_set_error(self::FORM_FIELD_ASSERTION_URL, t('The assertion URL is invalid (%url).', array('%url' => $assertion_url)));
    }
    else {
      // Now that we have a valid assertion-URL, save this info for later use.
      $values[self::FORM_FIELD_ASSERTION_URL] = $assertion_url;
      $values[self::FORM_FIELD_GROUP_ID] = $group_id;
      $assertion_url = &$this->setStoredAssertionURL($form_state, $assertion_url);
      $group_id = &$this->setStoredGroupID($form_state, $group_id);
      $detail_box_checked = &$this->setStoredDetailFlag($form_state, $detail_box_checked);

      // Move to the next step, now that valid data exists for the current step.
      // Synch'ed with storage.
      $step_id++;
    }
  }

  // Implements the submit handler that gets called once the
  // assertion-URL selection step is completely validated.
  // NOTE: This method gets called only if the data that was submitted
  //       for the assertion-URL step was valid and placed in storage.
  protected function submitTriggerBadgeSelect($form, &$form_state) {
    // Rebuild the form with the current valid assertion-info that are available in storage.
    $form_state['rebuild'] = TRUE;
  }

  // Implements the AJAX-callback handler that gets called once
  // the assertion-URL selection step's web-request completes.
  // NOTE: This method gets called for every AJAX assertion-selection request,
  //       irrespective of success; however, this method is not called
  //       if the selection request was made without JavaScript support
  //       (in which case the full form from the rebuild step is returned).
  protected function ajaxTriggerBadgeSelect($form, &$form_state) {
    // Select the region to be replaced by the AJAX response.
    $output = $form[self::AJAX_CONTENT_COMPLETION];

    // If any validation errors occurred, only handle those.
    $errors = form_get_errors();
    if (!empty($errors)) {
      // Includes default error handling (to display messages).
      return $output;
    }

    // Render the next step on the form.
    $commands = array();
    $commands[] = ajax_command_replace(NULL, drupal_render($output));

    // Ensure that any error and status messages get displayed.
    $commands[] = ajax_command_prepend(NULL, theme('status_messages'));

    // Continue with the current badge info, if no error occurred.
    $commands[] = ajax_command_invoke(NULL, 'BadgeDepotInitiateAssertionJSON');
    //$commands[] = ajax_command_invoke(NULL, 'BadgeDepotInitiateAssertionJSON', array($this->getStoredAssertionURL($form_state)));
    //$commands[] = ajax_command_alert($this->getStoredAssertionURL($form_state));

    return array('#type' => 'ajax', '#commands' => $commands);
  }

  // Retrieves the user identifier for the specified email address of a user at the Mozilla OBI API.
  protected static function getMozillaUserID($user_email = NULL) {
    $user_id = NULL;
    if (empty($user_email)) {
      // Get access to the current Drupal user, if any.
      global $user;
      $user_email = (isset($user->mail) ? ((string) ($user->mail)) : '');
    }

    if (!empty($user_email)) {
      // Detect any previously cached user-identifier.
      $cache_id = (BADGE_DEPOT_MODULE_NAME . ':obi_uid:' . hash('md5', ($user_email . '#Salt1n3ss')));
      $cache_bin = 'cache';
      $cache_now = time();
      $cache_item = cache_get($cache_id, $cache_bin);
      if ($cache_item !== FALSE) {
        // Extract data; may be expired.
        $user_id = $cache_item->data;
      }

      // Call the web-API, if needed, to get the user ID.
      if ((!isset($user_id)) || ($cache_now >= $cache_item->expire)) {
        $data = array('email' => $user_email);
        $json = static::getWebPostResponseJson(BadgeDepotGlobalSettings::getObiDisplayerApiUserUrl(), $data);

        // JSON will be NULL upon failure, so check for that.
        if (isset($json['userId'])) {
          $user_id = $json['userId'];
        }

        // Store the latest user-info to the cache, even if
        // it is outdated (that is to avoid frequent web calls).
        if (!empty($user_id)) {
          // 14 days in terms of seconds.
          $cache_expire_seconds = (14 * 24 * 60 * 60);
          $cache_expire = ($cache_now + $cache_expire_seconds);
          cache_set($cache_id, $user_id, $cache_bin, $cache_expire);
        }
      }
    }

    return $user_id;
  }

  // Performs a POST web-request and returns the content of the response as a string.
  protected static function getWebPostResponseString($url, $data = NULL, $authorization_username = NULL, $authorization_password = NULL) {
    $response = FALSE;
    if ((!empty($url)) && (DataUriCreatorString::startsWith($url, 'http://') || DataUriCreatorString::startsWith($url, 'https://'))) {
      $options = array(
        'http' => array(
          'method' => 'POST',
          'header' => array('Content-type: application/x-www-form-urlencoded'),
        ),
      );

      if (!empty($authorization_username)) {
        $options['http']['header'][] = ('Authorization: Basic ' . base64_encode("$authorization_username:$authorization_password"));
      }

      if (isset($data)) {
        $content = http_build_query($data);
        $options['http']['content'] = $content;
      }

      $context = stream_context_create($options);

      // Note: Response may be FALSE upon failure.
      $response = file_get_contents($url, FALSE, $context);
    }

    return $response;
  }

  // Performs a POST web-request and returns the content of the response as decoded JSON data.
  protected static function getWebPostResponseJson($url, $data = NULL, $decode_to_array = TRUE, $authorization_username = NULL, $authorization_password = NULL) {
    $json = NULL;

    // Note: Response may be FALSE upon failure.
    $response = static::getWebPostResponseString($url, $data, $authorization_username, $authorization_password);
    if ($response !== FALSE) {
      // Note: JSON result may be NULL upon failure.
      $json = json_decode($response, $decode_to_array);
    }

    return $json;
  }

  // Implements the main handler for AJAX callbacks of this form.
  public function ajaxForm($form, &$form_state) {
    return parent::ajaxForm($form, $form_state);
  }

  // Implements the main handler for validation callbacks of this form.
  public function validateForm($form, &$form_state) {
    parent::validateForm($form, $form_state);
  }

  // Implements the main handler for submission callbacks of this form.
  public function submitForm($form, &$form_state) {
    return parent::submitForm($form, $form_state);
  }
}
