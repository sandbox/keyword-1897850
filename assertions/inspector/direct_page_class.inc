<?php
/**
 * @file
 * Contains BadgeDepotInspectorDirectPage.
 */

/**
 * Implements a form page for the Badge Inspector that takes
 * a hosted badge-assertion URL as input (that is "direct" data).
 */
class BadgeDepotInspectorDirectPage extends BadgeDepotInspectorPageBase {

  public function __construct() {
    parent::__construct();
  }

  const FORM_FIELD_DEBUG = 'debug';
  const FORM_FIELD_DETAIL = 'detail';
  const FORM_FIELD_ASSERTION_URL = 'badgeAssertion';

  public function buildForm($form, &$form_state) {
    // Debug does not show anything useful yet for this page.
    $debug_box_offered = FALSE;

    // NOTE: Do not change this default value; if needed, change the default
    // value of $this->debug_mode instead.
    $debug_box_checked = FALSE;

    $detail_box_checked = FALSE;
    $badge_assertion_url = '';
    if ($this->is_post_request) {
      $detail_box_checked = isset($_POST[self::FORM_FIELD_DETAIL]);
      $debug_box_checked = ($debug_box_offered && isset($_POST[self::FORM_FIELD_DEBUG]));
      $this->debug_mode = ($this->debug_mode || $debug_box_checked);
      $badge_assertion_url = (isset($_POST[self::FORM_FIELD_ASSERTION_URL]) ? ((string) $_POST[self::FORM_FIELD_ASSERTION_URL]) : '');
    }

    $form['intro'] = array('#markup' => t('<p>Use the tool below to view the details of a hosted badge-assertion.</p>'));
    $has_user_input = (($this->is_post_request) && (!empty($badge_assertion_url)));
    $this->renderMainForm($form, $detail_box_checked, $has_user_input, $debug_box_offered, $debug_box_checked, $badge_assertion_url);
    if ($has_user_input) {
      $this->renderBadgeInfo($form, $detail_box_checked, 0, NULL, $badge_assertion_url);
    }

    return parent::buildForm($form, $form_state);
  }

  public function submitForm($form, &$form_state) {
    // Do not redirect after a page submit, but display the newly built page
    // instead.  Also, do not call the base class's submitForm method, since
    // we do not want to display the default form-submitted message.
    $form_state['redirect'] = FALSE;
  }

  // Append the badge-assertion GUI to the output render-array
  protected function renderMainForm(&$output, $detail_box_checked, $is_form_collapsed, $debug_box_offered, $debug_box_checked, $badge_assertion_url) {
    $form = array(
      '#type' => 'form',
      '#attributes' => array('enctype' => 'multipart/form-data'),
      '#method' => 'post');
    $output['assertion_url_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Hosted Badge Assertion'),
      '#description' => t('To start, enter the hosted assertion\'s URL for the awarded badge to be inspected.'),
      '#collapsible' => $is_form_collapsed,
      '#collapsed' => $is_form_collapsed,
      'badge_upload_form' => &$form);

    $form[self::FORM_FIELD_ASSERTION_URL] = array(
      '#title' => t('Assertion&nbsp;URL'),
      '#type' => 'textfield',
      '#size' => 80,
      '#default_value' => $badge_assertion_url,
      '#required' => TRUE,
      '#attributes' => array('name' => self::FORM_FIELD_ASSERTION_URL),
    );

    $form[self::FORM_FIELD_DETAIL] = array(
      '#title' => t('Show advanced details'),
      '#type' => 'checkbox',
      '#default_value' => ($detail_box_checked ? 1 : 0),
      '#attributes' => array('name' => self::FORM_FIELD_DETAIL),
    );

    if ($debug_box_offered) {
      $form[self::FORM_FIELD_DEBUG] = array(
        '#title' => t('Show debug information'),
        '#type' => 'checkbox',
        '#default_value' => (($this->debug_mode) ? 1 : 0),
        '#disabled' => ((!$debug_box_checked) && ($this->debug_mode)),
        '#attributes' => array('name' => self::FORM_FIELD_DEBUG),
      );
    }

    $form['start_button'] = array('#type' => 'submit', '#value' => t('Inspect Badge Assertion'));
  }
}
