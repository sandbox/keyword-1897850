<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

/**
 * Adds the hook_menu info for the badge-inspector pages.
 */
function _badge_depot_append_assertions_inspector_menu(&$items) {
  $items[BADGE_DEPOT_INSPECTOR_DEFAULT_PATH] = array(
    'title' => 'Inspect a Badge',
    'type' => MENU_CALLBACK,
    // This include file handles OOP Forms API callbacks.
    'file' => 'core/forms/page_callback.inc',
    'page callback' => 'drupal_get_form',
    // Badge Inspector page that takes a badge-image upload
    // as input (for example, the "default" inspector).
    'page arguments' => array('BadgeDepotInspectorDefaultPage'),
    'access callback' => TRUE,  /* Public access */
  );

  $items[BADGE_DEPOT_INSPECTOR_DIRECT_PATH] = array(
    'title' => 'Inspect a Badge Assertion',
    'type' => MENU_CALLBACK,
    // This include file handles OOP Forms API callbacks.
    'file' => 'core/forms/page_callback.inc',
    'page callback' => 'drupal_get_form',
    // Badge Inspector page that takes a hosted badge-assertion
    // URL as input (for example, "direct" data).
    'page arguments' => array('BadgeDepotInspectorDirectPage'),
    'access arguments' => array('authenticated user'),  /* Logged-in access */
  );

  $items[BADGE_DEPOT_INSPECTOR_BACKPACK_PATH] = array(
    'title' => 'Inspect a Badge Backpack',
    'type' => MENU_CALLBACK,
    // This include file handles OOP Forms API callbacks.
    'file' => 'core/forms/page_callback.inc',
    'page callback' => 'drupal_get_form',
    // Badge Inspector page that takes a shared Mozilla OBI
    // backpack as input (for example, the "backpack" inspector).
    'page arguments' => array('BadgeDepotInspectorBackpackPage'),
    'access callback' => TRUE,  /* Public access */
  );
}
