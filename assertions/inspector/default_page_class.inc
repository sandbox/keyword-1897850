<?php
/**
 * @file
 * Contains BadgeDepotInspectorDefaultPage.
 */

/**
 * Implements a form page for the Badge Inspector that takes a
 * badge-image upload as input (that is the "default" inspector).
 */
class BadgeDepotInspectorDefaultPage extends BadgeDepotInspectorPageBase {

  public function __construct() {
    parent::__construct();
  }

  const FORM_FIELD_DEBUG = 'debug';
  const FORM_FIELD_DETAIL = 'detail';
  const FORM_FIELD_BADGE_FILE = 'badgeImage';

  public function buildForm($form, &$form_state) {
    // Defaults to 256KB as required by the OBI assertion specification.
    $max_file_size = 262144;

    // Offer the debug option when logged in as a Drupal administrator.
    $debug_box_offered = user_access('access administration pages');

    // NOTE: Do not change this default value; if needed, change the default
    // value of $this->debug_mode instead.
    $debug_box_checked = FALSE;

    $detail_box_checked = FALSE;
    if ($this->is_post_request) {
      $detail_box_checked = isset($_POST[self::FORM_FIELD_DETAIL]);
      $debug_box_checked = ($debug_box_offered && isset($_POST[self::FORM_FIELD_DEBUG]));
      $this->debug_mode = ($this->debug_mode || $debug_box_checked);
    }

    $form['intro'] = array('#markup' => t('<p>Use the tool below to view the details of a badge that was issued to you or someone else.</p>'));
    $has_user_input = (isset($_FILES) && ($this->is_post_request));
    $this->renderMainForm($form, $detail_box_checked, $max_file_size, $has_user_input, $debug_box_offered, $debug_box_checked);
    if ($has_user_input) {
      static::addFileUploadErrorMessages();
      $badge_image_upload = (isset($_FILES[self::FORM_FIELD_BADGE_FILE]) ? $_FILES[self::FORM_FIELD_BADGE_FILE] : NULL);
      $this->renderBadgeInfo($form, $detail_box_checked, $max_file_size, $badge_image_upload);
    }

    return parent::buildForm($form, $form_state);
  }

  public function submitForm($form, &$form_state) {
    // Do not redirect after a page submit, but display the newly built page
    // instead.  Also, do not call the base class's submitForm method, since
    // we do not want to display the default form-submitted message.
    $form_state['redirect'] = FALSE;
  }

  // Append the badge-upload GUI to the output render-array.
  protected function renderMainForm(&$output, $detail_box_checked, $max_file_size, $is_form_collapsed, $debug_box_offered, $debug_box_checked) {
    $form = array(
      '#type' => 'form',
      '#attributes' => array('enctype' => 'multipart/form-data'),
      '#method' => 'post');
    $output['badge_upload_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Badge Upload'),
      '#description' => t('To start, upload the PNG image of the awarded badge to be inspected.'),
      '#collapsible' => $is_form_collapsed,
      '#collapsed' => $is_form_collapsed,
      'badge_upload_form' => &$form);

    if ($max_file_size > 0) {
      $form['MAX_FILE_SIZE'] = array(
        '#type' => 'hidden',
        '#value' => $max_file_size,
        '#attributes' => array('name' => 'MAX_FILE_SIZE'),
      );
    }

    $form[self::FORM_FIELD_BADGE_FILE] = array(
      '#title' => t('Badge&nbsp;PNG'),
      '#type' => 'file',
      '#size' => 80,
      '#attributes' => array('name' => self::FORM_FIELD_BADGE_FILE, 'accept' => 'image/*'),
    );

    $form[self::FORM_FIELD_DETAIL] = array(
      '#title' => t('Show advanced details'),
      '#type' => 'checkbox',
      '#default_value' => ($detail_box_checked ? 1 : 0),
      '#attributes' => array('name' => self::FORM_FIELD_DETAIL),
    );

    if ($debug_box_offered) {
      $form[self::FORM_FIELD_DEBUG] = array(
        '#title' => t('Show debug information'),
        '#type' => 'checkbox',
        '#default_value' => (($this->debug_mode) ? 1 : 0),
        '#disabled' => ((!$debug_box_checked) && ($this->debug_mode)),
        '#attributes' => array('name' => self::FORM_FIELD_DEBUG),
      );
    }

    $form['upload_button'] = array('#type' => 'submit', '#value' => t('Upload Badge Image'));
  }

  protected static function getFileUploadErrorMessage($error_code, $success_message = NULL) {
    switch ($error_code) {
      case UPLOAD_ERR_OK:
        // Value 0: There is no error; the file uploaded successfully.
        return $success_message;
      case UPLOAD_ERR_INI_SIZE:
        // Value 1.
        $error_message = 'The uploaded file exceeds the upload_max_filesize directive in "php.ini"';
        break;
      case UPLOAD_ERR_FORM_SIZE:
        // Value 2 (exceeds MAX_FILE_SIZE directive).
        $error_message = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        break;
      case UPLOAD_ERR_PARTIAL:
        // Value 3.
        $error_message = 'The uploaded file was only partially uploaded';
        break;
      case UPLOAD_ERR_NO_FILE:
        // Value 4.
        $error_message = 'No file was uploaded';
        break;
      case UPLOAD_ERR_NO_TMP_DIR:
        // Value 6.
        $error_message = 'Missing a temporary folder';
        break;
      case UPLOAD_ERR_CANT_WRITE:
        // Value 7.
        $error_message = 'Failed to write file to disk';
        break;
      case UPLOAD_ERR_EXTENSION:
        // Value 8.
        $error_message = 'File upload stopped by a PHP extension';
        break;
      default:
        $error_message = 'Unknown file upload error';
        break;
    }

    return ($error_message . ' (' . $error_code . ')');
  }

  protected static function addFileUploadErrorMessages() {
    if (isset($_FILES)) {
      foreach ($_FILES as $key => &$value) {
        $error_message = static::getFileUploadErrorMessage($value['error']);
        if (isset($error_message)) {
          $value['error_message'] = $error_message;
        }
      }
    }
  }
}
