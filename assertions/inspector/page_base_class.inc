<?php
/**
 * @file
 * Contains BadgeDepotInspectorPageBase.
 */

/**
 * Implements a base class for the Badge Inspector form pages.
 */
class BadgeDepotInspectorPageBase extends BadgeDepotFormPage {

  // Indicates whether the current web-request is a POST request (versus GET).
  protected $is_post_request;

  // Determines whether this page is to be rendered with debug information.
  protected $debug_mode;

  // Define the HTML identifiers for AJAX-replacement contents.
  const AJAX_CONTENT_COMPLETION = 'badge_info_fieldset';

  // Define the HTML identifiers for AJAX-replacement wrappers.
  const AJAX_WRAPPER_COMPLETION = 'badge_info_fieldset_wrapper';

  public function __construct() {
    parent::__construct();
    $this->is_post_request = ($_SERVER['REQUEST_METHOD'] === 'POST');
    $this->debug_mode = FALSE;
  }

  public function buildForm($form, &$form_state) {
    $this->renderDebugInfo($form);
    return parent::buildForm($form, $form_state);
  }

  // Append a wrapper for the badge-info GUI to the output render-array.
  protected function &renderBadgeInfoWrapper(&$output) {
    $result_fieldset = array(
      '#prefix' => $this->getAjaxWrapperMarkupPrefix(self::AJAX_WRAPPER_COMPLETION),
      '#suffix' => $this->getAjaxWrapperMarkupSuffix(),
    );
    $output[self::AJAX_CONTENT_COMPLETION] = &$result_fieldset;
    return $result_fieldset;
  }

  // Append the badge-info GUI to the output render-array.
  protected function renderBadgeInfo(&$output, $is_detail_requested = FALSE, $max_file_size = 0, $badge_image_upload = NULL, &$badge_assertion_url = NULL, $user_email = NULL) {
    $result_fieldset = &$this->renderBadgeInfoWrapper($output);
    $result_fieldset = array_merge($result_fieldset, array(
      '#type' => 'fieldset',
      '#title' => t('Badge Inspection Results'),
      '#description' => t('Detailed information about the awarded badge can be found below.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ));
    $rows = array();
    $result_fieldset['results_table'] = array(
      '#attributes' => array('id' => 'ResultsTable'),
      '#theme' => 'table',
      //'#header' => array(t('Label'), t('Value')),
      '#header' => array(),
      '#rows' => &$rows,
    );

    $image_source_data = NULL;
    $warning_message = NULL;
    if (isset($badge_image_upload)) {
      if ($is_detail_requested && (!empty($badge_image_upload['name']))) {
        $rows[] = array(
          static::getRowLabelMarkup('Upload&nbsp;File'),
          array('data' => (htmlentities($badge_image_upload['name']) . '&nbsp;')));
      }

      if ($is_detail_requested && ($badge_image_upload['size'])) {
        $rows[] = array(
          static::getRowLabelMarkup('File&nbsp;Size'),
          array('data' => (htmlentities($badge_image_upload['size']) . '&nbsp;bytes')));
      }

      $mime_type = $badge_image_upload['type'];
      if ($is_detail_requested && (!empty($mime_type))) {
        $rows[] = array(
          static::getRowLabelMarkup('MIME&nbsp;Type'),
          array('data' => (htmlentities($mime_type) . '&nbsp;')));
      }

      if (isset($badge_image_upload['error_message'])) {
        $rows[] = array(
          static::getRowLabelMarkup('Upload&nbsp;Error'),
          array('data' => ('<b>' . htmlentities($badge_image_upload['error_message']) . '</b>&nbsp;')));
      }

      if ($badge_image_upload['error'] == UPLOAD_ERR_OK) {
        $image_file = NULL;
        if (isset($badge_image_upload['tmp_name']) && (!empty($badge_image_upload['tmp_name']))) {
          $image_file = $badge_image_upload['tmp_name'];
        }

        if (($max_file_size > 0) && ($badge_image_upload['size'] > $max_file_size)) {
          $warning_message = 'The badge image size must be less than ' . $max_file_size . ' bytes!';
        }
        elseif (isset($image_file)) {
          $image_size = getimagesize($image_file);
          if ($image_size !== FALSE) {
            $value = htmlentities($image_size[0]) . '&nbsp;x&nbsp;' . htmlentities($image_size[1]);
          }
          else {
            $value = 'N/A';
          }

          $rows[] = array(static::getRowLabelMarkup('Pixel&nbsp;Size'), array('data' => $value));

          if (DataUriCreatorString::startsWith($mime_type, 'image/') && isset($image_file)) {
            $image_source_data = DataUriCreator::encodeFile($image_file, $mime_type);
          }

          if ($mime_type != 'image/png') {
            $warning_message = 'The badge image must be a PNG type of file!';
          }
          else {
            $badge_assertion_url = BadgeDepotImagePngMetadata::getBadgeAssertionUrl($image_file);
            if (!isset($badge_assertion_url)) {
              $warning_message = 'This image does not appear to be a valid awarded badge!';
            }
          }
        }
      }
    }

    if (isset($image_source_data) || isset($badge_assertion_url)) {
      $badge_images_row = array(
        'id' => 'BadgeImagesRow',
        'data' => array(
          array(
            'valign' => 'top',
            'data' => static::getRowLabelMarkup('Image(s)'),
          ),
          array(
            'valign' => 'top',
            'id' => 'BadgeImages',
            'data' => (isset($image_source_data) ? '<img width="90" title="Uploaded badge image" src="' . htmlspecialchars($image_source_data) . '" />&nbsp;' : ''),
          ),
        ),
      );
      $rows[] = &$badge_images_row;
      if (!isset($image_source_data)) {
        $badge_images_row['style'] = 'display: none;';
      }
    }

    if (isset($badge_assertion_url)) {
      $scripts = array();
      $rows[] = array(
        array(
          'valign' => 'top',
          'data' => static::getRowLabelMarkup('Assertion&nbsp;URL'),
        ),
        array(
          'data' => array(
            '#type' => 'link',
            '#title' => htmlentities($badge_assertion_url),
            '#href' => trim($badge_assertion_url),
            '#options' => array('attributes' => array('id' => 'BadgeAssertionLink', 'target' => '_blank')),
            '#suffix' => '&nbsp;<br /><span id="BadgeIssuerResult">For additional detail, JavaScript is required.</span>',
            '#attached' => array('js' => &$scripts),
          )
        ),
      );

      if (!isset($user_email)) {
        // Get access to the current Drupal user, if any.
        global $user;
        $user_email = (isset($user->mail) ? ($user->mail) : '');
      }

      $setting = array(
        BADGE_DEPOT_MODULE_NAME => array(
          'showBadgeDetail' => $is_detail_requested,
          'defaultEmailEncoded' => ((!empty($user_email)) ? check_plain($user_email) : ''),
          'resultRowCount' => count($rows),
          'badgeBakerUrlPrefix' => BadgeDepotGlobalSettings::getObiBadgeBakerPrefixUrl(),
          'recipientValidatorUrl' => url(BADGE_DEPOT_INSPECTOR_CHECK_RECIPIENT_PATH, array('absolute' => TRUE)),
          'assertionsProxyUrl' => url(BADGE_DEPOT_PROXY_ASSERTIONS_PATH, array('absolute' => TRUE)),
          'badgeClassesProxyUrl' => url(BADGE_DEPOT_PROXY_BADGE_CLASSES_PATH, array('absolute' => TRUE)),
          'badgeIssuersProxyUrl' => url(BADGE_DEPOT_PROXY_ISSUERS_PATH, array('absolute' => TRUE)),
          'microdataProxyUrl' => url(BADGE_DEPOT_PROXY_MICRODATA_PATH, array('absolute' => TRUE)),
        ),
      );
      $scripts[] = array('data' => $setting, 'type' => 'setting');
      $scripts[_badge_depot_get_module_path() . '/assertions/inspector/page_script.js'] = array('type' => 'file');
    }

    if (isset($warning_message)) {
      $rows[] = array(
        static::getRowLabelMarkup('WARNING'),
        array('data' => ('<b>' . htmlentities($warning_message) . '</b>&nbsp;')));
    }
  }

  // Append the debug-info GUI to the output render-array.
  protected function renderDebugInfo(&$output, $debug_info = NULL) {
    if (!$this->debug_mode) {
      return;
    }

    if (!is_array($debug_info)) {
      $debug_info = array();
    }

    if (($this->is_post_request) && isset($_FILES) && (!empty($_FILES))) {
      // Start caching of text output to an internal buffer.
      ob_start();

      // Write output to buffer.
      var_dump($_FILES);

      // Stop caching of text output, retrieve and clear buffered content.
      $value = ob_get_clean();

      $debug_info['files'] = array(
        '#type' => 'fieldset',
        '#title' => '$_FILES',
        '#description' => t('The following files were attached to the web request.'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        'content' => array('#markup' => ('<pre>' . $value . '</pre>')),
      );
    }

    if (!empty($debug_info)) {
      $output['badge_debug_fieldset'] = array_merge(
        $debug_info,
        array(
          '#type' => 'fieldset',
          '#title' => t('Debug Information'),
          '#description' => t('For development purposes, details about internal variables are shown below.'),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        )
      );
    }
  }

  protected static function getRowLabelMarkup($field_label = NULL, $field_name = NULL) {
    if (empty($field_label)) {
      return '&nbsp;';
    }
    else {
      $attributes = (empty($field_name) ? '' : (' for="' . $field_name . '"'));
      return ('<label' . $attributes . '>' . t($field_label) . ':</label>');
    }
  }
}
