<?php
/**
 * @file
 * Contains the page-callback function for the web service that validates
 * the recipients of issued badges.
 */

// Include the dependency files, for example to define BADGE_DEPOT_ROOT.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';
require_once BADGE_DEPOT_ROOT . '/core/delivery/json.inc';

/**
 * Validates the recipient of a badge assertion against a provided email
 * address to determine whether the badge was awarded to the specific user.
 */
function _badge_depot_callback_assertions_check_recipient($email = NULL, $recipient = NULL, $salt = NULL) {

  // Define a helper function that retrieves a named input argument from the web request.
  $get_input_argument = function($name) {
    return isset($_REQUEST[$name]) ? (string) $_REQUEST[$name] : NULL;
  };

  // Extract the badge arguments to be validated.
  if (!isset($recipient)) {
    $recipient = $get_input_argument('recipient');
  }

  if (!isset($salt)) {
    $salt = $get_input_argument('salt');
  }

  if (!isset($email)) {
    $email = $get_input_argument('email');
  }

  $is_recipient_matched = FALSE;
  if (isset($recipient) && isset($email) && (strpos($email, '@') !== FALSE)) {
    $delimiter_index = strpos($recipient, '$');
    if ($delimiter_index == FALSE) {
      $hash_type = NULL;
      $hash_code = $recipient;
    }
    else {
      $hash_type = substr($recipient, 0, $delimiter_index);
      $hash_code = substr($recipient, (1 + $delimiter_index));
    }

    if (in_array($hash_type, hash_algos(), TRUE) && in_array($hash_type, array('sha1', 'sha256', 'sha512', 'md5'), TRUE)) {
      $hash_test = hash($hash_type, ($email . $salt));
    }
    else {
      // Invalid signature type!
      $hash_test = $email;
    }

    $is_recipient_matched = ($hash_code === $hash_test);
  }

  return array('match' => $is_recipient_matched);
}
