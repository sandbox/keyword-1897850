<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

/**
 * Adds the hook_menu info for the badge-assertions recipient-validation web-service.
 */
function _badge_depot_append_assertions_check_recipient_menu(&$items) {
  $items[BADGE_DEPOT_INSPECTOR_CHECK_RECIPIENT_PATH] = array(
    'title' => 'Web service for validation of a badge recipient',
    'type' => MENU_CALLBACK,
    'file' => 'assertions/check_recipient/page_callback.inc',
    'page callback' => '_badge_depot_callback_assertions_check_recipient',
    'delivery callback' => 'badge_depot_deliver_json',
    'access callback' => TRUE,  /* Public access */
  );
}
