<?php
/**
 * @file
 * Contains BadgeDepotBadgeClassProxy.
 */

// Include the dependency files, for example to define BADGE_DEPOT_MODULE_NAME.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';

class BadgeDepotBadgeClassProxy extends BadgeDepotWebProxy {

  /**
   * Implements a proxy web-service for badge-class data.
   *
   * @return
   *   Upon success, an array containing the proxied data for JSON-rendering
   *   purposes; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED).
   *   The return value should be processed by this module's JSON delivery
   *   callback function (i.e., badge_depot_deliver_json).
   */
  public static function requestJsonResponse($document_id = NULL) {
    $proxy = new BadgeDepotBadgeClassProxy();
    $result_json = $proxy->getJsonResponse(
      $document_id,
      'badge_depot_badge_class_proxy',
      (BADGE_DEPOT_MODULE_NAME . ':badge_class_proxy:'),
      '#Is5u3rsC@CHE',
      'BadgeClassClosure');
    return $result_json;
  }

  /**
   * Overrides the base method to implement completion actions
   * that need to be performed when a queued proxy-task finish
   * downloading any requested badge-class data.
   */
  protected function queueItemDownloadComplete(&$queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents) {
    $rule_result = FALSE;
    switch ($completion_rule) {
      case 'BadgeClassClosure':
        // For successfully downloaded badge-class data, indicate success
        $rule_result = ($download_status === TRUE);
        break;
      default:
        $rule_result = parent::queueItemDownloadComplete($queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents);
        break;
    }

    return $rule_result;
  }

  // Implements a method for data validation of downloaded badge classes
  protected function validateData($data, $source_url, $http_status_code) {
    // Check for JSON data that looks like an OBI badge classes (i.e., definitions)
    $is_valid_data = FALSE;
    try {
      $json_data = drupal_json_decode($data);
      if (is_array($json_data) && isset($json_data['name']) && isset($json_data['description']) && isset($json_data['image']) && isset($json_data['criteria']) && isset($json_data['issuer'])) {
        $is_valid_data = TRUE;
      }
      else {
        watchdog(BADGE_DEPOT_MODULE_NAME, 'Invalid OBI badge-class detected (#@httpCode) at !source.', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank')))), WATCHDOG_WARNING);
      }
    }
    catch (Exception $e) {
      $is_valid_data = FALSE;
      watchdog(BADGE_DEPOT_MODULE_NAME, 'Very bad OBI badge-class detected (#@httpCode) at !source (@error).', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank'))), '@error' => $e->getMessage()), WATCHDOG_WARNING);
    }

    if (!$is_valid_data) {
      // Replace invalid data with default content
      $data = '{}';
    }

    return ($is_valid_data ? NULL : $data);
  }
}
