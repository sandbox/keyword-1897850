<?php
/**
 * @file
 * Contains additional hook_menu() information.
 */

/**
 * Adds the hook_menu info for the badge-assertion proxy web-services,
 * including those for the separate BadgeClass and IssuerOrganization
 * data for version 1.0 of the OBI Assertion Specification.
 */
function _badge_depot_append_assertions_proxy_menu(&$items) {
  $items[BADGE_DEPOT_PROXY_ASSERTIONS_PATH] = array(
    'title' => 'Web service for proxying of badge assertions',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeDepotAssertionsProxy::requestJsonResponse',
    'file' => 'core/delivery/json.inc',
    'delivery callback' => 'badge_depot_deliver_json',
    'access callback' => TRUE,  /* Public access */
  );

  $items[BADGE_DEPOT_PROXY_BADGE_CLASSES_PATH] = array(
    'title' => 'Web service for proxying of badge classes (i.e., definitions)',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeDepotBadgeClassProxy::requestJsonResponse',
    'file' => 'core/delivery/json.inc',
    'delivery callback' => 'badge_depot_deliver_json',
    'access callback' => TRUE,  /* Public access */
  );

  $items[BADGE_DEPOT_PROXY_ISSUERS_PATH] = array(
    'title' => 'Web service for proxying of badge issuer-organizations',
    'type' => MENU_CALLBACK,
    'page callback' => 'BadgeDepotBadgeIssuerProxy::requestJsonResponse',
    'file' => 'core/delivery/json.inc',
    'delivery callback' => 'badge_depot_deliver_json',
    'access callback' => TRUE,  /* Public access */
  );
}
