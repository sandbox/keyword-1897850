<?php
/**
 * @file
 * Contains BadgeDepotAssertionsProxy.
 */

// Include the dependency files, for example to define BADGE_DEPOT_MODULE_NAME.
require_once dirname(dirname(__DIR__)) . '/core/globals.inc';

class BadgeDepotAssertionsProxy extends BadgeDepotWebProxy {

  /**
   * Implements a proxy web-service for badge-assertion data.
   *
   * @return
   *   Upon success, an array containing the proxied data for JSON-rendering
   *   purposes; otherwise, an integer error-code (e.g., MENU_ACCESS_DENIED).
   *   The return value should be processed by this module's JSON delivery
   *   callback function (i.e., badge_depot_deliver_json).
   */
  public static function requestJsonResponse($document_id = NULL) {
    $proxy = new BadgeDepotAssertionsProxy();
    $result_json = $proxy->getJsonResponse(
      $document_id,
      'badge_depot_assertions_proxy',
      (BADGE_DEPOT_MODULE_NAME . ':assertion_proxy:'),
      '#docum3ntC@CHE',
      'BadgeAssertionCreate');
    return $result_json;
  }

  /**
   * Overrides the base method to implement completion actions
   * that need to be performed when a queued proxy-task finish
   * downloading any requested badge-assertion data.
   */
  protected function queueItemDownloadComplete(&$queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents) {
    $rule_result = FALSE;
    switch ($completion_rule) {
      case 'BadgeAssertionCreate':
        // For successfully downloaded badge-assertion data,
        // create or update the badge-memo entity-data.
        if ($download_status === TRUE) {
          $rule_result = static::createMemoEntity($document_contents, $document_url);
        }

        break;
      default:
        $rule_result = parent::queueItemDownloadComplete($queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents);
        break;
    }

    return $rule_result;
  }

  // Implements a method for data validation of downloaded assertions.
  protected function validateData($data, $source_url, $http_status_code) {
    // Check for JSON data that looks like an OBI badge assertion.
    $is_valid_data = FALSE;
    try {
      $json_data = drupal_json_decode($data);
      if (is_array($json_data) && ((isset($json_data['recipient']) && isset($json_data['badge'])) || isset($json_data['revoked']))) {
        $is_valid_data = TRUE;
      }
      else {
        watchdog(BADGE_DEPOT_MODULE_NAME, 'Invalid OBI badge assertion data detected (#@httpCode) at !source.', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank')))), WATCHDOG_WARNING);
      }
    }
    catch (Exception $e) {
      $is_valid_data = FALSE;
      watchdog(BADGE_DEPOT_MODULE_NAME, 'Very bad OBI badge assertion data detected (#@httpCode) at !source (@error).', array('@httpCode' => $http_status_code, '!source' => l($source_url, $source_url, array('attributes' => array('target' => '_blank'))), '@error' => $e->getMessage()), WATCHDOG_WARNING);
    }

    if (!$is_valid_data) {
      // Replace invalid data with default content.
      $data = '{}';
    }

    return ($is_valid_data ? NULL : $data);
  }

  // Define a helper function to extract URLs from the assertion data.
  protected static function getQualifiedUrl($url, $base_url) {
    $qualified_url = '';
    if (isset($url)) {
      $qualified_url = (string) $url;
      if (strpos($qualified_url, 'http') !== 0) {
        $base_url = (string) $base_url;
        if (($base_url !== '') && (strpos($base_url, '/', (strlen($base_url) - 1)) === FALSE) && (strpos($qualified_url, '/') !== 0)) {
          $qualified_url = ('/' . $qualified_url);
        }

        $qualified_url = ($base_url . $qualified_url);
      }
    }

    return $qualified_url;
  }

  // Create or update the badge-memo entity-data.
  protected static function createMemoEntity($assertion_json, $assertion_url) {
    // Extract some basic badge info, such as the criteria URL
    // and origin URL (i.e., the base URL for relative URLs).
    $rule_result = FALSE;
    if (isset($assertion_json)) {
      $assertion_data = drupal_json_decode($assertion_json);
      $origin_prefix = (isset($assertion_data['badge']['issuer']['origin']) ? $assertion_data['badge']['issuer']['origin'] : '');
      $badge_criteria_url = static::getQualifiedUrl((isset($assertion_data['badge']['criteria']) ? $assertion_data['badge']['criteria'] : NULL), $origin_prefix);
      // Ignore unsafe URLs.
      if ((!empty($badge_criteria_url)) && ($badge_criteria_url === drupal_strip_dangerous_protocols($badge_criteria_url))) {
        // Load the badge-memo entity associated
        // with this this criteria URL (if any).
        $safe_criteria_url = check_url($badge_criteria_url);
        $memo_entity = BadgeMemoEntity::loadFromCriteriaURL($badge_criteria_url);
        if (!isset($memo_entity)) {
          // Extract additional badge info, such as the
          // name, description, image URL and issuer detail.
          $badge_image_url = static::getQualifiedUrl((isset($assertion_data['badge']['image']) ? $assertion_data['badge']['image'] : NULL), $origin_prefix);
          $badge_name = (isset($assertion_data['badge']['name']) ? ((string) ($assertion_data['badge']['name'])) : '');
          $badge_description = (isset($assertion_data['badge']['description']) ? ((string) ($assertion_data['badge']['description'])) : '');
          $issuer_url = $origin_prefix;
          $issuer_name = (isset($assertion_data['badge']['issuer']['name']) ? ((string) ($assertion_data['badge']['issuer']['name'])) : '');
          $issuer_organization = (isset($assertion_data['badge']['issuer']['org']) ? ((string) ($assertion_data['badge']['issuer']['org'])) : '');
          $issuer_email = (isset($assertion_data['badge']['issuer']['contact']) ? ((string) ($assertion_data['badge']['issuer']['contact'])) : '');
          if (isset($issuer_email) && (!valid_email_address($issuer_email))) {
            $issuer_email = '';
          }

          if (isset($assertion_url)) {
            $safe_assertion_url = check_url($assertion_url);
          }
          else {
            $safe_assertion_url = '';
          }

          // Attempt to format the JSON data to remain
          // constant after entity-export round-trips.
          $pretty_json = (empty($assertion_data) ? '{}' : _badge_depot_json_encode_pretty($assertion_data));
          if (is_string($pretty_json)) {
            $assertion_json = $pretty_json;
          }

          // Create a new memo for this criteria URL (with sanitation).
          $memo_entity = new BadgeMemoEntity(array(
            'title' => check_plain($badge_name),
            'body' => check_markup($badge_description, 'filtered_html'),
            'image_url' => check_url($badge_image_url),
            'criteria_url' => $safe_criteria_url,
            'criteria_meta' => '',
            'source_assertion_url' => $safe_assertion_url,
            'source_assertion_json' => $assertion_json,
            'issuer_name' => check_plain($issuer_name),
            'issuer_url' => check_url($issuer_url),
            'issuer_email' => $issuer_email,
            'issuer_organization' => check_plain($issuer_organization),
            'is_published' => (BadgeDepotGlobalSettings::getAutoPublishMemoFlag() ? 1 : 0),
            'created' => time()));
          $rule_result = $memo_entity->save();
        }
      }
    }

    return $rule_result;
  }
}
