<?php
/**
 * @file
 * Contains BadgeDepotOpenSsl.
 */

// Include the dependency file(s).
require_once 'globals.inc';

/**
 * Implements a helper class with utility methods for OpenSSL functionality.
 */
abstract class BadgeDepotOpenSsl {

  /**
   * Determines whether the OpenSSL functionality is available.
   *
   * @return
   *   TRUE if OpenSSL functionality is available; otherwise FALSE.
   */
  public static function isAvailable() {
    // Avoid a crash when using openssl_sign with
    // Uniform Server on Windows during development.
    $is_signing_problematic = (stristr(PHP_OS, 'WIN') && isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'], array('localhost', '127.0.0.1')));
    return ((!$is_signing_problematic) && extension_loaded('openssl'));
  }

  /**
   * Create the a new digital signature.
   *
   * @param $public_key_pem
   *   The output variable to contain the newly generated public-key PEM data.
   * @param $private_key_pem
   *   The output variable to contain the newly generated private-key PEM data.
   * @param $clear_errors_at_start
   *   An optional boolean to indicate whether the OpenSSL error-log
   *   must be cleared when this method is starting; defaults to TRUE.
   *
   * @return
   *   TRUE upon success, such that the output variables have values;
   *   otherwise FALSE.
   */
  public static function createKey(&$public_key_pem, &$private_key_pem, $clear_errors_at_start = TRUE) {
    $public_key_pem = NULL;
    $private_key_pem = NULL;
    if ($clear_errors_at_start) {
      static::getErrors();
    }

    // Create the signature data.  Note the statement from the OBI release 1.0:
    // "For compatibility purposes, using an RSA-SHA256 is highly recommended".
    // TODO: Consider adding the configuration parameters to the Badge Depot
    //       general settings administration page (see openssl_get_md_methods
    //       and openssl_get_cipher_methods).
    $config = array(
      'private_key_type' => OPENSSL_KEYTYPE_RSA,
      'digest_alg' => 'sha256',
      'private_key_bits' => 2048,
    );
    $key_resource_id = openssl_pkey_new($config);
    if ($key_resource_id !== FALSE) {
      // Extract the private key.
      if (openssl_pkey_export($key_resource_id, $private_key_pem) === FALSE) {
        $private_key_pem = NULL;
      }
      else {
        // Extract the public key.
        $public_key_details = openssl_pkey_get_details($key_resource_id);
        if (($public_key_details !== FALSE) && isset($public_key_details['key'])) {
          $public_key_pem = $public_key_details['key'];
        }
      }

      openssl_free_key($key_resource_id);
    }

    return (isset($public_key_pem) && isset($private_key_pem));
  }

  /**
   * Retrieves the localized OpenSSL errors as a array.
   *
   * @return
   *   NULL if no error was found; otherwise an array of
   *   strings with localized text describing each error.
   */
  public static function getErrors() {
    $errors = array();
    while (TRUE) {
      $error = openssl_error_string();
      if ($error === FALSE) {
        break;
      }
      elseif (is_string($error) && ($error !== '')) {
        $errors[] = t($error);
      }
    }

    return (empty($errors) ? NULL : $errors);
  }
}
