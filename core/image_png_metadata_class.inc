<?php
/**
 * @file
 * Contains BadgeDepotImagePngMetadata.
 */

/**
 * Utility class for working with metadata of badge PNG-images.
 */
final class BadgeDepotImagePngMetadata {

  /**
   * Hides the constructor so that it cannot be instantiated or derived from.
   */
  protected function __construct() {
    // This should never execute.
  }

  /**
   * Extract the Mozilla OBI badge-assertion URL from a baked-badge PNG-image.
   *
   * @param string $filename
   *   The filename of the PNG image to be analyzed.
   *
   * @return string|null
   *   The assertion-URL of the badge, or NULL if no such metadata was found.
   */
  public static function getBadgeAssertionUrl($filename) {
    // Define the keyword of the textual information to be searched for.
    // In this case it is the OBI keyword for an Open Badges Assertion URL.
    $keyword = 'openbadges';

    // Search for the specific textual information in the PNG image.
    $badge_assertion_url = self::getTextualInformation($filename, $keyword);
    return $badge_assertion_url;
  }

  /**
   * Search for the specific textual-information in a PNG image.
   *
   * @param string $filename
   *   The filename of the PNG image to be analyzed.
   * @param string $keyword
   *   The keyword of the textual information to be searched for. For example,
   *   this can be one of the predefined keywords, such as "Title", "Comment",
   *   "Description", "Copyright", "Software", and so on, or it can be an
   *   extension keyword, such as "openbadges" for Mozilla's OBI baked-badges.
   * @param bool $keyword_ignore_case
   *   (optional) A boolean that indicates whether character-case should be
   *   ignored when looking for the text keyword. Specify TRUE to ignore case
   *   during the search (that is the default option); otherwise, use FALSE.
   *   According to the PNG Specification (ISO/IEC 15948:2003), the keywords
   *   are supposed to be case sensitive, but in practice this is apparently
   *   not implemented correctly for all PNG images. To ensure that text is
   *   found in most cases, the character case is ignored by default; when
   *   needed, use this flag to force case-sensitive searches.
   * @param bool $multiple_values
   *   (optional) Some keywords may have multiple values associated with them.
   *   Specify FALSE to indicate that only a single value should be returned,
   *   even when multiple values may be present in the PNG image for the
   *   specified keyword; this is the default value for this argument.
   *   Else, specify TRUE to indicate that all text values must be extracted.
   *
   * @return null|string|array
   *   If the specified keyword could not be found, or no text could not be
   *   decoded for the keyword, the method will return NULL.  For a successful
   *   request that has at least one valid value, either a string or an array
   *   will be returned (for a single-valued or multi-valued request,
   *   respectively). For successful single-valued requests, the returned string
   *   will be the text that is associated with the first value that was
   *   encountered in the PNG file; for successful multi-valued requests,
   *   an array of strings will be returned, containing at least one string.
   *   The keyword's associated text will be extracted as UTF-8 strings.
   *   Note that ;
   *   only the first value will be returned by this method.
   */
  protected static function getTextualInformation(
    $filename, $keyword, $keyword_ignore_case = TRUE, $multiple_values = FALSE) {

    $text = NULL;
    try {
      // Verify that a valid filename and keyword was specified.
      if (is_string($keyword) && is_string($filename) && file_exists($filename) && (!is_dir($filename))) {
        // Open the specified file for reading its binary contents.
        $file_handle = fopen($filename, 'rb');
        if ($file_handle !== FALSE) {
          // Verify the PNG image signature.
          $signature = fread($file_handle, 8);
          if ($signature === "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A") {
            // The chunk data of textual information starts with the keyword and
            // is followed by a null-character separator; setup for searching.
            $prefix = ($keyword . "\x00");
            $prefix_length = strlen($prefix);

            // Define the output variables for the chunk reader.
            $data_length = NULL;
            $chunk_type = NULL;
            $chunk_data = NULL;
            $checksum = NULL;

            // Search through the PNG-image stream for the textual information.
            while (TRUE) {
              // Read the next chunk of data from the PNG-image stream.
              self::readChunk($file_handle, $data_length, $chunk_type, $chunk_data, $checksum);
              if ((!is_string($chunk_type)) || (strlen($chunk_type) !== 4) || ($chunk_type === 'IEND') || (!is_string($checksum)) || (strlen($checksum) !== 8) || ($data_length !== strlen($chunk_data))) {
                // The end of the PNG data stream was reached; stop reading.
                break;
              }

              // Detect valid textual information for the desired keyword.
              if ((($chunk_type === 'tEXt') || ($chunk_type === 'zTXt') || ($chunk_type === 'iTXt'))
                && ($data_length >= $prefix_length)
                && DataUriCreatorString::startsWith($chunk_data, $prefix, $keyword_ignore_case)
                && ($checksum === self::calculateChecksum($chunk_type, $chunk_data))) {

                // Extract the data that follows the keyword and null-separator.
                $text_data = substr($chunk_data, $prefix_length);
                if ($text_data === FALSE) {
                  $text_data = '';
                }

                // Extract the actual text associated with the desired keyword.
                switch ($chunk_type) {
                  case 'tEXt':
                    // Convert the plain Latin-1 text to UTF-8 format.
                    $text_data = drupal_convert_to_utf8($text_data, 'ISO-8859-1');
                    if ($text_data !== FALSE) {
                      // Store the extracted UTF-8 string.
                      if ($multiple_values) {
                        $text[] = $text_data;
                      }
                      else {
                        $text = $text_data;
                      }
                    }

                    break;

                  case 'zTXt':
                    if ($text_data !== '') {
                      $compression_method = substr($text_data, 0, 1);
                      $text_data = substr($text_data, 1);
                      if (($compression_method === "\x00") && function_exists('gzuncompress')) {
                        // For compression method 0, the datastream is a zlib
                        // data-stream with deflate compression; uncompress it.
                        $text_data = @gzuncompress($text_data);
                        if ($text_data !== FALSE) {
                          // Convert the decoded Latin-1 text to UTF-8 format.
                          $text_data = drupal_convert_to_utf8($text_data, 'ISO-8859-1');
                          if ($text_data !== FALSE) {
                            // Store the extracted UTF-8 string.
                            if ($multiple_values) {
                              $text[] = $text_data;
                            }
                            else {
                              $text = $text_data;
                            }
                          }
                        }
                      }
                    }

                    break;

                  case 'iTXt':
                    // TODO: Decode the UTF-8 text embedded in this chunk.
                    break;
                }

                // When looking for a single value only, stop reading
                // the stream once the desired text value has been found.
                // Note that for multiple values the output value will be
                // an array and this condiftion will never be triggered.
                if (is_string($text)) {
                  break;
                }
              }
            }
          }

          // Close the PNG-image file that was opened.
          fclose($file_handle);
        }
      }
    }
    catch (Exception $e) {
      $text = NULL;
    }

    return $text;
  }

  /**
   * Reads the next chunk of data from a PNG-image file-stream.
   *
   * @param object $file_handle
   *   The opened file-handle resource of the PNG image to be read.
   * @param int|null $data_length
   *   An output variable to be set to the number of bytes in the data field.
   * @param string|null $chunk_type
   *   An output variable to be set to a four-bytes string defining the type
   *   of chunk data that is returned, for example, "IHDR", "tEXt" or "iTXt".
   * @param string|null $chunk_data
   *   An output variable to be set to the data bytes of the chunk, if any.
   * @param string|null $checksum
   *   An output variable to contain the Cyclic Redundancy Code (CRC) checksum.
   * @param bool $raw_checksum
   *   (optional) Specify FALSE to indicate that the checksum must be returned
   *   as a hex string, which is the default; otherwise, specify TRUE to
   *   indicate that the raw four-byte CRC must be returned.
   *
   * @return bool
   *   TRUE upon success when all values could be read; otherwise, FALSE.
   */
  protected static function readChunk(
    $file_handle, &$data_length, &$chunk_type, &$chunk_data, &$checksum, $raw_checksum = FALSE) {

    // Clear the output variables.
    $is_success = FALSE;
    $data_length = NULL;
    $chunk_type = NULL;
    $chunk_data = NULL;
    $checksum = NULL;

    // Check that a valid file-handle was specified.
    if (is_resource($file_handle)) {
      // Extract the number of bytes in the chunk's data field.
      $buffer = fread($file_handle, 4);
      if (is_string($buffer) && (strlen($buffer) === 4)) {
        $int32_array = unpack('N', $buffer);
        if (is_array($int32_array) && (count($int32_array) === 1)) {
          $data_length = reset($int32_array);

          // Read the sequence of four bytes defining the chunk type.
          $buffer = fread($file_handle, 4);
          if (is_string($buffer) && (strlen($buffer) === 4)) {
            $chunk_type = $buffer;

            // Read the data bytes appropriate to the chunk type, if any.
            if ($data_length === 0) {
              $chunk_data = '';
            }
            elseif ($data_length > 0) {
              $buffer = fread($file_handle, $data_length);
              if (is_string($buffer) && (strlen($buffer) === $data_length)) {
                $chunk_data = $buffer;
              }
            }

            if (isset($chunk_data)) {
              // Read the four-byte Cyclic Redundancy Code (CRC).
              $buffer = fread($file_handle, 4);
              if (is_string($buffer) && (strlen($buffer) === 4)) {
                // Convert the checksum to human-readable format, if requested.
                if ($raw_checksum) {
                  $checksum = $buffer;
                }
                else {
                  $checksum = bin2hex($buffer);
                }

                // The complete chunk was read successfully.
                $is_success = TRUE;
              }
            }
          }
        }
      }
    }

    return $is_success;
  }

  /**
   * Calculates the Cyclic Redundancy Code (CRC) checksum for a chunk of data.
   *
   * @param string $chunk_type
   *   The four-bytes identifier of the type of chunk data, for example, "IHDR".
   * @param string|null $chunk_data
   *   The data bytes of the chunk, if any, to be used for calculating the CRC.
   * @param bool $raw_checksum
   *   (optional) Specify FALSE to indicate that the checksum must be returned
   *   as a hex string, which is the default; otherwise, specify TRUE to
   *   indicate that the raw four-byte CRC must be returned.
   *
   * @return string|null
   *   The calculated Cyclic Redundancy Code (CRC) checksum as a string,
   *   or NULL if invalid arguments were provided.
   */
  protected static function calculateChecksum(
    $chunk_type, $chunk_data, $raw_checksum = FALSE) {

    $checksum = NULL;
    if (is_string($chunk_type) && (strlen($chunk_type) === 4) && (is_string($chunk_data) || is_null($chunk_data))) {
      if (is_null($chunk_data)) {
        $chunk = $chunk_type;
      }
      else {
        $chunk = ($chunk_type . $chunk_data);
      }

      $checksum = hash('crc32b', $chunk, $raw_checksum);
    }

    return $checksum;
  }
}
