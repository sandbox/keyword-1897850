<?php
/**
 * @file
 * Contains BadgeDepotJsonWebSignature.
 */

// Include the dependency file(s).
require_once 'globals.inc';

/**
 * Implements a helper class with utility methods for JSON Web Signature (JWS) functionality.
 */
abstract class BadgeDepotJsonWebSignature {

  /**
   * Encodes the specified data in Base-64 URL format as described in RFC 4648,
   * tiled "The Base16, Base32, and Base64 Data Encodings", section 5, under
   * the heading "Base 64 Encoding with URL and Filename Safe Alphabet"
   * (previously this was RFC 3548, section 4).  This is also the
   * encoding used for the three parts of a JSON Web Signature in
   * Compact Serialization, where it is referred to as "base64url encoding",
   * namely for the JWS Header, JWS Payload and JWS Signature values.
   *
   * @param $raw_data
   *   The data to be encoded, which must be specified as a string.
   *
   * @return
   *   An encoded string representation of the raw input data; otherwise NULL upon failure.
   */
  public static function encodeBase64Url($raw_data) {
    $encoded_data = NULL;
    if (is_string($raw_data)) {
      $encoded_data = rtrim(strtr(base64_encode($raw_data), '+/', '-_'), '=');
    }

    return $encoded_data;
  }

  /**
   * Decodes the specified data from Base-64 URL format into the original raw data.
   *
   * @param $encoded_data
   *   The data to be decoded, which must be a string encoded in Base-64 URL format.
   *
   * @return
   *   An encoded string representation of the raw input data; otherwise NULL upon failure.
   */
  public static function decodeBase64Url($encoded_data) {
    // Note: PHP's base64_decode function automatically appends padding as
    //       needed before decoding, so there is no need to do that manually
    return base64_decode(strtr($encoded_data, '-_', '+/'));
  }

  /**
   * Converts an array of bytes to a string representation.
   *
   * @param $bytes
   *   An array of bytes to be converted.  The individual byte elements of the
   *   array must be within the range from 0 to 255.
   *
   * @return
   *   A string value where each character represents a single byte of the array.
   */
  public static function convertByteArrayToString(array $bytes) {
    $string_value = NULL;
    if (is_array($bytes)) {
      $string_value = implode(array_map('chr', $bytes));
    }

    return $string_value;
  }

  /**
   * Converts the specified data into a JSON Text Object, i.e., a JSON string
   * representation of the data.
   *
   * @param $json_data
   *   The data to be converted into a JSON string, which would generally
   *   be specified as an associative array representing a JSON object.
   *
   * @return
   *   The JSON string representation of the data (i.e., a JSON Text Object).
   */
  public static function getJsonTextObject($json_data) {
    $json_text_object = NULL;
    if (isset($json_data)) {
      if (is_array($json_data) && empty($json_data)) {
        $json_text_object = '{}';
      }
      else {
        $json_options = 0;
        if (defined('JSON_UNESCAPED_SLASHES')) {
          // Available since PHP 5.4.0.
          $json_options |= JSON_UNESCAPED_SLASHES;
        }

        $json_text_object = _badge_depot_json_encode($json_data, $json_options);
      }
    }

    return $json_text_object;
  }

  /**
   * Generate the JSON Web Signature (JWS) for the specified payload data,
   * using the provided private-key for actual signing of the data.
   *
   * @param $payload
   *   The data for the JWS Payload, which should either be a string
   *   representing the bytes to be signed or other types of data that
   *   will be converted to a JSON string.
   * @param $private_key
   *   The private key to be used for signing, which can be specified
   *   as the PEM private-key string of the signee.
   * @param $header
   *   The optional JWS Header to be included in the signature.  If not
   *   specified, a default header will be included.
   * @param $signature
   *   The optional JWS Signature to be applied to the signature.  If not
   *   specified, the signature will be generated using the private key.
   *
   * @return
   *   The JSON string representation of the data (i.e., a JSON Text Object).
   */
  public static function getCompactSerialization($payload, $private_key, &$header = NULL, &$signature = NULL) {
    $jws = NULL;
    if (isset($payload) && (isset($private_key) || isset($signature))) {
      if (!isset($header)) {
        // Corresponds with array('alg' => 'RS256').
        $header = '{"alg":"RS256"}';
      }

      if (!is_string($header)) {
        $header = static::getJsonTextObject($header);
      }

      if (!is_string($payload)) {
        $payload = static::getJsonTextObject($payload);
      }

      if (is_string($header) && ($header !== '') && is_string($payload) && ($payload !== '')) {
        $header_encoded = static::encodeBase64Url($header);
        $payload_encoded = static::encodeBase64Url($payload);
        if (is_string($header_encoded) && ($header_encoded !== '') && is_string($payload_encoded) && ($payload_encoded !== '')) {
          $signing_input = ($header_encoded . '.' . $payload_encoded);

          // If not specified, generate the signature.
          if ((!isset($signature)) && BadgeDepotOpenSsl::isAvailable()) {
            // OBI release 1.0: "For compatibility purposes,
            // using an RSA-SHA256 is highly recommended".
            if (!openssl_sign($signing_input, $signature, $private_key, 'sha256')) {
              $signature = NULL;
            }
          }

          $signature_encoded = static::encodeBase64Url($signature);
          if (is_string($signature_encoded) && ($signature_encoded !== '')) {
            $jws = ($signing_input . '.' . $signature_encoded);
          }
        }
      }
    }

    return $jws;
  }
}
