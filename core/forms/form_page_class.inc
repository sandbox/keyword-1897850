<?php
/**
 * @file
 * Contains BadgeDepotFormPage.
 */

// Include the dependency files, for example to define BADGE_DEPOT_ROOT,
// and to ensure that the callback methods will be visible (for example
// _badge_depot_form_proxy, _badge_depot_form_proxy_validate and
// _badge_depot_form_proxy_submit).
require_once dirname(__DIR__) . '/globals.inc';
require_once 'page_callback.inc';

class BadgeDepotFormPage {

  // Array for any extra parameters passed to form functions (such as "nojs").
  public $extra_form_args;

  public function __construct() {
  }

  // Retrieves an existing page-instance for the current web-page request,
  // or creates a new instance if missing; this allows a single instance of
  // this class to be shared among the various functions that are called by
  // the Form API, such as for the form build, validate and submit phases.
  // This method can be called on a derived class to return an instance of
  // that specific class type, or when called on the base class this method
  // will return an instance of the page class that maps to the form ID.
  public static function getFormHandlerInstance(&$form, &$form_state, $form_id = NULL) {
    if ((!isset($form_id)) && isset($form_state['build_info']['form_id'])) {
      $form_id = $form_state['build_info']['form_id'];
    }

    // For exampe, the class may be 'BadgeDepotInspectorBackpackPage'.
    $most_derived_class_name = get_called_class();
    if ($most_derived_class_name === get_class()) {
      // When this method is called on the base class where this method
      // is defined (as opposed to calling it on a derived class),
      // try to find a class name that corresponds with the form ID.
      $form_page_class_name = static::getClassNameOfFormPage($form_id);
      if (!empty($form_page_class_name)) {
        $most_derived_class_name = $form_page_class_name;
      }
    }

    // Lookup a singleton instance of the current page-class.
    $page = &drupal_static($most_derived_class_name);
    if (!isset($page)) {
      // Create a new instance that is automatically stored
      // in the drupal_static cache for the current web-page
      // request, since it was retrieved by reference.
      $page = new $most_derived_class_name();
    }

    return $page;
  }

  // Maps a form ID to a corresponding name of a class that can process the
  // form, if the class exists; otherwise returns NULL; for example, converts
  // "_badge_depot_inspector_backpack_form" to "BadgeDepotInspectorBackpackPage".
  // Note: Valid form IDs start with the module name as prefix (i.e., either
  //       "badge_depot_" or "_badge_depot_") and end with the suffix "_form";
  //       alternatively, the form ID may be the class name of a derived page;
  //       or the form ID may be an entity name suffixed with "_form" for one
  //       of the entities defined by this module (e.g., badge_memo_form that
  //       corresponds with the administration GUI for badge_memo entities).
  public static function getClassNameOfFormPage($form_id) {
    if ((!empty($form_id)) && is_string($form_id)) {
      // Strip underscores to cater for internal/hidden forms.
      $form_id = trim($form_id, '_');
      if (!empty($form_id)) {
        if ((strlen($form_id) > 5) && DataUriCreatorString::endsWith($form_id, ('_form'))) {
          $class_name = NULL;
          if (DataUriCreatorString::startsWith($form_id, (BADGE_DEPOT_MODULE_NAME . '_'))) {
            // The form ID matches the standard pattern: badge_depot_*_form
            // (for example, _badge_depot_inspector_backpack_form).
            $class_name = $form_id;
          }
          else {
            // Determine whether the form ID corresponds with a data entity of
            // the module: e.g., badge_memo (i.e., without the "_form" suffix).
            $entity_type = substr($form_id, 0, -5);
            $entity_info = entity_get_info($entity_type);
            if (isset($entity_info) && ($entity_info['module'] === BADGE_DEPOT_MODULE_NAME)) {
              // The form ID is likely an administration page of one of our data
              // entities, e.g., badge_memo_form will map to BadgeMemoAdminPage
              $class_name = ($entity_type . '_admin_page');
            }
          }

          // Determine whether the candidate name does indeed map to a class
          if (!empty($class_name)) {
            $parts = explode('_', $class_name);

            // Replace the 'form' suffix with 'page'.
            $parts[count($parts) - 1] = 'page';

            $delegate = function(&$text, $key) {
              // Use upper-case first characters.
              $text = drupal_ucfirst($text);
            };

            if (array_walk($parts, $delegate)) {
              $class_name = implode($parts);
              if (class_exists($class_name)) {
                return $class_name;
              }
            }
          }
        }
        elseif (class_exists($form_id) && is_subclass_of($form_id, get_called_class())) {
          // The form ID corresponds directly with a page class
          // name, for example, BadgeDepotInspectorDefaultPage.
          return $form_id;
        }
      }
    }

    return NULL;
  }

  // Implements the Drupal Form API's builder function for the form
  // represented by this class.
  public function buildForm($form, &$form_state) {
    // Search for a specific form-builder method that
    // corresponds with the current step of the form.
    $method_name = $this->getFormHandlerMethod($form_state, 'build');
    if (isset($method_name)) {
      // Build the form for the current form-step.
      return $this->$method_name($form, $form_state);
    }

    // Execute the default build-handler.
    return $this->buildDefault($form, $form_state);
  }

  // Implements the default build-handler that executes when
  // no specific form-step/trigger handler is not found.
  public function buildDefault($form, &$form_state) {
    // Simply render the current form-data for this form request.
    return $form;
  }

  // Implements the Drupal Form API's validation function for the form
  // represented by this class.  For example, call form_set_error for
  // any $form_state['values'] item that contain an invalid value.
  public function validateForm($form, &$form_state) {
    // Search for a specific form-validator method that
    // corresponds with the current step of the form.
    $method_name = $this->getFormHandlerMethod($form_state, 'validate');
    if (isset($method_name)) {
      // Validate the form for the current form-step.
      $this->$method_name($form, $form_state);
    }
    else {
      // Execute the default validation-handler.
      $this->validateDefault($form, $form_state);
    }
  }

  // Implements the default validation-handler that executes
  // when no specific form-step/trigger handler is not found.
  public function validateDefault($form, &$form_state) {
  }

  // Implements the Drupal Form API's submit function for the form
  // represented by this class.
  public function submitForm($form, &$form_state) {
    // Search for a specific form-submitter method that
    // corresponds with the current step of the form.
    $method_name = $this->getFormHandlerMethod($form_state, 'submit');
    if (isset($method_name)) {
      // Submit the form for the current form-step.
      $this->$method_name($form, $form_state);
    }
    else {
      // Execute the default submit-handler.
      $this->submitDefault($form, $form_state);
    }
  }

  // Implements the default submit-handler that executes
  // when no specific form-step/trigger handler is not found.
  public function submitDefault($form, &$form_state) {
    // Display a generic form-submission message.
    drupal_set_message(t('The form has been submitted.'));
  }

  // Implements the Drupal Form API's AJAX callback for the form
  // represented by this class.  Returns a renderable array (most often a
  // form or form fragment), an HTML string, or an array of AJAX commands
  // (refer to Drupal's documentation on "#ajax['callback']" for details).
  public function ajaxForm($form, &$form_state) {
    // Search for a specific form-AJAX handler that corresponds
    // with the current step of the form and/or trigger.
    $method_name = $this->getFormHandlerMethod($form_state, 'ajax');
    if (isset($method_name)) {
      // Render the output for the AJAX handler of the current form-step.
      return $this->$method_name($form, $form_state);
    }

    // Execute the default AJAX-handler.
    return $this->ajaxDefault($form, $form_state);
  }

  // Implements the default AJAX-handler that executes when
  // no specific form-step/trigger handler is not found.
  public function ajaxDefault($form, &$form_state) {
    // Render no output for this AJAX request.
    return NULL;
  }

  // Obtains the name of the method that handles the current form-callback, if
  // it exists; otherwise NULL.  This method takes both the triggering element
  // and the form-step into account when searching for a callback-handler method.
  // Handler methods for a prefix "ajax", trigger name "Xyz" and step ID "1" are:
  //   $this->ajaxTriggerXyzStep1($form, &$form_state)
  //   $this->ajaxTriggerXyz($form, &$form_state)
  //   $this->ajaxStep1($form, &$form_state)
  protected function getFormHandlerMethod($form_state, $method_name_prefix) {
    $method_name = NULL;
    if (isset($method_name_prefix)) {
      if (is_array($method_name_prefix)) {
        foreach ($method_name_prefix as $prefix_item) {
          $method_name = $this->getFormHandlerMethod($form_state, $prefix_item);
          if (isset($method_name)) {
            break;
          }
        }
      }
      else {
        $trigger_prefix = ((!empty($form_state['triggering_element']['#name'])) ? ($method_name_prefix . 'Trigger' . ($form_state['triggering_element']['#name'])) : NULL);
        if (isset($trigger_prefix)) {
          $method_name = $this->getFormStepMethod($form_state, $trigger_prefix);
          if ((!isset($method_name)) && is_callable(array($this, $trigger_prefix))) {
            $method_name = $trigger_prefix;
          }
        }

        if (!isset($method_name)) {
          $method_name = $this->getFormStepMethod($form_state, $method_name_prefix);
        }
      }
    }

    return $method_name;
  }

  // Obtains the name of the current form-step method, if it exists; otherwise NULL.
  // For example, a valid form-step method for a prefix "ajax" and step ID "1" is:
  //   $this->ajaxStep1($form, &$form_state)
  protected function getFormStepMethod($form_state, $method_name_prefix) {
    $step_method = NULL;
    if (isset($method_name_prefix)) {
      $id = $this->getFormStepID($form_state);
      if (isset($id)) {
        if (is_array($method_name_prefix)) {
          foreach ($method_name_prefix as $prefix_item) {
            if (isset($prefix_item)) {
              $method_name = ($prefix_item . 'Step' . $id);
              if (is_callable(array($this, $method_name))) {
                $step_method = $method_name;
                break;
              }
            }
          }
        }
        else {
          $method_name = ($method_name_prefix . 'Step' . $id);
          if (is_callable(array($this, $method_name))) {
            $step_method = $method_name;
          }
        }
      }
    }

    return $step_method;
  }

  // Gets the name of the current step of the form, if previously defined; otherwise NULL.
  protected function &getFormStepID(&$form_state, $default = NULL) {
    $id = &$this->getStorageValue($form_state, 'step', $default);
    return $id;
  }

  // Sets the name of the current step of the form, if previously defined; specify no identifier to clear the current step.
  protected function &setFormStepID(&$form_state, $id = NULL) {
    if (isset($id) && (_badge_depot_string_equals('Form', $id, TRUE) || _badge_depot_string_equals('Step', $id, TRUE) || _badge_depot_string_equals('Trigger', $id, TRUE) || _badge_depot_string_equals('Default', $id, TRUE))) {
      trigger_error(filter_xss(format_string('The specified step-identifier (that is "@id") may cause unexpected behaviour and should not be used.', array('@id' => $id))), E_USER_WARNING);
    }

    $id = &$this->setStorageValue($form_state, 'step', $id);
    return $id;
  }

  // Retrieves the value of a specified variable from persistent form-storage.
  protected function &getStorageValue(&$form_state, $key = NULL, $default = NULL) {
    if (!isset($key)) {
      if (isset($default)) {
        trigger_error('A default value should not be specified when a storage key is not provided, since the entire storage object is then returned.', E_USER_WARNING);
      }

      return $form_state['storage'];
    }

    if (isset($default) && (!isset($form_state['storage'][$key]))) {
      $form_state['storage'][$key] = $default;
    }

    return $form_state['storage'][$key];
  }

  // Stores the value of a specified variable in persistent form-storage.
  protected function &setStorageValue(&$form_state, $key, $value) {
    if (isset($key)) {
      $form_state['storage'][$key] = $value;
      return $form_state['storage'][$key];
    }

    trigger_error('The storage key must be specified for a persistent value!', E_USER_ERROR);
  }

  protected function getAjaxWrapperMarkupPrefix($html_element_id) {
    return '<div id="' . $html_element_id . '">';
  }

  protected function getAjaxWrapperMarkupSuffix() {
    return '</div>';
  }
}
