<?php
/**
 * @file
 * Contains generic Form API functions to create, validate and submit OOP forms.
 */

/**
 * Implements a generic Form API form constructor for this module.
 *
 * This Form API form builder forwards the call to the appropriate page-class.
 *
 * @param array $form
 *   The form's current render-array.
 * @param array $form_state
 *   Reference to the current form state.
 *
 * @return array
 *   An array representing the form definition.
 *
 * @ingroup forms
 * @see _badge_depot_form_proxy_validate()
 * @see _badge_depot_form_proxy_submit()
 * @see _badge_depot_form_proxy_ajax()
 */
function _badge_depot_form_proxy($form, &$form_state) {
  $page = BadgeDepotFormPage::getFormHandlerInstance($form, $form_state);
  if (is_callable(array($page, 'buildForm'))) {
    $page->extra_form_args = ((func_num_args() > 2) ? array_slice(func_get_args(), 2) : array());
    $result = $page->buildForm($form, $form_state);
    $page->extra_form_args = NULL;
    return $result;
  }

  return $form;
}

/**
 * Implements a generic form validation handler for _badge_depot_form_proxy().
 *
 * This Form API form validator forwards the call to the appropriate page-class.
 * It executes whenever an associated form request is submitted.
 *
 * @param array $form
 *   The form's current render-array.
 * @param array $form_state
 *   Reference to the current form state.
 *
 * @see _badge_depot_form_proxy()
 * @see _badge_depot_form_proxy_submit()
 * @see _badge_depot_form_proxy_ajax()
 */
function _badge_depot_form_proxy_validate($form, &$form_state) {
  $page = BadgeDepotFormPage::getFormHandlerInstance($form, $form_state);
  if (is_callable(array($page, 'validateForm'))) {
    $page->extra_form_args = ((func_num_args() > 2) ? array_slice(func_get_args(), 2) : array());
    $page->validateForm($form, $form_state);
    $page->extra_form_args = NULL;
  }
}

/**
 * Implements a generic form submission handler for _badge_depot_form_proxy().
 *
 * This Form API form submitter forwards the call to the appropriate page-class.
 * It gets called after the form request has been validated completely and
 * only if all the data that was submitted to the form was valid.
 *
 * @param array $form
 *   The form's current render-array.
 * @param array $form_state
 *   Reference to the current form state.
 *
 * @see _badge_depot_form_proxy()
 * @see _badge_depot_form_proxy_validate()
 * @see _badge_depot_form_proxy_ajax()
 */
function _badge_depot_form_proxy_submit($form, &$form_state) {
  $page = BadgeDepotFormPage::getFormHandlerInstance($form, $form_state);
  if (is_callable(array($page, 'submitForm'))) {
    $page->extra_form_args = ((func_num_args() > 2) ? array_slice(func_get_args(), 2) : array());
    $page->submitForm($form, $form_state);
    $page->extra_form_args = NULL;
  }
}
