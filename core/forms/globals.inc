<?php
/**
 * @file
 * Provides global functions and other definitions for the core Forms API
 * features of this module.
 */

/**
 * Implements a generic AJAX-callback handler for _badge_depot_form_proxy().
 *
 * This Form API callback handler forwards AJAX requests to the appropriate
 * page-class. The function executes for every AJAX web-request of an associated
 * form, irrespective of success; however, this function is not called if the
 * web request was made without JavaScript support (in which case the full form
 * from a rebuild step using _badge_depot_form_proxy should likely be returned).
 *
 * @param array $form
 *   The form's current render-array.
 * @param array $form_state
 *   Reference to the current form state.
 *
 * @return array
 *   An array representing the AJAX response, either a render-array fragment
 *   or an array of AJAX commands.
 *
 * @see _badge_depot_form_proxy()
 * @see _badge_depot_form_proxy_validate()
 * @see _badge_depot_form_proxy_submit()
 */
function _badge_depot_form_proxy_ajax($form, &$form_state) {
  $page = BadgeDepotFormPage::getFormHandlerInstance($form, $form_state);
  if (is_callable(array($page, 'ajaxForm'))) {
    $page->extra_form_args = ((func_num_args() > 2) ? array_slice(func_get_args(), 2) : array());
    $result = $page->ajaxForm($form, $form_state);
    return $result;
  }

  return NULL;
}
