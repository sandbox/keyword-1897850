<?php
/**
 * @file
 * Contains BadgeDepotEntityAdminPage.
 */

/**
 * Implements a base class for an entity's administrative GUI.
 *
 * Refer to see hook_form for general information about the context.
 */
class BadgeDepotEntityAdminPage extends BadgeDepotFormPage {

  public function __construct() {
    parent::__construct();
  }

  // Obtains the extra arguments that are passed to the buildForm method of an entity's administrative user-interface.
  protected function getBuildFormExtraArgs(&$entity, &$op) {
    $extra_args_count = count($this->extra_form_args);
    $entity = (($extra_args_count > 0) ? $this->extra_form_args[0] : NULL);
    $op = (($extra_args_count > 1) ? $this->extra_form_args[1] : 'edit');
  }

  // Overrides the base buildForm implementation to add extra logic.
  public function buildForm($form, &$form_state) {
    //$this->getBuildFormExtraArgs($entity, $op);
    $this->fixEntityUiFormDefaults($form, $form_state);
    return parent::buildForm($form, $form_state);
  }

  // Obtains a render array for a button that launch a browser window with the URL pointed to by another HTML element.
  public function buildLaunchButton($url_element_name, $button_text = NULL, $launch_action = NULL, $failure_message = NULL) {
    if (empty($button_text)) {
      $button_text = t('Launch Page');
    }

    return array(
      '#type' => 'button',
      '#value' => $button_text,
      '#attributes' => array('onclick' => $this->buildLaunchJavaScript($url_element_name, $launch_action, $failure_message)),
    );
  }

  // Generate the JavaScript for a button that launch a browser window with the URL pointed to by some HTML element(s).
  protected function buildLaunchJavaScript($url_element_name, $launch_action = NULL, $failure_message = NULL) {
    if (!isset($launch_action)) {
      $launch_action = 'window.open(url, "_blank");';
    }

    if (empty($failure_message)) {
      $failure_message = t('The URL to launch is not currently specified.');
    }

    if (!is_array($url_element_name)) {
      $url_element_name = array($url_element_name);
    }

    $url_query = array();
    foreach ($url_element_name as $selector_item) {
      if (DataUriCreatorString::startsWith($selector_item, '#')) {
        // An element ID is specified instead of a name
        $selector = $selector_item;
      }
      else {
        $selector = ('[name=' . $selector_item . ']');
      }

      $url_query[] = ('jQuery("' . $selector . '").val()');
    }

    // JavaScript to override client-side action and disable form submission.
    $url_query = implode(' || ', $url_query);
    $script =
      'var launchAction = function(url) { ' . $launch_action . ' }; ' .
      'var launchURL = (' . $url_query . '); ' .
      'if (launchURL) { launchAction(launchURL); } ' .
      'else { alert("' . htmlspecialchars($failure_message) . '"); }; ' .
      'return false;';
    return $script;
  }

  // Process data submitted through the administrative user-interface form of the entity.
  public function submitDefault($form, &$form_state) {
    //debug($form_state, 'state: submitDefault');
    $entity = entity_ui_form_submit_build_entity($form, $form_state);

    // Save the entity data.
    $entity->save();

    // Locate the admin page to navigate towards.
    $entity_info = $entity->entityInfo();

    // For exampe: BADGE_DEPOT_ADMIN_MEMO_PATH.
    $path = (isset($entity_info['admin ui']['path']) ? $entity_info['admin ui']['path'] : NULL);

    if (!empty($path)) {
      // Navigate to the next page.
      $form_state['redirect'] = $path;
    }
    else {
      parent::submitDefault($form, $form_state);
    }
  }

  // Helper function to add field info to a form for a given property of an entity.
  protected function buildEntityFieldAdmin(&$form, $property_name, &$properties, $entity, $custom_values = NULL) {
    if (!isset($properties)) {
      $property_info = entity_get_property_info($entity->entityType());
      $properties = $property_info['properties'];
    }

    $description = $properties[$property_name]['description'];
    $default_value = $entity->$property_name;
    switch ($properties[$property_name]['type']) {
      case 'boolean':
        $type = 'checkbox';
        break;
      default:
        if ((strlen($default_value) <= 255) && (empty($default_value) || (strpos($default_value, "\n") === FALSE))) {
          $type = 'textfield';
        }
        else {
          $type = 'textarea';
        }

        break;
    }

    $field_info = array(
      '#title' => $properties[$property_name]['label'],
      '#type' => $type,
      '#default_value' => $default_value,
      '#description' => $description,
    );

    if (isset($custom_values)) {
      $field_info = array_merge($field_info, $custom_values);
    }

    $form[$property_name] = $field_info;
    return $default_value;
  }

  // Workaround to undo the settings for admin pages as setup by
  // entity_ui_form_defaults through entity_ui_main_form_defaults, i.e.,
  // remove the $form['#validate'] and $form['#submit'] values because
  // entity_ui_controller_form_validate and entity_ui_controller_form_submit
  // assume that form IDs will be in the format "entity_ui_*_form", which is
  // not the case when using this admin-page class.
  protected function fixEntityUiFormDefaults(&$form, &$form_state) {
    if (($form_state['wrapper_callback'] === 'entity_ui_main_form_defaults') || ($form_state['wrapper_callback'] === 'entity_ui_form_defaults')) {
      if (isset($form['#validate']) && (($key = array_search('entity_ui_controller_form_validate', $form['#validate'])) !== FALSE)) {
        if (count($form['#validate']) === 1) {
          unset($form['#validate']);
        }
        else {
          unset($form['#validate'][$key]);
        }
      }

      if (isset($form['#submit']) && (($key = array_search('entity_ui_controller_form_submit', $form['#submit'])) !== FALSE)) {
        if (count($form['#submit']) === 1) {
          unset($form['#submit']);
        }
        else {
          unset($form['#submit'][$key]);
        }
      }
    }
  }
}
