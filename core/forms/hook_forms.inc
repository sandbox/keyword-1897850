<?php
/**
 * @file
 * Contains additional hook_forms() information.
 */

/**
 * Adds the hook_forms data for the core Forms API features of this module.
 * This method will be called whenever Drupal cannot find a specific form ID.
 * Recognized generic forms can be handled dynamically with a shared handler.
 */
function _badge_depot_append_form_proxy_forms(&$forms, $form_id, $args) {
  // For form IDs that can be mapped to page classes, use the
  // generic form-callback handler that will forward the standard
  // form callback functions (such as for the build, validate and
  // submit hooks) to methods that are defined on that specific
  // class (for example, to buildForm, validateForm and submitForm).
  try {
    $form_page_class_name = BadgeDepotFormPage::getClassNameOfFormPage($form_id);
  }
  catch (Exception $e) {
    // Ignore some cases where Drupal cannot find the BadgeDepotFormPage
    // class for some reason, for example initially after a database restore.
    $form_page_class_name = NULL;
  }

  if (!empty($form_page_class_name)) {
    // Ensure that the callback method will always be visible for
    // form IDs that are dynamically discovered by this function.
    require_once __DIR__ . '/page_callback.inc';

    // Specify the callback method to be used for this form.
    $forms[$form_id] = array(
      'callback' => '_badge_depot_form_proxy',
    );
  }
}
