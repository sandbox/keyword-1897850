<?php
/**
 * @file
 * Contains additional hook_help() information.
 */

/**
 * Provides online help for the module by extending Drupal's hook_help function.
 */
function _badge_depot_core_help($path, $arg) {
  if ($path === ('admin/help#' . BADGE_DEPOT_MODULE_NAME)) {
    $url_options = array('attributes' => array('target' => '_blank'));
    $args = array(
      '!obi_url' => l(t('Mozilla Open Badges Initiative (OBI)'), 'http://www.openbadges.org/', $url_options),
      '!lrmi_url' => l(t('Learning Resource Metadata Initiative (LRMI) markup'), 'http://www.lrmi.net/', $url_options),
      '!ccss_url' => l(t('Common Core State Standards (CCSS)'), 'http://www.corestandards.org/', $url_options),
      '!assertion_url' => l(t('Award the badge to users'), (BADGE_DEPOT_ADMIN_ASSERTION_PATH . '/add'), $url_options),
      '!issuer_url' => l(t('Create an issuer organization'), (BADGE_DEPOT_ADMIN_ISSUER_PATH . '/add'), $url_options),
      '!manifest_url' => l(t('Define a badge to be issued'), (BADGE_DEPOT_ADMIN_MANIFEST_PATH . '/add'), $url_options),
      '!criteria_url' => l(t('Create an advanced badge-criteria page'), 'node/add/badge-criteria-page', $url_options),
    );
    return t(
      'The Badge Depot module allows Drupal sites to award and inspect '
      . 'digital badges that are compatible with the !obi_url. It also '
      . 'supports advanced features such as the definition of badge '
      . 'metadata using !lrmi_url and references to alignments with '
      . '!ccss_url for educational purposes.'
      . '<br />&nbsp;<br />'
      . 'Once the module is installed and enabled, you can setup and issue OBI badges as follows:'
      . '<ol>'
      . '<li>!issuer_url,</li>'
      . '<li>!manifest_url (that is create a badge manifest), and</li>'
      . '<li>!assertion_url (that is create badge assertions).</li>'
      . '<li>Repeat the steps as needed. For example, define and award as many badges as you want.</li>'
      . '</ol>'
      . 'Optional: !criteria_url, and refer to it from the badge manifest.'
      . '<br />&nbsp;<br />',
      $args
    );
  }
}
