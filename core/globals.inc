<?php
/**
 * @file
 * Provides global definitions for the badge-depot module.
 */

// Specify the base directory of this module.
define('BADGE_DEPOT_ROOT', dirname(__DIR__));

// Specify the name of this module.
define('BADGE_DEPOT_MODULE_NAME', 'badge_depot');

// Specify the base URLs for this module.
define('BADGE_DEPOT_HOME_PATH_PREFIX', 'badge-depot/');
define('BADGE_DEPOT_ADMIN_PATH_PREFIX', 'admin/structure/badge-depot/');
define('BADGE_DEPOT_CONFIG_MAIN_PATH', 'admin/config/badges');
define('BADGE_DEPOT_CONFIG_PATH_PREFIX', (BADGE_DEPOT_CONFIG_MAIN_PATH . '/'));
define('BADGE_DEPOT_INSPECTOR_DEFAULT_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'assertions/inspector'));
define('BADGE_DEPOT_INSPECTOR_DIRECT_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'assertions/inspector-direct'));
define('BADGE_DEPOT_INSPECTOR_BACKPACK_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'assertions/inspector-backpack'));
define('BADGE_DEPOT_INSPECTOR_CHECK_RECIPIENT_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'assertions/check_recipient'));
define('BADGE_DEPOT_PROXY_ASSERTIONS_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'assertions/proxy'));
define('BADGE_DEPOT_PROXY_BADGE_CLASSES_PATH', (BADGE_DEPOT_PROXY_ASSERTIONS_PATH . '/badge'));
define('BADGE_DEPOT_PROXY_ISSUERS_PATH', (BADGE_DEPOT_PROXY_ASSERTIONS_PATH . '/issuer'));
define('BADGE_DEPOT_PROXY_MICRODATA_PATH', (BADGE_DEPOT_HOME_PATH_PREFIX . 'microdata/proxy'));

// Retrieves the full URL for the base path of this module.
function _badge_depot_get_module_url() {
  global $base_url;
  return ($base_url . '/' . _badge_depot_get_module_path());
}

// Retrieves the base path of this module relative to base_path().
function _badge_depot_get_module_path() {
  return drupal_get_path('module', BADGE_DEPOT_MODULE_NAME);
}

// Determines whether the two strings are equal.
function _badge_depot_string_equals($string1, $string2, $ignore_case = FALSE) {
  if ($ignore_case && is_string($string1) && is_string($string2)) {
    return (strcasecmp($string1, $string2) === 0);
  }

  return ($string1 === $string2);
}

// Returns the JSON representation of a value, while attempting to incorporate
// the JSON-formatting options.
function _badge_depot_json_encode($value, $json_options = 0) {
  if (($json_options !== 0) && version_compare(PHP_VERSION, '5.3.0', '>=')) {
    return json_encode($value, $json_options);
  }

  return drupal_json_encode($value);
}

// Returns the JSON representation of a value, while attempting to incorporate
// pretty-printing, if requested.
function _badge_depot_json_encode_pretty($value, $attempt_pretty_print = TRUE) {
  $json_options = 0;
  if ($attempt_pretty_print) {
    if (defined('JSON_PRETTY_PRINT')) {
      // Available since PHP 5.4.0.
      $json_options |= JSON_PRETTY_PRINT;
    }

    if (defined('JSON_UNESCAPED_SLASHES')) {
      // Available since PHP 5.4.0.
      $json_options |= JSON_UNESCAPED_SLASHES;
    }
  }

  return _badge_depot_json_encode($value, $json_options);
}
