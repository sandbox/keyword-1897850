<?php
/**
 * @file
 * Contains the delivery callback function for Data URI content.
 */

/**
 * This is a delivery callback function, similar to drupal_deliver_html_page(),
 * which serves Data URI content from a Drupal page; it is used by web-service
 * endpoints such as to serve images of badges or issuer-organizations.
 * The input URL can be either a data URI that will be served as content or
 * it can be a regular URL that will be redirected to.
 */
function badge_depot_deliver_data_uri($page_callback_result) {
  if (is_null($page_callback_result) || ($page_callback_result === '')) {
    $page_callback_result = MENU_NOT_FOUND;
  }

  if (is_int($page_callback_result)) {
    switch ($page_callback_result) {
      case MENU_NOT_FOUND:
        drupal_add_http_header('Status', '404 Not Found');
        watchdog('page not found', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
      case MENU_ACCESS_DENIED:
        drupal_add_http_header('Status', '403 Forbidden');
        watchdog('access denied', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
      case MENU_SITE_OFFLINE:
        drupal_add_http_header('Status', '503 Service unavailable');
        break;
      default:
        drupal_add_http_header('Status', '501 Not Implemented');
        watchdog('not implemented', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
    }
  }
  elseif (is_string($page_callback_result)) {
    if (DataUriCreator::isDataUri($page_callback_result)) {
      // The result appears to be a Data URI.
      $data_content = DataUriCreator::decode($page_callback_result, $content_type);
      if (!isset($data_content)) {
        // The specified Data URI is not formatted correctly!
        drupal_add_http_header('Status', '400 Bad Request');
        watchdog(BADGE_DEPOT_MODULE_NAME, 'Invalid Data URI detected (%pageUrl => @dataUri).', array('%pageUrl' => $_GET['q'], '@dataUri' => $page_callback_result), WATCHDOG_ERROR);
      }
      else {
        // Serve the content of the Data URI...
        if (is_null(drupal_get_http_header('Content-Type')) && (!empty($content_type))) {
          drupal_add_http_header('Content-Type', $content_type);
        }

        print $data_content;
      }
    }
    else {
      // The result appears to be a regular URL.
      $http_response_code = 302;
      if (url_is_external($page_callback_result)) {
        // Perform a page redirection to an external resource.
        header(('Location: ' . $page_callback_result), TRUE, $http_response_code);
        drupal_exit($page_callback_result);
      }
      else {
        // Perform a page redirection to an internal resource.
        drupal_goto($page_callback_result, array(), $http_response_code);
      }
    }
  }
}
