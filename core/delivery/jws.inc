<?php
/**
 * @file
 * Contains the delivery callback function for JWS content.
 */

/**
 * This is a delivery callback function, similar to drupal_deliver_html_page(),
 * which serves JSON Web Signature (JWS) content from a Drupal page; it is used
 * by web-service endpoints such as to serve signed badges.
 */
function badge_depot_deliver_jws($page_callback_result) {
  if (is_null(drupal_get_http_header('Content-Type'))) {
    // Note: "application/jws" is used for JWS Compact Serialization, while
    //       "application/jws+json" is used for JWS JSON Serialization.
    $mime_type = (is_array($page_callback_result) ? 'application/jws+json' : 'application/jws');
    drupal_add_http_header('Content-Type', ($mime_type . '; charset=utf-8'));
  }

  if (is_null($page_callback_result) || ($page_callback_result === '') || (is_array($page_callback_result) && empty($page_callback_result))) {
    $page_callback_result = MENU_NOT_FOUND;
  }

  if (is_int($page_callback_result)) {
    switch ($page_callback_result) {
      case MENU_NOT_FOUND:
        drupal_add_http_header('Status', '404 Not Found');
        watchdog('page not found', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
      case MENU_ACCESS_DENIED:
        drupal_add_http_header('Status', '403 Forbidden');
        watchdog('access denied', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
      case MENU_SITE_OFFLINE:
        drupal_add_http_header('Status', '503 Service unavailable');
        break;
      default:
        drupal_add_http_header('Status', '501 Not Implemented');
        watchdog('not implemented', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
    }
  }
  elseif (is_string($page_callback_result)) {
    // Handles content for JWS Compact Serialization.
    print $page_callback_result;
  }
  elseif (is_array($page_callback_result)) {
    // Handles content for JWS JSON Serialization.
    print BadgeDepotJsonWebSignature::getJsonTextObject($page_callback_result);
  }
}
