<?php
/**
 * @file
 * Contains the delivery callback function for PEM content.
 */

/**
 * This is a delivery callback function, similar to drupal_deliver_html_page(),
 * which serves PEM content from a Drupal page; it is used by web-service
 * endpoints such as to serve public-keys of badge issuers (see RFC 1421
 * through RFC 1424 on Privacy Enhancement for Internet Electronic Mail).
 */
function badge_depot_deliver_pem($page_callback_result) {
  if (is_null(drupal_get_http_header('Content-Type'))) {
    drupal_add_http_header('Content-Type', 'application/x-pem-file; charset=utf-8');
  }

  if (is_null($page_callback_result) || ($page_callback_result === '')) {
    $page_callback_result = MENU_NOT_FOUND;
  }

  if (is_int($page_callback_result)) {
    switch ($page_callback_result) {
      case MENU_NOT_FOUND:
        drupal_add_http_header('Status', '404 Not Found');
        watchdog('page not found', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
      case MENU_ACCESS_DENIED:
        drupal_add_http_header('Status', '403 Forbidden');
        watchdog('access denied', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
      case MENU_SITE_OFFLINE:
        drupal_add_http_header('Status', '503 Service unavailable');
        break;
      default:
        drupal_add_http_header('Status', '501 Not Implemented');
        watchdog('not implemented', check_plain($_GET['q']), NULL, WATCHDOG_WARNING);
        break;
    }
  }
  elseif (is_string($page_callback_result)) {
    print $page_callback_result;
  }
}
