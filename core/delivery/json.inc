<?php
/**
 * @file
 * Contains the delivery callback function for JSON content.
 */

/**
 * This is a delivery callback function, similar to drupal_deliver_html_page(),
 * which serves JSON content from a Drupal page; it is used by web-services
 * such as for the badge assertions proxy and the recipient email validation.
 */
function badge_depot_deliver_json($page_callback_result) {
  if (is_null(drupal_get_http_header('Content-Type'))) {
    drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
  }

  if (is_int($page_callback_result)) {
    $page_callback_result = array('error' => $page_callback_result);
  }

  if (is_string($page_callback_result) && ($page_callback_result !== '')) {
    print $page_callback_result;
  }
  elseif (!empty($page_callback_result)) {
    $json_options = 0;
    if (defined('JSON_UNESCAPED_SLASHES')) {
      // Available since PHP 5.4.0.
      $json_options |= JSON_UNESCAPED_SLASHES;
    }

    print _badge_depot_json_encode($page_callback_result, $json_options);
  }
  else {
    print '{}';
  }
}
