<?php
/**
 * @file
 * Example theme's implementation (based on the Zen theme) to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   - view-mode-[mode]: The view mode, e.g. 'full', 'teaser'...
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $pubdate: Formatted date and time for when the node was published wrapped
 *   in a HTML5 time element.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content. Currently broken; see http://drupal.org/node/823380
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */

function get_node_value(&$node, $field_name, $value_name = 'value', $index = 0, $language = LANGUAGE_NONE, $default_value = NULL) {
  $value = $default_value;
  if (isset($node->$field_name)) {
    $field = $node->$field_name;
    if (isset($field[$language][$index])) {
      $field_item = $field[$language][$index];
      if (!isset($value_name)) {
        $value = $field_item;
      }
      elseif (isset($field_item[$value_name])) {
        $value = $field_item[$value_name];
      }
    }
  }

  if ($value && ($value == '<none>')) {
    $value = $default_value;
  }

  return $value;
}

function get_node_value_count(&$node, $field_name, $language = LANGUAGE_NONE) {
  if (isset($node->$field_name)) {
    $field = $node->$field_name;
    if (isset($field[$language])) {
      $values = $field[$language];
      return count($values);
    }
  }

  return 0;
}

?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title): ?>
    <header>
      <?php print render($title_prefix); ?>
      <?php if (!$page && $title): ?>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($display_submitted): ?>
        <p class="submitted">
          <?php print $user_picture; ?>
          <?php print $submitted; ?>
        </p>
      <?php endif; ?>

      <?php if ($unpublished): ?>
        <p class="unpublished"><?php print t('Unpublished'); ?></p>
      <?php endif; ?>
    </header>
  <?php endif; ?>

<?php
  // We hide the comments and links now so that we can render them later.
  hide($content['comments']);
  hide($content['links']);
  //print render($content);
  if (get_node_value($node, 'field_badge_use_custom_body')) {
    print render($content['body']);
  }
  else {
    hide($content['body']);

    $newline = "\n";
    $lrmi_image_url = get_node_value($node, 'field_badge_lrmi_image_url');
    $lrmi_in_language = get_node_value($node, 'field_badge_lrmi_language');
    $lrmi_keywords = get_node_value($node, 'field_badge_lrmi_keywords');
    $lrmi_intended_end_user_role = get_node_value($node, 'field_badge_lrmi_intended_enduse');
    $lrmi_educational_use = get_node_value($node, 'field_badge_lrmi_educational_use');
    $lrmi_time_required = get_node_value($node, 'field_badge_lrmi_time_required');
    $lrmi_typical_age_range = get_node_value($node, 'field_badge_lrmi_typical_ages');
    $lrmi_interactivity_type = get_node_value($node, 'field_badge_lrmi_interact_type');
    $lrmi_about = get_node_value($node, 'field_badge_lrmi_about');
    $lrmi_badge_name = get_node_value($node, 'field_badge_lrmi_name', 'value', 0, LANGUAGE_NONE, $title);
    $lrmi_badge_description = get_node_value($node, 'field_badge_lrmi_description', 'value');

    $lrmi_publisher_url = get_node_value($node, 'field_badge_lrmi_publisher_url');
    $lrmi_publisher_name = get_node_value($node, 'field_badge_lrmi_publisher_name', 'value', 0, LANGUAGE_NONE, $lrmi_publisher_url);

    $activity_url = get_node_value($node, 'field_badge_activity_url');
    $activity_title = get_node_value($node, 'field_badge_activity_title', 'value', 0, LANGUAGE_NONE, $activity_url);

    $header_title = get_node_value($node, 'field_badge_header_title', 'value', 0, LANGUAGE_NONE, $lrmi_badge_name);
    $header_url = get_node_value($node, 'field_badge_header_url', 'value', 0, LANGUAGE_NONE, $activity_url);

    $badge_copyright = get_node_value($node, 'field_badge_copyright');

    if ($lrmi_image_url) {
      print $newline . '<meta itemprop="image" content="' . url($lrmi_image_url) . '" />';
    }

    if ($lrmi_in_language) {
      print $newline . '<meta itemprop="inLanguage" content="' . htmlspecialchars($lrmi_in_language) . '" />';
    }

    if ($lrmi_keywords) {
      print $newline . '<meta itemprop="keywords" content="' . htmlspecialchars($lrmi_keywords) . '" />';
    }

    if ($lrmi_intended_end_user_role) {
      print $newline . '<meta itemprop="intendedEndUserRole" content="' . htmlspecialchars($lrmi_intended_end_user_role) . '" />';
    }

    if ($lrmi_educational_use) {
      print $newline . '<meta itemprop="educationalUse" content="' . htmlspecialchars($lrmi_educational_use) . '" />';
    }

    if ($lrmi_time_required) {
      print $newline . '<meta itemprop="timeRequired" content="' . htmlspecialchars($lrmi_time_required) . '" />';
    }

    if ($lrmi_typical_age_range) {
      print $newline . '<meta itemprop="typicalAgeRange" content="' . htmlspecialchars($lrmi_typical_age_range) . '" />';
    }

    if ($lrmi_interactivity_type) {
      print $newline . '<meta itemprop="interactivityType" content="' . htmlspecialchars($lrmi_interactivity_type) . '" />';
    }

    if ($lrmi_about) {
      print $newline . '<meta itemprop="About" content="' . htmlspecialchars($lrmi_about) . '" />';
    }

    print $newline . '<div id="badge-notice">';
    if ($header_url || $header_title) {
      print $newline;
      if ($header_url) {
        print '<a href="' . url($header_url) . '">';
      }

      if ($header_title) {
        print '<div id="badge-header" title="' . htmlspecialchars($header_title, ENT_QUOTES, 'UTF-8', FALSE) . '"></div>';
      }

      if ($header_url) {
        print '</a>';
      }
    }
?>
<div id="badge-headering" class="info">
<h2 itemprop="name"><?php print htmlspecialchars($lrmi_badge_name, ENT_QUOTES, 'UTF-8', FALSE); ?></h2>
</div>
<div id="badge-detail" class="showme">
<ul>
<?php
    $ignore_integer_value = -1;
    if ($lrmi_badge_description) {
      print $newline . '<li itemprop="description">' . $lrmi_badge_description . '</li>';
    }

    $schema_attributes = ' itemprop="educationalAlignment" itemscope="itemscope" itemtype="http://www.lrmi.net/the-specification/alignment-object"';
    $alignment_count = max(
      get_node_value_count($node, 'field_badge_lrmi_align_titles'),
      get_node_value_count($node, 'field_badge_lrmi_align_types'),
      get_node_value_count($node, 'field_badge_lrmi_align_tar_descs'),
      get_node_value_count($node, 'field_badge_lrmi_align_tar_names'),
      get_node_value_count($node, 'field_badge_lrmi_align_tar_urls'),
      get_node_value_count($node, 'field_badge_lrmi_align_tar_ccsss'),
      get_node_value_count($node, 'field_badge_lrmi_align_tar_tries'));
    for ($index = 0; $index < $alignment_count; $index++) {
      $lrmi_alignmentName = get_node_value($node, 'field_badge_lrmi_align_titles', 'value', $index);
      $lrmi_alignmentType = get_node_value($node, 'field_badge_lrmi_align_types', 'value', $index);
      $lrmi_targetDescription = get_node_value($node, 'field_badge_lrmi_align_tar_descs', 'value', $index);
      $lrmi_targetName = get_node_value($node, 'field_badge_lrmi_align_tar_names', 'value', $index);
      $lrmi_targetUrl = get_node_value($node, 'field_badge_lrmi_align_tar_urls', 'value', $index);
      $lrmi_suggestedCCSS = get_node_value($node, 'field_badge_lrmi_align_tar_ccsss', 'value', $index);
      $lrmi_successfulSequentialAttempts = get_node_value($node, 'field_badge_lrmi_align_tar_tries', 'value', $index);

      if ($lrmi_alignmentName) {
        $item_element = 'li';
      }
      else {
        $item_element = 'div';
      }

      print $newline . "<{$item_element}{$schema_attributes}>";
      if ($lrmi_alignmentName) {
        print $newline . htmlspecialchars($lrmi_alignmentName, ENT_QUOTES, 'UTF-8', FALSE);
      }

      if ($lrmi_alignmentType) {
        print $newline . '<meta itemprop="alignmentType" content="' . htmlspecialchars($lrmi_alignmentType) . '" />';
      }

      if ($lrmi_targetDescription) {
        print $newline . '<meta itemprop="targetDescription" content="' . htmlspecialchars($lrmi_targetDescription) . '" />';
      }

      if ($lrmi_targetName) {
        print $newline . '<meta itemprop="targetName" content="' . htmlspecialchars($lrmi_targetName) . '" />';
      }

      if ($lrmi_targetUrl) {
        print $newline . '<meta itemprop="targetUrl" content="' . htmlspecialchars($lrmi_targetUrl) . '" />';
      }

      if ($lrmi_suggestedCCSS) {
        print $newline . '<meta itemprop="suggestedCCSS" content="' . htmlspecialchars($lrmi_suggestedCCSS) . '" />';
      }

      if (($lrmi_successfulSequentialAttempts || ($lrmi_successfulSequentialAttempts === 0)) && ($lrmi_successfulSequentialAttempts != $ignore_integer_value)) {
        print $newline . '<meta itemprop="successfulSequentialAttempts" content="' . htmlspecialchars($lrmi_successfulSequentialAttempts) . '" />';
      }

      print "</{$item_element}>";
    }

    if ($activity_url) {
      print $newline . '<li>Badge Content: <a itemprop="url" href="' . url($activity_url) . '">' . htmlspecialchars($activity_title, ENT_QUOTES, 'UTF-8', FALSE) . '</a></li>';
    }

    if ($lrmi_publisher_name || $lrmi_publisher_url) {
      print $newline . '<li itemprop="publisher" itemscope="itemscope" itemtype="http://schema.org/Organization">Publisher: ';
      if ($lrmi_publisher_name) {
        print $newline . '<meta itemprop="name" content="' . htmlspecialchars($lrmi_publisher_name) . '" />';
      }

      if ($lrmi_publisher_url) {
        print $newline . '<a rel="publisher" itemprop="url" href="' . url($lrmi_publisher_url) . '">' . htmlspecialchars($lrmi_publisher_name, ENT_QUOTES, 'UTF-8', FALSE) . '</a>';
      }
      else {
        print $newline . htmlspecialchars($lrmi_publisher_name, ENT_QUOTES, 'UTF-8', FALSE);
      }

      print $newline . '</li>';
    }

    print $newline . '</ul>';
    if ($badge_copyright) {
        print $newline . '<div id="badge-copyright">' . htmlspecialchars($badge_copyright, ENT_QUOTES, 'UTF-8', FALSE) . '</div>';
    }

    print $newline . '</div></div>';
  }
?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</article><!-- /.node -->
