<?php
/**
 * @file
 * badge_depot.features.inc
 */

/**
 * Implements hook_views_api().
 */
function badge_depot_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function badge_depot_node_info() {
  $items = array(
    'badge_criteria_page' => array(
      'name' => t('Badge Criteria Page'),
      'base' => 'node_content',
      'description' => t('Defines a criteria page for a badge that may be available for issuing to a badge user.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Herein you may create a criteria page as in the Open-Badge Initiative (OBI). A criteria page is referred to from the metadata of a baked badge (PNG file).
Starlite Badges and Astronaut Academy Badges made in conjunction with NASA have LRMI data made with the Learning Resource Metadata Initiative definition in mind ( http://www.lrmi.net/ ).
We make use of this data on this Drupal site using <a href="http://astronautacademy.org/stem-badges/drupal_module">our badge module.</a>
Therefore, if you <a href="http://astronautacademy.org/stem-badges/badge-depot/assertions/inspector">Upload a badge for analysis </a> on a site with this module in place (for example here), we are able to provide further information, including common core alignment and details.
<br />
<img src="/stem-badges/media/criteria_info1.png" />
'),
    ),
  );
  return $items;
}
