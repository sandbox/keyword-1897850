<?php
/**
 * @file
 * Contains BadgeDepotQuizBridgeUtility.
 */

/**
 * Implements a helper class with utility methods to award badges.
 */
abstract class BadgeDepotQuizBridgeUtility {

  /**
   * Award a badge to the specified user.
   *
   * @param string $badge_id
   *   The ID of the badge to be awarded, specied as the unique identified
   *   (that is, the manifest machine name).
   * @param string $recipient_email
   *   The email address of the user that will be receiving the badge award.
   * @param BadgeAssertionEntity|null $assertion
   *   (optional) An ouput variable to contain the created assertion entity.
   *
   * @return string|null
   *   Upon success, the absolute URL of the badge-award page; otherwise NULL.
   */
  public static function awardBadgeToUser($badge_id, $recipient_email, &$assertion = NULL) {
    $assertion = NULL;
    $award_url = NULL;
    if (isset($badge_id) && isset($recipient_email) && valid_email_address($recipient_email)) {
      $manifest = BadgeManifestEntity::loadFromMachineName($badge_id);
      if (isset($manifest)) {
        $assertion = new BadgeAssertionEntity();
        $assertion->manifest_id = $manifest->manifest_id;
        $assertion->recipient_email = $recipient_email;
        $assertion->save();
        if (isset($assertion->assertion_id)) {
          $award_url = $assertion->getEntityViewPath();
          if (isset($award_url)) {
            $variables = array(
              '!link' => l(t('link'), $award_url, array('attributes' => array('target' => '_blank'))),
            );
            watchdog(BADGE_DEPOT_QUIZ_BRIDGE_MODULE_NAME, 'Awarded a quiz badge (!link).', $variables);
            $award_url = url($award_url, array('absolute' => TRUE));
          }
        }
        else {
          $assertion = NULL;
        }
      }
    }

    return $award_url;
  }
}
