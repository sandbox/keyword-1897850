<?php
/**
 * @file
 * Contains BadgeDepotWebProxy.
 */

// Include the dependency files, for example to define BADGE_DEPOT_MODULE_NAME.
require_once dirname(__DIR__) . '/core/globals.inc';

class BadgeDepotWebProxy {

  /**
   * Implements a utility method for page callbacks of proxy web-service endpoints.
   */
  protected function getJsonResponse(
    &$document_id,
    $async_queue_name = 'badge_depot_async_proxy',
    $cache_identifier_prefix = NULL,
    $hash_salt = '#salt3dC@CHE',
    $download_completion_rule = NULL,
    $download_completion_data = NULL) {

    if (!isset($cache_identifier_prefix)) {
      $cache_identifier_prefix = (BADGE_DEPOT_MODULE_NAME . ':proxy:');
    }

    // Detect requests for asynchronous processing of queued downloads
    $input_source = 'GET';
    if (static::getInputArgument('async', NULL, $input_source) === '1') {
      // Handle requests for asynchronous processing of queued downloads
      // NOTE: To initiate the download, the web-client is
      //       responsible for making a request to a URL such
      //       as "badge-depot/microdata/proxy/?async=1"
      //       or "badge-depot/assertions/proxy/?async=1"
      return $this->downloadData($async_queue_name);
    }

    // Provide a default value for the hash salt
    if (!isset($hash_salt)) {
      $hash_salt = $cache_identifier_prefix;
    }

    // Extract the ID and URL of the document to be queried
    $document_url = static::getInputArgument('url', NULL, $input_source);
    if (!isset($document_id)) {
      $document_id = static::getInputArgument('id', NULL, $input_source);
    }

    // Equivalent of 24 hours in seconds.
    $cache_expire_seconds = (24 * 60 * 60);

    // Equivalent of 2 minutes in seconds.
    $download_timeout_seconds = (2 * 60);

    if (isset($document_id)) {
      // Ensure that the ID contains no bad data.
      if (preg_match('/^[0-9a-zA-Z_-]+$/', $document_id) !== 1) {
        // Ignore bad data.
        $document_id = NULL;
      }
    }

    // Generate the document ID if needed.
    if (isset($document_url) && (!isset($document_id))) {
      $document_id = $this->getDocumentIdFromUrl($document_url, $hash_salt);
      if (!isset($document_id)) {
        // As a security measure, clear the document URL,
        // since the URL could not be converted to an ID.
        $document_url = NULL;
      }
    }
    else {
      // As a security measure, clear the document URL, since
      // it may  not correspond correctly with the specified
      // document ID, and no URL is needed if the ID is known.
      $document_url = NULL;
    }

    $result_json = NULL;
    if (isset($document_id)) {
      // Generate the filename of the document in the cache.
      $cache_id = ($cache_identifier_prefix . $document_id);
      $cache_bin = 'cache';
      $cache_now = time();

      // Detect any previously cached data.
      $cache_item = cache_get($cache_id, $cache_bin);
      if (($cache_item !== FALSE) && ($cache_now < $cache_item->expire)) {
        // Extract the cached data that has not yet expired.
        $result_json = $cache_item->data;
      }
      elseif (isset($document_url)) {
        // Generate an intermediate result while the actual data is downloaded asynchronously.
        $result_json = _badge_depot_json_encode(array('proxy' => array('id' => (string) $document_id)));

        // Ignore user aborts and allow this script to run to the end.
        ignore_user_abort(TRUE);
        set_time_limit($download_timeout_seconds);

        // Save the transient result to the cache.
        $cache_expire = ($cache_now + $cache_expire_seconds);
        cache_set($cache_id, $result_json, $cache_bin, $cache_expire);

        // Schedule a task for asynchronous downloading of actual data.
        // NOTE: To initiate the download, the web-client is responsible for
        //       making a request to "badge-depot/path/to/proxy/?async=1".
        $queue = DrupalQueue::get($async_queue_name , TRUE);
        $queue->createQueue();
        $is_async_queued = $queue->createItem(array(
          'url' => $document_url,
          'cacheID' => $cache_id,
          'cacheBin' => $cache_bin,
          'cacheExpire' => $cache_expire,
          'completionRule' => $download_completion_rule,
          'completionData' => $download_completion_data));
        if (!$is_async_queued) {
          $result_json = _badge_depot_json_encode(array('error' => 'Failed to queue async task!'));
          cache_set($cache_id, $result_json, $cache_bin, $cache_expire);
        }
      }
      else {
        watchdog(BADGE_DEPOT_MODULE_NAME, 'No data is cached for web-proxy identifier %documentID', array('%documentID' => $document_id), WATCHDOG_WARNING);
      }
    }
    else {
      watchdog(BADGE_DEPOT_MODULE_NAME, 'Cannot determine the document ID for the web-proxy request!', array(), WATCHDOG_WARNING);
    }

    return $result_json;
  }

  // Converts the specified document's URL to an identifier.
  protected function getDocumentIdFromUrl($document_url, $hash_salt = NULL, $hash_type = 'md5') {
    $document_id = NULL;
    // For example, a document URL of "http://x.y".
    if ((strlen($document_url) > 9)
      && (preg_match('/^(http|https|HTTP|HTTPS):\\/\\/[0-9a-zA-Z_-]+/', $document_url) === 1)
      && in_array($hash_type, hash_algos(), TRUE)) {

      $document_id = hash($hash_type, ($document_url . $hash_salt));
    }

    return $document_id;
  }

  /**
   * Dequeues proxy-tasks and download any requested data.
   */
  protected function downloadData($async_queue_name) {
    // Proceed with processing of the task queue.
    $failure_messages = array();
    $failure_count = 0;
    $success_count = 0;
    $actions_count = 0;
    $skip_ssl_verification = TRUE;
    $default_ssl_version = 3;
    $timeout_seconds = 5 * 60;
    $queue = DrupalQueue::get($async_queue_name, TRUE);
    $queue_length = (int) $queue->numberOfItems();

    // Keep going after this point.
    ignore_user_abort(TRUE);

    while ($queue_item = $queue->claimItem()) {
      // Delete the queued task, since were going to do our best
      // to process the request now and do not plan on repeating
      // the download (e.g., to prevent infinite retry loops).
      $queue->deleteItem($queue_item);

      // Execute the script for no longer than the maximum duration per item.
      set_time_limit($timeout_seconds);

      // Get the download arguments from the queued task.
      $document_url = static::getQueueData($queue_item, 'url');
      $document_filename = static::getQueueData($queue_item, 'file');
      $completion_rule = (string) static::getQueueData($queue_item, 'completionRule');
      $completion_data = static::getQueueData($queue_item, 'completionData');
      $cache_id = (string) static::getQueueData($queue_item, 'cacheID');
      if (isset($cache_id) && ($cache_id !== '')) {
        $cache_bin = static::getQueueData($queue_item, 'cacheBin', 'cache');
        $cache_expire = static::getQueueData($queue_item, 'cacheExpire', CACHE_PERMANENT);
      }
      else {
        $cache_id = NULL;
        $cache_bin = NULL;
        $cache_expire = NULL;
      }

      // Proceed with downloading of the scheduled document.
      if (isset($document_url)) {
        $download_status = static::downloadWebContents($document_url, $document_filename, array($this, 'validateData'), $document_contents, $skip_ssl_verification, $default_ssl_version);
        if (is_string($download_status) && isset($default_ssl_version) && DataUriCreatorString::startsWith($document_url, 'https:', TRUE) && (DataUriCreatorString::endsWith($download_status, '(35)') || DataUriCreatorString::endsWith($download_status, '(4)'))) {
          // Let PHP try to figure out the SSL version when we encounter
          // errors such as "OpenSSL was built without SSLv2 support (4)"
          // or "Unknown SSL protocol error in connection to ... (35)".
          $download_status = static::downloadWebContents($document_url, $document_filename, array($this, 'validateData'), $document_contents, $skip_ssl_verification);
        }

        // Save the download data to the Drupal cache, if requested
        // NOTE: The cache value may also have to be set in cases
        //       where the download "fails" but where default content
        //       is returned from the validation function, such that
        //       we do not test for download success at this point.
        if (isset($cache_id) && isset($document_contents)) {
          cache_set($cache_id, $document_contents, $cache_bin, $cache_expire);
        }

        // Update the result counters.
        if ($download_status === TRUE) {
          $success_count++;
        }
        else {
          $failure_count++;
          if (is_string($download_status)) {
            $failure_messages[] = $download_status;
          }
        }

        // Process any completion actions.
        // TODO: Incorporate Drupal's Trigger & Action mechanism here for enhanced integration possibilities.
        $rule_result = $this->queueItemDownloadComplete($queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents);
        if ($rule_result !== FALSE) {
          $actions_count++;
        }
      }
    }

    // As a result, return the number of processed items.
    $result = array();
    if (($success_count > 0) || ($failure_count <= 0)) {
      $result['success'] = $success_count;
    }

    if ($failure_count > 0) {
      $result['failure'] = $failure_count;
      if (!empty($failure_messages)) {
        $result['errors'] = $failure_messages;
      }
    }

    if ($actions_count > 0) {
      $result['actions'] = $actions_count;
    }

    if ($queue_length !== ($success_count + $failure_count)) {
      $result['queue'] = $queue_length;
    }

    return $result;
  }

  /**
   * Perform any completion actions when a queued proxy-task finish
   * downloading any requested data.  Note that this method may be called
   * multiple times and that the completed item may also be for a different
   * request than the one caused the current proxy instance to be created.
   * The queue-item argument provides details about the completed request.
   */
  protected function queueItemDownloadComplete(&$queue_item, $completion_rule, $completion_data, $download_status, $document_url, $document_contents) {
    // Specify the completion rule's result.
    // Note: FALSE indicate failure or that no action was performed.
    $rule_result = FALSE;
    return $rule_result;
  }

  /*
   * Retrieves a named input argument provided by the web request.
   *
   * @param $name
   *   The name of the value to be retrieved.
   *
   * @return
   *   The value corresponding with the named argument.
   */
  protected static function getInputArgument($name, $default_value = NULL, $input_source = 'REQUEST') {
    switch ($input_source) {
      case 'REQUEST':
        if (isset($_REQUEST[$name])) {
          return (string) $_REQUEST[$name];
        }

        break;
      case 'POST':
        if (isset($_POST[$name])) {
          return (string) $_POST[$name];
        }

        break;
      case 'GET':
        if (isset($_GET[$name])) {
          return (string) $_GET[$name];
        }

        break;
    }

    return $default_value;
  }

  /**
   * Provides a helper function that retrieves a named value from a queue item.
   */
  protected static function getQueueData($queue_item, $name, $default_value = NULL) {
    if (isset($queue_item->data[$name])) {
      return $queue_item->data[$name];
    }

    return $default_value;
  }

  /**
   * Downloads the specified URL to a file.
   *
   * @param $data_source_url
   *   The web URL from where the data must be downloaded.
   *
   * @param $data_destination_filename
   *   The file path where the downloaded contents should be saved.
   *
   * @param $data_validation_function
   *   An optional function that can be specified to validate the downloaded
   *   content.  The data contents is provided as the first argument, and the
   *   function must return NULL if the contents appears to be valid; otherwise
   *   a different or default value can be returned for the contents that will
   *   replace the downloaded data.  The URL of the data source is passed to
   *   function as an optional second argument, and the third parameter is the
   *   HTTP status-code returned by the server (e.g., 200 for success).
   *
   * @param $data_contents
   *   An optional output variable that will contain the downloaded contents.
   *
   * @param $skip_ssl_verification
   *   An optional boolean flag that specifies whether to verify SSL certificates.
   *
   * @param $forced_ssl_version
   *   An optional value that specifies the version number to use for SSL (e.g., 2 or 3).
   *
   * @param $http_status_code
   *   An optional output variable that will contain the returned HTTP code for successful downloads.
   *
   * @return
   *   TRUE upon success, otherwise FALSE or a descriptive error message as a string to indicate failure.
   */
  public static function downloadWebContents(
    $data_source_url,
    $data_destination_filename = NULL,
    $data_validation_function = NULL,
    &$data_contents = NULL,
    $skip_ssl_verification = FALSE,
    $forced_ssl_version = NULL,
    &$http_status_code = NULL) {

    $download_status = FALSE;
    $http_status_code = NULL;
    $data_contents = NULL;
    $data_temp_filename = tempnam(sys_get_temp_dir(), 'curl_');
    if ($data_temp_filename !== FALSE) {
      $data_file_handle = fopen($data_temp_filename, 'w');
      if ($data_file_handle !== FALSE) {
        $curl_handle = curl_init($data_source_url);
        if ($curl_handle !== FALSE) {
          $curl_options = array(
            CURLOPT_FILE => $data_file_handle,
            CURLOPT_HEADER => FALSE,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_MAXREDIRS => 3,
            CURLOPT_PROTOCOLS => (CURLPROTO_HTTP | CURLPROTO_HTTPS),
            CURLOPT_REDIR_PROTOCOLS => (CURLPROTO_HTTP | CURLPROTO_HTTPS));
          if ($skip_ssl_verification) {
            $curl_options[CURLOPT_SSL_VERIFYPEER] = FALSE;
            $curl_options[CURLOPT_SSL_VERIFYHOST] = 0;
          }

          if (isset($forced_ssl_version)) {
            $curl_options[CURLOPT_SSLVERSION] = $forced_ssl_version;
          }

          if (curl_setopt_array($curl_handle, $curl_options)) {
            $download_status = curl_exec($curl_handle);
            if ($download_status === TRUE) {
              $http_status_code = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
            }
            else {
              $download_status = (trim((string) (curl_error($curl_handle))) . ' (' . curl_errno($curl_handle) . ')');
            }
          }

          curl_close($curl_handle);
        }

        if ((!fclose($data_file_handle)) && ($download_status === TRUE)) {
          $download_status = 'Failed to close download!';
        }

        if ($download_status === TRUE) {
          // Validate the result as something that we are expecting.
          if (isset($data_validation_function)) {
            $data_contents = file_get_contents($data_temp_filename);
            $altered_contents = NULL;
            if (is_string($data_validation_function) && method_exists(get_called_class(), $data_validation_function)) {
              // Handles a validator that is implemented as a class-static method by a derived class...
              $altered_contents = call_user_func(array(get_called_class(), $data_validation_function), $data_contents, $data_source_url, $http_status_code);
            }
            elseif (is_callable($data_validation_function)) {
              // Handles the following kinds of validators...
              //  * class-instance methods (e.g., an array value),
              //  * class-static methods (e.g., a string value containing "::") and
              //  * regular functions (e.g., a plain string value).
              $altered_contents = call_user_func($data_validation_function, $data_contents, $data_source_url, $http_status_code);
            }

            if (isset($altered_contents)) {
              // Replace invalid contents with the returned default contents.
              $data_contents = $altered_contents;
              if ((!file_put_contents($data_temp_filename, $data_contents)) && ($download_status === TRUE)) {
                $download_status = 'Failed to store downloaded contents!';
              }
            }
          }
        }

        // Promote the intermediate file to the actual output file, if requested.
        if ($download_status === TRUE) {
          if (isset($data_destination_filename) && ($data_destination_filename !== '')) {
            if ((!rename($data_temp_filename, $data_destination_filename)) && ($download_status === TRUE)) {
              $download_status = 'Failed to replace downloaded contents!';
            }
          }
          else {
            // Delete the temporary file.
            unlink($data_temp_filename);
          }
        }
      }
    }

    return $download_status;
  }
}
