===============================================================================
The Badge Depot module for Drupal
===============================================================================

*******************************************************************************
1.  DESCRIPTION
-------------------------------------------------------------------------------

The Badge Depot module enables a Drupal website to publish content of Badge
Criteria pages. These pages are required as part of assertion data when issuing
badges using the Mozilla Open Badge Infrastructure (OBI). The module automates
the creation of markup from the Learning Resource Metadata Initiative (LRMI)
specification on criteria pages.

*******************************************************************************
2.  INSTALLATION
-------------------------------------------------------------------------------

Enabling this module will create the content type for a Badge Criteria Page,
which allows the data to be defined for a specific badge.  However, the
default rendering of the criteria pages will likely need to be modified to
integrate well with the rest of your website's look and feel, and for LRMI
data to be embedded correctly on your badge-criteria pages.  Also, the
module was designed so that each badge-criteria page can be themed or styled
independently, although a default style can alternatively be used.  The theme
customizations are probably the most difficult part of the installation process,
especially since this is dependent on your site's current theme, and this makes
it difficult to provide exact instructions about what has to be done to make
the module work in every possible situation.

-------------------------------------------------------------------------------
2.1.  Configuration
-------------------------------------------------------------------------------

Once the module is installed and activated, you may want to edit the newly
created content-type and change some of the default settings before creating
any content that uses this type.  To do so, while logged in as a site
administrator, use the menu at Structure | Content types | Badge Criteria Page |
Edit (i.e., using "admin/structure/types/manage/badge-criteria-page"), and
consider changing some of the following default settings:
 * You may want to uncheck the checkbox under "Publishing options" for "Promoted
   to front page".
 * Consider unchecking the checkbox under "Display settings" for "Display author
   and date information."
 * Under "Comment settings" perhaps...
    - Change "Default comment setting for new content" to "Closed",
    - Uncheck the checkbox for "Threading", and/or
    - Uncheck the checkbox that would "Allow comment title".

Of course, these settings are site specific and depends on how you will theme
the Badge Criteria Page content.

-------------------------------------------------------------------------------
2.2.  Theme Customization
-------------------------------------------------------------------------------

The module includes example files of theme customizations that illustrate how
template files can be specified per content-type.  If you decide to customize
the style of your badge-criteria pages using similar customizations based on
content-type, you need to modify your theme's "template.php" file to add extra
suggestions for template filenames to Drupal.  This file is often located in
a Drupal folder such as "sites\all\themes\THEME".  (Note that "THEME" would
need to be replaced by the actual name of your website's theme.)  To make these
changes, either add the following code to the "template.php" file, if the
functions do not already exist, or combine the code with that of each existing
function.  If you need to combine the code with existing functions, watch out
for the "$vars" argument; you may sometimes need to change this to "$variables"
or some other name that is used by the theme for the function's first argument.

function THEME_preprocess_html(&$vars) {
  // Add a suggestion for an HTML template filename using the node's
  // content-type, such as html--article.tpl.php for article nodes
  if ($node = menu_get_object()) {
    $vars['theme_hook_suggestions'][] = ('html__' . $node->type);
  }
}

function THEME_preprocess_maintenance_page(&$vars) {
  // Add a suggestion for a maintenance-page template filename using the
  // node's content-type, such as page--article.tpl.php for article nodes
  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = ('page__' . $vars['node']->type);
  }
}

function THEME_preprocess_page(&$vars) {
  // Add a suggestion for a page template filename using the node's
  // content-type, such as page--article.tpl.php for article nodes
  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = ('page__' . $vars['node']->type);
  }
}

Once this is done, you'll be able to override the templates for "html", "page"
and "node" based on the new content-type named badge_criteria_page (or any
other content-type for that matter).  Although one can do as little or as much
theming as desired, the example templates illustrate how one can override
three templates to have full control over how the badge-criteria pages will
be rendered.  For example, copy and customize the following three files from
the module's "examples\templates" folder to your theme's templates directory:

 * html--badge_criteria_page.tpl.php
 * page--badge_criteria_page.tpl.php
 * node--badge_criteria_page.tpl.php

Usually one would base any customizations of template files on the current
theme's templates.  So, also refer to your current theme's "html.tpl.php",
"page.tpl.php" and "node.tpl.php" templates for additional changes that may
be required.  The provided examples are variants of the Zen theme's templates,
and it is recommended to compare the example files with the original ones that
ship with version 7.x-5.1 of the Zen theme; similar changes are likely needed
for your websites current theme.  Also, if your theme doesn't have an "html"
or "page" template, it is probably using a default one located in Drupal's
"modules\system" folder, which can then be used as a baseline for changes.

The examples folder also contains "css" and "images" directories that you may
want to copy to or merge with your current theme's folder.  The content-type
of a badge-criteria page has an optional field that may specify a custom
stylesheet for each particular page instance.  If no stylesheet URL is
specified, the example "html" template will include the default stylesheet
located at "css/badge-criteria-page_default.css".  In turn, this default
stylesheet relies on two images in the "images" folder for a default header
and background image; customize as you see fit.

===============================================================================
