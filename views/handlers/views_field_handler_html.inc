<?php
/**
 * @file
 * Contains BadgeDepotViewsFieldHandlerHtml.
 */

/**
 * A handler to provide proper displays for a field that contains HTML content.
 */
class BadgeDepotViewsFieldHandlerHtml extends views_handler_field {
  function construct() {
    $this->definition['title'] = 'HTML Field Handler';
    parent::construct();
    $this->format = 'full_html';
  }

  function render($values) {
    $value = $this->get_value($values);
    if (is_array($this->format)) {
      $format = $this->get_value($values, 'format');
    }
    else {
      $format = $this->format;
    }

    if ($value) {
      $value = str_replace('<!--break-->', '', $value);
      return check_markup($value, $format, '');
    }
  }
}
